//
//  ViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/04/30.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDefine.h"
#import "Utils.h"
#import "LoginViewController.h"
#import "WebServerViewController.h"
#import "AgreementViewController.h"
#import "SheetListExtViewController.h"

#define LABEL_FONT_SIZE	18.0

#define EDIT_COMMBUTTON_BUTTON_LEFT_MARGIN_X   10
#define EDIT_COMMBUTTON_BUTTON_LEFT_MARGIN_Y   6
#define EDIT_COMMBUTTON_BUTTON_WIDTH           145
#define EDIT_COMMBUTTON_BUTTON_HEIGHT          31


@interface ViewController : UIViewController<UIActionSheetDelegate, UITextFieldDelegate>{
    
}

-(void)loadingAnimation;
-(void)endLoadingAnimation;
-(void)addButtonParts;

@end
