//
//  AppDelegate.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/04/30.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "BusinessDataCtrlManager.h"


@implementation AppDelegate

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [TestFlight takeOff:@"77b40218-89bc-4b55-a7df-6d07b5dab83d"];
    
    [self activeService];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    
    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController_iPhone" bundle:nil] autorelease];
    
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//        self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController_iPhone" bundle:nil] autorelease];
//    } else {
//        self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController_iPad" bundle:nil] autorelease];
//    }
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //データをサーバーへのアップ処理をストップする
    [[[UploadToWebServerSYNController getUploadServerInstance] initData:nil] stopUpLoad];
    
    //Tempファイルをクリアする
    [Utils clearTempFileDirectory:CASH_DATA_SAVE_TEMP_PATH];

    exit(0);
    

    
    
    //シート一覧情報を保存
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    NSString *sheetList =  [[BusinessDataCtrlManager getSheetList] JSONRepresentation];
//    [ud setObject:sheetList forKey:sheetId_Key];
//    [ud synchronize];  // NSUserDefaultsに即時反映させる（即時で無くてもよい場合は不要）

    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    //各サービス起動
    //[self activeService];

    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return  [self openUrlExtention:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {

    return  [self openUrlExtention:url];
}

-(BOOL)openUrlExtention:(NSURL *)url{
    
    NSString *param = [url.absoluteString stringByReplacingOccurrencesOfString: [NSString stringWithFormat:@"%@://",url.scheme] withString:@""];
    param = [param stringByReplacingOccurrencesOfString:@"com.komugi.vulb?" withString:@""];
    
    //NSLog(@"param:%@", param);
    NSMutableArray *parmArray = [Utils splitParameterString:param];
    NSMutableDictionary *option = [[BusinessDataCtrlManager getInstance] getGlobalOptionData] ;
    for (int i=0; i<[parmArray count]; i++) {
        NSDictionary *p = [parmArray objectAtIndex:i];
        [option setObject:[p objectForKey:DIC_VALUE] forKey:[p objectForKey:DIC_KEY]];
    }
    
    if([url.scheme isEqualToString:APP_SCHEME_VULB_APP]){
        //ユーザーアカウント確認場合のみ、確認処理を行う
        NSString *confirm_key =[option objectForKey:@"confirm_key"];
        NSString *user_id =[option objectForKey:@"user_id"];
        if (confirm_key != nil) {
            [[BusinessDataCtrlManager getInstance] comfirmUser:confirm_key userId:user_id delegate:self.viewController];
        }
        
       return TRUE;
    }else if([url.scheme isEqualToString:APP_SCHEME_VULB_APP_FB]){
        
        NSString *tokenId =  [option objectForKey:@"token"];

        if (tokenId != nil) {
            [[BusinessDataCtrlManager getInstance] loginWithTokenId:tokenId delegate:self.viewController];
        }
        
        return TRUE;
    }else{
        return FALSE;
    }

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/**
 * サービスを起動する
 */
-(void)activeService{
 
    [DataRegistry getInstance];

	//外部Web問い合わせようサービスを起動
	[WebServerDownLoadController getInstance];
        
	//画像データダウンロード用サービスを起動
	[[[DownLoadImageResourceController getServerInstance] initData] startService];
    
}


@end
