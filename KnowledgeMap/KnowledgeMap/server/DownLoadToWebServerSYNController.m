//
//  DownLoadToWebServerSYNController.m
//  BukurouSS
//
//  Created by k-kin on 11/12/13.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import "DownLoadToWebServerSYNController.h"


@implementation DownLoadToWebServerSYNController

@synthesize fileMIMEType;
@synthesize bookCode;
@synthesize thread;
@synthesize bookCodeList;
@synthesize downLoadMetaDatas;

DownLoadToWebServerSYNController *downLoadToWebServerSYNController;

+(DownLoadToWebServerSYNController *)getDownLoadServerInstance{
	if (downLoadToWebServerSYNController == nil) {
		downLoadToWebServerSYNController = [[DownLoadToWebServerSYNController alloc] init] ;		
	}
	return downLoadToWebServerSYNController;
}

-(DownLoadToWebServerSYNController *)initDownLoadData:(id)inDelegate{
	if (self.bookCodeList != nil) {
        self.delegate = inDelegate;
		return downLoadToWebServerSYNController;
	}
	
	[super initData:inDelegate];
	self.bookCodeList = [[NSMutableArray alloc] init];
	self.downLoadMetaDatas = [[NSMutableArray alloc] init];

	return downLoadToWebServerSYNController;
		
}


-(void)stopDownLoad{
    [self cancelDownload];
    [self clearDatas];
}

-(void)clearDatas{
	[lockData lock];
	[self.bookCodeList removeAllObjects];
	[self.downLoadMetaDatas removeAllObjects];
	[receiveData release];
	receiveData = nil;
	isDataUploadLoading = FALSE;	
	[lockData unlock];
	
}

// 非同期のスレッドエントリー（TCP/IP）
- (void) startDownload:(NSString *)inBookCode{	
	//NSLog(@"--------startDownload----------");
	//NSLog(@"======= LOG OUT ========");
    /*
	if( self.userinfo == nil || self.userinfo.userCode == nil || [self.userinfo.userCode isEqualToString:@""]){
		//NSLog(@"======= LOG OUT ========");
		return;	
	}	
     */
	
	[lockData lock];
	if (inBookCode != nil && ![inBookCode isEqualToString:@""] && ![self.bookCodeList containsObject:inBookCode]) {
		[self.bookCodeList addObject:inBookCode];
	}
	[lockData unlock];					
	
	//ネット接続エラーの場合：　スキップする
	if (![Utils checkWebServerStatus]) {
		return;
	} 
		
	[lockData lock];
	isDataUploadLoading = TRUE;
	[lockData unlock];
	
	//Upload開始
	[self toStartDownLoadWebServer:METHOD_NAME_searchBooksMetaData];
	//[self  performSelectorInBackground:@selector(toStartDownLoadWebServer:) withObject:METHOD_NAME_searchBookTitleDictionaryData];
	//self.thread = [[NSThread alloc] initWithTarget:self selector:@selector(toStartDownLoadWebServer:) object:METHOD_NAME_searchBookTitleDictionaryData ];
	//[self.thread start];
	//ネットワークエラーの場合、６０秒後に再トライする
	//self.timer = [NSTimer scheduledTimerWithTimeInterval:SYNC_WEBSEVER_RETRY_TIME target:self selector: @selector(startDownload:) userInfo: nil repeats: NO];	


	//NSLog(@"------getServerData-------");
}

-(void)cancelDownload{
	if (self.thread != nil && ![self.thread isCancelled] && ![self.thread isFinished] && [self.thread isExecuting]) {
		//NSLog(@"=====nowThread Cenceled======");
		[self.thread cancel];
	}		
}

-(void) toStartDownLoadWebServer:(NSString *)methodName{
	if (methodName == nil) {
		methodName = METHOD_NAME_searchBookTitleDictionaryData;
	}
	//NSLog(@"======= methodName ========%@: %@",methodName,self.userinfo.activeFlg);
	if( self.userinfo == nil ){
		return;	
	}	
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	

	NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
	NSMutableDictionary *searchCondition = [[NSMutableDictionary alloc] init];
	[searchCondition setValue:self.userinfo.userCode forKey:USER_CODE];
	[searchCondition setValue:self.userinfo.deviceCode forKey:DEVICE_CODE];	
	[searchCondition setValue:self.userinfo.webLoginCode forKey:WEB_LOGIN_CODE];	
	[searchCondition setValue:self.userinfo.webLoginPass forKey:WEB_LOGIN_PASS];	
  	[searchCondition setValue:self.userinfo.lang forKey:LANG];	
  
	if ([self.bookCodeList count] > 0) {
        [searchCondition setValue:[self.bookCodeList lastObject] forKey:BOOK_CODE];		
    }
    
    //作成した本情報をサーバーに送る
    [[DataRegistry getInstance] selectMyCreateBookInfo:self.userinfo.userCode result:searchCondition];
    
	[param setValue:searchCondition forKey:PARAMETER_KEY];
	[searchCondition release];
	[param setValue:methodName forKey:METHOD_NAME_KEY]; 

	//NSLog([param JSONRepresentation]);
	
	[self downLoadServerSyncData:param];
	[param release];
	
	[pool release];

}

-(void)downLoadServerSyncData:(NSMutableDictionary *)param{		
	//NSLog(@"======= sendWorkDataToWebServer ========");
	[self sendWorkDataToWebServer:param photoData:nil fileName:nil serverUrl:BssWorkDataDownloadServerURLAdress];		
}


- (void)connection:(NSURLConnection *)i_connection didReceiveResponse:(NSURLResponse *)i_response{		
	self.fileMIMEType = [i_response MIMEType];
	
    // データ蓄積用に NSMutableData を初期化
	receiveData = nil;
    receiveData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{	
	[self clearDatas];
	
	NSLog(@"-- didFailWithError --");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
}

- (void)connectionDidFinishLoading:(NSURLConnection *)i_connection{
	//NSLog(@"---download is complted--- !");		
	NSRange fileType = [self.fileMIMEType rangeOfString:@"image/"];
	if (fileType.location != NSNotFound) {
		//Imgファイルの取得処理
		if ([self.downLoadMetaDatas count] > 0) {
			NSMutableDictionary *recieve_values = [self.downLoadMetaDatas objectAtIndex:0];
			[recieve_values setValue:[NSNumber numberWithInt:SAVE_STATUS_ON] forKey:SAVE_STATUS];
			[recieve_values setValue:[NSNumber numberWithInt:SYNC_STATUS_COMP] forKey:SYNC_STATUS];
			[recieve_values setValue:receiveData forKey:IMG_DATA];
			[[ImageResourceManager getInstance] saveImageResourceDataWithSync:recieve_values ];

			
			//ローカル同期が正常に終わったので、WebサーバーのDictionaryデータから削除する。
			//検索条件にUserCodeとDiviceCode追加する
			NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
			[recieve_values setValue:self.userinfo.userCode forKey:USER_CODE];
			[recieve_values setValue:self.userinfo.deviceCode forKey:DEVICE_CODE];		

			//削除対象データのメソッドを指定：
			[param setValue:METHOD_NAME_deleteImgDictionaryData forKey:METHOD_NAME_KEY];				
			//WebServerに送信
			[recieve_values removeObjectForKey:IMG_DATA];
			[param setValue:recieve_values forKey:PARAMETER_KEY];		
			[self sendWorkDataToWebServer:param photoData:nil fileName:nil serverUrl:BssWorkDataDownloadServerURLAdress];
			[param release];
		}
		
		[receiveData release];
		receiveData = nil ;
		
	}
	else{
		NSString *ret_str = [[NSString alloc] initWithData:receiveData encoding:NSUTF8StringEncoding];
		//NSLog(ret_str);
		[receiveData release];
		receiveData = nil ;
		NSMutableDictionary *recieve_data = nil;
		NSString *mothodName = nil;
		NSString *resultStatus = nil;
		NSMutableArray *resutlData = nil;
		if (ret_str == nil || [ret_str isEqualToString:@""]) {
			/****   何かの原因で受信データが空きダッタ場合、各変数を初期化し、処理を再スタートする。   ***/
			NSLog(@"------SYNC---Error--- EmptyDatas!--- ");
			[self clearDatas];
		}else {
			recieve_data = [ret_str JSONValue];
			if (recieve_data == nil) {
				[self clearDatas];				
			}else {
				mothodName = [recieve_data objectForKey:METHOD_NAME_KEY];
				resultStatus = [recieve_data objectForKey:RESULT_STATUS_KEY];
				resutlData = [recieve_data objectForKey:RESULT_VALUE_KEY];							
			}			
		}
		
		[ret_str release];
		// 自分の書いた本の読まれた回数、GOOD回数、コメント回数などの情報を取得し、ローカルサーバーに反映する。
        if ([mothodName isEqualToString:METHOD_NAME_searchBooksMetaData]) {
            if ([resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {
                for (int i=0; i<[resutlData count]; i++) {
                    NSMutableDictionary *bookInfo = [resutlData objectAtIndex:i];
                    [dataRegistry updateBookTitleDataMetaInfo:bookInfo];
                    //NSLog(@"book_code:%@", [bookInfo objectForKey:BOOK_CODE]);
                }
                
                //本のメタ情報を画面にも反映する
                if (self.delegate != nil) {
                    [self.delegate updateBooksMetaDatas:resutlData];
                }
                
            }
            
            //自分がコメントを書いた本の新着コメント数を取得し、保存する。　読書モード画面で取り出す。
            NSMutableDictionary *writeCommentBookNewCountInfo = [recieve_data objectForKey:WRITE_COMMENT_BOOK_NEW_COUNT_KEY];
            if (writeCommentBookNewCountInfo != nil) {
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                NSString *newData = [ud stringForKey:WRITE_COMMENT_BOOK_NEW_COUNT_KEY];
                if (newData != nil) {
                    [ud removeObjectForKey:WRITE_COMMENT_BOOK_NEW_COUNT_KEY];
                }
                NSNumber *writeCommentBookNewCount = [writeCommentBookNewCountInfo objectForKey:NEW_COMMENT_COUNT];
                [ud setObject:[NSString stringWithFormat:@"%d",[writeCommentBookNewCount intValue]] forKey:WRITE_COMMENT_BOOK_NEW_COUNT_KEY];
            }

            [lockData lock];
            isDataUploadLoading = FALSE;
            [lockData unlock];					

        }
		//同期対象データ情報を検索したデータの次の同期を行うべき、メソッド名前を書き換えで、同期処理を実行する。
		else if ([mothodName isEqualToString:METHOD_NAME_searchImgDictionaryData]) {
			if ([resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {//同期対象画像データが存在する場合、次の同期処理を実行する
				self.downLoadMetaDatas = resutlData;
				NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
				[param setValue:METHOD_NAME_downLoadImgData forKey:METHOD_NAME_KEY]; 
				NSMutableDictionary *info = [self.downLoadMetaDatas objectAtIndex:0];
				[info setValue:[UserCtrl getInstance].userInfo.imgsavedrive forKey:IMG_SAVE_DRIVE];
				[param setValue:info forKey:PARAMETER_KEY];
				[self sendWorkDataToWebServer:param photoData:nil fileName:nil serverUrl:BssWorkDataDownloadServerURLAdress];				
				[param release];
			}
		}else if ([mothodName isEqualToString:METHOD_NAME_searchPageDictionaryData]) {
			if ([resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {//同期対象画像データが存在する場合、次の同期処理を実行する
				//同期処理を開始する。
				self.downLoadMetaDatas = resutlData;
				NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
				[param setValue:METHOD_NAME_searchPageData forKey:METHOD_NAME_KEY]; 
				[param setValue:[self.downLoadMetaDatas objectAtIndex:0] forKey:PARAMETER_KEY];
				[self sendWorkDataToWebServer:param photoData:nil fileName:nil serverUrl:BssWorkDataDownloadServerURLAdress];
				[param release];
            }else {	//対象データがないとき		
				/** ページデータの同期対象の受信データがないので, 画像データの同期を始める。 */
				[self toStartDownLoadWebServer: METHOD_NAME_searchImgDictionaryData];
			}
		}else if ([mothodName isEqualToString:METHOD_NAME_searchBookTitleDictionaryData]) {
			if ([resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {//同期対象画像データが存在する場合、次の同期処理を実行する
				//同期処理を開始する。
				self.downLoadMetaDatas = resutlData;
				NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
				[param setValue:METHOD_NAME_searchBookTitle forKey:METHOD_NAME_KEY]; 
				[param setValue:[self.downLoadMetaDatas objectAtIndex:0] forKey:PARAMETER_KEY];
				[self sendWorkDataToWebServer:param photoData:nil fileName:nil serverUrl:BssWorkDataDownloadServerURLAdress];
				[param release];
			}else {	//対象データがないとき		
				/** 本タイトルの同期対象の受信データがないので, ページデータの同期を始める。 */
				[self toStartDownLoadWebServer:METHOD_NAME_searchPageDictionaryData];
			}		
		}else if ([mothodName isEqualToString:METHOD_NAME_deleteImgDictionaryData] ) {			
			//Metaデータの次のデータをダウンロードする
			[self.downLoadMetaDatas removeObjectAtIndex:0];
			if ([self.downLoadMetaDatas count] > 0) {
				NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
				[param setValue:METHOD_NAME_downLoadImgData forKey:METHOD_NAME_KEY]; 
				NSMutableDictionary *info = [self.downLoadMetaDatas objectAtIndex:0];
				[info setValue:[UserCtrl getInstance].userInfo.imgsavedrive forKey:IMG_SAVE_DRIVE];
				[param setValue:info forKey:PARAMETER_KEY];
				[self sendWorkDataToWebServer:param photoData:nil fileName:nil serverUrl:BssWorkDataDownloadServerURLAdress];				
				[param release];
			}else {
/****--------------------------------------------******/				
				//ここで一つの本の全体ループが終了ので、次の本があれば、再開する
				[lockData lock];
				[self.bookCodeList removeLastObject];
				[lockData unlock];					
				if ([self.bookCodeList count] > 0) {
					[self toStartDownLoadWebServer:METHOD_NAME_searchBookTitleDictionaryData];
				}else {
					[lockData lock];
					isDataUploadLoading = FALSE;
					[lockData unlock];					
				}
			}

		}else if ([mothodName isEqualToString:METHOD_NAME_deletePageDictionaryData]) {			
			//Metaデータの次のデータをダウンロードする
			[self.downLoadMetaDatas removeObjectAtIndex:0];
			if ([self.downLoadMetaDatas count] > 0) {
				NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
				[param setValue:METHOD_NAME_searchPageData forKey:METHOD_NAME_KEY]; 
				[param setValue:[self.downLoadMetaDatas objectAtIndex:0] forKey:PARAMETER_KEY];
				[self sendWorkDataToWebServer:param photoData:nil fileName:nil serverUrl:BssWorkDataDownloadServerURLAdress];				
				[param release];
			}else{
				/** ページデータの同期対象の受信データがサーバーに残っていないか再度確認する。 */
				[self toStartDownLoadWebServer:METHOD_NAME_searchPageDictionaryData];			
			}
		}else if ([mothodName isEqualToString:METHOD_NAME_deleteBookTitleDictionaryData] ) {			
			//Metaデータの次のデータをダウンロードする
			[self.downLoadMetaDatas removeObjectAtIndex:0];
			if ([self.downLoadMetaDatas count] > 0) {
				NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
				[param setValue:METHOD_NAME_searchBookTitle forKey:METHOD_NAME_KEY]; 
				[param setValue:[self.downLoadMetaDatas objectAtIndex:0] forKey:PARAMETER_KEY];
				[self sendWorkDataToWebServer:param photoData:nil fileName:nil serverUrl:BssWorkDataDownloadServerURLAdress];			
				[param release];
			}else {
				/** 本タイトルの同期対象の受信データがサーバーに残っていないか再度確認する。 */
				[self toStartDownLoadWebServer:METHOD_NAME_searchBookTitleDictionaryData];
			}

		}else if ([resultStatus isEqualToString:RESULT_STATUS_VALUE_0] || [resultStatus isEqualToString:RESULT_STATUS_VALUE_2]) {
			//サーバーからの受信データを受け取って、処理する。
			//NSLog(@"----=============-----");
			//ローカル同期が正常に終わったので、WebサーバーのDictionaryデータから削除する。
			NSMutableDictionary *recieve_values = [resutlData objectAtIndex:0];
			NSMutableDictionary *param = [[NSMutableDictionary alloc] init];			
			if ([mothodName isEqualToString:METHOD_NAME_searchPageData]) {
				if ([resultStatus isEqualToString:RESULT_STATUS_VALUE_0] ) {
					//ローカルデータベースのデータを更新する。
					[recieve_values setValue:[NSNumber numberWithInt:SYNC_STATUS_COMP] forKey:SYNC_STATUS];
					[dataRegistry savePageData:recieve_values];
				}				
				//削除対象データのメソッドを指定：ページデータ
				[param setValue:METHOD_NAME_deletePageDictionaryData forKey:METHOD_NAME_KEY];								
			}else if ([mothodName isEqualToString:METHOD_NAME_searchBookTitle]) {
				if ([resultStatus isEqualToString:RESULT_STATUS_VALUE_0] ) {
					//ローカルデータベースのデータを更新する。
					[recieve_values setValue:[NSNumber numberWithInt:SYNC_STATUS_COMP] forKey:SYNC_STATUS];
					[dataRegistry saveBookTitleData:recieve_values];
				}				
				//削除対象データのメソッドを指定：BookTitleデータ
				[param setValue:METHOD_NAME_deleteBookTitleDictionaryData forKey:METHOD_NAME_KEY];								
			}
			
			//検索条件にUserCodeとDiviceCode追加し、データ管理テーブルからデータを削除する
			[recieve_values setValue:self.userinfo.userCode forKey:USER_CODE];
			[recieve_values setValue:self.userinfo.deviceCode forKey:DEVICE_CODE];		
			//コンテンツデータがサイズが大きいし、不要のため、条件から削除する
			[recieve_values removeObjectForKey:@"contentsdata"];
			[param setValue:recieve_values forKey:PARAMETER_KEY];	
			//NSLog([param JSONRepresentation]);
			[self sendWorkDataToWebServer:param photoData:nil fileName:nil serverUrl:BssWorkDataDownloadServerURLAdress];
			[param release];
			
		}else if ([resultStatus isEqualToString:RESULT_STATUS_VALUE_1]) {
			//NSLog(@"--upload error ---");			
            [self clearDatas];						
		}else {
            [self clearDatas];
        }
		
	}
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
}


- (void)dealloc {
	[self.lockData release];
	[self.bookCodeList removeAllObjects];
	[self.bookCodeList release];
	[self.downLoadMetaDatas removeAllObjects];
	[self.downLoadMetaDatas release];
	[self.thread release];
	[self.bookCode release];
	[self.fileMIMEType release];
	
	[super dealloc];
	
}

@end
