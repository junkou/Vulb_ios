//
//  DownLoadImageResourceController.h
//  BukurouSS
//
//  Created by k-kin on 11/12/19.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <unistd.h>
#include <netdb.h>
#import "EnvDefine.h"
#import "AppDefine.h"
#import "ServerParamDef.h"
#import "BssAssetsController.h"
#import "DatabaseTableNmaes.h"


@interface DownLoadImageResourceController : NSObject {

	int		listeningSocket; 
	int		portNumber;
	BOOL	isWorking;
	int		progress;
	NSLock				*lockData;
	NSMutableArray	*pageCodeList;

}

@property int	listeningSocket;
@property int	portNumber;
@property BOOL	isWorking;
@property int	progress;
@property (nonatomic, retain) NSLock			*lockData;
@property (nonatomic, retain) NSMutableArray	*pageCodeList; 

+(DownLoadImageResourceController *)getServerInstance;
-(DownLoadImageResourceController *)initData;

- (void) startService;
- (void) stopService;
- (int)  getPort;
-(void)mainDownLoadImageResourceMethod;
-(void)insertPhotoLibraryDataToDataBase:(NSMutableDictionary *)param;


@end
