//
//  Server.h
//  BukurouSS
//
//  Created by k-kin on 11/11/14.
//  Copyright 2011 iccesoft. All rights reserved.
//

@protocol WebServerDelegate

@optional

//データ通信をキャンセルしたとき
-(void)cancelOptarion;

//データ通信エラーが発生したとき
-(void)connectNetWorkError;

// Fileデータを受信したとき
- (void)recieveApplicationData:(NSMutableData	*)data fileType:(NSString *)fileType;

// 処理データを受信したとき
- (void)recieveOprationData:(NSString *)data;


@end


#import "ServerParamDef.h"

@interface WebServerDownLoadController : NSObject <WebServerDelegate>{ 
	id delegate;
	NSURLConnection *urlConnection;
	NSString		*fileMIMEType;
	NSMutableData	*receiveData;

}

@property (nonatomic, assign) id delegate;
@property (nonatomic, retain) NSURLConnection	*urlConnection; 
@property (nonatomic, retain) NSString			*fileMIMEType;
@property (nonatomic, retain) NSMutableData		*receiveData; 

+(WebServerDownLoadController *)getInstance;
- (void) sendWorkDataToWebServer:(NSMutableDictionary *)params 
			  showConnectionFlag:(BOOL)showConnectionIndicator 
					   serverUrl:(NSString *)inUrl
						delegate:(id)inDelegate;


- (void) sendOtherApplicationSerivice:(NSString *)inUrl delegate:(id)inDelegate;

-(void)clearDatas;
-(void)clearDatasFinal;

@end
