//
//  DownLoadThumbnailImageServerController.m
//  BukurouSS
//
//  Created by k-kin on 11/12/12.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import "DownLoadThumbnailImageServerController.h"


@implementation DownLoadThumbnailImageServerController
@synthesize imageEditFlag;
@synthesize resizeRate;

NSLock				*downLoadLockData;

DownLoadThumbnailImageServerController	*ctrl;
NSMutableDictionary	*filesCtrl;

+(DownLoadThumbnailImageServerController *)getInstance{
	if (ctrl == nil) {
		ctrl = [[DownLoadThumbnailImageServerController alloc] init] ;
		filesCtrl = [[NSMutableDictionary alloc] init];
        downLoadLockData = [[NSLock alloc] init];

	}
	return ctrl;
}

-(void)clearThread{
    NSArray *keys = [filesCtrl allKeys];
	for (int i=0; i<[keys count]; i++) {
		NSThread *thread = [filesCtrl objectForKey:[keys objectAtIndex:i]];
		if (thread != nil && ![thread isCancelled] && ![thread isFinished] && [thread isExecuting]) {
			//NSLog(@"=====nowThread Cenceled======");
			[thread cancel];
		}		
	}
	
	[filesCtrl removeAllObjects];
}

-(void)getImageDataWithFilePath:(NSString *)filePath imageView:(UIImageView *)imageView{
    /*
    NSArray *allkey = [filesCtrl allKeys];
    for (int i; i<[allkey count]; i++) {
        NSLog([allkey objectAtIndex:i]);
    }
     */
	if ([filesCtrl objectForKey:filePath] != nil && imageView == nil) {
		//すでにダウンロード中になので、スキップする
        //NSLog(@"return  filePath:%@", filePath);
		return;
	}
	
    if ([filePath isKindOfClass:[ NSNull  class]] || filePath == nil || [filePath isEqualToString:@""]) { 
        return;
    }

    
	NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:2];
	[param setObject:filePath forKey:FILE_PATH_KEY];
    if (imageView != nil) {
        [param setObject:imageView forKey:OUPUT_VIEW_KEY];	
    }
    

	NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(handleRequest:) object:param ];
    [downLoadLockData lock];
    self.imageEditFlag = FALSE;
   [filesCtrl setObject:thread forKey:filePath];
    [downLoadLockData unlock];
	[thread start];
    

	[thread release];
	[param release];
}

-(void)getImageDataWithFilePathWithEditImage:(NSString *)filePath imageView:(UIImageView *)imageView{
    [downLoadLockData lock];
    self.resizeRate = 1;
    [downLoadLockData unlock];

    [self getImageDataWithFilePathWithEditImageFinal:filePath imageView:imageView];
	
}

-(void)getImageDataWithFilePathWithEditImageExt:(NSString *)filePath imageView:(UIImageView *)imageView{
    [downLoadLockData lock];
    self.resizeRate = 2;
    [downLoadLockData unlock];
    
    [self getImageDataWithFilePathWithEditImageFinal:filePath imageView:imageView];
	
}

-(void)getImageDataWithFilePathWithEditImageFinal:(NSString *)filePath imageView:(UIImageView *)imageView{
	if ([filesCtrl objectForKey:filePath] != nil && imageView == nil) {
		//すでにダウンロード中になので、スキップする
        //NSLog(@"return  filePath:%@", filePath);
		return;
	}
	
    if ([filePath isKindOfClass:[ NSNull  class]] || filePath == nil || [filePath isEqualToString:@""]) { 
        return;
    }
    
    
	NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:2];
	[param setObject:filePath forKey:FILE_PATH_KEY];
    if (imageView != nil) {
        [param setObject:imageView forKey:OUPUT_VIEW_KEY];	
    }
    
    
	NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(handleRequest:) object:param ];
    [downLoadLockData lock];
    self.imageEditFlag = TRUE;
    [filesCtrl setObject:thread forKey:filePath];
    [downLoadLockData unlock];
	[thread start];
    
    
	[thread release];
	[param release];
}



-(void)handleRequest:(NSMutableDictionary *)param{
	NSAutoreleasePool *serverPool = [[NSAutoreleasePool alloc] init];

	NSString *allFilePath = [param objectForKey:FILE_PATH_KEY];
	UIImageView *imageView = [param objectForKey:OUPUT_VIEW_KEY];
    NSString *fb_pic_id = [param objectForKey:FACEBOOK_PICTURE_ID];

    //NSLog(@"filePath:%@",allFilePath);
    
    NSRange range_fn2 = [ allFilePath rangeOfString:@"-"];
	NSArray *fn_separate;
    NSString *filePath = @"";
	if (range_fn2.location != NSNotFound) {
		fn_separate = [allFilePath componentsSeparatedByString:@"-"];
        if ([fn_separate count] == 4) {
            filePath = [fn_separate objectAtIndex:3];
            //NSLog(@"filePath:%@",filePath);
       }
	}


    NSData *data = [Utils readFileData:filePath];
    UIImage *image = nil;
    //NSLog(@"filePath:%@",filePath);
    //NSLog(@"data length:%d",[data length]);
    if (data == nil) {
        if (fb_pic_id == nil) {
            data = [Utils readFileData:[NSString stringWithFormat:@"%@%@", CASH_DATA_SAVE_TEMP_PATH, allFilePath]];
        }else{
            data = [Utils readFileData:[NSString stringWithFormat:@"%@%@", CASH_EVER_NOTE_DATA_SAVE_TEMP_PATH, fb_pic_id]];

        }
        if (data == nil) {
            NSRange cmd_type_post = [allFilePath rangeOfString:@"://"];
            NSString *phicalFilPath = @"";
            if (cmd_type_post.location == NSNotFound) {
//                phicalFilPath = [NSString stringWithFormat:@"%@%@",BssImgDataDownloadServerURLAdress,allFilePath];
            }else{
                phicalFilPath = allFilePath;
            }
            //NSLog(@"phicalFilPath:%@",phicalFilPath);
            NSURL *url = [[[NSURL alloc] initWithString:phicalFilPath] autorelease];
            data = [[[NSData alloc] initWithContentsOfURL:url] autorelease];
            //NSLog(@"------[data length]:%d",[data length]);
            if (data != nil) {

                if (fb_pic_id == nil) {
                    [Utils makeDir:CASH_DATA_SAVE_TEMP_PATH];
                    [Utils writeFileData:[NSString stringWithFormat:@"%@%@", CASH_DATA_SAVE_TEMP_PATH, allFilePath] fileData:data];

                }else{ //FaceBook Face Picture
                    [Utils makeDir:CASH_EVER_NOTE_DATA_SAVE_TEMP_PATH];                    
                    [Utils writeFileData:[NSString stringWithFormat:@"%@%@", CASH_EVER_NOTE_DATA_SAVE_TEMP_PATH, fb_pic_id] fileData:data]; 

                    
                }
                //NSLog(@"------[data length]:%d",[data length]);

            }
        }
    }
     
    if (data != nil && [data length] > 0) {
        image = [[[UIImage alloc] initWithData: data] autorelease];
        //NSLog(@"------------image.size.width:%f",image.size.width);
         if (imageView != nil) {
             if (self.imageEditFlag) {
                 //NSLog(@"---[trimImageToCenterPos]---");
                 //サムネイル画像
                 image = [Utils trimImageToCenterPos:image trimSize:PHOT_LIBARY_THUMBNAIL_SIZE*self.resizeRate] ;
                 [imageView setImage:image];
             }else{
                 [imageView setImage:image];
                 if (image.size.width >= SCREEN_WIDTH) {
                     imageView.alpha = 0;
                     [UIView beginAnimations:@"showImageAnimation" context:nil];
                     [UIView setAnimationDuration:0.2];
                     [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                     imageView.alpha = 1.0;
                     [UIView commitAnimations];		
                 }

             }
            //NSLog(@"[allFilePath]:%@",allFilePath);
         }
      //  [image release];
   }
    
    [downLoadLockData lock];
    [filesCtrl removeObjectForKey:allFilePath];
    [downLoadLockData unlock];
    
	
    //NSLog(@"[allFilePath]:%@",allFilePath);
	//NSLog(@"------[filesCtrl count]:%d",[filesCtrl count]);
	//[data release];
	[serverPool release];
}

@end
