//
//  DownLoadThumbnailImageServerController.h
//  BukurouSS
//
//  Created by k-kin on 11/12/12.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utils.h"


@interface DownLoadThumbnailImageServerController : NSObject {
    BOOL imageEditFlag;
    int     resizeRate;
}

@property       BOOL imageEditFlag;
@property       int     resizeRate;

+(DownLoadThumbnailImageServerController *)getInstance;
-(void)clearThread;
-(void)getImageDataWithFilePath:(NSString *)filePath imageView:(UIImageView *)imageView;
-(void)getImageDataWithFilePathWithEditImage:(NSString *)filePath imageView:(UIImageView *)imageView;
-(void)getImageDataWithFilePathWithEditImageExt:(NSString *)filePath imageView:(UIImageView *)imageView;
-(void)getImageDataWithFilePathWithEditImageFinal:(NSString *)filePath imageView:(UIImageView *)imageView;

@end
