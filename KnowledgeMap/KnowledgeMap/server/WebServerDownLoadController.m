//
//  Server.m
//  BukurouSS
//
//  Created by k-kin on 11/11/14.
//  Copyright 2011 iccesoft. All rights reserved. 
//

#import "WebServerDownLoadController.h"


@implementation WebServerDownLoadController

@synthesize delegate;
@synthesize urlConnection;
@synthesize fileMIMEType;
@synthesize receiveData;

WebServerDownLoadController	*server;
NSLock			*lockData;

+(WebServerDownLoadController *)getInstance{
	if (server == nil) {
		server = [[WebServerDownLoadController alloc] init] ;
		lockData = [[NSLock alloc] init];
	}
	return server;
}


#pragma mark -
#pragma mark sendWorkDataToWebServer
- (void) sendWorkDataToWebServer:(NSMutableDictionary *)params 
			  showConnectionFlag:(BOOL)showConnectionIndicator 
					   serverUrl:(NSString *)inUrl
						delegate:(id)inDelegate{	
		
	self.delegate = inDelegate;
	
	if (![Utils checkWebServerStatus]) {
        UIAlertView *alert = [[UIAlertView alloc]  
                              initWithTitle:MultiLangString(@"Information")  
                              message:[Utils MultiLangStringInfomation:@"I0006"]  
                              delegate:nil 
                              cancelButtonTitle:MultiLangString(@"Yes") 
                              otherButtonTitles: nil];  
        [alert show];  
        [alert release]; 		

        [self cancelOptarion];
		return;
	} 
	
	if (showConnectionIndicator) {
		[Utils startDataLoadingAnimationAndSetTimeInterVal];
	}
	
	//setting up the body:
	NSMutableData* postData = [[NSMutableData alloc] init];	
	

	// POST実行
//    NSLog(@"%@",[params JSONRepresentation]);
    
    NSMutableURLRequest *postRequest = nil;
	NSURL *cgiUrl = [NSURL URLWithString:inUrl];
	postRequest = [NSMutableURLRequest requestWithURL:cgiUrl];

    NSArray *key_list = [params allKeys];
	for (int i=0; i < [key_list count] ; i++) {
		NSString *key = [key_list objectAtIndex:i];
        id valueData = [params objectForKey:key];
		NSString *value = @"";
        if ( [valueData isKindOfClass:[ NSString class]]) {
			value = valueData;
		}else if ( [valueData isKindOfClass:[ NSMutableDictionary class]]) {
            value = [valueData JSONRepresentation];
		}else if ( [valueData isKindOfClass:[ NSMutableArray class]]) {
            value = [valueData JSONRepresentation];
        }else{
            value = valueData;
        }
        
        //NSLog(@"key: %@", key);
        //NSLog(@"value: %@", value);
        [postData appendData:[[NSString stringWithFormat:@"%@=%@&", key,value] dataUsingEncoding:NSUTF8StringEncoding]];

    }
	//[postData appendData:[[NSString stringWithFormat:@"--%@--\r\n",BOUNDARY] dataUsingEncoding:NSUTF8StringEncoding]];

	[postRequest setHTTPMethod:@"POST"]; 
	[postRequest setTimeoutInterval:SYN_SEVER_CONNECT_SEND_DATA_INTERVAL_10];
	[postRequest setHTTPBody:postData];		
    
	self.urlConnection = [[NSURLConnection connectionWithRequest:postRequest delegate:self] retain];
    //[NSURLConnection connectionWithRequest:postRequest delegate:self] ;

	[postData release];
}

#pragma mark -
#pragma mark sendOtherApplicationSerivice
- (void) sendOtherApplicationSerivice:(NSString *)inUrl delegate:(id)inDelegate{	
	if (![Utils checkWebServerStatus]) {
        UIAlertView *alert = [[UIAlertView alloc]  
                              initWithTitle:MultiLangString(@"Information")  
                              message:[Utils MultiLangStringInfomation:@"I0006"]  
                              delegate:nil 
                              cancelButtonTitle:MultiLangString(@"Yes") 
                              otherButtonTitles: nil];  
        [alert show];  
        [alert release];
        
        [self cancelOptarion];

		return;
	}
    
	self.delegate = inDelegate;

	// POST実行
	NSURL *cgiUrl = [NSURL URLWithString:inUrl];		
	NSURLRequest* postRequest =
	[NSURLRequest requestWithURL:cgiUrl 
					 cachePolicy:NSURLRequestUseProtocolCachePolicy 
				 timeoutInterval:60.0];
	
	self.urlConnection = [[NSURLConnection connectionWithRequest:postRequest delegate:self] retain];
	
}



#pragma mark -
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)i_connection didReceiveResponse:(NSURLResponse *)i_response{	
	//NSLog(@"---download is didReceiveResponse--- !");
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
//	self.fileMIMEType = [i_response MIMEType];
//    NSLog(@"%@",[i_response description]);
//    NSString *fileName = [i_response suggestedFilename];
//    NSLog(@"fileName:%@",fileName);
    
    // (NSHTTPURLResponse *)にキャストする
    NSHTTPURLResponse *httpResopnse = (NSHTTPURLResponse *)i_response;
    // レスポンスヘッダを列挙
    NSDictionary *headers = httpResopnse.allHeaderFields;
    for (id key in headers) {
        if ([[key lowercaseString] isEqualToString:@"content-type"]) {
            self.fileMIMEType = [headers objectForKey:key];
        }
        //NSLog(@"%@: %@", key, [headers objectForKey:key]);
    }
    
    // データ蓄積用に NSMutableData を初期化
	self.receiveData = nil;
    self.receiveData = [[NSMutableData alloc] init];
}

- ( void ) connection:( NSURLConnection *) connection didReceiveData:( NSData *) data {	
	
	[self.receiveData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
	
    
	[self connectNetWorkError];

	NSLog(@"-- didFailWithError --:%@",[error localizedDescription]);
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)i_connection{
	//NSLog(@"---download is connectionDidFinishLoading--- !");
    
    [Utils endDataLoadingAnimation];
    

	NSRange fileTypeImage = [self.fileMIMEType rangeOfString:@"image/"];
    NSRange fileTypeApplication = [self.fileMIMEType rangeOfString:@"application/"];

	if (       (fileTypeImage.location != NSNotFound)
            || (fileTypeApplication.location != NSNotFound)
        ){
		[self recieveApplicationData:self.receiveData fileType:self.fileMIMEType];
	}
	else{
		NSString *ret_str = [[[NSString alloc] initWithData:self.receiveData encoding:NSUTF8StringEncoding] autorelease];
		//NSLog(@"--recieveData :%@",ret_str);
		//NSMutableDictionary *dictData = [ret_str JSONValue];
		//データ処理、拡張
		[self recieveOprationData:ret_str];		
	}
		
	[self clearDatas];
	
}


#pragma mark -
#pragma mark recieveData Delegate
//データ通信をキャンセルしたとき
-(void)cancelOptarion{
	[self.urlConnection cancel];
    
    [self clearDatas];

	if ([self.delegate respondsToSelector:@selector(cancelOptarion)]) {
		[self.delegate cancelOptarion];
	}	
	[self clearDatasFinal];
	
}

//データ通信エラーが発生したとき
-(void)connectNetWorkError{
    
	[self clearDatas];

	if ([self.delegate respondsToSelector:@selector(connectNetWorkError)]) {
 		[self.delegate connectNetWorkError];       
	}	
    
	
}

// 画像データを受信したとき
- (void)recieveApplicationData:(NSMutableData	*)data fileType:(NSString *)fileType{
	[lockData lock];
    if ([self.delegate respondsToSelector:@selector(recieveApplicationData:fileType:)]) {
		[self.delegate recieveApplicationData:data fileType:fileType];
	
    }
	[lockData unlock];
    
    [self clearDatas];

}

// 処理データを受信したとき
- (void)recieveOprationData:(NSString *)data{
	[lockData lock];
	if ([self.delegate respondsToSelector:@selector(recieveOprationData:)]) {
		[self.delegate recieveOprationData:data];
	}	
	[lockData unlock];
    
    [self clearDatas];

    
}


#pragma mark -
#pragma mark clearDatas
-(void)clearDatas{
    
    [Utils endDataLoadingAnimation];
    
	[self clearDatasFinal];

}

-(void)clearDatasFinal{
    
	[lockData lock];
//	self.delegate = nil;
	[self.receiveData release];
	self.receiveData = nil ;
	if (self.urlConnection != nil) {
		[self.urlConnection release];
		self.urlConnection = nil;
	}
	[lockData unlock];
	
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}


- (void)dealloc {
	if (self.urlConnection != nil) {
		[self.urlConnection release];
		self.urlConnection = nil;
	}
	[lockData release];
	[self.fileMIMEType release];
	if (self.receiveData != nil) {
		[self.receiveData release];
	}
	
	[super dealloc];
	
}

@end
