//
//  AppDelegate.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/04/30.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServerDownLoadController.h"
#import "UploadToWebServerSYNController.h"
#import "TestFlight.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

-(void)activeService;

@end
