//
//  AppDefine.h
//  BookMaker
//
//  Created by k-kin on 11/01/24.
//  Copyright 2011 ICCESOFT. All rights reserved. 
//

#import <UIKit/UIKit.h>


@protocol AppDefine

extern NSString* const reroadNotification;

#define APP_SCHEME_VULB_APP          @"vulbapp"
#define APP_SCHEME_VULB_APP_FB       @"vulbappfb001"

#define MAIL_NUMBER                  @"support@vulb.jp"

#define  TESTFLIGHT_TOKEN @"77b40218-89bc-4b55-a7df-6d07b5dab83d"

#define BUTTON_TEXT_INIT_SIZE                   16.0
#define NAVICATION_BAR_BUTTON_TEXT_INIT_SIZE    12.0

#define SCREEN_WIDTH                            [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT                           ([[UIScreen mainScreen] bounds].size.height-20)

#define FULL_MAX_RECT_SIZE                      CGRectMake(0, 0, 320, 480)
#define NAVICATION_BAR_LEFT_BUTTON_RECT         CGRectMake(-10, 0, 88, 44)
#define NAVICATION_BAR_RIGHT_BUTTON_LARGE_RECT  CGRectMake(271, 0, 44, 44)


#define PHOTO_RECT_HD_WIDTH                     1500

#define SHEET_LIST_LARGE_HEIGHT                 80
#define SHEET_LIST_HEIGHT                       60
#define CATEGORY_LIST_HEIGHT                    45

#define USER_ICON_SIZE                          36

#define THUMBNAIL_WIDTH                         245
#define THUMBNAIL_HEIGHT                        180
#define THUMBNAIL_LIST_DELTA                    10

#define CARD_WIDTH                              300
#define CARD_HEIGHT                             100

#define KEY_BOARD_HEIGHT                        256
#define LOADING_PROGRESS_UNIT_HEIGHT            25
#define TOOL_BAR_HIGHT                          44
#define COLOR_BAR_WIDTH                         5


#define inCellImgOffsetX        1
#define inCellImgOffsetY        1

#define LOGIN_TYPE_FACEBOOK     1
#define LOGIN_TYPE_MAIL         2
#define CREATE_NEW_ACCOUNT      3

#define INIT_FONT_SIZE                          16

#define CARD_DETAIL_TEXT_FONT_SIZE              14

#define clearTempFileDelta                      31

#define MAX_UPLOAD_PHTO_COUNT               10

#define PHOT_LIBARY_THUMBNAIL_SIZE			106.5

#define CARD_ROW_COUNT                      24
#define SHOW_CARD_LIST_INIT_COUNT           20

#define CONTENTS_MODE_CAMERA                1
#define CONTENTS_MODE_PHOTOLIBRARY			2

#define LIST_DATA_TYPE_SHEET                    @"1"
#define LIST_DATA_TYPE_CATEGORY                 @"2"

#define CARD_VALUATION_GOOD                     @"1"
#define CARD_VALUATION_BAD                      @"2"

#define READ_STATUS_NORMAL                      @"0"
#define READ_STATUS_NEW                         @"1"
#define READ_STATUS_UPDATE                      @"2"

#define SHEET_OPERATION_TYPE_NEW                1
#define SHEET_OPERATION_TYPE_EDIT               2


#define LAST_CLEAR_TEMP_FILE_INTEVER_KEY    @"l_c_t_F_i_"

#define	ASSETS_DATA_KEY						@"_a_d_k_"

#define DIC_KEY                                 @"_d_k_"
#define DIC_VALUE                               @"_d_v_"

#define ALL_CATEGORY_SHOW_FLAG_KEY              @"_a_c_s_f_k_"

#define FILE_PATH_KEY	@"_f_p_"
#define OUPUT_VIEW_KEY	@"_o_v_"

#define PERSON_IMG_KEY  @"p_i_k"

#define	CONTENTS_META_DUMMY @"hogehogehoge"
#define	CONTENTS_META_TEXT  @"<?xml version=\"1.0\" encoding=\"UTF-8\"?><card> <p>hogehogehoge</p></card>"
#define	CONTENTS_META_PHOTO @"<?xml version=\"1.0\" encoding=\"UTF-8\"?><card> <resource id=\"hogehogehoge\" /> </card>"
#define WEBLINK_META        @"<span class=\"vulb_link\" onclick=\"window.open('%@')\">%@<br></span>"

#define CARD_START_TAG  @"<card>"
#define CARD_END_TAG    @"</card>"

#define	IMAGE_RESOURCE_TYPE_KEY						@"_i_r_t_k_"
#define IMAGE_RESOURCE_TYPE_NOTHING					@"0"
#define IMAGE_RESOURCE_TYPE_LIBRARY					@"1"
#define IMAGE_RESOURCE_TYPE_NETWORK_FACEBOOK		@"2"
#define IMAGE_RESOURCE_TYPE_NETWORK_INSTAGRAM		@"3"
#define IMAGE_RESOURCE_TYPE_NETWORK_TWITTER			@"4"
#define IMAGE_RESOURCE_TYPE_NETWORK_MIXI			@"5"
#define IMAGE_RESOURCE_TYPE_YOUTUBE_VIDEO			@"6"
#define IMAGE_RESOURCE_TYPE_GOOGLE_IMAGE			@"7"
#define IMAGE_RESOURCE_TYPE_CAMERA_IMAGE			@"8"
#define IMAGE_RESOURCE_TYPE_EVERNOTE_IMAGE			@"9"
#define IMAGE_RESOURCE_TYPE_EVERNOTE_IMAGE_SEND_EVERNOTE    @"10"

#define WORK_VIEW_BASE_VIEW_TAG                  299
#define SHEET_NAME_LABEL_TAG                     300
#define CATEGORY_ICON_TAG                        301
#define CATEGORY_COUNT_TAG                       302
#define CARD_COUNT_TAG                           303
#define MEMBER_LIST_ICON_TAG                     304
#define MEMBER_LIST_DATA_TAG                     305
#define CATEGORY_NAME_LABEL_TAG                  306
#define CATEGORY_BAR_ICON_TAG                    307
#define SELECT_SHEET_BUTTON_TAG                  308
#define SELECT_CATEGORY_BUTTON_TAG               309
#define TEXT_INPUT_VIEW_TAG                      310
#define WORK_VIEW_MAINBUTTON_COLOR_BAR_TAG       311
#define WORK_VIEW_LOADING_BASE_TAG               312
#define TEXT_INPUT_VIEW_OLD_TAG                  313
#define BEGINNING_NAME_LABEL_TAG                 314
#define CARD_THUMBNAIL_BASE_VIEW_TAG             315
#define WORK_VIEW_BASE_BACK_VIEW_TAG             316
#define WORK_VIEW_BUTTON_BASE_TAG                317
#define RESOURCE_OPEN_BUTTON_TAG                 318
#define RESOURCE_OPEN_LABEL_TAG                  319
#define RESOURCE_OPEN_RESOURCE_ID_LABEL_TAG      320
#define RESOURCE_TYPE_LABEL_TAG                  321
#define BEGININING_LABEL_TAG                     322
#define SHEET_LIST_CONTENTS_VIEW_TAG             323
#define RESOURCE_OPEN_RESOURCE_TYPE_LABEL_TAG    324
#define RESOURCE_OPEN_RESOURCE_YOUTUBE_MOVE_LABEL_TAG    325
#define RESOURCE_OPEN_RESOURCE_FILE_NAME_LABEL_TAG       326
#define CATEGORY_LIST_CONTENTS_VIEW_TAG          327
#define TOUCH_MASK_VIEW_TAG                      328
#define SHEET_PASSCODE_TAG                       329
#define READ_STATUS_TAG                          330
#define COLOR_TYPE_BUTTON_TAG                    700
#define COLOR_ACTIVE_TEXT_BASE_TAG               332
#define TEXT_INPUT_VAULE_VIEW_TAG                333
#define CARD_CELL_BASE_VIEW_TAG                  334
#define TEXT_EDIT_COLOR_BASE_TAG				 335
#define TEXT_EDIT_COLOR_ACTIVE_TEXT_BASE_TAG	 336
#define TEXT_EDIT_COLOR_ACTIVE_BASE_BASE_TAG	 337
#define TEXT_EDIT_TOOL_BAR_SHADOW_TAG            338
#define TEXT_EDIT_TEXT_COLOR_TYPE_BUTTON_TAG		500
#define TEXT_EDIT_BASE_COLOR_TYPE_BUTTON_TAG		550
#define TEXT_EDIT_BASE_POSTION_TYPE_BUTTON_TAG		600
#define TEXT_EDIT_BASE_IMG_PAPER_TYPE_BUTTON_TAG	650
#define TEXT_EDIT_BASE_EFFECT_TYPE_BUTTON_TAG		700

#define TEST_DATA_SHEET_CODE                     @"_test_sheet_code_"

#define RESOURCE_SPLIT                           @"esource id="
#define RESOURCE_SPLIT_EXT                       @"__REOURCE_SPLIT__"
#define RESOURCE_SPLIT_EXT_SPAN_START            @"__SPAN_SPLIT__START__"
#define RESOURCE_SPLIT_EXT_SPAN_END              @"__SPAN_SPLIT__END__"
#define RESOURCE_WEB_LINK_SPEC_CHAR              @"<span class=\"vulb_link\""


#define SYN_SEVER_CONNECT_SEND_DATA_INTERVAL_10 10.0
#define SYN_SEVER_CONNECT_IMG_DATA_INTERVAL_20	20.0
#define SYN_SEVER_CONNECT_IMG_DATA_INTERVAL_60	60.0

#define SYNC_STATUS_ON          @"1"
#define SYNC_STATUS_OFF         @"0"

#define IMG_FILE_EXT_JPG                @"jpeg"
#define IMG_FILE_EXT_PNG                @"png"
#define EDIT_CHANGE_PHOTO_IMG_KEY		@"_img_"
#define EDIT_CHANGE_PHOTO_GPS_KEY		@"_gps_"
#define GPS_Latitude                    @"Latitude"
#define GPS_Longitude                   @"Longitude"

#define HEXCOLOR(c) [UIColor colorWithRed:((c>>16)&0xFF)/255.0 green:((c>>8)&0xFF)/255.0 blue:(c&0xFF)/255.0 alpha:1.0];

#define MultiLangString(s) [[NSBundle mainBundle] localizedStringForKey:s value:s table:nil]

//#define WORK_DATA_SAVE_PATH    @"workDir/"
#define CASH_DATA_SAVE_TEMP_PATH    @"cashTmp/"
#define CASH_EVER_NOTE_DATA_SAVE_TEMP_PATH    @"cashTmpEverNote/"


#define KEY_DATA_TYPE	@"_t_"
#define KEY_GROUP		@"_group_"
#define KEY_PHOTO		@"_photo_"
#define KEY_DATA_GROUP	@"_d_g_"
#define KEY_DATA_PHOTO	@"_d_p_"
#define KEY_LABEL		@"_l_"
#define KEY_DATA_CNT	@"_c_"
#define KEY_IDENTI		@"_i_"
#define KEY_THUMBNAIL_IMG @"_t_i_"
#define KEY_GROP_NAME	@"_g_n_"
#define KEY_DATA		@"_data_"


//Tag定義
#define DATA_LOADIN_ANI_MASK_VIEW_TAG                   200
#define MEDIA_PLAY_BUTTON_TAG                           201
#define MEDIA_PLAY_BUTTON_TAG_2                         202
#define DOWNLOAD_PROGRESS_BASE_TAG                      70000
#define DOWNLOAD_PROGRESS_UNIT_TAG                      750
#define CHECK_TAG                                       50000
#define CHECK_NUMBER_TAG                                60000

@end
