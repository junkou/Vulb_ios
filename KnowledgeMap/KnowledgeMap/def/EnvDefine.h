//
//  EnvDefine.h
//  BookMaker
//
//  Created by k-kin on 10/05/11.
//  Copyright 2010 iccesoft. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol EnvDefine

#define HTTP_PORT			7001
#define HTTP_PORT_ERROR		7777

#define __APP_VER_KEY__ @"__APP_VER__"
#define APP_VERSION		@"1.0.00"

#define MESSAGE_TEXT_DUMMY  @"{0}"

#define TARGET_READ_TYPE	@"1"
#define BUFFER_SIZE			254800
#define QUEUE_SIZE			16
#define IMG_COMPRESS_RAITE  0.7



@end
