//
//  DatabaseTableNmaes.h
//  BookMaker
//
//  Created by k-kin on 10/09/07.
//  Copyright 2010 ICCESOFT. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol DatabaseTableNmaes



#define SESSIONID				@"sessionId"
#define JSESSIONID				@"jsessionid"
#define PARAMS                  @"params"
#define COLORS                  @"colors"
#define CONTENTS				@"contents"
#define RESOURCEIDS				@"resourceIds"
#define FILE                    @"file"
#define MEMBERS                 @"members"
#define SHEETS                  @"sheets"
#define UNCATEGORYIZED          @"uncategorized"
#define CATEGORYIZED0           @"categorized0"
#define CATEGORYIZED1           @"categorized1"
#define START                   @"start"
#define COUNT                   @"count"
#define CATEGORYIES             @"categories"
#define RESOURCEDATA            @"resourceData"
#define ORIGINAL                @"original"
#define COMMENTS                @"comments"

#define ACTION                              @"action"
#define ACTION_CODE_KEY                     @"code"
#define ACTION_CODE_VALUE_LOGIN             @"login"
#define ACTION_CODE_VALUE_ENTRY_SHEET        @"entrySheet"
#define ACTION_CODE_VALUE_EXIT_SHEET        @"exitSheet"

#define ACTION_TYPE_KEY                     @"type"
#define ACTION_TYPE_VALUE_REQ               @"req"

#define redirectSucess_key                      @"redirectSucess"
#define redirectFailure_key                     @"redirectFailure"

/*
 * テーブル列名
 */

// 会員テーブル
#define userId_key              @"userId"
#define mailAddress_key			@"mailAddress"
#define password_key			@"password"
#define tokenId_Key					@"tokenId"
#define sessionId_Key				@"sessionId"
#define userName_Key				@"userName"
#define cardSize_Key				@"cardSize"
#define local_Key                   @"locale"
#define personImg_Key               @"personImg"
#define sheetId_Key                 @"sheetId"
#define confirmKey_Key              @"confirmKey"
#define memberStatus_Key            @"memberStatus"

//Cardテーブル
#define cardId_key                  @"cardId"
#define cardDispNo_key              @"cardDispNo"
#define cardContents_key            @"cardContents"
#define begining_key                @"beginning"
#define beginningHtml_key           @"beginningHtml"
#define verNo_key                   @"verNo"
#define valuation_key               @"valuation"
#define content_key                 @"content"
#define updateDate_key              @"updateDate"
#define toUserId_key                @"toUserId"
#define commentId_key               @"commentId"
#define confirmId_key               @"confirmId"
#define confirmKey_key              @"confirmKey"
#define valuationStatus_key         @"valuationStatus"
#define readStatus_key              @"readStatus"
#define dispCol_key                 @"dispCol"
#define toUserName_key              @"toUserName"


//リソーステーブル
#define fileName_key                @"fileName"
#define resourceId_key              @"resourceId"
#define resourceType_key            @"resourceType"
#define illustrationId_key          @"illustrationId"
#define longitute_key               @"longitute"
#define latitute_key                @"latitute"
#define zoom_key                    @"zoom"
#define urlAddress_key              @"urlAddress"
#define time_key                    @"time"


// Sheetテーブル
#define sheetName_key                  @"sheetName"
#define totalCategory_key              @"totalCategory"
#define totalCard_key                  @"totalCard"
#define shareFlg_key                   @"shareFlg"
#define confirmCd_key                  @"confirmCd"
#define sheetDescription_key           @"sheetDescription"
#define newCardCount_key               @"newCardCount"
#define upCardCount_key                @"upCardCount"
#define updateCardCount_key            @"updateCardCount"
#define sampleSheetFlg_key             @"sampleSheetFlg"


//Categoryテーブル
#define categoryId_Key              @"categoryId"
#define categoryName_key            @"categoryName"
#define colorIdx_key                @"colorIdx"
#define rgbaR                       @"rgbaR"
#define rgbaG                       @"rgbaG"
#define rgbaB                       @"rgbaB"
#define rgbaA                       @"rgbaA"

#define TIME                        @"time"
#define PHOTO_IMAGE_DATA        @"photo_image_data"
#define FILE_EXT                @"file_ext"
#define IMG_URL					@"img_url"
#define IN_ASSETS_PHOTO_INDEX	@"asset_idx"
#define data_identity_key       @"data_identity_key"
#define SYNC_STATUS             @"sync_status"

/** リソース種別 */
#define DB_RESOURCE_TYPE_BEGINING     @"0"//テキスト
#define DB_RESOURCE_TYPE_PHOTO        @"1"//写真
#define DB_RESOURCE_TYPE_SOUND        @"2"//音声
#define DB_RESOURCE_TYPE_MOVIE        @"3"//動画
#define DB_RESOURCE_TYPE_PDF          @"4"//PDF
#define DB_RESOURCE_TYPE_IllUSTRATION @"5"//イラスト
#define DB_RESOURCE_TYPE_MAP          @"6"//地図
#define DB_RESOURCE_TYPE_WEB          @"7"//web
#define DB_RESOURCE_PERSON_IMG        @"8"//ユーザー画像(※db項目ではない、画像ファイルを格納するディレクトリ名である)
#define DB_RESOURCE_TYPE_OTHER_FILE   @"99"//PDF以外、ファイル



@end
