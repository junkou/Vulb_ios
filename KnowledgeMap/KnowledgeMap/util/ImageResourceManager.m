//
//  CaptureImageManager.m
//  BukurouSS
//
//  Created by k-kin on 11/10/30.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import "ImageResourceManager.h"


@implementation ImageResourceManager

ImageResourceManager	*globalImageResourceManager;

+(ImageResourceManager *)getInstance{
	if (globalImageResourceManager == nil) {
		globalImageResourceManager = [[ImageResourceManager alloc] init] ;
	}
	
	return globalImageResourceManager;
}




-(void)getLatestImageResourceInfo:(int)type  pageCode:(NSString *)pageCode index:(int)index result:(NSMutableDictionary *)outData{
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	int cnt = [[DataRegistry getInstance] loadImageResourceData:type pageCode:pageCode index:index result:imageDataList];
	if (cnt > 0) {
        //最後に入れたデータのみ取得する
		NSMutableDictionary *imageData = [imageDataList objectAtIndex:0];		
		[outData setObject:[imageData objectForKey:TYPE] forKey:TYPE] ;
		[outData setObject:[imageData objectForKey:PAGE_CODE] forKey:PAGE_CODE] ;
		[outData setObject:[imageData objectForKey:IDX] forKey:IDX] ;
		[outData setObject:[imageData objectForKey:TIME] forKey:TIME] ;
		[outData setObject:[imageData objectForKey:IMG_URL] forKey:IMG_URL] ;
		[outData setObject:[imageData objectForKey:SAVE_STATUS] forKey:SAVE_STATUS] ;
		[outData setObject:[imageData objectForKey:SYNC_STATUS] forKey:SYNC_STATUS] ;
		[outData setObject:[imageData objectForKey:REGIST_YM] forKey:REGIST_YM] ;
		[outData setObject:[imageData objectForKey:IN_ASSETS_PHOTO_INDEX] forKey:IN_ASSETS_PHOTO_INDEX] ;
	}
	[imageDataList removeAllObjects];
	[imageDataList release];
	imageDataList = nil;
	
	
}

-(NSString *)getLatestImageResourcePath:(int)type  pageCode:(NSString *)pageCode index:(int)index{
	NSString *filePath = nil;
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	int cnt = [[DataRegistry getInstance] loadImageResourceData:type pageCode:pageCode index:index result:imageDataList];
	if (cnt > 0) {
		NSMutableDictionary *imageData = [imageDataList objectAtIndex:0];
		filePath = [imageData objectForKey:IMG_URL];
	}
	[imageDataList removeAllObjects];
	[imageDataList release];
	imageDataList = nil;
	return filePath;
}

-(NSData *)getLatestImageResource:(int)type  pageCode:(NSString *)pageCode index:(int)index{
	NSData *fileData = nil;
	NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
	[self getLatestImageResourceInfo:type pageCode:pageCode index:index result:data];
	NSString *filePath = [data objectForKey:IMG_URL];
	//NSLog(@"filePath:%@",filePath);
	if (filePath != nil) {
		fileData = [Utils readFileData:filePath];
		//NSLog(@"fileData:%d",[fileData length]);
	
		if (fileData == nil) {
			//Assetsからまだデータを取り出していないので、ここで取り出し、　且つ　保存する.
			NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
			[param setObject:[data objectForKey:IN_ASSETS_PHOTO_INDEX] forKey:IN_ASSETS_PHOTO_INDEX];
			[param setObject:filePath forKey:IMG_URL];
			[Utils insertImgDataToDataBaseWithLoadingAnimation:param];
			[param release];
			fileData = [Utils readFileData:filePath];			
		}		
	}
	
	[data release];
	return fileData;
}

-(NSString *)getLatestSavedImageResourcePath:(int)type  pageCode:(NSString *)pageCode index:(int)index{
	//NSLog(@"getLatestSavedImageResourcePath type:%d pageCode:%@",type,pageCode);
	NSString *filePath = nil;
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	int cnt = [[DataRegistry getInstance] loadImageResourceData:type pageCode:pageCode index:index result:imageDataList];
	if (cnt > 0) {
		for (int i=0; i< [imageDataList count]; i++) {
			NSMutableDictionary *imageData = [imageDataList objectAtIndex:i];
			if ([[imageData objectForKey:SAVE_STATUS] intValue] == SAVE_STATUS_ON) {
				filePath = [imageData objectForKey:IMG_URL];
				break;
			}
		}
	}
	[imageDataList removeAllObjects];
	[imageDataList release];
	imageDataList = nil;
	return filePath;
}

-(NSData *)getLatestSavedImageResource:(int)type  pageCode:(NSString *)pageCode index:(int)index{
	NSData *fileData = nil;
	NSString *filePath = [self getLatestSavedImageResourcePath:type pageCode:pageCode index:index];
	if (filePath != nil) {
		fileData = [Utils readFileData:filePath];
	}	
	return fileData;
}

-(NSString *)saveImageResourceData:(int)type  pageCode:(NSString *)pageCode index:(int)index assetsIdx:(NSString *)in_assets_idx imageData:(NSData *)imageData bookCode:(NSString *)bookCode{
	//NSLog(@"insert imageData pageCode:%@ length:%d", pageCode, [imageData length]);
	NSDate* now_date = [NSDate date];	
	NSString *createTime = [Utils getNowTimeWithDate:now_date];
	NSString *registYm = [Utils getYYYYMM];
	NSString *imgUrl = [NSString stringWithFormat:@"%d_%@_%d_%@.%@",type,pageCode,index,createTime,IMG_FILE_EXT_JPG];
	if (imageData != nil) {
		[Utils writeFileData:imgUrl fileData:imageData];
	}
	//NSLog(@"imgUrl:%@",imgUrl);
	NSMutableDictionary *imageDataDicn = [[NSMutableDictionary alloc] initWithCapacity:1];
	[imageDataDicn setObject:[NSNumber numberWithInt:type] forKey:TYPE];
	[imageDataDicn setObject:pageCode forKey:PAGE_CODE];
	[imageDataDicn setObject:[NSNumber numberWithInt:index] forKey:IDX];
	[imageDataDicn setObject:[NSNumber numberWithInt:[Utils getNowTimeIntervalWithDate:now_date]] forKey:TIME];
	[imageDataDicn setObject:imgUrl forKey:IMG_URL];
	[imageDataDicn setObject:[NSNumber numberWithInt:SAVE_STATUS_OFF] forKey:SAVE_STATUS];
	[imageDataDicn setObject:[NSNumber numberWithInt:SYNC_STATUS_NONE] forKey:SYNC_STATUS];
	[imageDataDicn setObject:registYm forKey:REGIST_YM];
	[imageDataDicn setObject:in_assets_idx forKey:IN_ASSETS_PHOTO_INDEX];
	[imageDataDicn setObject:bookCode forKey:BOOK_CODE];
	//NSLog(@"saveImageResourceData type:%d  pageCode:%@", type, pageCode);	
	[[DataRegistry getInstance] saveImageResourceData:imageDataDicn];
	[imageDataDicn release];
	return [NSString stringWithFormat:@"%@-%@",registYm,imgUrl];
}

-(NSString *)saveImageResourceDataWithSync:(NSMutableDictionary *)imageDataDicn {
	//NSLog(@"insert imageData pageCode:%@ length:%d", pageCode, [imageData length]);
	NSData *imageData = [imageDataDicn objectForKey:IMG_DATA];
	if (imageData == nil) {
		return @"";
	}
	
	NSString *imgUrl = [imageDataDicn objectForKey:IMG_URL];
	[Utils writeFileData:imgUrl fileData:imageData];
	[[DataRegistry getInstance] saveImageResourceData:imageDataDicn];
	
	return imgUrl;
}

-(void)updateLatestImageResourceSaveStatus:(int)type  pageCode:(NSString *)pageCode index:(int)index saveStatus:(int)saveStatus{
	//NSLog(@"type:%d  pageCode:%@", type, pageCode);
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	int count = [[DataRegistry getInstance] loadImageResourceData:type pageCode:pageCode index:index result:imageDataList];
	if (count > 0) {
		NSMutableDictionary *getImageData = [imageDataList objectAtIndex:0];
		NSMutableDictionary *imageData = [[NSMutableDictionary alloc] initWithCapacity:1];
		[imageData setObject:[NSNumber numberWithInt:type] forKey:TYPE];
		[imageData setObject:pageCode forKey:PAGE_CODE];
		[imageData setObject:[NSNumber numberWithInt:index] forKey:IDX];
		[imageData setObject:[getImageData objectForKey:TIME] forKey:TIME];
		[imageData setObject:[NSNumber numberWithInt:saveStatus] forKey:SAVE_STATUS];
		
		[[DataRegistry getInstance] updateImageSourceSaveStatus:imageData];		
		[imageData release];
	}
	
	[imageDataList removeAllObjects];
	[imageDataList release];
	imageDataList = nil;
}

-(void)updateImageResourceSyncStatus:(NSMutableDictionary *)param{ 
	int count = [[DataRegistry getInstance] getImageResourceDataCount:[[param objectForKey:TYPE] intValue]  pageCode:[param objectForKey:PAGE_CODE] index:[[param objectForKey:IDX] intValue]];
	if (count > 0) {
		[[DataRegistry getInstance] updateImageSourceSyncStatus:param];		
	}
}

-(void)clearUnusedImageResource:(int)type  pageCode:(NSString *)pageCode index:(int)index{
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	[[DataRegistry getInstance] loadImageResourceData:type pageCode:pageCode index:index result:imageDataList];
	
	BOOL isLatestSavedCapture = FALSE;
	
	for (int i= 0; i<[imageDataList count]; i++) {
		NSMutableDictionary *imageData = [imageDataList objectAtIndex:i];
		NSString *imgUrl = [imageData objectForKey:IMG_URL];
		NSNumber *saveStatus = [imageData objectForKey:SAVE_STATUS];
		NSNumber *syncStatus = [imageData objectForKey:SYNC_STATUS];
		
		BOOL isDelete = FALSE;
		
		if ([saveStatus intValue] == SAVE_STATUS_ON) {
			if (!isLatestSavedCapture) {
				isLatestSavedCapture = TRUE;
			}else {
				if ([syncStatus intValue] != SYNC_STATUS_BUSY) {
					isDelete = TRUE;
				}
			}
		}else if ([saveStatus intValue] == SAVE_STATUS_OFF) {
			isDelete = TRUE;
		}
		
		if (isDelete) {
			[Utils removeFile:imgUrl];
			[[DataRegistry getInstance] deleteImageResourceData:imageData];
		}
	}

	[imageDataList removeAllObjects];
	[imageDataList release];
	imageDataList = nil;
	
}

-(void)clearLatestImageResource:(int)type  pageCode:(NSString *)pageCode index:(int)index{
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	[[DataRegistry getInstance] loadImageResourceData:type pageCode:pageCode index:index result:imageDataList];
	
    //最後に保存したデータを削除する
    NSMutableDictionary *imageData = [imageDataList objectAtIndex:0];
    NSString *imgUrl = [imageData objectForKey:IMG_URL];
    [Utils removeFile:imgUrl];
    [[DataRegistry getInstance] deleteImageResourceData:imageData];
   
	[imageDataList removeAllObjects];
	[imageDataList release];
	imageDataList = nil;
	
}

-(void)clearImageResource:(int)type  pageCode:(NSString *)pageCode index:(int)index{
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	[[DataRegistry getInstance] loadImageResourceData:type pageCode:pageCode index:index result:imageDataList];
	for (int i= 0; i<[imageDataList count]; i++) {
		NSMutableDictionary *imageData = [imageDataList objectAtIndex:i];
		NSString *imgUrl = [imageData objectForKey:IMG_URL];
		
		[Utils removeFile:imgUrl];
		[[DataRegistry getInstance] deleteImageResourceData:imageData];		
	}
	
	[imageDataList removeAllObjects];
	[imageDataList release];
	imageDataList = nil;
	
}

-(void)clearImageResourceWithBookCode:(NSString *)bookCode {
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	[[DataRegistry getInstance] loadImageResourceDataWithBookCode:bookCode result:imageDataList];
	for (int i= 0; i<[imageDataList count]; i++) {
		NSMutableDictionary *imageData = [imageDataList objectAtIndex:i];
		NSString *imgUrl = [imageData objectForKey:IMG_URL];
		
		[Utils removeFile:imgUrl];
		[[DataRegistry getInstance] deleteImageResourceData:imageData];		
	}
	
	[imageDataList removeAllObjects];
	[imageDataList release];
	imageDataList = nil;
	
}


#pragma mark Map画像関連:Capture画像を一時的に保存TMP
/**
 * 最後に保存された地図キャプチャ画像を取得する。
 -(NSData *)getLatestSavedMapCaptureImage:(NSString *)pageCode index:(int)index{
	NSData *fileData = nil;
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	int cnt = [self.dataRegistry loadMapCaptureImageData:pageCode index:index result:imageDataList];
	if (cnt > 0) {
		NSMutableDictionary *imageData = [imageDataList objectAtIndex:0];
		NSString *filePath = [imageData objectForKey:IMG_URL];
		fileData = [Utils readFileData:filePath];
	}
	[imageDataList release];
	return fileData;
}


 * 地図キャプチャ画像を保存する。
-(void)saveMapCaptureImage:(NSString *)pageCode index:(int)index imageData:(NSData *)inImageData{
	NSDate* now_date = [NSDate date];	
	NSString *createTime = [Utils getNowTimeWithDate:now_date];
	NSString *imgUrl = [NSString stringWithFormat:@"map_%@_%d_%@.%@",pageCode,index, createTime,IMG_FILE_EXT_JPG];
	[Utils writeFileData:imgUrl fileData:inImageData];
	
	NSMutableDictionary *imageData = [[NSMutableDictionary alloc] initWithCapacity:1];
	[imageData setObject:pageCode forKey:PAGE_CODE];
	[imageData setObject:[NSNumber numberWithInt:index] forKey:IDX];
	[imageData setObject:[NSNumber numberWithInt:[Utils getNowTimeIntervalWithDate:now_date]] forKey:TIME];
	[imageData setObject:createTime forKey:TIME];
	[imageData setObject:imgUrl forKey:IMG_URL];
	[imageData setObject:[NSNumber numberWithInt:SAVE_STATUS_OFF] forKey:SAVE_STATUS];
	[imageData setObject:[NSNumber numberWithInt:SYNC_STATUS_NONE] forKey:SYNC_STATUS];
	
	[self.dataRegistry saveMapCaptureImageData:imageData];
	[imageData release];
	
}

 * 最新の地図キャプチャ画像の保存ステータスを更新する。
-(void)updateLatestMapCaptureImageSaveStatus:(NSString *)pageCode index:(int)index saveStatus:(int)saveStatus{
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	int count = [self.dataRegistry loadMapCaptureImageData:pageCode index:index result:imageDataList];
	if (count > 0) {
		NSMutableDictionary *getImageData = [imageDataList objectAtIndex:0];
		NSMutableDictionary *imageData = [[NSMutableDictionary alloc] initWithCapacity:1];
		[imageData setObject:pageCode forKey:PAGE_CODE];
		[imageData setObject:[NSNumber numberWithInt:index] forKey:IDX];
		[imageData setObject:[getImageData objectForKey:TIME] forKey:TIME];
		[imageData setObject:[NSNumber numberWithInt:saveStatus] forKey:SAVE_STATUS];
		
		[self.dataRegistry updateMapCaptureImageSaveStatus:imageData];		
		[imageData release];
	}
	[imageDataList release];
	
}

-(void)updateMapCaptureImageSyncStatus:(NSMutableDictionary *)param{
	[self.dataRegistry updateMapCaptureImageSyncStatus:param];		
	
}

 * 不要な地図キャプチャ画像を削除する。
 * 保存ステータス=1であるキャプチャ画像の中で最新のものを残して削除する。
-(void)clearUnusedMapCaptureImage:(NSString *)pageCode index:(int)index{
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	[self.dataRegistry loadMapCaptureImageData:pageCode index:index result:imageDataList];
	
	BOOL isLatestSavedCapture = FALSE;
	
	for (int i= 0; i<[imageDataList count]; i++) {
		NSMutableDictionary *imageData = [imageDataList objectAtIndex:i];
		NSString *imgUrl = [imageData objectForKey:IMG_URL];
		NSNumber *saveStatus = [imageData objectForKey:SAVE_STATUS];
		NSNumber *syncStatus = [imageData objectForKey:SYNC_STATUS];
		
		BOOL isDelete = FALSE;
		
		if ([saveStatus intValue] == SAVE_STATUS_ON) {
			if (!isLatestSavedCapture) {
				isLatestSavedCapture = TRUE;
			}else {
				if ([syncStatus intValue] != SYNC_STATUS_BUSY) {
					isDelete = TRUE;
				}
			}
		}else if ([saveStatus intValue] == SAVE_STATUS_OFF) {
			isDelete = TRUE;
		}
		
		if (isDelete) {
			[Utils removeFile:imgUrl];
			[self.dataRegistry deleteMapCaptureImage:imageData];
		}		
	}	
}

 * 地図キャプチャ画像を削除する。
-(void)clearMapCaptureImage:(NSString *)pageCode index:(int)index{
	NSMutableArray *imageDataList = [[NSMutableArray alloc] init];
	[self.dataRegistry loadMapCaptureImageData:pageCode index:index result:imageDataList];
	for (int i= 0; i<[imageDataList count]; i++) {
		NSMutableDictionary *imageData = [imageDataList objectAtIndex:i];
		NSString *imgUrl = [imageData objectForKey:IMG_URL];
		
		[Utils removeFile:imgUrl];
		[self.dataRegistry deleteCaptureImage:imageData];		
	}
}
**/


- (void)dealloc {
	
	[super dealloc];
	
}


@end
