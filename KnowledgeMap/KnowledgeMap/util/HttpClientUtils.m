//
//  HttpClientUtils.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/09/11.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "HttpClientUtils.h"

@implementation HttpClientUtils

HttpClientUtils *clientUtils = nil;

+ (instancetype)sharedClient
{
    if (clientUtils == nil) {
        static dispatch_once_t once;
        dispatch_once(&once, ^{
            NSURL *baseURL = [NSURL URLWithString:BssBookAppURLAdress];
            NSLog(@"WebAPI baseURL: %@", baseURL);
            clientUtils = [[self alloc] initWithBaseURL:baseURL];            
        });
    }
    return clientUtils;
    
}

#pragma mark -getServerDataSetToView
- (void) getServerDataSetToView:(NSString *)methodName
                      parameter:(NSMutableDictionary *)parameters
                           view:(UIView*)inView
                       delegate:(id)inDelegate
                         option:(id)option
{
    
    NSString *resourceFileName = @"";
    if ([methodName isEqualToString:METHOD_PERSON_IMG]) {
        resourceFileName = [NSString stringWithFormat:@"user_%d.png",[option intValue]];
    }else{
        resourceFileName = [NSString stringWithFormat:@"%d.png",[option intValue]];
    }
    NSData *data = [Utils readFileData:resourceFileName];
    if(data != nil){
        if ([methodName isEqualToString:METHOD_PERSON_IMG]) {
            [(UIImageView*)inView setImage:[UIImage imageWithData:data]];
            return;
        }else if ([methodName isEqualToString:METHOD_DOWNLOAD_THUMBNAIL]) {
            UIImageView *imageView = [[[UIImageView alloc] initWithFrame:inView.bounds] autorelease];
            imageView.backgroundColor = [UIColor whiteColor];
            [inView addSubview:imageView];
            UIImage *image = [UIImage imageWithData:data];
            image = [image imageFilledInSize:CGSizeMake(imageView.frame.size.width, imageView.frame.size.height)];
            image = [image trimImageInRect:imageView.bounds];
            [imageView setImage:image];
            return;
        }
    }

    BOOL isShow = FALSE;
    
    //    NSLog(@"method=%@, webPath=%@, parameters=%@", method, webPath, parameters);
    NSString *webPath = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,methodName];

    id req = [self requestWithMethod:@"POST"
                                path:webPath
                          parameters:parameters];
    
    id op = [self HTTPRequestOperationWithRequest:req
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              
                                              if (isShow) {
                                                  [Utils endDataLoadingAnimation];
                                              }
                                              
                                              //データ処理 
                                              [self getServerDataSuccess:methodName parameter:parameters view:inView delegate:inDelegate option:option operation:operation responseObject:responseObject];
                                          }
                                          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              NSLog(@"error:%@", error);
                                              
                                              if (isShow) {
                                                  [Utils endDataLoadingAnimation];;
                                              }
                                              
                                              
                                          }];
    
    [self enqueueHTTPRequestOperation:op];
    
    if (isShow) {
    	[Utils startDataLoadingAnimation];
    }
    
}

#pragma mark -postDataWithMethod
- (void) postDataWithMethod:(NSString *)method
                    methodName:(NSString *)methodName
                 parameters:(NSMutableDictionary *)parameters
                     option:(NSMutableDictionary *)option
              isShowLoading:(BOOL)isShow
                   delegate:(id)delegate
{
//    NSLog(@"method=%@, webPath=%@, parameters=%@", method, webPath, parameters);
    
    if (isShow) {
    	[Utils startDataLoadingAnimation];
    }

    NSString *webPath = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,methodName];
    
    id req = [self requestWithMethod:method
                                path:webPath
                          parameters:parameters];

    id op = [self HTTPRequestOperationWithRequest:req
                                            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                
                                                if (isShow) {
                                                    [Utils endDataLoadingAnimation];
                                                }
                                                //[SVProgressHUD showSuccessWithStatus:@"Loading successfully."];
                                                if ([delegate respondsToSelector:@selector(dataOperationSuccess:option:)]) {
                                                    
                                                    id recieveData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                                             options:(NSJSONReadingOptions) 0
                                                                                               error:NULL];
                                                    
                                                    NSMutableDictionary *result = [recieveData objectForKey:RESULT_KEY];
                                                    
                                                    NSString *resultStatus = [result objectForKey:RESULT_STATUS_KEY];
                                                    
                                                    if (resultStatus != nil && [resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {
                                                        //正常
                                                        NSMutableDictionary *resultDatas = [recieveData objectForKey:RESULT_DATA_VALUE_KEY];
                                                        
                                                        NSDictionary *op = [[[NSDictionary alloc] initWithDictionary:option] autorelease];
                                                        
                                                        [delegate dataOperationSuccess:resultDatas option:op];

                                                    }
                                                    
                                                }
                                            }
                                            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                NSLog(@"error:%@", error);
                                                
                                                if (isShow) {
                                                    [Utils endDataLoadingAnimation];
                                                }
                                                
                                                if ([delegate respondsToSelector:@selector(dataOperationFailed:option:)]) {                                                    
                                                    [delegate dataOperationFailed:error option:[[NSDictionary alloc] initWithDictionary:option]];
                                                }
                                                
                                            }];
    
    [self enqueueHTTPRequestOperation:op];
    
    
}

#pragma mark -postMultipartFormDataWithMethod
- (void) postMultipartFormDataWithMethod:(NSString *)method
                                 webPath:(NSString *)webPath
                              parameters:(NSMutableDictionary *)parameters
                                    data:(NSData *)data
                                    name:(NSString *)name
                                fileName:(NSString *)fileName
                                mimeType:(NSString *)mimeType
                           isShowLoading:(BOOL)isShow
                                  option:(NSMutableDictionary *)option
                                delegate:(id)delegate
{
//    NSLog(@"method=%@, webPath=%@, parameters=%@", method, webPath, parameters);
    
    id req = [self multipartFormRequestWithMethod:method
                                               path:webPath
                                         parameters:parameters
                          constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
                              if (data != nil) {
                                  [formData appendPartWithFileData:data name:name fileName:fileName mimeType:mimeType];
                              }
                          }];
    
    id op = [self HTTPRequestOperationWithRequest:req
                                            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                if (isShow) {
                                                    [SVProgressHUD dismiss];
                                                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Successfully posted.", @"Successfully posted.")];
                                                }
                                                
                                                if ([delegate respondsToSelector:@selector(dataOperationSuccess:option:)]) {
                                                    id obj = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                                             options:(NSJSONReadingOptions) 0
                                                                                               error:NULL];
                                                    [delegate dataOperationSuccess:obj option:option];
                                                }
                                            }
                                            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                NSLog(@"error:%@", error);
                                                
                                                if (isShow) {
                                                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Post failed.", @"Post failed.")];
                                                }
                                            }];
    
    [self enqueueHTTPRequestOperation:op];
    
    if (isShow) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Posting...", @"Posting...")
                             maskType:SVProgressHUDMaskTypeGradient];
    }
}


- (void) getServerDataSuccess:(NSString *)methodName
                      parameter:(NSMutableDictionary *)parameters
                           view:(UIView*)inView
                       delegate:(id)inDelegate
                         option:(id)option
                    operation:(AFHTTPRequestOperation *)operation
               responseObject:(id) responseObject
{

    id recieveData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                     options:(NSJSONReadingOptions) 0
                                                       error:NULL];
    
    
    //NSLog(@"recieveData:%@",[recieveData description]);
    
    NSMutableDictionary *result = [recieveData objectForKey:RESULT_KEY];
    
    NSString *resultStatus = [result objectForKey:RESULT_STATUS_KEY];
    
    if (resultStatus != nil && [resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {
        //正常
        NSMutableDictionary *resultDatas = [recieveData objectForKey:RESULT_DATA_VALUE_KEY];
        
        if ([methodName isEqualToString:METHOD_PERSON_IMG]) {
            NSString *personImgStr = [Utils parseWebDataToString:resultDatas dataKey:personImg_Key];
             if (![personImgStr isEqualToString:@""]) {
                 NSData *imageData = [NSData dataFromBase64String:personImgStr];
                 [(UIImageView*)inView setImage:[UIImage imageWithData:imageData]];
                 
                 [Utils writeFileData:[NSString stringWithFormat:@"user_%d.png",[option intValue]] fileData:imageData];
            }
            
        }else if ([methodName isEqualToString:METHOD_DOWNLOAD_THUMBNAIL]) {
            NSString *resourceData = [resultDatas objectForKey:RESOURCEDATA];
            NSData *imageData = [NSData dataFromBase64String:resourceData];
            
            UIImageView *imageView = [[[UIImageView alloc] initWithFrame:inView.bounds] autorelease];
            imageView.backgroundColor = [UIColor whiteColor];
            [inView addSubview:imageView];
            UIImage *image = [UIImage imageWithData:imageData];
            image = [image imageFilledInSize:CGSizeMake(imageView.frame.size.width, imageView.frame.size.height)];
            image = [image trimImageInRect:imageView.bounds];
            [imageView setImage:image];
            
            if ([imageData length] > 0) {
                [Utils writeFileData:[NSString stringWithFormat:@"%d.png",[option intValue]] fileData:imageData];
            }
            
            
        }else if ([methodName isEqualToString:METHOD_RESOURCE_DOWNLOAD]
                  ) {
            NSString *resourceType = [resultDatas objectForKey:resourceType_key];
            if([resourceType isEqualToString:DB_RESOURCE_TYPE_PHOTO] || [resourceType isEqualToString:DB_RESOURCE_TYPE_IllUSTRATION]){
                
                NSString *resourceData = [resultDatas objectForKey:RESOURCEDATA];
                NSData *imageData = [NSData dataFromBase64String:resourceData];
                
                UIImageView *imageView = [[[UIImageView alloc] initWithFrame:inView.bounds] autorelease];
                [inView addSubview:imageView];
                imageView.backgroundColor = [UIColor whiteColor];
                UIImage *image = [UIImage imageWithData:imageData];
                image = [image imageFilledInSize:CGSizeMake(imageView.frame.size.width, imageView.frame.size.height)];
                image = [image trimImageInRect:imageView.bounds];
                [imageView setImage:image];
                
                
            } else if([resourceType isEqualToString:DB_RESOURCE_TYPE_PDF]
                      || [resourceType isEqualToString:DB_RESOURCE_PERSON_IMG]){
                //PDF OR Youtube DB_RESOURCE_TYPE_OTHER_FILE
                
                NSString *resourceData = [resultDatas objectForKey:RESOURCEDATA];
                NSData *imageData = [NSData dataFromBase64String:resourceData];
                
                UIImageView *imageView = [[[UIImageView alloc] initWithFrame:inView.bounds] autorelease];
                imageView.backgroundColor = [UIColor whiteColor];
                [inView addSubview:imageView];
                UIImage *image = nil;
                
                NSString *YoutubeMoveId = nil;
                if ([resourceType isEqualToString:DB_RESOURCE_TYPE_PDF] ) {
                    image = [UIImage imageWithData:imageData];
                    
                }else if([resourceType isEqualToString:DB_RESOURCE_PERSON_IMG]){
                    // Youtube
                    NSString *urlAddress = [resultDatas objectForKey:urlAddress_key];
                    NSRange range = [urlAddress rangeOfString:@"?"];
                    NSString *param = [urlAddress substringFromIndex:range.location+range.length];
                    NSArray *splits = [Utils splitParameterString:param];
                    for (NSDictionary *dic in splits) {
                        NSString *key = [dic objectForKey:DIC_KEY];
                        if ([[key uppercaseString] isEqualToString:@"V"]) {
                            YoutubeMoveId = [dic objectForKey:DIC_VALUE];
                            break;
                        }
                    }
                    
                    NSString *url = [NSString stringWithFormat:YouTubeThumnailImageURL,YoutubeMoveId];
                    imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                    
                    image = [UIImage imageWithData:imageData];
                }
                
                image = [image imageFilledInSize:CGSizeMake(imageView.frame.size.width, imageView.frame.size.height)];
                image = [image trimImageInRect:imageView.bounds];
                [imageView setImage:image];
                
                UIView *openButtonBaseView = [[[UIView alloc] initWithFrame:CGRectMake(0, inView.frame.size.height-30, inView.frame.size.width, 30)] autorelease];
                openButtonBaseView.backgroundColor = [UIColor blackColor];
                openButtonBaseView.alpha = 0.5;
                [inView addSubview:openButtonBaseView];

                //DummyButtonをかぶせる
                UIButton *button = [[[UIButton alloc] initWithFrame:inView.bounds] autorelease];
                [inView addSubview:button];
                
                //Pdf or Youtube Open button
                NSString *openStr = @"";
                openStr =  MultiLangString(@"Open");
                CGSize size = [openStr sizeWithFont:[UIFont  boldSystemFontOfSize:16] constrainedToSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT) lineBreakMode:NSLineBreakByCharWrapping];
                size.width = size.width+10;
                
                CGRect rect = CGRectMake(inView.frame.size.width-size.width-5, openButtonBaseView.frame.origin.y, size.width, openButtonBaseView.frame.size.height);
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = rect;
                [button addTarget:inDelegate action:@selector(clickOpenResourceButton:) forControlEvents:UIControlEventTouchUpInside];
                //[button setImage:[Utils getWhiteButtonNormalImage:button.bounds] forState:UIControlStateNormal];
                //[button setImage:[Utils getWhiteButtonSelectedImage:button.bounds] forState:UIControlStateHighlighted];
                [inView addSubview:button];
                
                //Openテキスト文字を表示
                UILabel *label = [[[UILabel alloc] initWithFrame:button.bounds] autorelease];
                label.backgroundColor = [UIColor clearColor];
                label.font = [UIFont  boldSystemFontOfSize:16];
                label.textColor = [UIColor whiteColor];
                label.textAlignment = UITextAlignmentCenter;
                label.text = openStr;
                [button addSubview:label];
                
                //ResourceIdを保存 optionでパラメーターとして渡してくる。
                label = [[[UILabel alloc] initWithFrame:button.bounds] autorelease];
                label.tag = RESOURCE_OPEN_RESOURCE_ID_LABEL_TAG;
                label.hidden = YES;
                label.text =  [NSString stringWithFormat:@"%d",[option intValue]];
                [button addSubview:label];
                
                //ResourceType
                label = [[[UILabel alloc] initWithFrame:button.bounds] autorelease];
                label.tag = RESOURCE_OPEN_RESOURCE_TYPE_LABEL_TAG;
                label.hidden = YES;
                label.text =  resourceType;
                [button addSubview:label];
                
                //fileName
                NSString *fileName = [Utils parseWebDataToString:resultDatas dataKey:fileName_key];
                label = [[[UILabel alloc] initWithFrame:button.bounds] autorelease];
                label.tag = RESOURCE_OPEN_RESOURCE_FILE_NAME_LABEL_TAG;
                label.hidden = YES;
                label.text =  fileName;
                [button addSubview:label];

                
                //Youtube MoveId
                if([resourceType isEqualToString:DB_RESOURCE_PERSON_IMG]){
                    label = [[[UILabel alloc] initWithFrame:button.bounds] autorelease];
                    label.tag = RESOURCE_OPEN_RESOURCE_YOUTUBE_MOVE_LABEL_TAG;
                    label.hidden = YES;
                    label.text =  YoutubeMoveId;
                    [button addSubview:label];
                    
                }else if([resourceType isEqualToString:DB_RESOURCE_TYPE_PDF]){
                    NSString *fileName = [Utils parseWebDataToString:resultDatas dataKey:fileName_key];
                    UIImage *iconImage = [Utils getFileThumbnailIconImage:fileName];
                    UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(5, inView.frame.size.height-(5+iconImage.size.height), iconImage.size.width, iconImage.size.height)] autorelease];
                    iconView.image = iconImage;
                    [inView addSubview:iconView];
                    
                }
                
                
            } else {
                
                //File Icon
                NSString *fileName = [Utils parseWebDataToString:resultDatas dataKey:fileName_key];
                if (![fileName isEqualToString:@""]) {
                    if ([Utils isShowFileName:fileName]) {
                        UILabel *beginningLable = (UILabel *)[inView viewWithTag:BEGINNING_NAME_LABEL_TAG];
                        if (beginningLable != nil) {
                            [beginningLable removeFromSuperview];
                        }
                        NSString *beginning = fileName;
                        CGFloat forntSize = 20;
                        CGSize size = [fileName sizeWithFont:[UIFont boldSystemFontOfSize:forntSize] constrainedToSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT) lineBreakMode:NSLineBreakByWordWrapping];
                        
                        beginningLable = [[UILabel alloc] initWithFrame:inView.bounds];
                        beginningLable.tag = BEGINNING_NAME_LABEL_TAG;
                        beginningLable.backgroundColor = [UIColor whiteColor];
                        beginningLable.font = [UIFont  systemFontOfSize:forntSize];
                        //beginningLable.textColor = [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0];
                        beginningLable.textColor = [UIColor blackColor];
                        beginningLable.textAlignment = UITextAlignmentCenter;
                        beginningLable.numberOfLines = size.height/forntSize;
                        beginningLable.text = beginning;
                        [inView addSubview:beginningLable];
                        [beginningLable release];
                        
                        
                        //word,excel,powerpoint
                        UIView *openButtonBaseView = nil;
                        if ([Utils isOfficeFile:fileName]) {
                            openButtonBaseView = [[[UIView alloc] initWithFrame:CGRectMake(0, inView.frame.size.height-30, inView.frame.size.width, 30)] autorelease];
                            openButtonBaseView.backgroundColor = [UIColor blackColor];
                            openButtonBaseView.alpha = 0.5;
                            [inView addSubview:openButtonBaseView];
                        }
                        
                        UIImage *iconImage = [Utils getFileThumbnailIconImage:fileName];
                        UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(5, inView.frame.size.height-(5+iconImage.size.height), iconImage.size.width, iconImage.size.height)] autorelease];
                        iconView.image = iconImage;
                        [inView addSubview:iconView];
                        
                        //dummyBUtton
                        UIButton *button = [[[UIButton alloc] initWithFrame:inView.bounds] autorelease];
                        [inView addSubview:button];
                        
                        //word,excel,powerpoint
                        if ([Utils isOfficeFile:fileName]) {
                            //DummyButtonをかぶせる
                            
                            //Pdf or Youtube Open button
                            NSString *openStr = @"";
                            openStr =  MultiLangString(@"Open");
                            CGSize size = [openStr sizeWithFont:[UIFont  boldSystemFontOfSize:16] constrainedToSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT) lineBreakMode:NSLineBreakByCharWrapping];
                            size.width = size.width+10;
                            
                            CGRect rect = CGRectMake(inView.frame.size.width-size.width-5, openButtonBaseView.frame.origin.y, size.width, openButtonBaseView.frame.size.height);

                            button = [UIButton buttonWithType:UIButtonTypeCustom];
                            button.frame = rect;
                            [button addTarget:inDelegate action:@selector(clickOpenResourceButton:) forControlEvents:UIControlEventTouchUpInside];
                            //[button setImage:[Utils getWhiteButtonNormalImage:button.bounds] forState:UIControlStateNormal];
                            //[button setImage:[Utils getWhiteButtonSelectedImage:button.bounds] forState:UIControlStateHighlighted];
                            [inView addSubview:button];
                            
                            //Openテキスト文字を表示
                            UILabel *label = [[[UILabel alloc] initWithFrame:button.bounds] autorelease];
                            label.backgroundColor = [UIColor clearColor];
                            label.font = [UIFont  boldSystemFontOfSize:16];
                            label.textColor = [UIColor whiteColor];
                            label.textAlignment = UITextAlignmentCenter;
                            label.text = openStr;
                            [button addSubview:label];
                            
                            //ResourceIdを保存 optionでパラメーターとして渡してくる。
                            label = [[[UILabel alloc] initWithFrame:button.bounds] autorelease];
                            label.tag = RESOURCE_OPEN_RESOURCE_ID_LABEL_TAG;
                            label.hidden = YES;
                            label.text =  [NSString stringWithFormat:@"%d",[option intValue]];
                            [button addSubview:label];
                            
                            //ResourceType
                            label = [[[UILabel alloc] initWithFrame:button.bounds] autorelease];
                            label.tag = RESOURCE_OPEN_RESOURCE_TYPE_LABEL_TAG;
                            label.hidden = YES;
                            label.text =  resourceType;
                            [button addSubview:label];
                            
                            //fileName
                            NSString *fileName = [Utils parseWebDataToString:resultDatas dataKey:fileName_key];
                            label = [[[UILabel alloc] initWithFrame:button.bounds] autorelease];
                            label.tag = RESOURCE_OPEN_RESOURCE_FILE_NAME_LABEL_TAG;
                            label.hidden = YES;
                            label.text =  fileName;
                            [button addSubview:label];
                            
                        }

                        
                    }
                    
                    
                }
                
            }
            
            
        }
        
    }

}

@end
