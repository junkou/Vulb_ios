//
//  Utils.m
//  BookMaker
//
//  Created by k-kin on 10/06/03.
//  Copyright 2010 ICCESOFT. All rights reserved.
//

#import "Utils.h"


@implementation Utils



#pragma mark -
#pragma mark Get_active_memory
+ (natural_t)get_active_memory {
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);
    vm_statistics_data_t vm_stat;
    if (host_statistics(host_port, HOST_BASIC_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS) {
        NSLog(@"Failed to fetch vm statistics");
        return 0;
    }
    
    natural_t mem_free = vm_stat.active_count * pagesize / (1024*1024) ;
//    natural_t mem_free = vm_stat.active_count * pagesize / (1024) ;
//	NSLog(@"active_memory: %d",mem_free );
    return mem_free;
}

#pragma mark checkVersion
+(BOOL)versionIsfive{
	NSString *versions = [[UIDevice currentDevice] systemVersion];
	//NSLog(@"versions:%@",versions);
	if ([versions compare:@"5.0"] != NSOrderedAscending) {
		return TRUE;
	}else {
		return FALSE;
	}
}

+(BOOL)versionIsSix{
	NSString *versions = [[UIDevice currentDevice] systemVersion];
	//NSLog(@"versions:%@",versions);
	if ([versions compare:@"6.0"] != NSOrderedAscending) {
		return TRUE;
	}else {
		return FALSE;
	}
}

+(BOOL)versionIsSeven
{
	NSString *versions = [[UIDevice currentDevice] systemVersion];
	//NSLog(@"versions:%@",versions);
	if ([versions compare:@"7.0"] != NSOrderedAscending) {
		return TRUE;
	}else {
		return FALSE;
	}
}

#pragma mark -
#pragma mark checkWebServerStatus
+(BOOL)checkWebServerStatus{	
	Reachability *rb = [Reachability reachabilityForInternetConnection];
	if ([rb currentReachabilityStatus] == NotReachable) {
		return FALSE;
	} else {
		return TRUE;
	}
}

+(BOOL)checkWiFiServerStatus{	
	Reachability *rb = [Reachability reachabilityForLocalWiFi];
	if ([rb currentReachabilityStatus] == NotReachable) {
		return FALSE;
	} else {
		return TRUE;
	}
}


+ (void)addCommonButtonProPerty:(UIButton *)button imgName:(NSString *)imgName label:(NSString *)labelText {
    //button.backgroundColor = [UIColor redColor];
    if (imgName != nil && ![imgName isEqualToString:@""]) {
        UIImage *backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:[NSString stringWithFormat:@"%@Normal.png", imgName]]];
        [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
        
        UIImage *backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:[NSString stringWithFormat:@"%@Touched.png", imgName]]];
        [button setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
        
        UILabel *label = [[UILabel alloc] initWithFrame:button.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = UITextAlignmentCenter;
        label.font = [UIFont  boldSystemFontOfSize:BUTTON_TEXT_INIT_SIZE];
        label.text =  NSLocalizedString(labelText, labelText);
        [button addSubview:label];
        [label release];

    }else{
        [self setCommonButtonProperti:button rect:button.bounds title:labelText];
    }
    
	
}

+ (void)setNaviBarButtonProPerty:(UIButton *)button title:(NSString *)title{
    if (title != nil) {
        [button setTitle:NSLocalizedString(title,title) forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont  boldSystemFontOfSize:NAVICATION_BAR_BUTTON_TEXT_INIT_SIZE]  ;
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    }
    UIImage *backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_back_normal.png"]];
    [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    UIImage *backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_back_touch.png"]];
    [button setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    
}

+(void)setImageToNavigationBar:(UINavigationBar *)navigationBar imageName:(NSString *)imageName{
	// Insert imageView
	UIImageView *imageView;
	UIImage *image;
	image = [[UIImage imageNamed:imageName]
			 stretchableImageWithLeftCapWidth:0 topCapHeight:1];
    
    if ([self versionIsfive]) {
        [navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }else{
        imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = navigationBar.bounds;
        imageView.autoresizingMask = (UIViewAutoresizingFlexibleWidth
                                      | UIViewAutoresizingFlexibleHeight);
        imageView.layer.zPosition = -FLT_MAX;
        
        [navigationBar insertSubview:imageView atIndex:0];
        [imageView release];
    }
    
}


+(void)setNavigationBarLeftButton:(UINavigationBar *)navigationBar navigationItem:(UINavigationItem *)navigationItem controller:(id)controller{

    //HeaderView
    [Utils setImageToNavigationBar:navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];
    
    UIView *titleView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, TOOL_BAR_HIGHT)] autorelease];
    navigationItem.titleView = titleView;
    
    //キャンセルボタンを生成
    UIButton *cancelButton = [[[UIButton alloc] initWithFrame:NAVICATION_BAR_LEFT_BUTTON_RECT] autorelease];
    //cancelButton.backgroundColor = [UIColor blueColor];
    [cancelButton addTarget:controller action:@selector(clickCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:cancelButton];
    
    UIImageView *imageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_back_normal.png"]]] autorelease];
    [cancelButton addSubview:imageView];

    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.size.width-10, cancelButton.frame.origin.y, 44, imageView.frame.size.height)] autorelease];
 	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont  systemFontOfSize:18];
	label.textColor = [UIColor whiteColor];
	label.textAlignment = UITextAlignmentLeft;
	label.numberOfLines = 1;
	label.text = MultiLangString(@"Back");
	[cancelButton addSubview:label];

}

+(void)setNavigationBarTitleLabel:(UIView *)view text:(NSString *)title{
    CGFloat buttonsize = 65;
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(buttonsize, 0, view.bounds.size.width-buttonsize*2, view.bounds.size.height)] autorelease];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = UITextAlignmentCenter;
    label.font = [UIFont  systemFontOfSize:20];
    label.text =  NSLocalizedString(title,title);
    [view addSubview:label];

}

+(UIButton *)setNavigationBarRightSaveButton:(UINavigationItem *)navigationItem controller:(id)controller{
    
    UIButton *saveButton = [[[UIButton alloc] initWithFrame:NAVICATION_BAR_RIGHT_BUTTON_LARGE_RECT] autorelease];
    [saveButton addTarget:controller action:@selector(clickSaveButton:) forControlEvents:UIControlEventTouchUpInside];
    //[Utils setNaviBarButtonProPerty:saveButton title:@"Save"];
    UIImage *backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"bt_save_normal.png"]];
    [saveButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    UIImage *backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"bt_save_touch.png"]];
    [saveButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    UIBarButtonItem *doneButton= [[[UIBarButtonItem alloc] initWithCustomView:saveButton] autorelease];
	[navigationItem setRightBarButtonItem:doneButton];

    return saveButton;
}

+(UIButton *)setNavigationBarRightWriteButton:(UINavigationItem *)navigationItem controller:(id)controller{
    
    UIButton *writeButton = [[[UIButton alloc] initWithFrame:NAVICATION_BAR_RIGHT_BUTTON_LARGE_RECT] autorelease];
    [writeButton addTarget:controller action:@selector(clickWriteButton:) forControlEvents:UIControlEventTouchUpInside];
    //[Utils setNaviBarButtonProPerty:saveButton title:@"Save"];
    UIImage *backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_write_normal.png"]];
    [writeButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    UIImage *backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_write_touch.png"]];
    [writeButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    UIBarButtonItem *doneButton= [[[UIBarButtonItem alloc] initWithCustomView:writeButton] autorelease];
	[navigationItem setRightBarButtonItem:doneButton];
    
    return writeButton;
    
}


+(void)setBackgroundBaseImage:(UIView *)view{
    //BaseImage
    UIImageView *baseImageView = [[UIImageView alloc] initWithFrame:view.bounds];
    baseImageView.tag = WORK_VIEW_BASE_BACK_VIEW_TAG;
    baseImageView.image = [UIImage imageNamed:[Utils getResourceIconPath:@"bss_back_image.png"]];
    [view addSubview:baseImageView];
    [baseImageView release];

}


+ (NSString *)MultiLangStringInfomation:(NSString *)key{
	NSString *value = MultiLangString(key);
	return value;
}

+(UIImage*)resizeImage:(UIImage*)image Size:(CGSize) size{
	UIImage*    shrinkedImage;
    //	CGSize  size = { 320, 480 };
	UIGraphicsBeginImageContext(size);
	CGRect  rect;
	rect.origin = CGPointZero;
	rect.size = size;
	[image drawInRect:rect];
	shrinkedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return shrinkedImage;
}

+(UIImage*)trimImageToCenterPos:(UIImage*)image trimSize:(CGSize)trimSize {

    CGFloat x = 0;
    CGFloat y = 0;
    
    if (image.size.width <= trimSize.width || image.size.height <= trimSize.height) {
        return image;
    }
    
    if(image.size.width > trimSize.width){
        x = (image.size.width-trimSize.width)/2;
    }
    
    if(image.size.height > trimSize.height){
        y = (image.size.height-trimSize.height)/2;
    }
    UIImage *trimImage = [self trimImage:image frameRect:CGRectMake(x, y, trimSize.width, trimSize.height)];
//    if (imgThumb.size.width > SCREEN_WIDTH/2) {
//        imgThumb = [self resizeImage:imgThumb Size:CGSizeMake(SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
//    }
    
	return trimImage;
    
    /*
     if(re_size.width > trimSize){
     x = (re_size.width-trimSize)/2;
     re_size.width = trimSize;
     }
     if(re_size.height > trimSize){
     y = (re_size.height-trimSize)/2;
     re_size.height = trimSize;
     }
     */
    //    NSLog(@"x:%f, y:%f",x,y);
    //    NSLog(@"re_size: %@", NSStringFromCGSize(re_size));
    
    
}

+(UIImage*)trimImage:(UIImage*)image  frameRect:(CGRect)frameRect {
    /*
     UIImage*    shrinkedImage;
     UIGraphicsBeginImageContext(frameRect.size);
     [image drawAtPoint:frameRect.origin];
     shrinkedImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     return shrinkedImage;
     */
	UIImage*    shrinkedImage;
	CGImageRef cgimage = CGImageCreateWithImageInRect(image.CGImage, frameRect);
	shrinkedImage = [UIImage imageWithCGImage:cgimage];
	CGImageRelease(cgimage);
	return shrinkedImage;
    
}


+ (NSString *)getResourceIconPath:(NSString *)fileName{
	//NSLog([NSString stringWithFormat:@"%@%@", RESOURCES_PATH, fileName]);
	return [NSString stringWithFormat:@"resourceIcon/%@", fileName];
}

+ (NSString *)getResourceIllustPath:(NSString *)fileName{
	//NSLog([NSString stringWithFormat:@"%@%@", RESOURCES_PATH, fileName]);
	return [NSString stringWithFormat:@"resourceIllust/%@", fileName];
}

+ (NSString *) getRanDomCode{
	CFUUIDRef uudiRef = CFUUIDCreate(kCFAllocatorDefault);
	CFStringRef uuidStr = CFUUIDCreateString(kCFAllocatorDefault, uudiRef);
	
    NSString *retStr = [NSString stringWithFormat:@"%@",uuidStr] ;
  	CFRelease(uudiRef);
	CFRelease(uuidStr);
    
	return retStr ;
}

+ (void)setSheetDataContents:(NSMutableDictionary *)inData sampleSheetFlag:(NSString *)sampleFlag view:(UIView *)inBaseView iconImage:(NSString *)openIcon delegate:(id)inDelegate{
    /*
     self.contentView.backgroundColor = [UIColor clearColor];
     self.backgroundColor = [UIColor clearColor];
     */
    UIView *baseView = [inBaseView viewWithTag:SHEET_LIST_CONTENTS_VIEW_TAG];
    if (baseView != nil) {
        [baseView removeFromSuperview];
    }
    
    baseView = [[[UILabel alloc] initWithFrame:inBaseView.bounds] autorelease];
    baseView.tag = SHEET_LIST_CONTENTS_VIEW_TAG;
    baseView.backgroundColor = [UIColor clearColor];
    [inBaseView addSubview:baseView];
    
    CGFloat x = 0;
	CGFloat y = 0;
    CGFloat iconSize = USER_ICON_SIZE;

    NSArray *memberList = [inData objectForKey:MEMBERS];
   
    NSNumber *newCardCount = [inData objectForKey:newCardCount_key];
    NSNumber *updateCardCount = [inData objectForKey:upCardCount_key];
    

    if ([sampleFlag isEqualToString:@"0"] && ([newCardCount intValue] > 0 || [updateCardCount intValue] > 0)) {
        y = (baseView.frame.size.height - (SHEET_LIST_LARGE_HEIGHT - SHEET_LIST_HEIGHT)-iconSize)/2;
    }else{
        y = (baseView.frame.size.height-iconSize)/2;
    }

    //open Icon
    UIImage *iconImage =[UIImage imageNamed:[Utils getResourceIconPath:openIcon]];
    UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, y, iconImage.size.width, iconImage.size.height)] autorelease];
    iconView.image = iconImage;
    [baseView addSubview:iconView];
    x +=iconImage.size.width;

    
    //User Icon
    UIImageView *UserIconView;
    UserIconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, y, iconSize, iconSize)] autorelease];
    UserIconView.layer.cornerRadius = 5.0f;
    UserIconView.clipsToBounds = YES;
    UserIconView.image = [UIImage imageNamed:[Utils getResourceIconPath:@"defaultProfImg.png"]];
    for (NSDictionary *memberInfo in memberList) {
        NSString *ownFlag = [memberInfo objectForKey:@"ownFlg"];
        if ([ownFlag isEqualToString:@"1"]) {
            NSNumber *userId = [memberInfo objectForKey:userId_key];
            //UserIcon
            [baseView addSubview:UserIconView];
            [Utils setUserIconWith:[userId intValue] imageView:UserIconView];
            
            x += iconSize ;
            break;
        }
    }
     
    //Set SheetName
	NSString *sheetName = [inData objectForKey:sheetName_key];
	if (sheetName == nil || [sheetName isKindOfClass:[ NSNull  class]]) {
		sheetName=@"";
	}
    NSNumber *categoryCnt = [Utils parseWebDataToNumber:inData dataKey:totalCategory_key];
    sheetName = [NSString stringWithFormat:@"%@(%d)",sheetName,[categoryCnt intValue]];
    UILabel *sheetNameLable = (UILabel *)[baseView viewWithTag:SHEET_NAME_LABEL_TAG];
    if (sheetNameLable != nil) {
        [sheetNameLable removeFromSuperview];
    }
	sheetNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(x+UserIconView.frame.origin.y, UserIconView.frame.origin.y, SCREEN_WIDTH-x-40, 26)] autorelease];
    sheetNameLable.tag = SHEET_NAME_LABEL_TAG;
	sheetNameLable.backgroundColor = [UIColor clearColor];
	sheetNameLable.font = [UIFont  systemFontOfSize:18];
	sheetNameLable.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
	sheetNameLable.textAlignment = UITextAlignmentLeft;
	sheetNameLable.numberOfLines = 1;
	sheetNameLable.text = sheetName;
	[baseView addSubview:sheetNameLable];
	
    //MemberList
    UILabel *memberListLable = (UILabel *)[baseView viewWithTag:MEMBER_LIST_DATA_TAG];
    if (memberListLable != nil) {
        [memberListLable removeFromSuperview];
    }
    memberListLable = [[[UILabel alloc] initWithFrame:CGRectMake(x+UserIconView.frame.origin.y, sheetNameLable.frame.origin.y+sheetNameLable.frame.size.height+3, sheetNameLable.frame.size.width, 12)] autorelease];
    memberListLable.tag = MEMBER_LIST_DATA_TAG;
    memberListLable.backgroundColor = [UIColor clearColor];
    memberListLable.font = [UIFont  systemFontOfSize:10];
    memberListLable.textColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:1.0];
    memberListLable.textAlignment = UITextAlignmentLeft;
    memberListLable.numberOfLines = 1;
    [baseView addSubview:memberListLable];
    
    NSString *otherMember = @"";
    for (int i=0; i < [memberList count]; i++) {
        NSMutableDictionary *memberItem = [memberList objectAtIndex:i];
        NSString *userName = [memberItem objectForKey:userName_Key];
        if (i == 0) {
            otherMember = userName;
        }else{
            otherMember = [NSString stringWithFormat:@"%@,%@", otherMember,userName];
        }
        
    }
    memberListLable.text = otherMember;

    x = UserIconView.frame.origin.x;
    y = UserIconView.frame.origin.y+UserIconView.frame.size.height+10;
    
    if ([sampleFlag isEqualToString:@"0"] ){
        //NewCardCount
        if ([newCardCount intValue] > 0) {
            //new Icon
            UIImage *iconImage =[UIImage imageNamed:[Utils getResourceIconPath:@"icon_sheetlist_new.png"]];
            UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, y, iconImage.size.width, iconImage.size.height)] autorelease];
            iconView.image = iconImage;
            [baseView addSubview:iconView];
            
            x = iconView.frame.origin.x + iconView.frame.size.width + 5;
            
            NSString *count = [NSString stringWithFormat:@"%d",[newCardCount intValue]];
            UILabel *countLabel = [[[UILabel alloc] initWithFrame:CGRectMake(x, y, 40, iconView.frame.size.height)] autorelease];
            countLabel.backgroundColor = [UIColor clearColor];
            countLabel.font = [UIFont  boldSystemFontOfSize:16];
            countLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:50.0/255.0 blue:150.0/255.0 alpha:1.0];
            countLabel.textAlignment = UITextAlignmentLeft;
            countLabel.numberOfLines = 1;
            countLabel.text = count;
            [baseView addSubview:countLabel];
            
            x = countLabel.frame.origin.x + countLabel.frame.size.width + 10;
            
        }
        
        //UpdateCount
        if ([updateCardCount intValue] > 0) {
            //update Icon
            UIImage *iconImage =[UIImage imageNamed:[Utils getResourceIconPath:@"icon_sheetlist_update.png"]];
            UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, y, iconImage.size.width, iconImage.size.height)] autorelease];
            iconView.image = iconImage;
            [baseView addSubview:iconView];
            
            x = iconView.frame.origin.x + iconView.frame.size.width + 5;
            
            NSString *count = [NSString stringWithFormat:@"%d",[updateCardCount intValue]];
            UILabel *countLabel = [[[UILabel alloc] initWithFrame:CGRectMake(x, y, 40, iconView.frame.size.height)] autorelease];
            countLabel.backgroundColor = [UIColor clearColor];
            countLabel.font = [UIFont  boldSystemFontOfSize:18];
            countLabel.textColor = [UIColor colorWithRed:40.0/255.0 green:150.0/255.0 blue:35.0/255.0 alpha:1.0];
            countLabel.textAlignment = UITextAlignmentLeft;
            countLabel.numberOfLines = 1;
            countLabel.text = count;
            [baseView addSubview:countLabel];
            
        }
    }

    
 
}

+ (void)setCategoryDataContents:(NSMutableDictionary *)inData status:(NSMutableDictionary *)statusData checked:(BOOL)checkedFlag  sampleSheetFlag:(NSString *)sampleFlag view:(UIView *)inBaseView {
     
    UIView *baseView = [inBaseView viewWithTag:CATEGORY_LIST_CONTENTS_VIEW_TAG];
    if (baseView != nil) {
        [baseView removeFromSuperview];
    }
    
    baseView = [[[UILabel alloc] initWithFrame:inBaseView.bounds] autorelease];
    baseView.tag = CATEGORY_LIST_CONTENTS_VIEW_TAG;
    baseView.backgroundColor = [UIColor clearColor];
    [inBaseView addSubview:baseView];

    //Set categoryName
    CGFloat x = 0;
    CGFloat y = 0;

    NSNumber *newCardCount = [statusData objectForKey:newCardCount_key];
    NSNumber *updateCardCount = [statusData objectForKey:updateCardCount_key];
    
    if ([sampleFlag isEqualToString:@"0"] && ([newCardCount intValue] > 0 || [updateCardCount intValue] > 0)) {
        y = (SHEET_LIST_LARGE_HEIGHT - SHEET_LIST_HEIGHT)*1;
    }

    
    if (checkedFlag) {
        //open Icon
        UIImage *iconImage =[UIImage imageNamed:[Utils getResourceIconPath:@"icon_check.png"]];
        UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, (baseView.frame.size.height-y-iconImage.size.height)/2, iconImage.size.width, iconImage.size.height)] autorelease];
        iconView.image = iconImage;
        [baseView addSubview:iconView];
    }
    x =42;

    //すべてのカードを見るCellの設定
    id allCategoryFlag = [inData objectForKey:ALL_CATEGORY_SHOW_FLAG_KEY];
    if (allCategoryFlag != nil) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, baseView.bounds.size.width-x, baseView.bounds.size.height-y)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont  boldSystemFontOfSize:18];
        label.textColor = [UIColor blackColor];
        label.textAlignment = UITextAlignmentLeft;
        label.numberOfLines = 1;
        label.text = MultiLangString(@"All cards this sheet");
        [baseView addSubview:label];
        [label release];
        
        UIView *lineView = [[[UIView alloc] initWithFrame:CGRectMake(x, baseView.bounds.size.height-0.5, baseView.bounds.size.width-x, 0.5)] autorelease];
        lineView.backgroundColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:0.4];
        [baseView addSubview:lineView];

    }else{
        NSString *categoryName = [inData objectForKey:categoryName_key];
        NSNumber *cardCnt = [Utils parseWebDataToNumber:inData dataKey:totalCard_key];
        NSNumber *categoryColoer = [Utils parseWebDataToNumber:inData dataKey:colorIdx_key];
        if (categoryName == nil || [categoryName isKindOfClass:[ NSNull  class]]) {
            categoryName=@"";
        }
        categoryName = [NSString stringWithFormat:@"%@(%d)",categoryName,[cardCnt intValue]];
        
        UILabel *categoryNameLable = (UILabel *)[baseView viewWithTag:CATEGORY_NAME_LABEL_TAG];
        if (categoryNameLable != nil) {
            [categoryNameLable removeFromSuperview];
        }
        categoryNameLable = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, baseView.bounds.size.width-x, baseView.bounds.size.height-y)];
        categoryNameLable.tag = CATEGORY_NAME_LABEL_TAG;
        categoryNameLable.backgroundColor = [UIColor clearColor];
        categoryNameLable.font = [UIFont  systemFontOfSize:18];
        categoryNameLable.textColor = [Utils getColor:[categoryColoer intValue]];
        categoryNameLable.textAlignment = UITextAlignmentLeft;
        categoryNameLable.numberOfLines = 1;
        categoryNameLable.text = categoryName;
        [baseView addSubview:categoryNameLable];
        [categoryNameLable release];
        
        UIView *lineView = [[[UIView alloc] initWithFrame:CGRectMake(x, baseView.bounds.size.height-0.5, baseView.bounds.size.width-x, 0.5)] autorelease];
        lineView.backgroundColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:0.4];
        [baseView addSubview:lineView];

    }


    if ([sampleFlag isEqualToString:@"0"] ) {
        //NewCardCount
        if ([newCardCount intValue] > 0) {
            //new Icon
            UIImage *iconImage =[UIImage imageNamed:[Utils getResourceIconPath:@"icon_sheetlist_new.png"]];
            UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, baseView.bounds.size.height-y, iconImage.size.width, iconImage.size.height)] autorelease];
            iconView.image = iconImage;
            [baseView addSubview:iconView];
            
            x = iconView.frame.origin.x + iconView.frame.size.width + 5;
            
            NSString *count = [NSString stringWithFormat:@"%d",[newCardCount intValue]];
            UILabel *countLabel = [[[UILabel alloc] initWithFrame:CGRectMake(x, baseView.bounds.size.height-y, 40, iconView.frame.size.height)] autorelease];
            countLabel.backgroundColor = [UIColor clearColor];
            countLabel.font = [UIFont  boldSystemFontOfSize:16];
            countLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:50.0/255.0 blue:150.0/255.0 alpha:1.0];
            countLabel.textAlignment = UITextAlignmentLeft;
            countLabel.numberOfLines = 1;
            countLabel.text = count;
            [baseView addSubview:countLabel];
            
            x = countLabel.frame.origin.x + countLabel.frame.size.width + 10;
            
        }
        
        //UpdateCount
        if ([updateCardCount intValue] > 0) {
            //update Icon
            UIImage *iconImage =[UIImage imageNamed:[Utils getResourceIconPath:@"icon_sheetlist_update.png"]];
            UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, baseView.bounds.size.height-y, iconImage.size.width, iconImage.size.height)] autorelease];
            iconView.image = iconImage;
            [baseView addSubview:iconView];
            
            x = iconView.frame.origin.x + iconView.frame.size.width + 5;
            
            NSString *count = [NSString stringWithFormat:@"%d",[updateCardCount intValue]];
            UILabel *countLabel = [[[UILabel alloc] initWithFrame:CGRectMake(x, baseView.bounds.size.height-y, 40, iconView.frame.size.height)] autorelease];
            countLabel.backgroundColor = [UIColor clearColor];
            countLabel.font = [UIFont  boldSystemFontOfSize:18];
            countLabel.textColor = [UIColor colorWithRed:40.0/255.0 green:150.0/255.0 blue:35.0/255.0 alpha:1.0];
            countLabel.textAlignment = UITextAlignmentLeft;
            countLabel.numberOfLines = 1;
            countLabel.text = count;
            [baseView addSubview:countLabel];
            
        }
    }


}



+ (void)setCardListDataContents:(NSMutableDictionary *)inData view:(UIView *)baseView colorBar:(int)colorIdx delegate:(id)inDelegate{
    /*
     self.contentView.backgroundColor = [UIColor clearColor];
     self.backgroundColor = [UIColor clearColor];
     */
    
    CGFloat width = CARD_WIDTH;
    CGFloat height = CARD_HEIGHT;
    
    baseView.layer.cornerRadius = 5.0f;
    baseView.clipsToBounds = YES;

    //color Bar
    UIImageView *categoryIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, COLOR_BAR_WIDTH, height)];
    categoryIcon.tag = CATEGORY_BAR_ICON_TAG;
    categoryIcon.backgroundColor = [Utils getColor:colorIdx];
    //[categoryIcon setImage:[UIImage imageNamed:@"bss_side_menu_icon_favorite_nomral.png"]];
    [baseView addSubview:categoryIcon ];
    [categoryIcon release];

    NSString *resourceType = [Utils parseWebDataToString:inData dataKey:resourceType_key];
    if ([resourceType isEqualToString:DB_RESOURCE_TYPE_BEGINING]) {
        //Set categoryName
        //NSLog([inData JSONRepresentation]);
        NSString *beginningHtml = [Utils parseWebDataToString:inData dataKey:beginningHtml_key];
        if ([beginningHtml isEqualToString:@""]) {
            NSString *beginning = [Utils parseWebDataToString:inData dataKey:begining_key];
            beginning = [beginning gtm_stringByUnescapingFromHTML];
            beginning = [beginning stringByReplacingMatchesOfPattern:@"<.*?>" withString:@""];
            
            UILabel *beginningLable = (UILabel *)[baseView viewWithTag:BEGINNING_NAME_LABEL_TAG];
            if (beginningLable != nil) {
                [beginningLable removeFromSuperview];
            }
            
            CGFloat forntSize = 14;
            CGSize size = [beginning sizeWithFont:[UIFont systemFontOfSize:forntSize] constrainedToSize:CGSizeMake(width-COLOR_BAR_WIDTH-10, height) lineBreakMode:NSLineBreakByWordWrapping];
            if (size.height > height) {
                size.height = height;
            }
            beginningLable = [[[UILabel alloc] initWithFrame:CGRectMake(COLOR_BAR_WIDTH+5, 5, width-20, size.height)] autorelease];
            beginningLable.tag = BEGINNING_NAME_LABEL_TAG;
            beginningLable.backgroundColor = [UIColor clearColor];
            beginningLable.font = [UIFont  systemFontOfSize:forntSize];
            //beginningLable.textColor = [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0];
            beginningLable.textColor = [UIColor blackColor];
            beginningLable.textAlignment = UITextAlignmentLeft;
            beginningLable.numberOfLines = size.height/forntSize;
            beginningLable.text = beginning;
            [baseView addSubview:beginningLable];
        }else{
            UIWebView *webView = [[[UIWebView alloc] initWithFrame:CGRectMake(COLOR_BAR_WIDTH+5, 5, width-20, height)] autorelease];
            webView.userInteractionEnabled = NO;
            [baseView addSubview:webView];
          
            [webView loadHTMLString:beginningHtml baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];

        }
        
 
   } else if([resourceType isEqualToString:DB_RESOURCE_PERSON_IMG]){
        NSString *beginning = [Utils parseWebDataToString:inData dataKey:begining_key];
        beginning = [beginning substringFromIndex:[beginning rangeOfString:@"?"].location+1];
        NSArray *splits = [self splitParameterString:beginning];
        NSString *value = nil;
        for (NSDictionary *dic in splits) {
            NSString *key = [dic objectForKey:DIC_KEY];
            if ([[key uppercaseString] isEqualToString:@"V"]) {
                value = [dic objectForKey:DIC_VALUE];
                break;
            }
        }

        NSString *url = [NSString stringWithFormat:YouTubeThumnailImageURL,value];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
       
       CGRect rect = baseView.bounds;
       //NSLog(@"rect.siez:%f",rect.size.width);
       rect.origin.x = COLOR_BAR_WIDTH;
       rect.size.width = rect.size.width-COLOR_BAR_WIDTH;
       UIImageView *imageView = [[[UIImageView alloc] initWithFrame:rect] autorelease];
       [baseView addSubview:imageView];
       image = [image imageFilledInSize:CGSizeMake(imageView.frame.size.width, imageView.frame.size.height)];
       image = [image trimImageInRect:imageView.bounds];
       [imageView setImage:image];
       
       UIImage *iconImage = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_file_movie.png"]];
       UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(baseView.frame.size.width-(5+iconImage.size.width), 0, iconImage.size.width, iconImage.size.height)] autorelease];
       iconView.image = iconImage;
       [baseView addSubview:iconView];

       
    }else{
        NSNumber *resourceId = [Utils parseWebDataToNumber:inData dataKey:resourceId_key];
        
        //UIActivityIndicatorView
        CGFloat IndicatorSize = 20;
        UIActivityIndicatorView *activityIndicator = [[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((baseView.frame.size.width-IndicatorSize)/2, (baseView.frame.size.height-IndicatorSize)/2, IndicatorSize, IndicatorSize)] autorelease];
        [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [baseView addSubview:activityIndicator];
        [activityIndicator startAnimating];

        //thumbnailView
        CGRect rect = baseView.bounds;
//        NSLog(@"rect.siez:%f",rect.size.width);
        rect.origin.x = COLOR_BAR_WIDTH;
        rect.size.width = rect.size.width-COLOR_BAR_WIDTH;
        UIImageView *thumbnailView = [[[UIImageView alloc] initWithFrame:rect] autorelease];
        [baseView addSubview:thumbnailView];
        

//        if ([resourceType isEqualToString:DB_RESOURCE_TYPE_PDF]) {
//            UIImage *iconImage = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_file_pdf.png"]];
//            UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(baseView.frame.size.width-(5+iconImage.size.width), 0, iconImage.size.width, iconImage.size.height)] autorelease];
//            iconView.image = iconImage;
//            rect = iconView.frame;
//            [baseView addSubview:iconView];
//        }

        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setObject:resourceId forKey:resourceId_key];
        [paramDic setObject:[param JSONRepresentation] forKey:PARAMS];
        [param release];
 
        [[HttpClientUtils sharedClient] getServerDataSetToView:METHOD_DOWNLOAD_THUMBNAIL parameter:paramDic view:thumbnailView delegate:nil option:resourceId];

        [paramDic release];

    }
    
    //File Icon
    NSString *fileName = [Utils parseWebDataToString:inData dataKey:fileName_key];
    if (![fileName isEqualToString:@""]) {
        if ([self isShowFileName:fileName]) {
            UILabel *beginningLable = (UILabel *)[baseView viewWithTag:BEGINNING_NAME_LABEL_TAG];
            if (beginningLable != nil) {
                [beginningLable removeFromSuperview];
            }
            NSString *beginning = fileName;
            CGFloat forntSize = 14;
            CGSize size = [beginning sizeWithFont:[UIFont systemFontOfSize:forntSize] constrainedToSize:CGSizeMake(width-COLOR_BAR_WIDTH-10, height) lineBreakMode:NSLineBreakByWordWrapping];
            if (size.height > height) {
                size.height = height;
            }
            beginningLable = [[UILabel alloc] initWithFrame:CGRectMake(COLOR_BAR_WIDTH+5, 5, baseView.frame.size.width-(COLOR_BAR_WIDTH+5)/2, baseView.frame.size.height-10)];
            beginningLable.tag = BEGINNING_NAME_LABEL_TAG;
            beginningLable.backgroundColor = [UIColor clearColor];
            beginningLable.font = [UIFont  systemFontOfSize:forntSize];
            //beginningLable.textColor = [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0];
            beginningLable.textColor = [UIColor blackColor];
            beginningLable.textAlignment = UITextAlignmentCenter;
            beginningLable.numberOfLines = size.height/forntSize;
            beginningLable.text = beginning;
            [baseView addSubview:beginningLable];
            [beginningLable release];
            
            UIImage *iconImage = [self getFileIconImage:fileName];
            UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(baseView.frame.size.width-(5+iconImage.size.width), 0, iconImage.size.width, iconImage.size.height)] autorelease];
            iconView.image = iconImage;
            [baseView addSubview:iconView];

        }

        if ([resourceType isEqualToString:DB_RESOURCE_TYPE_PDF]) {
            UIImage *iconImage = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_file_pdf.png"]];
            UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(baseView.frame.size.width-(5+iconImage.size.width), 0, iconImage.size.width, iconImage.size.height)] autorelease];
            iconView.image = iconImage;
            CGRect rect = baseView.bounds;
            rect = iconView.frame;
            [baseView addSubview:iconView];
        }

    }

    
    NSMutableDictionary *sheetInfo =  [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    if (sheetInfo != nil) {
        NSString *sampleSheetFlag = [Utils parseWebDataToString:sheetInfo dataKey:sampleSheetFlg_key];
        if ([sampleSheetFlag isEqualToString:@""]) {
            sampleSheetFlag = @"0";
        }
        
        if ([sampleSheetFlag isEqualToString:@"0"]) {
            //New Update Icon
            NSString *readStatus = [Utils parseWebDataToString:inData dataKey:readStatus_key];
            if (![readStatus isEqualToString:READ_STATUS_NORMAL]) {
                if ([readStatus isEqualToString:READ_STATUS_NEW]) {
                    UIImage *newIcon = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_new.png"]];
                    UIImageView *newIconView = [[[UIImageView alloc] initWithFrame:CGRectMake(baseView.bounds.size.width-newIcon.size.width, baseView.bounds.size.height-newIcon.size.height, newIcon.size.width, newIcon.size.height)] autorelease];
                    newIconView.tag = READ_STATUS_TAG;
                    [newIconView setImage:newIcon];
                    [baseView addSubview:newIconView ];
                    
                } else if ([readStatus isEqualToString:READ_STATUS_UPDATE]) {
                    UIImage *updateIcon = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_update.png"]];
                    UIImageView *updateIconView = [[[UIImageView alloc] initWithFrame:CGRectMake(baseView.bounds.size.width-updateIcon.size.width, baseView.bounds.size.height-updateIcon.size.height, updateIcon.size.width, updateIcon.size.height)] autorelease];
                    updateIconView.tag = READ_STATUS_TAG;
                    [updateIconView setImage:updateIcon];
                    [baseView addSubview:updateIconView ];
                    
                }
            }

        }
    }

    
 }

+ (void)setCardDetailDataContents:(NSMutableDictionary *)inData view:(UIView *)inBaseView delegate:(id)inDelegate{
    
    UIView *baseView = [inBaseView viewWithTag:CARD_CELL_BASE_VIEW_TAG];
    [baseView removeFromSuperview];
    
    CGSize size = [Utils getCardDetailTextSize:inData delegate:inDelegate];
    CGFloat x = 10;
    baseView = [[[UIView alloc] initWithFrame:CGRectMake(x, 0, SCREEN_WIDTH-x*2, size.height-2)] autorelease];
    [inBaseView addSubview:baseView];
    

    NSString *resourceType = [Utils parseWebDataToString:inData dataKey:resourceType_key];
    if ([resourceType isEqualToString:DB_RESOURCE_TYPE_BEGINING]) {
        baseView.backgroundColor = [Utils getBaseTextColor];
        [baseView setNeedsDisplay];

        //Set categoryName
        //NSLog([inData JSONRepresentation]);
        NSString *beginningHtml = [Utils parseWebDataToString:inData dataKey:begining_key];
        //NSLog(@"%@",beginningHtml);
        beginningHtml = [beginningHtml stringByReplacingOccurrencesOfString:@"'" withString:@"\""];

        beginningHtml = [NSString stringWithFormat:@"<!DOCTYPE html><html><head><style type=\"text/css\">body { overflow:hidden; background-color:rgb(230,230,230); margin:0; padding:0; width:300; word-wrap:break-word;}</style></head><body contenteditable=\"true\" >%@</body></html>",beginningHtml];
//        beginningHtml = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">body { overflow:hidden; background-color:rgb(230,230,230); margin:0; padding:0; width:300; word-wrap:break-word;}</style></head><body>%@</body></html>",beginningHtml];
       CGFloat height = [inDelegate getHtmlHeight:beginningHtml];
        UIWebView *webView = [[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, baseView.bounds.size.width, height)] autorelease];
        webView.backgroundColor = [Utils getBaseTextColor];
        webView.delegate = inDelegate;
        webView.scrollView.scrollEnabled=NO;
        //webView.userInteractionEnabled = NO;
        [baseView addSubview:webView];

        [webView loadHTMLString:beginningHtml baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
        [webView setNeedsDisplay];


//        NSString *path = [[NSBundle mainBundle] pathForResource: @"index_show" ofType: @"html"];
//        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
//        NSString *html = [NSString stringWithFormat:@"function execCreateHtml(){document.body.innerHTML =\"%@\";};execCreateHtml();", beginningHtml];
//        [webView stringByEvaluatingJavaScriptFromString:html];
 
        
        //NSLog(@"html:%@",html);
        

        
//        NSString *htmlExec = [NSString stringWithFormat:@"function execCreateHtml(){document.body.style.margin = \"0 0 0 0\"; ;};execCreateHtml();"];
//        [webView stringByEvaluatingJavaScriptFromString:htmlExec];

        /*
        UILabel *beginningLable = (UILabel *)[baseView viewWithTag:BEGINNING_NAME_LABEL_TAG];
        if (beginningLable != nil) {
            [beginningLable removeFromSuperview];
        }
        
        CGFloat forntSize = CARD_DETAIL_TEXT_FONT_SIZE;
        CGSize size = [self getCardDetailTextSize:inData];
  
        beginningLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, baseView.bounds.size.width, size.height)];
        beginningLable.tag = BEGINNING_NAME_LABEL_TAG;
        beginningLable.backgroundColor = [UIColor clearColor];
        beginningLable.font = [UIFont  systemFontOfSize:forntSize];
        beginningLable.textColor = [UIColor blackColor];
        beginningLable.textAlignment = UITextAlignmentLeft;
        beginningLable.numberOfLines = size.height/forntSize;
        beginningLable.text = beginning;
        [baseView addSubview:beginningLable];
        [beginningLable release];
         */
    }else if ([resourceType isEqualToString:DB_RESOURCE_TYPE_WEB]) {
        CGFloat forntSize = CARD_DETAIL_TEXT_FONT_SIZE;
        CGSize size = [self getCardDetailTextSize:inData delegate:inDelegate];
        
        NSString *beginning = [Utils parseWebDataToString:inData dataKey:begining_key];
        
        NSMutableAttributedString *attrStr;
        attrStr = [[[NSMutableAttributedString alloc] initWithString:beginning] autorelease];

        NSRange linkRange = [beginning rangeOfString:beginning];
        NSURL *linkUrl = [NSURL URLWithString:beginning];
        [attrStr addAttribute:NSLinkAttributeName value:linkUrl range:linkRange];
        
        UILabel *beginningLable = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, baseView.bounds.size.width, size.height)] autorelease];
        beginningLable.tag = BEGINNING_NAME_LABEL_TAG;
        beginningLable.backgroundColor = [UIColor clearColor];
        beginningLable.font = [UIFont  systemFontOfSize:forntSize];
        beginningLable.textColor = [UIColor blueColor];
        beginningLable.textAlignment = UITextAlignmentLeft;
        beginningLable.lineBreakMode = NSLineBreakByCharWrapping;
        beginningLable.numberOfLines = 0;// size.height/forntSize;
        beginningLable.attributedText = attrStr;
        //beginningLable.text = beginning;
        //[beginningLable sizeToFit];
        [baseView addSubview:beginningLable];


    }else{
        NSNumber *resourceId = [Utils parseWebDataToNumber:inData dataKey:resourceId_key];
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setObject:resourceId forKey:resourceId_key];
        [paramDic setObject:[param JSONRepresentation] forKey:PARAMS];
        [param release];
        [[HttpClientUtils sharedClient] getServerDataSetToView:METHOD_RESOURCE_DOWNLOAD parameter:paramDic view:baseView delegate:inDelegate option:resourceId];
        [paramDic release];
        
        //UIActivityIndicatorView
        CGFloat IndicatorSize = 20;
        UIActivityIndicatorView *activityIndicator = [[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((baseView.frame.size.width-IndicatorSize)/2, (baseView.frame.size.height-IndicatorSize)/2, IndicatorSize, IndicatorSize)] autorelease];
        [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [baseView addSubview:activityIndicator];
        [activityIndicator startAnimating];


        
    }
    
    
}

+ (CGSize)getCardDetailTextSize:(NSMutableDictionary *)inData delegate:(id)inDelegate{
    NSString *resourceType = [Utils parseWebDataToString:inData dataKey:resourceType_key];
    CGFloat width = SCREEN_WIDTH-20;
    CGFloat height = SCREEN_HEIGHT;
    CGSize retSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
    
    if (resourceType != nil && [resourceType isEqualToString:DB_RESOURCE_TYPE_BEGINING]) {
        //Set categoryName
        //NSLog([inData JSONRepresentation]);
        NSString *beginningHtml = [Utils parseWebDataToString:inData dataKey:begining_key];
        NSString *htmlEscape = [beginningHtml stringByReplacingMatchesOfPattern:@"<.*?>" withString:@""];
        if ([htmlEscape isEqualToString:@""]) {
            retSize.height = 0;
        }else{
            CGFloat height_2 = [inDelegate getHtmlHeight:beginningHtml];
            retSize.height = height_2+10;
        }
 
    }else if(resourceType != nil &&  [resourceType isEqualToString:DB_RESOURCE_TYPE_WEB]){
        NSString *beginning = [Utils parseWebDataToString:inData dataKey:begining_key];
        
        CGFloat forntSize = CARD_DETAIL_TEXT_FONT_SIZE;
        retSize = [beginning sizeWithFont:[UIFont systemFontOfSize:forntSize] constrainedToSize:CGSizeMake(width, height*10) lineBreakMode:NSLineBreakByWordWrapping];
        
        retSize.height = retSize.height+10;

    }else{
        retSize = CGSizeMake(width, height/2);
    }
    
    return retSize;
}


+(CGSize)sheetNameLabelProperty:(NSString *)inString view:(UIView *)baseView startX:(CGFloat)inX color:(UIColor *)inColor{
    CGSize boundSize = baseView.bounds.size;
    //NSLog(@"::::boundSize: %@", NSStringFromCGSize(boundSize));
    CGSize size = [inString sizeWithFont:[UIFont  boldSystemFontOfSize:16] constrainedToSize:boundSize  lineBreakMode:NSLineBreakByCharWrapping];
    
    UILabel *infoDetailLable = [[UILabel alloc] initWithFrame:CGRectMake(inX, 0, size.width, size.height)];
    infoDetailLable.backgroundColor = [UIColor clearColor];
    infoDetailLable.font = [UIFont  boldSystemFontOfSize:16];
    infoDetailLable.textColor = inColor;
    infoDetailLable.textAlignment = UITextAlignmentLeft;
    infoDetailLable.numberOfLines = 1;
    infoDetailLable.text = inString;
    [baseView addSubview:infoDetailLable];
    [infoDetailLable release];
    
    return size;

}
+(UIImage *)getListFourceImage:(CGRect)rect{
	ImagePartitionView *extImageView = [[[ImagePartitionView alloc] initWithFrame:rect] autorelease];
	
	NSArray *parts = [NSArray arrayWithObjects:
					  [Utils getResourceIconPath:@"bss_list_focus.png"],
					  [Utils getResourceIconPath:@"bss_list_focus.png"],
                      [Utils getResourceIconPath: @"bss_list_focus.png"],
					  nil];
	
	extImageView.images = parts;
	//[extImageView setNeedsDisplay];
	return  [extImageView getScreenShotImage];
}

+(void)sendDataToLocalServer:(NSString *)inData{
	NSURL *cgiUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%d",LocalServerURLAdress, [[DownLoadImageResourceController getServerInstance] getPort] ] ];
	
	NSMutableData* postData = [[NSMutableData alloc] init];
	[postData appendData:[inData dataUsingEncoding:NSUTF8StringEncoding]];
	NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:cgiUrl];
	[postRequest setHTTPMethod:@"POST"];
	[postRequest setTimeoutInterval:SYNC_WEBSEVER_TIME_STOP_WORK];
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BOUNDARY];
	[postRequest setValue: contentType forHTTPHeaderField: @"Content-Type"];
	[postRequest setHTTPBody:postData];
	[postData release];
    
	[NSURLConnection connectionWithRequest:postRequest delegate:nil];
	
}

#pragma mark -
#pragma mark File Opretion
// ファイルの作成
+ (BOOL)writeFileData:(NSString *)fileName fileData:(NSData *)data {
    //	[[DataRegistry getInstance] LockDatas];
    if (data == nil || fileName == nil) {
        return FALSE;
    }
    
    [Utils makeDir:CASH_DATA_SAVE_TEMP_PATH];
    
    fileName = [NSString stringWithFormat:@"%@%@", CASH_DATA_SAVE_TEMP_PATH, fileName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return FALSE;
    }
    //	NSString *filename= [NSString stringWithFormat:@"%@.%@",fileName, TEMP_FILE_ZIP_EXT];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:fileName];
	[data writeToFile:appFile atomically:YES];
    //	[[DataRegistry getInstance] unLockDatas];
	return TRUE;
}

+ (NSString *)writeMovieData:(NSString *)dataKey movie_data:(NSData *)movie {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return NO;
    }
	NSString *filename= [NSString stringWithFormat:@"%@.%@",dataKey, @"MOV"];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:filename];
	[movie writeToFile:appFile atomically:YES];
	
	//	NSLog([NSString stringWithFormat:@"MovieFilename=%@", filename]);
    return filename;
}

//ファイルの読み込む
+ (NSData *)readFileData:(NSString *)fileName {
	NSData *fileData = nil;
    NSArray *paths = nil;
    
    fileName = [NSString stringWithFormat:@"%@%@", CASH_DATA_SAVE_TEMP_PATH, fileName];

	if ([self existsFileNSCachesDirectory:fileName]) {
		paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        
	}else if([self existsFileDocumentDirectory:fileName]){
 		paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
    }
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:fileName];
    fileData = [[[NSData alloc] initWithContentsOfFile:appFile] autorelease];
    
    return fileData;
}

//ファイルの削除
+ (void)removeFile:(NSString*)path {
    if (![self existsFileNSCachesDirectory:path]) return;
	//	NSLog([NSString stringWithFormat:@"delete file=%@",path]);
    NSArray* paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES);
    NSString* dir=[paths objectAtIndex:0];
    path=[dir stringByAppendingPathComponent:path];
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}


//Tempファイルのクリア
+ (void)clearTempFileDirectory:(NSString*)path {
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
	NSString *lastDeleteTimIntervalData = [ud stringForKey:LAST_CLEAR_TEMP_FILE_INTEVER_KEY];
    if (lastDeleteTimIntervalData != nil) {
        NSTimeInterval lastDeleteTimInterval = (NSTimeInterval)[lastDeleteTimIntervalData doubleValue];
        NSTimeInterval nowTimInterval = [[NSDate date] timeIntervalSince1970];
        NSTimeInterval delta = round((nowTimInterval -lastDeleteTimInterval) / (60 * 60 * 24));
        
        if (delta >= clearTempFileDelta) {
            [self removeFile:path];
            
            [ud setObject:[NSString stringWithFormat:@"%f",nowTimInterval] forKey:LAST_CLEAR_TEMP_FILE_INTEVER_KEY];
            [ud synchronize];
            
        }
        
    }else{
        [ud setObject:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]] forKey:LAST_CLEAR_TEMP_FILE_INTEVER_KEY];
        [ud synchronize];
    }
    
}



//ファイルが存在するか
+ (BOOL)existsFile:(NSString*)fileName {
    NSArray* paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString* dir=[paths objectAtIndex:0];
    //NSLog(@"----dir:%@",dir);
    NSString *phischlFileName=[dir stringByAppendingPathComponent:fileName];
    //NSLog(@"phischlFileName:%@",phischlFileName);
    return [[NSFileManager defaultManager] fileExistsAtPath:phischlFileName];
}

+ (BOOL)existsFileDocumentDirectory:(NSString*)fileName {
    NSArray* paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString* dir=[paths objectAtIndex:0];
    //NSLog(@"----dir:%@",dir);
    NSString *phischlFileName=[dir stringByAppendingPathComponent:fileName];
    //NSLog(@"phischlFileName:%@",phischlFileName);
    return [[NSFileManager defaultManager] fileExistsAtPath:phischlFileName];
}

+ (BOOL)existsFileNSCachesDirectory:(NSString*)fileName {
    NSArray* paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES);
    NSString* dir=[paths objectAtIndex:0];
    //NSLog(@"----dir:%@",dir);
    NSString *phischlFileName=[dir stringByAppendingPathComponent:fileName];
    //NSLog(@"phischlFileName:%@",phischlFileName);
    return [[NSFileManager defaultManager] fileExistsAtPath:phischlFileName];
}

//ディレクトリの生成
+ (void)makeDir:(NSString*)path {
    if ([self existsFileNSCachesDirectory:path]) return;
    NSArray* paths=NSSearchPathForDirectoriesInDomains(
													   NSCachesDirectory,NSUserDomainMask,YES);
    NSString* dir=[paths objectAtIndex:0];
    path=[dir stringByAppendingPathComponent:path];
	[[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:TRUE attributes:nil error:nil];
}


+(NSString *)getLocalCatchfilePath:(NSString *)fileName{
    fileName = [NSString stringWithFormat:@"%@%@", CASH_DATA_SAVE_TEMP_PATH, fileName];
    
    NSArray *paths = nil;
	if ([Utils existsFileNSCachesDirectory:fileName]) {
		paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        
	}else if([Utils existsFileDocumentDirectory:fileName]){
 		paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
    }
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if (appFile == nil || [appFile isKindOfClass:[NSNull class]] ) {
        return nil;
    }else{
        return appFile;
    }

}

#pragma mark LoadingAnimation
+ (void) startDataLoadingAnimation{
	UIWindow *mainWindow;
	mainWindow = [[UIApplication sharedApplication] keyWindow];
	UIButton *maskView = (UIButton *)[mainWindow viewWithTag:DATA_LOADIN_ANI_MASK_VIEW_TAG];
	if (maskView != nil) {
		//すでにアニメーション中のため、リターンする
		return;
	}
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	maskView = [[UIButton alloc] initWithFrame:FULL_MAX_RECT_SIZE];
//    maskView.backgroundColor = [UIColor blueColor];
    [maskView addTarget:self action:@selector(endDataLoadingAnimation) forControlEvents:UIControlEventTouchUpInside];
	maskView.tag = DATA_LOADIN_ANI_MASK_VIEW_TAG;
	[mainWindow addSubview:maskView];
	
	UIImageView *animationBaseView = [[UIImageView alloc] initWithFrame:CGRectMake(135, 215, 50, 50)];
	[animationBaseView setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"loadingBase.png"]]];
	[maskView addSubview:animationBaseView];
	[animationBaseView release];
	
	UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, 230, 20, 20)];
	indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
	[indicator startAnimating];
	[maskView addSubview:indicator];
	[indicator release];
    
    
    [maskView release];
    
	[pool release];
	
}

+ (void) endDataLoadingAnimation{
	//NSLog(@"----endDataLoadingAnimation----");
	UIWindow *mainWindow;
	mainWindow = [[UIApplication sharedApplication] keyWindow];
	UIView *maskView = [mainWindow viewWithTag:DATA_LOADIN_ANI_MASK_VIEW_TAG];
	if (maskView != nil) {
		[maskView removeFromSuperview];
	}
    
     
}

+ (void) startDataLoadingAnimationAndSetTimeInterVal{
	//一定期間（例えば：25秒）内に処理が帰ってこない場合、ローディングをおろす。
	//downLoadingTimer = [NSTimer scheduledTimerWithTimeInterval: LOADING_ANIMATION_MAX_TIME target: self selector: @selector(endDataLoadingAnimationWithAlert) userInfo: nil repeats: NO];
    
	[self startDataLoadingAnimation];
}


+(void)startDataLoadingProgressAnimation:(NSNumber *)dataCount{
	int cnt = [dataCount intValue];
	if (cnt == 0) {
		return;
	}
	//NSLog(@"cnt:%d",cnt);
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	UIWindow *mainWindow;
	mainWindow = [[UIApplication sharedApplication] keyWindow];
	UILabel *baseView = (UILabel *)[mainWindow viewWithTag:DOWNLOAD_PROGRESS_BASE_TAG];
	if (baseView != nil) {
		[baseView removeFromSuperview];
		//NSLog(@"---startDataLoadingProgressAnimation---");
	}
	
	baseView = [[UILabel alloc] initWithFrame:CGRectMake(0, 124, SCREEN_WIDTH, LOADING_PROGRESS_UNIT_HEIGHT)];
	baseView.hidden = YES; // 最初の１個目がダウンロードが終わったら、表示する
	baseView.backgroundColor = [UIColor clearColor];
	baseView.tag = DOWNLOAD_PROGRESS_BASE_TAG;
	[mainWindow addSubview:baseView];
	[baseView release];
    
    UILabel *label = [[[UILabel alloc] initWithFrame:baseView.bounds] autorelease];
    label.backgroundColor = [UIColor blackColor];
    label.alpha = 0.4;
 	[baseView addSubview:label];
   
    
    int initPageCount = MAX_UPLOAD_PHTO_COUNT;
    
    if (cnt > initPageCount) {
        cnt = initPageCount;
    }
    
    int MaxPageCont = MAX_UPLOAD_PHTO_COUNT;
    if (MaxPageCont > initPageCount) {
        MaxPageCont = initPageCount;
    }
    
 	CGFloat unitWidth = 12;
    CGFloat unitImageWidth = 5;
	UILabel *unitLabel = nil;
    CGFloat startPotion = SCREEN_WIDTH-unitWidth*cnt -10;
    //NSLog(@"unitWidth:%f",unitWidth);
    //NSLog(@"startPotion:%f",startPotion);
    
	for (int i=0; i<cnt; i++) {
		//NSLog(@"i:%d",i);
		unitLabel = [[UILabel alloc] initWithFrame:CGRectMake(startPotion + unitWidth*i, 0, unitWidth, LOADING_PROGRESS_UNIT_HEIGHT)];
        unitLabel.backgroundColor = [UIColor clearColor];
		[baseView addSubview:unitLabel];
		[unitLabel release];
		
		UIImageView *unitImageView = [[UIImageView alloc] initWithFrame:CGRectMake((unitWidth-unitImageWidth)/2, 0, unitImageWidth, LOADING_PROGRESS_UNIT_HEIGHT)];
		unitImageView.tag = DOWNLOAD_PROGRESS_UNIT_TAG + i;
        unitImageView.backgroundColor = [UIColor clearColor];
        unitImageView.image = [UIImage imageNamed:[Utils getResourceIconPath:@"upload_bar_noactive.png"]];
		[unitLabel addSubview:unitImageView];
		[unitImageView release];
	}
	
	[pool release];
}

+(void)setDataLoadingProgressStatus:(NSNumber *)dataIndex{
	//NSLog(@"------");
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
	UIWindow *mainWindow;
	mainWindow = [[UIApplication sharedApplication] keyWindow];
	UIView *baseView = [mainWindow viewWithTag:DOWNLOAD_PROGRESS_BASE_TAG];
	if (baseView == nil) {
		return;
	}else {
		baseView.hidden = NO;
	}
    
    //NSLog(@"[dataIndex intValue]:%d",[dataIndex intValue]);
	UIImageView *unitView = (UIImageView *)[baseView viewWithTag:DOWNLOAD_PROGRESS_UNIT_TAG + [dataIndex intValue]];
	if (unitView != nil) {
        unitView.image = [UIImage imageNamed:[Utils getResourceIconPath:@"upload_bar_active.png"]];
	}else {
		//NSLog(@"[dataIndex intValue]:%d",[dataIndex intValue]);
	}
    
	[pool release];
    
}

+(void)endDataLoadingProgressAnimation{
	UIWindow *mainWindow;
	mainWindow = [[UIApplication sharedApplication] keyWindow];
	UIView *baseView = [mainWindow viewWithTag:DOWNLOAD_PROGRESS_BASE_TAG];
	if (baseView != nil) {
		//NSLog(@"---endDataLoadingProgressAnimation---");
		[baseView removeFromSuperview];
	}
	
}


+(NSString *) getSNSImageSourceUrl:(NSMutableDictionary *)dataInfo{
    NSString *imgUrl = @"";
    /*
    NSString *snsType = [dataInfo objectForKey:IMAGE_RESOURCE_TYPE_KEY];
    if (snsType == nil) {
        return imgUrl;
    }else if([snsType isEqualToString:IMAGE_RESOURCE_TYPE_NETWORK_FACEBOOK]){
        imgUrl = [self parseWebDataToString:dataInfo dataKey:FACEBOOK_IMG_SOURCE]; //FaceBook
    }else if([snsType isEqualToString:IMAGE_RESOURCE_TYPE_GOOGLE_IMAGE]){
        imgUrl = [self parseWebDataToString:dataInfo dataKey:GOOGLE_IMG_URL]; //GoogleSearch
    }else if([snsType isEqualToString:IMAGE_RESOURCE_TYPE_YOUTUBE_VIDEO]){
        imgUrl = [self getVideoImgUrl:dataInfo]; //Video Image URL
    }
    */
    
    return imgUrl;
}

+(NSString *) getVideoImgUrl:(NSMutableDictionary *)dataInfo{
    NSDictionary *mediath = [dataInfo objectForKey:@"media$group"];
    NSMutableDictionary *data = [[mediath objectForKey:@"media$thumbnail"] objectAtIndex:0];
    NSString *thumbnail = [Utils parseWebDataToString:data dataKey:@"url"];
    return thumbnail;
}

+ (NSString *) parseWebDataToString:(NSMutableDictionary *)dataInfo dataKey:(NSString *)dataKey{
    NSString *value = [dataInfo objectForKey:dataKey];
    if ( [value isKindOfClass:[ NSNull class]] || value == nil ) {
        value = @"";
    }
	
	return value ;
}

+ (NSNumber *) parseWebDataToNumber:(NSMutableDictionary *)dataInfo dataKey:(NSString *)dataKey{
    NSNumber *value = [dataInfo objectForKey:dataKey];
    if ( [value isKindOfClass:[ NSNull class]] || value == nil ) {
        value = nil;
    }
    
    if ([value isKindOfClass:[ NSString class]]) {
        value = [NSNumber numberWithInt:[value intValue]];
    }
	
	return value ;
}




#pragma mark -
#pragma mark getTimeData With Formate
+ (NSString *) getNowTimeMill{
    //	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setDateFormat:@"ssSSS"];
	NSDate* now_date = [NSDate date];
	NSString *now_dateStr = [formatter stringFromDate:now_date] ;
	[formatter release];
	return now_dateStr;
}

+ (NSString *) getYYYYMM{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
	[formatter setDateFormat:@"YYYYMM"];
	NSDate* now_date = [NSDate date];
	NSString *now_dateStr = [formatter stringFromDate:now_date];
	[formatter release];
	return now_dateStr;
}

+ (NSString *) getNowTime{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
	[formatter setDateFormat:@"YYMMddHHmmssSSS"];
	NSDate* now_date = [NSDate date];
	NSString *now_dateStr = [formatter stringFromDate:now_date];
	[formatter release];
	return now_dateStr;
}

+ (NSString *) getNowTimeWithDate:(NSDate *)inDate{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
	[formatter setDateFormat:@"YYMMddHHmmssSSS"];
	NSString *now_dateStr = [formatter stringFromDate:inDate];
	[formatter release];
	return now_dateStr;
}

+ (NSTimeInterval) getNowTimeInterval{
	NSDate* now_date = [NSDate date];
    //NSLog(@"timeIntervalSince1970:%f",(double)[now_date timeIntervalSince1970]);
	return [now_date timeIntervalSince1970];
}

+ (NSTimeInterval) getNowTimeIntervalWithDate:(NSDate *)inDate{
	return [inDate timeIntervalSince1970];
}

+(NSString *)changeToDateString:(NSTimeInterval)dateTimeInterval{
    //NSTimeInterval pastSeconds = dateTimeInterval / (60 * 60 * 24);
    //NSLog(@"pastSeconds:%f",pastSeconds);
    // NSDate *newDate =  [[NSDate date] dateByAddingTimeInterval:(0 - pastSeconds)];
    NSDate* now_date = [NSDate date];
    NSDate *newDate =  [[NSDate date] initWithTimeInterval:dateTimeInterval-[now_date timeIntervalSince1970] sinceDate:now_date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
	[formatter setDateFormat:@"YYYY/MM/dd"];
	NSString *ret = [formatter stringFromDate:newDate];
    [newDate release];
	[formatter release];
    //NSLog(@"ret:%@",ret);
    
    return ret;
}

+ (NSString *) getDataCreateTime{
    //	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
	[formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	NSDate* now_date = [NSDate date];
	NSString *now_dateStr = [formatter stringFromDate:now_date];
	//NSLog(now_dateStr);
	[formatter release];
	return now_dateStr;
}


#pragma mark Ext StringData
+(void)setStringDataExt:(char *)inData key:(NSString *)key dict:(NSMutableDictionary *)dict{
	if (inData == nil){
		[dict setObject:@"" forKey:key];
	}else {
		[dict setObject:[NSString stringWithUTF8String:inData] forKey:key];
	}
}

+(void)setSqlStatmentExt:(sqlite3_stmt *)stmt data:(NSMutableDictionary *)inData key:(NSString *)key index:(int)index{
    
	NSString *value = [inData objectForKey:key];
	if (value != nil) {
		sqlite3_bind_text(stmt, index, [value UTF8String], -1, NULL);
	}else {
		sqlite3_bind_text(stmt, index, [@"" UTF8String], -1, NULL);
	}
}

+(void)setSqlStatmentExtWithNumber:(sqlite3_stmt *)stmt data:(NSMutableDictionary *)inData key:(NSString *)key index:(int)index{
    
	NSNumber *value = [inData objectForKey:key];
	if (value != nil) {
        sqlite3_bind_int (stmt, index, [value intValue]);
	}else {
        sqlite3_bind_int (stmt, index, 0);
	}
}

+(void)careateCardDataToSave:(NSMutableDictionary *)param contents:(NSString *)contents sync_status:(NSString *)sync_status option:(NSString *)fileName {
    NSDate* now_date = [NSDate date];
    NSNumber *nowTime = [NSNumber numberWithInt:[Utils getNowTimeIntervalWithDate:now_date]];
    NSMutableDictionary *cardDataDicn = [[NSMutableDictionary alloc] initWithCapacity:1];
    [cardDataDicn setObject:nowTime forKey:cardId_key];
    [cardDataDicn setObject:[param objectForKey:sheetId_Key] forKey:sheetId_Key];
    [cardDataDicn setObject:[param objectForKey:categoryId_Key] forKey:categoryId_Key];
    [cardDataDicn setObject:[NSNumber numberWithInt:0] forKey:cardDispNo_key];
    [cardDataDicn setObject:contents forKey:cardContents_key];
    [cardDataDicn setObject:fileName forKey:begining_key];//リソースからこのカードを探すための紐付け
    //NSLog(@"Utis fileName: %@",fileName);
    [cardDataDicn setObject:[NSNumber numberWithInt:0] forKey:verNo_key];
    [cardDataDicn setObject:nowTime forKey:TIME];
    [cardDataDicn setObject:@"" forKey:resourceId_key];//画像サーバーにアップ後、もらうもの
    [cardDataDicn setObject:sync_status forKey:SYNC_STATUS];
   
    [[DataRegistry getInstance] saveCardData:cardDataDicn];
    [cardDataDicn release];

}

+(NSString *)htmlEscape:(NSString *)inStr{
    //NSLog(@"before:%@",inStr);
    inStr = [inStr gtm_stringByUnescapingFromHTML];
    inStr = [inStr stringByReplacingMatchesOfPattern:@"<resource.*?>" withString:RESOURCE_SPLIT_EXT];
//    inStr = [inStr stringByReplacingOccurrencesOfString:@"<span " withString:RESOURCE_SPLIT_EXT_SPAN_START];
//    inStr = [inStr stringByReplacingOccurrencesOfString:@"</span>" withString:RESOURCE_SPLIT_EXT_SPAN_END];
   //NSLog(@"%@",inStr);
    
    inStr = [inStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    inStr = [inStr stringByReplacingOccurrencesOfString:@"\r" withString:@""];
//    inStr = [inStr stringByReplacingMatchesOfPattern:@"<p>" withString:@"\n"];
//    inStr = [inStr stringByReplacingMatchesOfPattern:@"</p>" withString:@"\n"];
//    inStr = [inStr stringByReplacingMatchesOfPattern:@"<br>" withString:@"\n"];
//    inStr = [inStr stringByReplacingMatchesOfPattern:@"</br>" withString:@"\n"];
//    inStr = [inStr stringByReplacingMatchesOfPattern:@"</div>" withString:@"\n"];
//    inStr = [inStr stringByReplacingMatchesOfPattern:@"<.*?>" withString:@""];
    inStr = [inStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //inStr = [inStr gtm_stringByEscapingForHTML];
    //NSLog(@"after:%@",inStr);
   
    return inStr;
}

+(void)getResourceList:(NSString *)inStr output:(NSMutableArray *)outputList{
    inStr = [inStr gtm_stringByUnescapingFromHTML];
    NSArray *list = [inStr componentsSeparatedByString:RESOURCE_SPLIT];
    for (int i=0; i<[list count]; i++) {
        NSString *part = [list objectAtIndex:i];
        NSRange range = [part rangeOfString:@"\""];

       if (range.location == 0) {
            part = [part stringByReplacingOccurrencesOfString:@" " withString:@""];
           NSRange rangeEnd = [part rangeOfString:@"\"/>"];
           NSString *fn = [part substringToIndex:rangeEnd.location ];
           fn = [fn stringByReplacingOccurrencesOfString:@"\"" withString:@""];
           [outputList addObject:fn];

        }
     }
    
 }

+(void)changeCardInfoDetailList:(NSString *)contents output:(NSMutableArray *)outputList{
    //Resource List
    NSMutableArray *resourceList = [[NSMutableArray alloc] init];
    [Utils getResourceList:contents output:resourceList];
    
    //TextList
    contents = [Utils htmlEscape:contents];
    NSArray *textList = [contents componentsSeparatedByString:RESOURCE_SPLIT_EXT];
    
    for (int i=0; i<[textList count]; i++) {
        NSString *text = [textList objectAtIndex:i];
        text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (![text isEqualToString:@""]) {
            //WebLinkの場合、linkを追加する
            NSRange range = [text rangeOfString:@"<span class=\"vulb_link\""];
            //NSLog(@"%@",text);
            if (range.location != NSNotFound) {
                NSArray *array_1 = [text componentsSeparatedByString:RESOURCE_WEB_LINK_SPEC_CHAR];
                for (NSString *strr in array_1) {
                    NSRange vulb_link_range = [strr rangeOfString:@"window.open"];
                    if (vulb_link_range.location != NSNotFound) {
                        //WebLink
                        NSRange r_start = [strr rangeOfString:@"('"];
                        NSRange r_end = [strr rangeOfString:@"')"];
                        NSRange r_delta ;
                        r_delta.location = r_start.location+r_start.length;
                        r_delta.length = r_end.location - r_start.location -2;
                        NSString *textValue = [strr substringWithRange:r_delta];
                        
                        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
                        [data setObject:DB_RESOURCE_TYPE_WEB forKey:resourceType_key];
                        [data setObject:textValue forKey:begining_key];
                        [outputList addObject:data];
                        [data release];
                        
                        //other Text
                        NSRange range_2 = [strr rangeOfString:@"</span>"];
                         if (range_2.location != NSNotFound) {
                             NSString *textValueOther = [strr substringFromIndex:range_2.location+range_2.length];
                             NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
                             [data setObject:DB_RESOURCE_TYPE_BEGINING forKey:resourceType_key];
                             [data setObject:textValueOther forKey:begining_key];
                             [outputList addObject:data];
                             [data release];

                         }
                    }else{
                        if (![strr isEqualToString:@""]) {
                            NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
                            [data setObject:DB_RESOURCE_TYPE_BEGINING forKey:resourceType_key];
                            [data setObject:strr forKey:begining_key];
                            [outputList addObject:data];
                            [data release];

                        }
                        
                        
                    }
                    
                 }
 
            }else{
                //text
                NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
                //NSLog(@"%@",text);
                //text = [text stringByReplacingMatchesOfPattern:@"<.*?>" withString:@""];
                [data setObject:DB_RESOURCE_TYPE_BEGINING forKey:resourceType_key];
                [data setObject:text forKey:begining_key];
                [outputList addObject:data];
                [data release];
                
            }

        }
        
        if (i<[resourceList count]) {
            NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
            [data setObject:[resourceList objectAtIndex:i] forKey:resourceId_key];
            [outputList addObject:data];
            [data release];
         }
    }
    
    [resourceList release];
 
}

+(void) setCommonButtonProperti:(UIButton *)button rect:(CGRect)rect title:(NSString *)title{
    [button setBackgroundImage:[self getWhiteButtonNormalImage:rect] forState:UIControlStateNormal];
	[button setBackgroundImage:[self getWhiteButtonSelectedImage:rect] forState:UIControlStateHighlighted];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:NAVICATION_BAR_BUTTON_TEXT_INIT_SIZE];
    [button setTitle:MultiLangString(title) forState:UIControlStateNormal];
    [button setTitle:MultiLangString(title) forState:UIControlStateHighlighted];
    UIColor *color1 = HEXCOLOR(0x3c3c3c);
    UIColor *color2 = HEXCOLOR(0x3c3c3c);
    [button setTitleColor: color1 forState:UIControlStateNormal];
    [button setTitleColor: color2 forState:UIControlStateHighlighted];
}

+(UIImage *)getWhiteButtonNormalImage:(CGRect)rect{
	ImagePartitionView *extImageView = [[[ImagePartitionView alloc] initWithFrame:rect] autorelease];
	
	NSArray *parts = [NSArray arrayWithObjects:
					  [Utils getResourceIconPath:@"bt_white_normal_left.png"],
					  [Utils getResourceIconPath:@"bt_white_normal_center.png"],
					  [Utils getResourceIconPath: @"bt_white_normal_right.png"],
					  nil];
	
	extImageView.images = parts;
	//[extImageView setNeedsDisplay];
	return  [extImageView getScreenShotImage];
}

+(UIImage *)getWhiteButtonSelectedImage:(CGRect)rect{
	ImagePartitionView *extImageView = [[[ImagePartitionView alloc] initWithFrame:rect] autorelease];
	NSArray *parts = [NSArray arrayWithObjects:
					  [Utils getResourceIconPath:@"bt_white_touch_left.png"],
					  [Utils getResourceIconPath:@"bt_white_touch_center.png"],
					  [Utils getResourceIconPath: @"bt_white_touch_right.png"],
					  nil];
	extImageView.images = parts;
    
	return  [extImageView getScreenShotImage];
}

+(UIImage *)getBlueButtonNormalImage:(CGRect)rect{
	ImagePartitionView *extImageView = [[[ImagePartitionView alloc] initWithFrame:rect] autorelease];
	
	NSArray *parts = [NSArray arrayWithObjects:
					  [Utils getResourceIconPath:@"bt_blue_normal_left.png"],
					  [Utils getResourceIconPath:@"bt_blue_normal_center.png"],
					  [Utils getResourceIconPath: @"bt_blue_normal_right.png"],
					  nil];
	
	extImageView.images = parts;
	//[extImageView setNeedsDisplay];
	return  [extImageView getScreenShotImage];
}

+(UIImage *)getBlueButtonSelectedImage:(CGRect)rect{
	ImagePartitionView *extImageView = [[[ImagePartitionView alloc] initWithFrame:rect] autorelease];
	NSArray *parts = [NSArray arrayWithObjects:
					  [Utils getResourceIconPath:@"bt_blue_touch_left.png"],
					  [Utils getResourceIconPath:@"bt_blue_touch_center.png"],
					  [Utils getResourceIconPath: @"bt_blue_touch_right.png"],
					  nil];
	extImageView.images = parts;
    
	return  [extImageView getScreenShotImage];
}

+(BOOL)isNewCardInSheet:(NSMutableDictionary *)inData{
    BOOL ret = FALSE;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
	NSString *sheetListStr = [ud stringForKey:sheetId_Key];
    if (sheetListStr != nil) {
        NSMutableArray *oldSheetList = [sheetListStr JSONValue];
        for (int i=0; i<[oldSheetList count]; i++) {
            NSMutableDictionary *oldSheetData = [oldSheetList objectAtIndex:i];
            NSNumber *oldSheetId = [Utils parseWebDataToNumber:oldSheetData dataKey:sheetId_Key];
            NSNumber *newSheetId = [Utils parseWebDataToNumber:inData dataKey:sheetId_Key];
            if ([oldSheetId intValue] == [newSheetId intValue]) {
                NSNumber *oldCardCount = [Utils parseWebDataToNumber:oldSheetData dataKey:totalCard_key];
                NSNumber *newCardCount = [Utils parseWebDataToNumber:inData dataKey:totalCard_key];
                if ([newCardCount intValue] > [oldCardCount intValue]) {
                    ret = TRUE;
                    break;
                }
            }
        }
    }

    
    return ret;    
}

+ (UIColor *) getColor:(int)idx{
    UIColor *ret = nil;
    
	NSMutableArray *list = [BusinessDataCtrlManager getColorMasterList];
    for (int i=0; i<[list count]; i++) {
        NSMutableDictionary *data = [list objectAtIndex:i];
        NSNumber *colorIdx = [data objectForKey:colorIdx_key];
        if (idx == [colorIdx intValue]) {
            NSNumber *rgbR = [data objectForKey:rgbaR];
            NSNumber *rgbG = [data objectForKey:rgbaG];
            NSNumber *rgbB = [data objectForKey:rgbaB];

            ret = [UIColor colorWithRed:[rgbR floatValue]/255.0 green:[rgbG floatValue]/255.0 blue:[rgbB floatValue]/255.0 alpha:1.0];
            break;
        }
    }
    
    return ret;
}

+ (UIColor *) getBaseTextColor{
    return [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
}

+(void)addShadowLine:(UIView *)baseView
{
    //shadowLine
    UIImage *shadowLine = [UIImage imageNamed:[Utils getResourceIconPath:@"header_shadow.png"]];
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, TOOL_BAR_HIGHT, shadowLine.size.width, shadowLine.size.height)];
    imageView.image = shadowLine;
    imageView.backgroundColor = [UIColor clearColor];
    [baseView addSubview:imageView];
}

+(void)setUserIconWith:(int)userId imageView:(UIView *)imageView
{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:[NSNumber numberWithInt:userId] forKey:userId_key];
    [paramDic setObject:[param JSONRepresentation] forKey:PARAMS];
    [param release];
    [[HttpClientUtils sharedClient] getServerDataSetToView:METHOD_PERSON_IMG parameter:paramDic view:imageView delegate:nil option:[NSNumber numberWithInt:userId]];
    
    [paramDic release];

}


+ (NSMutableArray *)splitParameterString:(NSString *)inParameterString
{
    NSMutableArray *ret = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray *pList = [inParameterString componentsSeparatedByString:@"&"];
    for (int i=0; i<[pList count]; i++) {
        NSString *pUnit = [pList objectAtIndex:i];
        NSRange range = [pUnit rangeOfString:@"="];
        if (range.location != NSNotFound) {
            NSArray *p = [pUnit componentsSeparatedByString:@"="];
            NSDictionary *dic = @{
                                  DIC_KEY  : [p objectAtIndex:0],
                                  DIC_VALUE: [p objectAtIndex:1],
                                  };
            [ret addObject:dic];
        }
    }
    
    return  ret;

}

+(NSString *) getVideoPlayHtml:(NSString *)videoUrl{
    NSString *htmlString = @"<html><head>\
    <meta name = \"viewport\" content = \"initial-scale = 1.0, user-scalable = no, width = 320\"/></head>\
    <body style=\"margin-top:0px;margin-left:0px\">\
    <div><object width=\"320\" height=\"460\">\
    <param name=\"movie\" value=\"%@\"></param>\
    <param name=\"wmode\" value=\"transparent\"></param>\
    <embed src=\"%@\"\
    type=\"application/x-shockwave-flash\" wmode=\"transparent\" width=\"320\" height=\"460\"></embed>\
    </object></div></body></html>";
    
    NSString* html = [NSString stringWithFormat:htmlString, videoUrl, videoUrl ];
    return html;
}

+(void)createColorMetaDatas:(NSMutableArray *)globalColorMasterList{
    int idx=0;
    NSMutableDictionary *data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [data setObject:[NSNumber numberWithInt:80] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:80] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:80] forKey:rgbaB];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [data setObject:[NSNumber numberWithInt:36] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:101] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:217] forKey:rgbaB];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [data setObject:[NSNumber numberWithInt:5] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:150] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:215] forKey:rgbaB];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [data setObject:[NSNumber numberWithInt:20] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:190] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:90] forKey:rgbaB];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [data setObject:[NSNumber numberWithInt:90] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:120] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:45] forKey:rgbaB];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:200] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:55] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:0] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:145] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:100] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:30] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:225] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:90] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:205] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:130] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:60] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:190] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:90] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:40] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:0] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:240] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:115] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:0] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:220] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:220] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:30] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
}



+(UIImage *)getFileThumbnailIconImage:(NSString *)fileName{
    UIImage *ret = nil;
    NSRange fileExtRange = [fileName rangeOfString:@"." options:NSBackwardsSearch];
    if (fileExtRange.location != NSNotFound) {
        NSString *fileExt = [fileName substringFromIndex :fileExtRange.location + fileExtRange.length ];
        NSString *iconFileName = @"";
        if ([[fileExt lowercaseString] isEqualToString:@"jpg"]
            || [[fileExt lowercaseString] isEqualToString:@"jpeg"]) {
            iconFileName = @"icon_Thumbnail_jpg.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"gif"]) {
            iconFileName = @"icon_Thumbnail_gif.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"png"]) {
            iconFileName = @"icon_Thumbnail_png.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"pdf"]) {
            iconFileName = @"icon_Thumbnail_pdf.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"txt"]) {
            iconFileName = @"icon_Thumbnail_txt.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"doc"]) {
            iconFileName = @"icon_Thumbnail_doc.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"xls"]) {
            iconFileName = @"icon_Thumbnail_xls.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"ppt"]) {
            iconFileName = @"icon_Thumbnail_ppt.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"psd"]) {
            iconFileName = @"icon_Thumbnail_psd.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"ai"]) {
            iconFileName = @"icon_Thumbnail_ai.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"zip"]) {
            iconFileName = @"icon_Thumbnail_zip.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"mp3"]) {
            iconFileName = @"icon_Thumbnail_mp3.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"m4a"]) {
            iconFileName = @"icon_Thumbnail_m4a.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"mpg"]
                  || [[fileExt lowercaseString] isEqualToString:@"mpeg"]) {
            iconFileName = @"icon_Thumbnail_mpeg.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"mp4"]) {
            iconFileName = @"icon_Thumbnail_mp4.png";
       }else{
            iconFileName = @"icon_Thumbnail_file.png";            
        }
        return [UIImage imageNamed:[Utils getResourceIconPath:iconFileName]];
               
    }  else{
        return [UIImage imageNamed:[Utils getResourceIconPath:@"icon_Thumbnail_file.png"]];
    }
    return ret;    
}

+(UIImage *)getFileIconImage:(NSString *)fileName{
    UIImage *ret = nil;
    NSRange fileExtRange = [fileName rangeOfString:@"." options:NSBackwardsSearch];
    if (fileExtRange.location != NSNotFound) {
        NSString *fileExt = [fileName substringFromIndex :fileExtRange.location + fileExtRange.length ];
        NSString *iconFileName = @"";
        if ([[fileExt lowercaseString] isEqualToString:@"jpg"]
            || [[fileExt lowercaseString] isEqualToString:@"jpeg"]) {
            iconFileName = @"icon_file_jpg.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"gif"]) {
            iconFileName = @"icon_file_gif.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"png"]) {
            iconFileName = @"icon_file_png.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"pdf"]) {
            iconFileName = @"icon_file_pdf.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"txt"]) {
            iconFileName = @"icon_file_txt.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"doc"]) {
            iconFileName = @"icon_file_doc.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"xls"]) {
            iconFileName = @"icon_file_xls.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"ppt"]) {
            iconFileName = @"icon_file_ppt.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"psd"]) {
            iconFileName = @"icon_file_psd.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"ai"]) {
            iconFileName = @"icon_file_ai.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"zip"]) {
            iconFileName = @"icon_file_zip.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"mp3"]) {
            iconFileName = @"icon_file_mp3.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"m4a"]) {
            iconFileName = @"icon_file_m4a.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"mpg"]
                  || [[fileExt lowercaseString] isEqualToString:@"mpeg"]) {
            iconFileName = @"icon_file_mpeg.png";
        }else if ([[fileExt lowercaseString] isEqualToString:@"mp4"]) {
            iconFileName = @"icon_file_mp4.png";
        }else{
            iconFileName = @"icon_file_file.png";
        }
        return [UIImage imageNamed:[Utils getResourceIconPath:iconFileName]];
        
    }  else{
        return [UIImage imageNamed:[Utils getResourceIconPath:@"icon_file_file.png"]];
    }
    return ret;
}

+(BOOL)isShowFileName:(NSString *)fileName{
    BOOL ret = FALSE;
    NSRange fileExtRange = [fileName rangeOfString:@"." options:NSBackwardsSearch];
    if (fileExtRange.location != NSNotFound) {
        NSString *fileExt = [fileName substringFromIndex :fileExtRange.location + fileExtRange.length ];
        if ([[fileExt lowercaseString] isEqualToString:@"jpg"]
            || [[fileExt lowercaseString] isEqualToString:@"jpeg"]) {
            ret = FALSE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"gif"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"png"]) {
            ret = FALSE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"pdf"]) {
            ret = FALSE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"txt"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"doc"]
                  || [[fileExt lowercaseString] isEqualToString:@"docx"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"xls"]
            || [[fileExt lowercaseString] isEqualToString:@"xlsx"]) {
           ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"ppt"]
                  || [[fileExt lowercaseString] isEqualToString:@"pptx"]) {
           ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"psd"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"ai"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"zip"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"mp3"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"m4a"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"mpg"]
                  || [[fileExt lowercaseString] isEqualToString:@"mpeg"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"mp4"]) {
            ret = TRUE;
        }else{
            ret = TRUE;
        }
        return ret;
        
    }  else{
        return ret;
    }
    return ret;

}


+(BOOL)isOfficeFile:(NSString *)fileName{
    BOOL ret = FALSE;
    NSRange fileExtRange = [fileName rangeOfString:@"." options:NSBackwardsSearch];
    if (fileExtRange.location != NSNotFound) {
        NSString *fileExt = [fileName substringFromIndex :fileExtRange.location + fileExtRange.length ];
        if ([[fileExt lowercaseString] isEqualToString:@"txt"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"doc"]
                  || [[fileExt lowercaseString] isEqualToString:@"docx"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"xls"]
                  || [[fileExt lowercaseString] isEqualToString:@"xlsx"]) {
            ret = TRUE;
        }else if ([[fileExt lowercaseString] isEqualToString:@"ppt"]
                  || [[fileExt lowercaseString] isEqualToString:@"pptx"]) {
            ret = TRUE;
        }else{
            ret = FALSE;
        }
        return ret;
        
    }  else{
        return ret;
    }
    return ret;
    
}

+(NSString *) getSystemLangCode{
    NSArray *langs = [NSLocale preferredLanguages];
    NSString *langID = [langs objectAtIndex:0];
    NSString *ret;
    if ([[langID lowercaseString] isEqualToString:@"ja"]) {
        ret = langID;
    }else if([[langID lowercaseString] isEqualToString:@"en"]){
        ret = langID;
    }else{
        ret = @"en";
    }
    return ret;
}


+(UIImage *)getEditTextToolAreaBaseButtonBaseImage:(CGRect)inRect{
    NSArray *parts = [NSArray arrayWithObjects:
					  [Utils getResourceIconPath:@"bss_tex_tool_toolarea_base.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_toolarea_base.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_toolarea_base.png"],
					  nil];
    
	ImagePartitionView *extImageView = [[[ImagePartitionView alloc] initWithFrame:inRect] autorelease];
	extImageView.images = parts;
	//[extImageView setNeedsDisplay];
	return  [extImageView getScreenShotImage];
}

+(UIImage *)getEditTextToolAreaBaseImage:(CGRect)inRect{
    NSArray *parts = [NSArray arrayWithObjects:
					  [Utils getResourceIconPath:@"bss_tex_tool_base.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_base.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_base.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_base.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_base.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_base.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_base.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_base.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_base.png"],
					  nil];
    
	ImagePartitionView *extImageView = [[[ImagePartitionView alloc] initWithFrame:inRect] autorelease];
	extImageView.images = parts;
	return  [extImageView getScreenShotImage];
}

+(UIImage *)getEditTextToolAreaBaseShadowImage:(CGRect)inRect{
    NSArray *parts = [NSArray arrayWithObjects:
					  [Utils getResourceIconPath:@"bss_tex_tool_toolarea_base_dropshadow.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_toolarea_base_dropshadow.png"],
					  [Utils getResourceIconPath:@"bss_tex_tool_toolarea_base_dropshadow.png"],
					  nil];
    
	ImagePartitionView *extImageView = [[[ImagePartitionView alloc] initWithFrame:inRect] autorelease];
	extImageView.images = parts;
	//[extImageView setNeedsDisplay];
	return  [extImageView getScreenShotImage];
}

+ (NSString *) getColorTypeString:(int)idx{
	if (idx == 0) {
		return @"#ffffff";
	}else if (idx == 1) {
		return @"#a0a0a0";
	}else if (idx == 2) {
		return @"#505050";
	}else if (idx == 3) {
		return @"#000000";
	}else if (idx == 4) {
		return @"#f04646";
	}else if (idx == 5) {
		return @"#ffaaf0";
	}else if (idx == 6) {
		return @"#3232be";
	}else if (idx == 7) {
		return @"#1e96ff";
	}else if (idx == 8) {
		return @"#b450f0";
	}else if (idx == 9) {
		return @"#50d250";
	}else if (idx == 10) {
		return @"#fff064";
	}else if (idx == 11) {
		return @"#784628";
	}else {
		return @"#000000";
	}
	
}

@end
