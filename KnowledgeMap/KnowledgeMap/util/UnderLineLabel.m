//
//  UnderLineLabel.m
//  BukurouSS
//
//  Created by k-kin on 11/11/27.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import "UnderLineLabel.h"


@implementation UnderLineLabel


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
	NSArray *separate;
	separate = [self.text componentsSeparatedByString:@"\n"];
	CGFloat textHeight = [self.text sizeWithFont:self.font].height;
	//NSLog(@"self.text.Width:%f",[self.text sizeWithFont:self.font].width);
	int i_sub = 0;
	for (int i=0; i<[separate count]; i++) {
		NSString *str = [separate objectAtIndex:i];
		CGFloat textWidth = 0;
		CGFloat charWidth = 0;
		NSString *lineTextString = @"";
		for (int j=0; j<[str length]; j++) {
			NSString *charStr = [str substringWithRange:NSMakeRange(j, 1)];
			charWidth = [charStr sizeWithFont:self.font].width;
			if (textWidth + charWidth >= SCREEN_WIDTH) {
				CGFloat oneLintTextWidth =  [lineTextString sizeWithFont:self.font].width;
				//NSLog(@"oneLintTextWidth:%f",oneLintTextWidth);
				//NSLog(@"textWidth:%f",textWidth);
				if (textWidth > oneLintTextWidth) {
					textWidth = oneLintTextWidth;
				}
				[self drawLine:textWidth height:textHeight*(i+i_sub+1)];
				textWidth = 0;
				lineTextString = @"";
				i_sub++;			
			}
			textWidth += charWidth;
			lineTextString = [lineTextString stringByAppendingString:charStr];

		}
		
		CGFloat oneLintTextWidth =  [lineTextString sizeWithFont:self.font].width;
		//NSLog(@"oneLintTextWidth:%f",oneLintTextWidth);
		//NSLog(@"textWidth:%f",textWidth);
		if (textWidth > oneLintTextWidth) {
			textWidth = oneLintTextWidth;
		}
		[self drawLine:textWidth height:textHeight*(i+i_sub+1)];
	}
	
	[super drawRect:rect];
	
}

-(void)drawLine:(CGFloat)textWidth height:(CGFloat)textHeight{
	CGFloat descender = self.font.descender;
	//NSLog(@"textHeight:%f",textHeight);
	//NSLog(@"descender:%f",descender);
	CGFloat startX = 0;
	CGFloat startxDelta = 2.0;
	if (self.textAlignment == UITextAlignmentLeft) {
		
	}else if (self.textAlignment == UITextAlignmentCenter) {
		startX = (SCREEN_WIDTH - textWidth) / 2.0 - startxDelta;
	}else if (self.textAlignment == UITextAlignmentRight) {
		startX = SCREEN_WIDTH  - textWidth - startxDelta ;
	}
	CGFloat endX = startX + textWidth;
	CGFloat startY = 0;
	//NSLog(@"self.font.familyName:%@",self.font.familyName); 
	if ([self.font.familyName isEqualToString:@"Zapfino"]) {
		startY = textHeight + descender/2;		
	}else if ([self.font.familyName isEqualToString:@"Oriya Sangam MN"]) {
		startY = textHeight - descender;				
	}else{
		startY = textHeight - descender*2;		
	}
	
	CGFloat endY = startY;
		
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context,self.textColor.CGColor);
	CGContextMoveToPoint(context, startX, startY);
	CGContextAddLineToPoint(context, endX, endY);
	CGContextStrokePath(context);
	
}


- (void)dealloc {
    [super dealloc];
}


@end
