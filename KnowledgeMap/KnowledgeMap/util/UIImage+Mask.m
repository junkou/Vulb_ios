//
//  UIImage+Mask.m
//  MemogramTest
//
//  Created by 雅彦 竹本 on 12/06/11.
//  Copyright (c) 2012年 小麦 株式会社. All rights reserved.
//

#import "UIImage+Mask.h"

@implementation UIImage (Mask)

- (UIImage *)trimImageInRect:(CGRect)rect
{
    BOOL retina = NO; // [UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0;
    
    CGRect scaledRect;
    if (retina == YES) {
        CGFloat scale = [[UIScreen mainScreen] scale];
        scaledRect = CGRectMake(rect.origin.x * scale,
                                rect.origin.y * scale,
                                rect.size.width * scale,
                                rect.size.height * scale);
    } else {
        scaledRect = rect;
    }
    
    scaledRect.origin.x = (self.size.width-scaledRect.size.width)/2;
    scaledRect.origin.y = (self.size.height-scaledRect.size.height)/2;
   
    CGImageRef selfImageRef = CGImageCreateWithImageInRect([self CGImage], scaledRect);
    UIImage *trimedImage = [UIImage imageWithCGImage:selfImageRef];
    CGImageRelease(selfImageRef);

    
//    CGImageRef maskRef = maskImage.CGImage;
//    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
//                                        CGImageGetHeight(maskRef),
//                                        CGImageGetBitsPerComponent(maskRef),
//                                        CGImageGetBitsPerPixel(maskRef),
//                                        CGImageGetBytesPerRow(maskRef),
//                                        CGImageGetDataProvider(maskRef), NULL, false);
//    
//    CGImageRef masked = CGImageCreateWithMask(selfImageRef, mask);
    
//    UIImage *maskedImage = nil;
//    if (retina == YES) {
//        maskedImage = [UIImage imageWithCGImage:masked scale:[[UIScreen mainScreen] scale] orientation:self.imageOrientation];
//    } else {
//        maskedImage = [UIImage imageWithCGImage:masked];
//    }
    
//    CGImageRelease(mask);
//    CGImageRelease(masked);
    
    return trimedImage;
}

@end
