//
//  UnderLineLabel.h
//  BukurouSS
//
//  Created by k-kin on 11/11/27.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDefine.h"

@interface UnderLineLabel : UILabel {

}

-(void)drawLine:(CGFloat)textWidth height:(CGFloat)textHeight;

@end
