//
//  DataRegistry.m
//  BookMaker
//
//  Created by k-kin on 10/05/06.
//  Copyright 2010 iccesoft. All rights reserved.
//

#import "DataRegistry.h"


@implementation DataRegistry

DataRegistry *dataRegistry;

+(DataRegistry *)getInstance{
	if (dataRegistry == nil) {
		dataRegistry = [[DataRegistry alloc] init] ;		
	}
	return dataRegistry;
}

-(void)LockDatas{
	[lockData lock];
}

-(void)unLockDatas{
	[lockData unlock];
}

- (NSString *)dataFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:kFilename];
}

#pragma mark init
- (DataRegistry *)init {	
    if (sqlite3_open([[self dataFilePath] UTF8String], &database)
        != SQLITE_OK) {
        sqlite3_close(database);
        NSAssert(0, @"Failed to open database"); 
    }
	
	lockData = [[NSLock alloc] init];
	
	//HTML保存テーブル作成
    char *errorMsg;

	//会員マスタテーブル作成
	NSString *createUserTblSQL = @"CREATE TABLE IF NOT EXISTS t_user_v103 (user_id int, user_name VARCHAR(48), user_pass VARCHAR(256), mail_address VARCHAR(256), token_id VARCHAR(256), session_id VARCHAR(256), local VARCHAR(256), person_img VARCHAR(256), card_size int, sheet_id int, category_id int,  CONSTRAINT pk_t_user_v103 PRIMARY KEY (user_id));";
	if (sqlite3_exec (database, [createUserTblSQL  UTF8String],NULL, NULL, &errorMsg) != SQLITE_OK) {
        sqlite3_close(database); 
        NSAssert1(0, @"Error creating t_user_v103: %s", errorMsg);
    }
	
	
	//Cardテーブル作成
    NSString *createCardDataTblSQL = @"CREATE TABLE IF NOT EXISTS t_card (card_id int, sheet_id int, category_id int, card_disp_no int, card_contents text,  begining VARCHAR(256),   ver_no int, time int, resource_id VARCHAR(256), sync_status VARCHAR(1), CONSTRAINT pk_t_card PRIMARY KEY (begining));";
    if (sqlite3_exec (database, [createCardDataTblSQL  UTF8String],NULL, NULL, &errorMsg) != SQLITE_OK) {
        sqlite3_close(database);
        NSAssert1(0, @"Error creating createCardDataTblSQL: %s", errorMsg);
    }
		
	//画像データ保存テーブル作成: 画像を一時的に保存TMP
    NSString *createCaptureImageTmpTblSQL = @"CREATE TABLE IF NOT EXISTS t_resource ( file_name VARCHAR(48), resource_type VARCHAR(48), illustration_id VARCHAR(256), longitute VARCHAR(48),latitute VARCHAR(48), zoom int, url_address VARCHAR(24), time int, sheet_id int, CONSTRAINT pk_t_resource PRIMARY KEY (file_name));";
    if (sqlite3_exec (database, [createCaptureImageTmpTblSQL  UTF8String],NULL, NULL, &errorMsg) != SQLITE_OK) {
        sqlite3_close(database);
        NSAssert1(0, @"Error creating t_resource table: %s", errorMsg);
    }
	
	
	UIApplication *app = [UIApplication sharedApplication];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillTerminate:) name:UIApplicationWillTerminateNotification object:app];
	
	return self;
}


#pragma mark 同期対象データのステータス初期化
-(void)reSetSyncStatus{
	
}


#pragma mark 会員情報関連
//ユーザーは１件のみ限定とする
- (int)loadUserMasterData:(NSMutableDictionary *)userInfo{
	NSString *query;
	query = @"SELECT * FROM t_user_v103 ";	
	int count = 0;
    sqlite3_stmt *statement;	
    if (sqlite3_prepare_v2( database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
		
        if (sqlite3_step(statement) == SQLITE_ROW) {
			[self changeUserMstDataToDicon:statement outData:userInfo];
			count++;
		}
        sqlite3_finalize(statement);
    }
    //NSLog([userInfo JSONRepresentation]);
	return count;
}

- (int)saveUserDataWithUserInfo:(NSMutableDictionary *)userInfo{
	[lockData lock];
	//	char *errorMsg;
	char *update = "INSERT OR REPLACE INTO t_user_v103 (user_id, user_name, user_pass, mail_address,   token_id, session_id, local, person_img, card_size, sheet_id, category_id) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
	sqlite3_stmt *stmt;
	//	NSLog([NSString stringWithFormat:@"BookTitleData.book_code=%@",bookTitleData.book_code ]);
	int ret = 1;
	if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
		[self setUserMstDataToStatement:stmt userInfo:userInfo ];
	}
	if (sqlite3_step(stmt) != SQLITE_DONE){
		NSAssert1(0, @"Error insert table saveUserDataWithUserInfo", @"");
		ret = 0;
	}
	sqlite3_finalize(stmt);
	[lockData unlock];
	return ret;
	
}

- (int)updateSheetIdAndCategoryId:(NSMutableDictionary *)inUserData {
	[lockData lock];
	//	char *errorMsg;
    //NSLog([inUserData JSONRepresentation]);
	char *update = "UPDATE t_user_v103 SET sheet_id=?, category_id=? WHERE mail_address=?";
	sqlite3_stmt *stmt;
	int ret = 1;
	if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
        sqlite3_bind_int (stmt, 1, [[inUserData objectForKey:sheetId_Key] intValue]);
        sqlite3_bind_int (stmt, 2, [[inUserData objectForKey:categoryId_Key] intValue]);
		sqlite3_bind_text(stmt, 3, [[inUserData objectForKey:mailAddress_key] UTF8String], -1, NULL);
	}
	if (sqlite3_step(stmt) != SQLITE_DONE){
		NSAssert1(0, @"Error updating table updateActiveFlag", @"");
		ret = 0;
	}
	sqlite3_finalize(stmt);
	[lockData unlock];
	
	return ret;
}

- (int)deleteUserMasterData{
	[lockData lock];
	//char *errorMsg;
	char *update = "DELETE FROM t_user_v103 ";
	sqlite3_stmt *stmt;
	int ret = 1;
	if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
        
	}
	if (sqlite3_step(stmt) != SQLITE_DONE){
		//	NSAssert1(0, @"Error delate table deleteBookTitle: %s", errorMsg);
		ret = 0;
	}
	
	sqlite3_finalize(stmt);
	[lockData unlock];
	
	return ret;
}

- (void)changeUserMstDataToDicon:(sqlite3_stmt  *)statement outData:(NSMutableDictionary *)outData{
	int cnt = 0;
	int user_id			=(int)sqlite3_column_int(statement, cnt);
	[outData setObject:[NSNumber numberWithInt:user_id] forKey:userId_key];
	cnt++;
	char *user_name		= (char *)sqlite3_column_text(statement, cnt);
	[Utils setStringDataExt:user_name key:userName_Key dict:outData];
	cnt++;
	char *user_pass				= (char *)sqlite3_column_text(statement, cnt);
	[Utils setStringDataExt:user_pass key:password_key dict:outData];
	cnt++;
	char *mail_address			= (char *)sqlite3_column_text(statement, cnt);
	[Utils setStringDataExt:mail_address key:mailAddress_key dict:outData];
	cnt++;
	char *token_id		= (char *)sqlite3_column_text(statement, cnt);
	[Utils setStringDataExt:token_id key:tokenId_Key dict:outData];
	cnt++;
	char *session_id			= (char *)sqlite3_column_text(statement, cnt);
	[Utils setStringDataExt:session_id key:sessionId_Key dict:outData];
	cnt++;
	char *local				= (char *)sqlite3_column_text(statement, cnt);
	[Utils setStringDataExt:local key:local_Key dict:outData];
	cnt++;
	char *person_img	= (char *)sqlite3_column_text(statement, cnt);
	[Utils setStringDataExt:person_img key:personImg_Key dict:outData];
	cnt++;
	int  card_size				= (int)sqlite3_column_int(statement, cnt);
	[outData setObject:[NSNumber numberWithInt:card_size] forKey:cardSize_Key];
	cnt++;
	int  sheet_id				= (int)sqlite3_column_int(statement, cnt);
	[outData setObject:[NSNumber numberWithInt:sheet_id] forKey:sheetId_Key];
	cnt++;
	int  category_id				= (int)sqlite3_column_int(statement, cnt);
	[outData setObject:[NSNumber numberWithInt:category_id] forKey:categoryId_Key];


}

- (void)setUserMstDataToStatement:(sqlite3_stmt  *)stmt userInfo:(NSMutableDictionary *)userInfo{
	//	NSLog(@"bookTitleData.max_mokuji_page_number=%@", bookTitleData.max_mokuji_page_number);
	int count = 0;
	count++;
	[Utils setSqlStatmentExtWithNumber:stmt data:userInfo key:userId_key index:count];
	count++;
	[Utils setSqlStatmentExt:stmt data:userInfo key:userName_Key index:count];
	count++;
	[Utils setSqlStatmentExt:stmt data:userInfo key:password_key index:count];
	count++;
	[Utils setSqlStatmentExt:stmt data:userInfo key:mailAddress_key index:count];
	count++;
	[Utils setSqlStatmentExt:stmt data:userInfo key:tokenId_Key index:count];
	count++;
	[Utils setSqlStatmentExt:stmt data:userInfo key:sessionId_Key index:count];
	count++;
	[Utils setSqlStatmentExt:stmt data:userInfo key:local_Key index:count];
	count++;
	[Utils setSqlStatmentExt:stmt data:userInfo key:personImg_Key index:count];
	count++;
	[Utils setSqlStatmentExtWithNumber:stmt data:userInfo key:cardSize_Key index:count];
	count++;
	[Utils setSqlStatmentExtWithNumber:stmt data:userInfo key:sheetId_Key index:count];
	count++;
	[Utils setSqlStatmentExtWithNumber:stmt data:userInfo key:categoryId_Key index:count];

}

#pragma mark Cardデータ関連
- (int)selectCardData:(NSMutableArray *)outDataArray {
	[lockData lock];
	NSString *query = @"SELECT * FROM t_card WHERE  sync_status = '1' ORDER BY sheet_id, begining ";
    sqlite3_stmt *statement;
	int count = 0;
	if (sqlite3_prepare_v2( database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
		
        while (sqlite3_step(statement) == SQLITE_ROW) {
			NSMutableDictionary *dataDicon = [[NSMutableDictionary alloc] init];
			[self changeCardDataToDicon:statement cardData:dataDicon];
            //NSLog([dataDicon JSONRepresentation]);
			[outDataArray addObject:dataDicon];
			[dataDicon release];
			count++;
			
		}
        sqlite3_finalize(statement);
    }
	[lockData unlock];
	//NSLog([outDataArray JSONRepresentation]);
	return count;
}

- (int)saveCardData:(NSMutableDictionary *)inCardData{
	[lockData lock];
	//	char *errorMsg;
    //NSLog(@"============saveCardData================");
	char *update = "INSERT OR REPLACE INTO t_card (card_id, sheet_id, category_id, card_disp_no, card_contents, begining, ver_no, time, resource_id, sync_status) VALUES (?,?,?,?,?,?,?,?,?,?);";
	sqlite3_stmt *stmt;
	int ret = 1;
	if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
		//		NSLog(dataBookPageTpl.image_url);
		[self setBookCardDataToStatement:stmt cardData:inCardData];
	}
	if (sqlite3_step(stmt) != SQLITE_DONE){
		NSAssert1(0, @"Error insert table insertPageData:", @"");
		ret = 0;
	}
	sqlite3_finalize(stmt);
	[lockData unlock];
	return ret;
}

- (int)updateResourceId:(NSMutableDictionary *)inCardData {
	[lockData lock];
	//	char *errorMsg;
    //NSLog([inCardData JSONRepresentation]);
    //NSLog(@"begining_key:%@",[inCardData objectForKey:begining_key]);
	char *update = "UPDATE t_card SET resource_id=?, card_contents=?, sync_status='1' WHERE begining=?";
	sqlite3_stmt *stmt;
	int ret = 1;
	if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
		sqlite3_bind_text(stmt, 1, [[inCardData objectForKey:resourceId_key] UTF8String], -1, NULL);
		sqlite3_bind_text(stmt, 2, [[inCardData objectForKey:cardContents_key] UTF8String], -1, NULL);
		sqlite3_bind_text(stmt, 3, [[inCardData objectForKey:begining_key] UTF8String], -1, NULL);
	}
	if (sqlite3_step(stmt) != SQLITE_DONE){
		NSAssert1(0, @"Error updating table updateActiveFlag", @"");
		ret = 0;
	}
	sqlite3_finalize(stmt);
	[lockData unlock];
	
	return ret;
}

- (int)deleteCardData:(NSString *)inBegining{
	[lockData lock];
	//char *errorMsg;
	char *update = "DELETE FROM t_card WHERE  begining=? ";
	sqlite3_stmt *stmt;
	int ret = 1;
	if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
  		sqlite3_bind_text(stmt, 1, [inBegining UTF8String], -1, NULL);

	}
	if (sqlite3_step(stmt) != SQLITE_DONE){
		//	NSAssert1(0, @"Error delate table deleteBookTitle: %s", errorMsg);
		ret = 0;
	}
	
	sqlite3_finalize(stmt);
	[lockData unlock];
	
	return ret;
}

- (int)deleteAllCardData{
	[lockData lock];
	//char *errorMsg;
	char *update = "DELETE FROM t_card ";
	sqlite3_stmt *stmt;
	int ret = 1;
	if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
        
	}
	if (sqlite3_step(stmt) != SQLITE_DONE){
		//	NSAssert1(0, @"Error delate table deleteBookTitle: %s", errorMsg);
		ret = 0;
	}
	
	sqlite3_finalize(stmt);
	[lockData unlock];
	
	return ret;
}

- (void)changeCardDataToDicon:(sqlite3_stmt  *)statement cardData:(NSMutableDictionary *)dataDicon{
	int cnt = 0;
	int card_id		= (int)sqlite3_column_int(statement, cnt);
	cnt++;
	int sheet_id		= (int)sqlite3_column_int(statement, cnt);
	cnt++;
	int category_id  = (int)sqlite3_column_int(statement, cnt);
	cnt++;
	int  card_disp_no			= (int)sqlite3_column_int(statement, cnt);
	cnt++;
	char *card_contents		= (char *)sqlite3_column_text(statement, cnt);
	cnt++;
	char *begining		= (char *)sqlite3_column_text(statement, cnt);
	cnt++;
	int ver_no			= (int)sqlite3_column_int(statement, cnt);
	cnt++;
	int time			= (int)sqlite3_column_int(statement, cnt);
	cnt++;
	char *resource_id		= (char *)sqlite3_column_text(statement, cnt);
	cnt++;
	char *snyc_status		= (char *)sqlite3_column_text(statement, cnt);
	
	[dataDicon setObject:[NSNumber numberWithInt:card_id] forKey:cardId_key];
	[dataDicon setObject:[NSNumber numberWithInt:sheet_id] forKey:sheetId_Key];
	[dataDicon setObject:[NSNumber numberWithInt:category_id] forKey:categoryId_Key];
	[dataDicon setObject:[NSNumber numberWithInt:card_disp_no] forKey:cardDispNo_key];
	[dataDicon setObject:[NSString stringWithUTF8String:card_contents] forKey:cardContents_key];
	[dataDicon setObject:[NSString stringWithUTF8String:begining] forKey:begining_key];
	[dataDicon setObject:[NSNumber numberWithInt:ver_no] forKey:verNo_key];
	[dataDicon setObject:[NSNumber numberWithInt:time] forKey:time_key];
	[dataDicon setObject:[NSString stringWithUTF8String:resource_id] forKey:resourceId_key];
 	[dataDicon setObject:[NSString stringWithUTF8String:snyc_status] forKey:SYNC_STATUS];
   //NSLog(@"thumbnailImgUrl==%@",[NSString stringWithUTF8String:thumbnailImgUrl]);
}

- (void)setBookCardDataToStatement:(sqlite3_stmt  *)stmt cardData:(NSMutableDictionary *)inData{
    //NSLog([pageData JSONRepresentation]);
	int cnt = 1;
	sqlite3_bind_int(stmt, cnt, [[inData objectForKey:cardId_key] intValue]);
	cnt++;
	sqlite3_bind_int(stmt, cnt, [[inData objectForKey:sheetId_Key] intValue]);
	cnt++;
	sqlite3_bind_int(stmt, cnt, [[inData objectForKey:categoryId_Key] intValue]);
	cnt++;
	sqlite3_bind_int (stmt, cnt, [[inData objectForKey:cardDispNo_key] intValue]);
	cnt++;
	sqlite3_bind_text(stmt, cnt, [[inData objectForKey:cardContents_key] UTF8String], -1, NULL);
	cnt++;
	sqlite3_bind_text(stmt, cnt, [[inData objectForKey:begining_key] UTF8String], -1, NULL);
	cnt++;
	sqlite3_bind_int (stmt, cnt, [[inData objectForKey:verNo_key] intValue]);
	cnt++;
	sqlite3_bind_int (stmt, cnt, [[inData objectForKey:time_key] intValue]);
	cnt++;
	sqlite3_bind_text(stmt, cnt, [[inData objectForKey:resourceId_key] UTF8String], -1, NULL);
	cnt++;
	sqlite3_bind_text(stmt, cnt, [[inData objectForKey:SYNC_STATUS] UTF8String], -1, NULL);
	//NSLog(@"[pageData objectForKey:THUMBNAIL_IMG_URL]:%@", [pageData objectForKey:THUMBNAIL_IMG_URL] );
	
}


#pragma mark 画像関連
-(int)loadImageResourceData:(NSMutableArray *)dataResult{
	NSString *query = @"SELECT * FROM t_resource ORDER BY sheet_id, file_name ";
    sqlite3_stmt *statement;
	int count = 0;
	//最初の１件のみ取得する
	[lockData lock];
    if (sqlite3_prepare_v2( database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
		
        while (sqlite3_step(statement) == SQLITE_ROW) {
			NSMutableDictionary *imageData = [[NSMutableDictionary alloc] init];			
			[self changeImageResourceDataToDicon:statement result:imageData];
			[dataResult addObject:imageData];
			[imageData release];
			count++;
		}
        sqlite3_finalize(statement);
    }	
	[lockData unlock];
    
	return count;	
}


-(void)saveImageResourceData:(NSMutableDictionary *)dataDicon{
	[lockData lock];
	char *update = "INSERT OR REPLACE INTO t_resource (file_name, resource_type, illustration_id, longitute, latitute, zoom, url_address,time, sheet_id) VALUES (?,?,?,'','',0,'',?,?);";
	sqlite3_stmt *stmt;
	int cnt = 1;
	if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
		sqlite3_bind_text(stmt, cnt, [[dataDicon objectForKey:fileName_key] UTF8String], -1, NULL);
		cnt++;
		sqlite3_bind_text(stmt, cnt, [[dataDicon objectForKey:resourceType_key] UTF8String], -1, NULL);
		cnt++;
		sqlite3_bind_text(stmt, cnt, [[dataDicon objectForKey:illustrationId_key] UTF8String], -1, NULL);
		cnt++;
 		sqlite3_bind_int(stmt, cnt, [[dataDicon objectForKey:time_key] intValue]);
		cnt++;
 		sqlite3_bind_int(stmt, cnt, [[dataDicon objectForKey:sheetId_Key] intValue]);
       //NSLog(@"[dataDicon objectForKey:BOOK_CODE]:%@",[dataDicon objectForKey:BOOK_CODE]);
	}
	if (sqlite3_step(stmt) != SQLITE_DONE){
		NSAssert1(0, @"Error insert table t_resource", @"");
	}
	sqlite3_finalize(stmt);
	[lockData unlock];
}



-(void)deleteImageResourceData:(NSString *)fileName{
	[lockData lock];
	char *update = "DELETE FROM t_resource WHERE file_name=? ";
	sqlite3_stmt *stmt;
	if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
		sqlite3_bind_text(stmt, 1, [fileName UTF8String], -1, NULL);
	}
	if (sqlite3_step(stmt) != SQLITE_DONE){
 		NSAssert1(0, @"Error delate table deleteCaptureImage", @"");
	}	
	sqlite3_finalize(stmt);
	[lockData unlock];
}

-(void)deleteAllImageResourceData{
	[lockData lock];
	char *update = "DELETE FROM t_resource  ";
	sqlite3_stmt *stmt;
	if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
	}
	if (sqlite3_step(stmt) != SQLITE_DONE){
 		NSAssert1(0, @"Error delate table deleteCaptureImage", @"");
	}
	sqlite3_finalize(stmt);
	[lockData unlock];
}

- (void)changeImageResourceDataToDicon:(sqlite3_stmt  *)statement result:(NSMutableDictionary *)dataDicon{
	int cnt = 0;
	char *file_name		= (char *)sqlite3_column_text(statement, cnt);
	cnt++;
	char *resource_type		= (char *)sqlite3_column_text(statement, cnt);
	cnt++;
	char *illustration_id		= (char *)sqlite3_column_text(statement, cnt);
	cnt++;
	char *longitute		= (char *)sqlite3_column_text(statement, cnt);
	cnt++;
	char *latitute		= (char *)sqlite3_column_text(statement, cnt);
	cnt++;
	int  zoom		= (int)sqlite3_column_int(statement, cnt);
	cnt++;
	char *url_address		= (char *)sqlite3_column_text(statement, cnt);
	cnt++;
	int  time		= (int)sqlite3_column_int(statement, cnt);
	cnt++;
	int  sheet_id		= (int)sqlite3_column_int(statement, cnt);
	
	//NSLog(@"[NSString stringWithUTF8String:pageCode]:%@",[NSString stringWithUTF8String:pageCode]);
	//NSLog(@"[NSNumber numberWithInt:time] :%d",[NSNumber numberWithInt:time] );
	
    
	[dataDicon setObject:[NSString stringWithUTF8String:file_name] forKey:fileName_key];
	[dataDicon setObject:[NSString stringWithUTF8String:resource_type] forKey:resourceType_key];
	[Utils setStringDataExt:illustration_id key:illustrationId_key dict:dataDicon];
	[Utils setStringDataExt:longitute key:longitute_key dict:dataDicon];
	[Utils setStringDataExt:latitute key:latitute_key dict:dataDicon];
	[dataDicon setObject:[NSNumber numberWithInt:zoom] forKey:zoom_key];
	[Utils setStringDataExt:url_address key:urlAddress_key dict:dataDicon];
	[dataDicon setObject:[NSNumber numberWithInt:time] forKey:time_key];
	[dataDicon setObject:[NSNumber numberWithInt:sheet_id] forKey:sheetId_Key];
	//	NSLog(@"img_url==%@",pageData.image_url);
}


- (void)applicationWillTerminate:(NSNotification *)notification {
    sqlite3_close(database);
	[lockData release];
}


@end
