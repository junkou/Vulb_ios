//
//  LoginManager.m
//  BukurouSS
//
//  Created by k-kin on 11/11/13.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import "BusinessDataCtrlManager.h"
#import "UploadToWebServerSYNController.h"


@implementation BusinessDataCtrlManager

@synthesize delegate;
@synthesize methodName;
@synthesize resourceFileName;
@synthesize currentSheetId;

BusinessDataCtrlManager     *globalBusinessDataCtrlManager;
UserInfo                    *globalUserInfo;
NSMutableArray              *globalSheetList;
NSMutableArray              *globalCategoryList;
NSMutableDictionary         *globalSheetIdAndCategoryList;
NSMutableArray              *globalColorMasterList;
NSMutableArray              *globalCardList;
NSMutableArray              *globalCategoryStatisticsList;


/*  メールから直接アプリを起動した時に使う   */
NSMutableDictionary           *globalOption;

+(BusinessDataCtrlManager *)getInstance{
	if (globalBusinessDataCtrlManager == nil) {
		globalBusinessDataCtrlManager = [[BusinessDataCtrlManager alloc] init] ;
        
		globalUserInfo = [[UserInfo alloc] init] ;
        [globalUserInfo clearData];
        
        globalSheetList = [[NSMutableArray alloc] init];
        globalCategoryList = [[NSMutableArray alloc] init];
        globalSheetIdAndCategoryList = [[NSMutableDictionary alloc] init];
        globalCardList = [[NSMutableArray alloc] init];
        globalColorMasterList = [[NSMutableArray alloc] init];
        globalOption = [[NSMutableDictionary alloc] init];
        globalCategoryStatisticsList = [[NSMutableArray alloc] init];
        [self setColorDatas];
	}
	
	return globalBusinessDataCtrlManager;
}

-(NSMutableDictionary *)getGlobalOptionData{
    
    return globalOption;
}


+(UserInfo *)getUserInfo{

    return globalUserInfo;
}

+(NSMutableArray *)getColorMasterList{
    
    return globalColorMasterList;
}

+(NSMutableArray *)getSheetList{
    
    return globalSheetList;
}

+(NSMutableArray *)getCategoryList{
    
    return globalCategoryList;
}

+(NSMutableDictionary *)getSheetIdAndCategoryList{
    
    return globalSheetIdAndCategoryList;
}

+(NSMutableArray *)getCardList{
    
    return globalCardList;
}

+(NSMutableArray *)getGlobalCategoryStatisticsList{
    
    return globalCategoryStatisticsList;
}

+(NSMutableDictionary *)getSheetInfo:(NSNumber *)sheetId{
    for (int i=0; i < [globalSheetList count]; i++) {
        NSMutableDictionary *sheetData = [globalSheetList objectAtIndex:i];
        NSNumber *sheetId2 = [sheetData objectForKey:sheetId_Key];
        if ([sheetId intValue] == [sheetId2 intValue]) {
            return sheetData;
        }
    }
    return  nil;

}

+(NSMutableDictionary *)getCategoryInfo:(NSNumber *)categoryId{
    
    for (int i=0; i < [globalCategoryList count]; i++) {
        NSMutableDictionary *categoryData = [globalCategoryList objectAtIndex:i];
        NSNumber *categoryId2 = [categoryData objectForKey:categoryId_Key];
        if ([categoryId intValue] == [categoryId2 intValue]) {
            return categoryData;
        }
    }
    return  nil;
}

+(NSMutableDictionary *)getCategoryInfo:(NSNumber *)categoryId sheetId:(NSNumber *)sheetId{
    NSMutableArray *categoryList = [globalSheetIdAndCategoryList objectForKey:sheetId];
    for (int i=0; i < [categoryList count]; i++) {
        NSMutableDictionary *categoryData = [categoryList objectAtIndex:i];
        NSNumber *categoryId2 = [categoryData objectForKey:categoryId_Key];
        if ([categoryId intValue] == [categoryId2 intValue]) {
            return categoryData;
        }
    }
    return  nil;
}


-(void)logOut{
    //データをサーバーへのアップ処理をStopする
    [[[UploadToWebServerSYNController getUploadServerInstance] initData:self] stopUpLoad];
    
    [[DataRegistry getInstance]  deleteAllImageResourceData];
    
    [[DataRegistry getInstance]  deleteAllCardData];

    [[DataRegistry getInstance]  deleteUserMasterData];

    [globalUserInfo clearData];
    
    [globalCategoryList removeAllObjects];
 
    [globalSheetIdAndCategoryList removeAllObjects];
    
    [globalSheetList removeAllObjects];

    [globalOption removeAllObjects];

}


/**
 * CreateUser
 */
-(void)createUser:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_CREATE_USER;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}


/**
 * ローカルデータベースにある、データをロードする。
 */
- (BOOL)loadUserInfoDataWithDelegate:(id)inDelegate{
    self.delegate = inDelegate;
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [[DataRegistry getInstance] loadUserMasterData:userInfo];
    
    BOOL ret = TRUE;
    if ([userInfo count] > 0) {
        //すでにログイン済みの場合、サーバーに再ログインさせ、最新情報を取得する
        [globalUserInfo toChangeObjectValues:userInfo];
        
        if (globalUserInfo.mailAddress != nil && ![globalUserInfo.mailAddress isEqualToString:@""]) {
            [self loginWithUserId:globalUserInfo.mailAddress pass:globalUserInfo.pass];
        }else if(globalUserInfo.tokenId != nil && ![globalUserInfo.tokenId isEqualToString:@""]){
            [self loginWithTokenId:globalUserInfo.tokenId delegate:inDelegate];
        }else{
            ret = FALSE;
        }

    }else{
        ret = FALSE;
    }
    
    [userInfo release];

    return ret;
}

/**
 * ログイン
 */
-(void)loginWithUserIdAndDelegete:(NSString *)inMailAddress pass:(NSString *)inPass delegate:(id)inDelegate{
    self.delegate = inDelegate;
    [self loginWithUserId:inMailAddress pass:inPass];
}

-(void)comfirmUser:(NSString *)confirmKey userId:(NSString *)userId delegate:(id)inDelegate{
    self.delegate =inDelegate;
    self.methodName = METHOD_NAME_CONFIRM_USER;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    //    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:confirmKey forKey:confirmKey_Key];
    [param setObject:userId forKey:userId_key];
    [paramDic setObject:param forKey:PARAMS];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [param release];
    [paramDic release];

}

-(void)loginWithTokenId:(NSString *)tokenId delegate:(id)inDelegate{
    
    globalUserInfo.tokenId = tokenId;
    
    self.delegate =inDelegate;
    self.methodName = METHOD_NAME_LOGIN_FAST;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    //    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:tokenId forKey:tokenId_Key];
    [paramDic setObject:param forKey:PARAMS];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [param release];
    [paramDic release];
    
	
}

-(void)loginWithJSessionId:(NSString *)sessionId delegate:(id)inDelegate{
    
    self.delegate =inDelegate;
    self.methodName = METHOD_REFERENCE_CONFIG;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    //    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:sessionId forKey:JSESSIONID];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [paramDic setObject:param forKey:PARAMS];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [param release];
    [paramDic release];
    
	
}



-(void)loginWithUserId:(NSString *)inMailAddress pass:(NSString *)inPass{

    globalUserInfo.mailAddress = inMailAddress;
    globalUserInfo.pass = inPass;
    
    self.methodName = METHOD_NAME_LOGIN;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
//    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:inPass forKey:password_key];
    [param setObject:inMailAddress forKey:mailAddress_key];
    [paramDic setObject:param forKey:PARAMS];

    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];

    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [param release];
    [paramDic release];
  
	
}

-(void)loginWithFacebook:(id)inDelegate{

    self.delegate =inDelegate;
    self.methodName = METHOD_NAME_LOGIN_FACEBOOK;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    //    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:redirectURL_Sucess forKey:redirectSucess_key];
    [param setObject:redirectURL_Failure forKey:redirectFailure_key];
    [param setObject:@"login,,email_import,token" forKey:@"action"];
    [paramDic setObject:param forKey:PARAMS];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [param release];
    [paramDic release];
    
	
}


-(void)getCardList:(id)inDelegate startIdx:(int )idx {
    [self getCardListWithCategoryId:globalUserInfo.lastCategoryId delegate:inDelegate startIdx:idx];
}

-(void)getCardListWithCategoryId:(NSNumber *)inCategoryId delegate:(id)inDelegate startIdx:(int )startIdx{
    self.delegate = inDelegate;
   
    self.methodName = METHOD_CARD_LIST;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    NSMutableArray *categories = [[NSMutableArray alloc] init];
    NSMutableDictionary *category = [[NSMutableDictionary alloc] init];
    [category setObject:inCategoryId forKey:categoryId_Key];
    [category setObject:[NSNumber numberWithInt:startIdx] forKey:START];
    [category setObject:[NSNumber numberWithInt:CARD_ROW_COUNT] forKey:COUNT];
    [categories addObject:category];
    [category release];
    [param setObject:categories forKey:CATEGORYIES];
    [categories release];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}

-(void)getCardInfoWithCardId:(NSNumber *)inCardId delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_REFERENCE_CARD;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:inCardId forKey:cardId_key];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}

-(void)getResourceDownloadKey:(NSNumber *)inResourceId delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_GET_DOWNLOAD_KEY;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:inResourceId forKey:resourceId_key];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:NO serverUrl:url delegate:self];
    [paramDic release];
    
	
}


-(void)getResourceFileDownload:(NSString *)inUrl delegate:(id)inDelegate filename:(NSString *)fileName{
    self.delegate = inDelegate;
    
    self.methodName = METHOD_GET_DOWNLOAD_RESOURCE_FILE;
    self.resourceFileName =fileName;
        
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,inUrl];
    [[WebServerDownLoadController getInstance] sendOtherApplicationSerivice:url delegate:self];
    
	
}

-(void)getResourceFileDownloadCancel {
    [[WebServerDownLoadController getInstance] cancelOptarion];
    
}


-(void)goodWithCardId:(NSNumber *)inCardId delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_CARD_VALUATION;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:inCardId forKey:cardId_key];
    [param setObject:CARD_VALUATION_GOOD forKey:valuation_key];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}

-(void)getCommentList:(NSNumber *)inCardId delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_CARD_COMMENT_LIST;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:inCardId forKey:cardId_key];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}

-(void)writeCommentData:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_CARD_NEW_COMMENT;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}

-(void)sendViolationData:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_VIOLATION;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}

-(void)getSheetListUnconfirm:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_SHEET_LIST_UNCONFIRM;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}

-(void)groupRegist:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_GROUP_REGIST;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}

-(void)reloadSheetList:(id)inDelegate {
    self.delegate = inDelegate;

    //getSheetlist And CategoryList
    self.methodName = METHOD_SHEET_LIST;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];

}


-(void)createCategory:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_CREATE_CATEGORY;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}

-(void)createNewSheet:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_CREATE_SHEET;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}

-(void)inviteWithUserid:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_INVITE_WITH_USERID;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
}


-(void)inviteWithMailaddress:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_INVITE_WITH_MAIL;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
	
}



-(void)getInvitedList:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_INVITED_LIST;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
}

-(void)getFbFreindsList:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_FB_GET_FRIENDS;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:[NSNumber numberWithInt:0] forKey:@"offset"];
    [param setObject:[NSNumber numberWithInt:50] forKey:@"limit"];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
}

-(void)getSheetMemberList:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_SHEET_MEMBER_LIST;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
}

-(void)getReferenceSheetSetting:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_REFERENCE_SHEET_SETTING;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
}

-(void)updateReferenceSheetSetting:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_UPDATE_SHEET_SETTING;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
}

-(void)getGategoryStatistics:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    [self getGategoryStatistics:param];
}

-(void)getGategoryStatistics:(NSMutableDictionary *)param{
    
    self.methodName = METHOD_CATEGORY_STATISTICS;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
}


-(void)startEditCard:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_START_EDIT_CARD;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
}

-(void)updateCard:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_UPDATE_CARD;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
}

-(void)endEditCard:(NSMutableDictionary *)param delegate:(id)inDelegate {
    self.delegate = inDelegate;
    
    self.methodName = METHOD_END_EDIT_CARD;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    
    //NSLog([paramDic JSONRepresentation]);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
}

#pragma mark Delegate
-(void)connectNetWorkError{
    if ([self.delegate respondsToSelector:@selector(loginFaild)]) {
 		[self.delegate loginFaild];
	}
  
    if ([self.delegate respondsToSelector:@selector(connectNetWorkError)]) {
 		[self.delegate connectNetWorkError];
	}

    //NetWrokError
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:MultiLangString(@"Information")
                          message:[Utils MultiLangStringInfomation:@"I0020"]
                          delegate:nil
                          cancelButtonTitle:MultiLangString(@"Yes")
                          otherButtonTitles: nil];
    [alert show];
    [alert release];

 
    
}

-(void)clearDatas{
    [[WebServerDownLoadController getInstance] clearDatas];
}

// 処理データを受信したとき
- (void)recieveOprationData:(NSString *)data{
    //NSLog(@"%@",data);
	NSMutableDictionary *recieveData = [data JSONValue];
    
    NSMutableDictionary *result = [recieveData objectForKey:RESULT_KEY];
    
    NSString *resultStatus = [result objectForKey:RESULT_STATUS_KEY];
    
    if (resultStatus != nil && [resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {
        //正常
        NSMutableDictionary *data = [recieveData objectForKey:RESULT_DATA_VALUE_KEY];
        //NSLog([data JSONRepresentation]);
       
        if ([self.methodName isEqualToString:METHOD_NAME_LOGIN]
            || [self.methodName isEqualToString:METHOD_NAME_LOGIN_FAST]
            || [self.methodName isEqualToString:METHOD_REFERENCE_CONFIG]
            
            ) {
            
             //Login
            [globalUserInfo toChangeObjectValues:data];
 

            //UserInfoデーターをDBに保存する
            NSMutableDictionary *param2 = [[NSMutableDictionary alloc] init];
            [globalUserInfo toCreateDictionaryValues:param2];
            //NSLog([param2 JSONRepresentation]);
            
            /*  URLから開いた場合、Last SheetId, categoryIdを再設定する   */
            NSMutableDictionary *option = [[BusinessDataCtrlManager getInstance] getGlobalOptionData] ;
            
            NSString *sheetId = [option objectForKey:@"sheet_id"];
            if (sheetId != nil) {
                [BusinessDataCtrlManager getUserInfo].lastSheetId = [NSNumber numberWithInt:[sheetId intValue]];
            }
            
            NSString *categoryId = [option objectForKey:@"category_id"];
            if (categoryId != nil) {
                [BusinessDataCtrlManager getUserInfo].lastCategoryId = [NSNumber numberWithInt:[categoryId intValue]];
            }


            [[DataRegistry getInstance] saveUserDataWithUserInfo:param2];
            [param2 release];

            
            //getSheetlist And CategoryList
            self.methodName = METHOD_SHEET_LIST;
            
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
            [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
            NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
            [paramDic setObject:param forKey:PARAMS];
            [param release];
            
            NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
            [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
            [paramDic release];
           

        }else if ([self.methodName isEqualToString:METHOD_NAME_CONFIRM_USER]) {
           	UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"Information",@"Information")
                                  message:NSLocalizedString(@"I0032",@"I0032")
                                  delegate:self
                                  cancelButtonTitle:MultiLangString(@"Yes")
                                  otherButtonTitles: nil];
            [alert show];
            [alert release];


        }else if ([self.methodName isEqualToString:METHOD_NAME_LOGIN_FACEBOOK]) {
            
            [self.delegate facebookUrlSucessed:data];
            
        }else if ([self.methodName isEqualToString:METHOD_CREATE_USER]) {
            
            [self.delegate createUserSucessed];

        }else if ([self.methodName isEqualToString:METHOD_SHEET_LIST_UNCONFIRM]) {

            [self.delegate sheetListUnconfirmSucessed:[data objectForKey:SHEETS]];

        }else if ([self.methodName isEqualToString:METHOD_GROUP_REGIST]) {
            
            [self.delegate groupRegistSucessed];
            
        }else if ([self.methodName isEqualToString:METHOD_SHEET_LIST]) {
             //Sheetlist getSheetList
            NSMutableArray *sheetList = [data objectForKey:SHEETS];
            //NSLog([sheetList JSONRepresentation]);
        
            [globalSheetList removeAllObjects];
            
            [globalSheetList addObjectsFromArray:sheetList];

            BOOL flag = FALSE;
            for (int i=0; i < [globalSheetList count]; i++) {
                NSMutableDictionary *sheetData = [globalSheetList objectAtIndex:i];
                NSNumber *sheetId = [sheetData objectForKey:sheetId_Key];
                if ([globalUserInfo.lastSheetId intValue] == [sheetId intValue]) {
                    flag = TRUE;
                }                
            }
            
            //招待メールのリンクから開いとき、すでに参加済みのシートの場合、
            NSMutableDictionary *option = [[BusinessDataCtrlManager getInstance] getGlobalOptionData] ;
            
            NSString *invitation_sheet_id = [option objectForKey:@"invitation_sheet_id"];
            if (invitation_sheet_id != nil) {
                for (int i=0; i < [globalSheetList count]; i++) {
                    NSMutableDictionary *sheetData = [globalSheetList objectAtIndex:i];
                    NSNumber *sheetId = [sheetData objectForKey:sheetId_Key];
                    if ([invitation_sheet_id intValue] == [sheetId intValue]) {
                        [BusinessDataCtrlManager getUserInfo].lastSheetId = [NSNumber numberWithInt:[invitation_sheet_id intValue]];
                    }
                }

            }
            
            //前回開いたシートIdがない場合、最初のシートIdを設定する
            if (!flag) {
                if ([globalSheetList count] > 0) {
                    NSMutableDictionary *sheetData = [globalSheetList objectAtIndex:0];
                    globalUserInfo.lastSheetId = [sheetData objectForKey:sheetId_Key];
                }
            }
            
            
            self.methodName = METHOD_CATEGORY_LIST;
            
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
            [paramDic setObject:globalUserInfo.sessionId forKey:JSESSIONID];
            NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
            [param setObject:globalUserInfo.lastSheetId forKey:sheetId_Key];
            [paramDic setObject:param forKey:PARAMS];
            [param release];
            
            NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,METHOD_CATEGORY_LIST];
            [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
            [paramDic release];
            


         }else if ([self.methodName isEqualToString:METHOD_CATEGORY_LIST]) {
             //Cardデータをクリアする
             [globalCardList removeAllObjects];

             //NSLog([data JSONRepresentation]);
             [globalCategoryList removeAllObjects];
             
             NSMutableArray *categoryList = [data objectForKey:UNCATEGORYIZED];
             [globalCategoryList addObjectsFromArray:categoryList];
             
             categoryList = [data objectForKey:CATEGORYIZED0];
             [globalCategoryList addObjectsFromArray:categoryList];
             
             categoryList = [data objectForKey:CATEGORYIZED1];
             [globalCategoryList addObjectsFromArray:categoryList];
            

             [globalSheetIdAndCategoryList removeObjectForKey:globalUserInfo.lastSheetId];
             [globalSheetIdAndCategoryList setObject:globalCategoryList forKey:globalUserInfo.lastSheetId];
             
             BOOL flag = FALSE;
             for (int i=0; i < [globalCategoryList count]; i++) {
                 NSMutableDictionary *categoryData = [globalCategoryList objectAtIndex:i];
                 NSNumber *categoryId = [categoryData objectForKey:categoryId_Key];
                 if ([globalUserInfo.lastCategoryId intValue] == [categoryId intValue]) {
                     flag = TRUE;
                 }
             }
             if (!flag) {
                 if ([globalCategoryList count] > 0) {
                     NSMutableDictionary *categoryData = [globalCategoryList objectAtIndex:0];
                     globalUserInfo.lastCategoryId = [categoryData objectForKey:categoryId_Key];

                 }
              }
 
             //各カテゴリーの新規、更新ステータスを取得する
             NSMutableArray *cateoryIds = [[[NSMutableArray alloc] init] autorelease];
             for (NSMutableDictionary *categoryInfo in globalCategoryList ) {
                 [cateoryIds addObject:[categoryInfo objectForKey:categoryId_Key]];
             }
             
             NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
             [param setObject:cateoryIds forKey:@"categoryIds"];
             [self getGategoryStatistics:param];

         }else if ([self.methodName isEqualToString:METHOD_CATEGORY_STATISTICS]) {
             
             NSMutableArray *categoryStatisticsList = [data objectForKey:CATEGORYIES];
             [globalCategoryStatisticsList addObjectsFromArray:categoryStatisticsList];

             //データをサーバーへのアップ処理を起動する
             [[[UploadToWebServerSYNController getUploadServerInstance] initData:self] startUpload];
             
             [self.delegate CategoryListSucessed];

         }else if ([self.methodName isEqualToString:METHOD_CARD_LIST]) {

             NSMutableDictionary *categoies = [data objectForKey:CATEGORYIES];
             NSMutableArray *cardList = [categoies objectForKey:[globalUserInfo.lastCategoryId stringValue]];
             [globalCardList addObjectsFromArray:cardList];

             [self.delegate cardListSucessed];
             
         }else if ([self.methodName isEqualToString:METHOD_REFERENCE_CARD]) {
             
             [self.delegate cardInfoSucessed:data];
             
         }else if ([self.methodName isEqualToString:METHOD_CARD_VALUATION]) {
             [self.delegate goodCardSucessed];

         }else if ([self.methodName isEqualToString:METHOD_CARD_COMMENT_LIST]) {
             NSArray *comments = [data objectForKey:COMMENTS];

             [self.delegate commentListSucessed:comments];

         }else if ([self.methodName isEqualToString:METHOD_CARD_NEW_COMMENT]) {
             
             [self.delegate newCommnetSucessed];

         }else if ([self.methodName isEqualToString:METHOD_VIOLATION]) {
             
             [self.delegate sendViolationSucessed];

         }else if ([self.methodName isEqualToString:METHOD_GET_DOWNLOAD_KEY]) {
             
             [self.delegate resourceInfoSucessed:data];
         }else if ([self.methodName isEqualToString:METHOD_CREATE_CATEGORY]) {
             
             NSNumber *categoryId = [data objectForKey:categoryId_Key];
             globalUserInfo.lastCategoryId = categoryId;
             
             [self.delegate createCategorySucessed];

         }else if ([self.methodName isEqualToString:METHOD_INVITED_LIST]) {
             
             [self.delegate invitedListSucessed:[data objectForKey:@"users"]];
            
         }else if ([self.methodName isEqualToString:METHOD_FB_GET_FRIENDS]) {
             
             [self.delegate fbFreindsListSucessed:[data objectForKey:@"connectedFriends"]];
         }else if ([self.methodName isEqualToString:METHOD_CREATE_SHEET]) {
             
              [self.delegate createNewSheetOrUpdateSheetSucessed:data];
         }else if ([self.methodName isEqualToString:METHOD_INVITE_WITH_USERID]) {
             
             [self.delegate inviteWithUseridSucessed];
         }else if ([self.methodName isEqualToString:METHOD_INVITE_WITH_MAIL]) {
             
             [self.delegate inviteWithMailAddressSucessed];
         }else if ([self.methodName isEqualToString:METHOD_SHEET_MEMBER_LIST]) {
             
             [self.delegate sheetMemberListSucessed:[data objectForKey:MEMBERS]];
         }else if ([self.methodName isEqualToString:METHOD_REFERENCE_SHEET_SETTING]) {
             
             [self.delegate referenceSheetSettingSucessed:data];
         }else if ([self.methodName isEqualToString:METHOD_UPDATE_SHEET_SETTING]) {
             
             [self.delegate createNewSheetOrUpdateSheetSucessed:data];

         }else if ([self.methodName isEqualToString:METHOD_START_EDIT_CARD]) {
             
             [self.delegate startEditCardSucessed:data];
         }else if ([self.methodName isEqualToString:METHOD_UPDATE_CARD]) {
             
             [self.delegate updateCardSucessed:data];
         }else if ([self.methodName isEqualToString:METHOD_END_EDIT_CARD]) {
             
             [self.delegate endEditCardSucessed:data];
         }
 
 
    }else{
        [Utils endDataLoadingAnimation];
        
//        //tokenIdが無効になった場合、再度、メールアドレスとパスワードで再ログインする
//        if ([self.methodName isEqualToString:METHOD_NAME_LOGIN_FAST]
//            || [self.methodName isEqualToString:METHOD_REFERENCE_CONFIG]
//
//            ) {
//            
//            [self loginWithUserId:globalUserInfo.mailAddress pass:globalUserInfo.pass];
//            return;
//        }
        NSString *errMsg = [result objectForKey:RESULT_ERROR_MESSGATE_KEY];

        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle:MultiLangString(@"Information")
							  message:errMsg
							  delegate:nil
							  cancelButtonTitle:MultiLangString(@"Yes")
							  otherButtonTitles: nil];
		[alert show];
		[alert release];
        
        if ([self.delegate respondsToSelector:@selector(loginFaild)]) {
            [self.delegate loginFaild];
        }
        
    }
    
}

// Fileデータを受信したとき
- (void)recieveApplicationData:(NSMutableData *)data fileType:(NSString *)fileType{
    
    NSArray *separate;
    separate = [fileType componentsSeparatedByString:@"/"];
    if ([separate count] == 2) {
//        NSDate* now_date = [NSDate date];
//        NSString *createTime = [Utils getNowTimeWithDate:now_date];
//        NSArray *separateExt = [self.resourceFileName componentsSeparatedByString:@"."];
//        NSString *fileName = [NSString stringWithFormat:@"%@.%@",createTime,[separateExt objectAtIndex:1]];
        
        [Utils writeFileData:self.resourceFileName fileData:data];
        
        if ([self.delegate respondsToSelector:@selector(readApplicationFile:)]) {
            [self.delegate readApplicationFile:self.resourceFileName];
        }

    }

    

}


+(void)setColorDatas{
    int idx=0;
    NSMutableDictionary *data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [data setObject:[NSNumber numberWithInt:80] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:80] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:80] forKey:rgbaB];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [data setObject:[NSNumber numberWithInt:36] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:101] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:217] forKey:rgbaB];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [data setObject:[NSNumber numberWithInt:5] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:150] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:215] forKey:rgbaB];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [data setObject:[NSNumber numberWithInt:20] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:190] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:90] forKey:rgbaB];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [data setObject:[NSNumber numberWithInt:90] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:120] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:45] forKey:rgbaB];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:200] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:55] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:0] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:145] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:100] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:30] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:225] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:90] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:205] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:130] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:60] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:190] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:90] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:40] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:0] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:240] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:115] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:0] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
    data=[[NSMutableDictionary alloc] initWithCapacity:5];
    [data setObject:[NSNumber numberWithInt:idx] forKey:colorIdx_key];
    idx++;
    [data setObject:[NSNumber numberWithInt:220] forKey:rgbaR];
    [data setObject:[NSNumber numberWithInt:220] forKey:rgbaG];
    [data setObject:[NSNumber numberWithInt:30] forKey:rgbaB];
    [data setObject:[NSNumber numberWithInt:255] forKey:rgbaA];
    [globalColorMasterList addObject:data];
    [data release];
    
}




- (void)dealloc {
    self.delegate = nil;
	[self.methodName release];
    
	[super dealloc];
	
}

@end
