//
//  AppBusinessLogicController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/09/13.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AppDefine.h"
#import "AppApiDelegate.h"
#import "Utils.h"
#import "UserInfo.h"

@interface AppBusinessLogicController : NSObject

@property  (nonatomic,strong) UserInfo            *globalUserInfo;
@property  (nonatomic,strong) NSMutableArray		*globalSheetList;
@property  (nonatomic,strong) NSMutableArray      *globalCategoryList;
@property  (nonatomic,strong) NSMutableArray      *globalColorMasterList;
@property  (nonatomic,strong) NSMutableArray		*globalCardList;
@property  (nonatomic,strong) NSMutableDictionary           *globalOption;//メールから直接アプリを起動した時に使う

@end
