//
//  LoginManager.h
//  BukurouSS
//
//  Created by k-kin on 11/11/13.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfo.h"
#import "WebServerDownLoadController.h"
#import "AppApiDelegate.h"
#import "DataRegistry.h"

@interface BusinessDataCtrlManager : NSObject {
    id delegate;

    NSString *methodName ;
    NSString *resourceFileName ;
    NSNumber *currentSheetId;
   

}

@property (nonatomic, assign) id delegate;

@property  (nonatomic,strong) NSString *methodName;
@property  (nonatomic,strong) NSString *resourceFileName;
@property  (nonatomic,strong) NSNumber *currentSheetId;


+(BusinessDataCtrlManager *)getInstance;


+(void)setColorDatas;
-(NSMutableDictionary *)getGlobalOptionData;

+(UserInfo *)getUserInfo;
+(NSMutableArray *)getColorMasterList;
+(NSMutableArray *)getSheetList;
+(NSMutableArray *)getCardList;
+(NSMutableDictionary *)getSheetInfo:(NSNumber *)sheetId;
+(NSMutableArray *)getCategoryList;
+(NSMutableDictionary *)getSheetIdAndCategoryList;
+(NSMutableDictionary *)getCategoryInfo:(NSNumber *)categoryId;
+(NSMutableDictionary *)getCategoryInfo:(NSNumber *)categoryId sheetId:(NSNumber *)sheetId;
+(NSMutableArray *)getGlobalCategoryStatisticsList;

-(void)createUser:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)comfirmUser:(NSString *)confirmKey userId:(NSString *)userId delegate:(id)inDelegate;
-(void)loginWithFacebook:(id)inDelegate;
-(void)loginWithTokenId:(NSString *)tokenId delegate:(id)inDelegate;
-(void)loginWithJSessionId:(NSString *)sessionId delegate:(id)inDelegate;
-(BOOL)loadUserInfoDataWithDelegate:(id)inDelegate;
-(void)loginWithUserIdAndDelegete:(NSString *)inMailAddress pass:(NSString *)inPass delegate:(id)inDelegate;
-(void)loginWithUserId:(NSString *)inMailAddress pass:(NSString *)inPass;
-(void)clearDatas;
-(void)logOut;
-(void)getCardList:(id)inDelegate startIdx:(int )idx;
-(void)getCardListWithCategoryId:(NSNumber *)inCategoryId delegate:(id)inDelegate startIdx:(int )startIdx;
-(void)getCardInfoWithCardId:(NSNumber *)inCardId delegate:(id)inDelegate ;
-(void)goodWithCardId:(NSNumber *)inCardId delegate:(id)inDelegate ;
-(void)getCommentList:(NSNumber *)inCardId delegate:(id)inDelegate ;
-(void)writeCommentData:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)getResourceDownloadKey:(NSNumber *)inResourceId delegate:(id)inDelegate ;
-(void)getResourceFileDownload:(NSString *)inUrl delegate:(id)inDelegate filename:(NSString *)fileName;
-(void)getResourceFileDownloadCancel ;
-(void)sendViolationData:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)getSheetListUnconfirm:(id)inDelegate ;
-(void)groupRegist:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)reloadSheetList:(id)inDelegate ;
-(void)createCategory:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)getInvitedList:(id)inDelegate ;
-(void)getFbFreindsList:(id)inDelegate ;
-(void)createNewSheet:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)inviteWithUserid:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)inviteWithMailaddress:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)getSheetMemberList:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)getReferenceSheetSetting:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)updateReferenceSheetSetting:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)getGategoryStatistics:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)getGategoryStatistics:(NSMutableDictionary *)param;
-(void)startEditCard:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)updateCard:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)endEditCard:(NSMutableDictionary *)param delegate:(id)inDelegate ;



@end
