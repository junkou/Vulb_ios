//
//  TestDataController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/02.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "TestDataController.h"

@implementation TestDataController



#pragma mark createSheetListTestData
+ (void)createSheetListTestData:(NSMutableArray *)dataList{
    /*
   
    NSMutableDictionary *data = nil;
     data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:1] forKey:SHEET_ID];
    [data setObject:@"Mysheets" forKey:sheetName_key];
    [data setObject:[NSNumber numberWithInt:13] forKey:CATEGORY_COUNT];
    [data setObject:[NSNumber numberWithInt:258] forKey:CARD_COUNT];
    [data setObject:@"asuka,shinji,tomoko,koji,tatuy" forKey:MEMBER_LIST];
    [dataList addObject:data];
    [data release];
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:2] forKey:SHEET_ID];
    [data setObject:@"Camera app Brainstorming" forKey:sheetName_key];
    [data setObject:[NSNumber numberWithInt:10] forKey:CATEGORY_COUNT];
    //[data setObject:[NSNumber numberWithInt:200] forKey:CARD_COUNT];
    [dataList addObject:data];
    [data release];
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:3] forKey:SHEET_ID];
    [data setObject:@"Knowledge map project" forKey:sheetName_key];
    [data setObject:[NSNumber numberWithInt:25] forKey:CATEGORY_COUNT];
    [data setObject:[NSNumber numberWithInt:110] forKey:CARD_COUNT];
    [data setObject:@"tarou,kin,arai,takemoto,cai,ri" forKey:MEMBER_LIST];
    [dataList addObject:data];
    [data release];
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:4] forKey:SHEET_ID];
    [data setObject:@"6/18 travel plane hukuoka" forKey:sheetName_key];
    [data setObject:[NSNumber numberWithInt:5] forKey:CATEGORY_COUNT];
    [data setObject:[NSNumber numberWithInt:23] forKey:CARD_COUNT];
    [dataList addObject:data];
    [data release];
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:5] forKey:SHEET_ID];
    [data setObject:@"New next camera app" forKey:sheetName_key];
    [data setObject:[NSNumber numberWithInt:12] forKey:CATEGORY_COUNT];
    [data setObject:[NSNumber numberWithInt:29] forKey:CARD_COUNT];
    [dataList addObject:data];
    [data release];
    */
}

#pragma mark createCategoryListTestData
+ (void)createCategoryListTestData:(NSMutableDictionary *)inputData{
    /*
    NSMutableArray *categoryList = [[NSMutableArray alloc] init];
    [inputData setObject:categoryList forKey:TEST_DATA_SHEET_CODE];
    [categoryList release];
    
    NSMutableDictionary *data = nil;
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:1] forKey:CATEGORY_ID];
    [data setObject:@"Mysheets" forKey:CATEGORY_NAME];
    [data setObject:[NSNumber numberWithInt:1] forKey:CATEGORY_COLOR];
    [data setObject:[NSNumber numberWithInt:258] forKey:CARD_COUNT];
    [data setObject:[NSNumber numberWithInt:1] forKey:SHEET_ID];
    [categoryList addObject:data];
    [data release];
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:2] forKey:CATEGORY_ID];
    [data setObject:@"idea01" forKey:CATEGORY_NAME];
    [data setObject:[NSNumber numberWithInt:2] forKey:CATEGORY_COLOR];
    [data setObject:[NSNumber numberWithInt:8] forKey:CARD_COUNT];
    [data setObject:[NSNumber numberWithInt:1] forKey:SHEET_ID];
    [categoryList addObject:data];
    [data release];
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:3] forKey:CATEGORY_ID];
    [data setObject:@"sketch" forKey:CATEGORY_NAME];
    [data setObject:[NSNumber numberWithInt:3] forKey:CATEGORY_COLOR];
    [data setObject:[NSNumber numberWithInt:8] forKey:CARD_COUNT];
    [data setObject:[NSNumber numberWithInt:1] forKey:SHEET_ID];
    [categoryList addObject:data];
    [data release];
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:4] forKey:CATEGORY_ID];
    [data setObject:@"schedule" forKey:CATEGORY_NAME];
    [data setObject:[NSNumber numberWithInt:4] forKey:CATEGORY_COLOR];
    [data setObject:[NSNumber numberWithInt:8] forKey:CARD_COUNT];
    [data setObject:[NSNumber numberWithInt:1] forKey:SHEET_ID];
    [categoryList addObject:data];
    [data release];
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:5] forKey:CATEGORY_ID];
    [data setObject:@"idea02" forKey:CATEGORY_NAME];
    [data setObject:[NSNumber numberWithInt:5] forKey:CATEGORY_COLOR];
    [data setObject:[NSNumber numberWithInt:8] forKey:CARD_COUNT];
    [data setObject:[NSNumber numberWithInt:1] forKey:SHEET_ID];
    [categoryList addObject:data];
    [data release];
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:6] forKey:CATEGORY_ID];
    [data setObject:@"dev.ver01" forKey:CATEGORY_NAME];
    [data setObject:[NSNumber numberWithInt:6] forKey:CARD_COUNT];
    [data setObject:[NSNumber numberWithInt:1] forKey:CATEGORY_COLOR];
    [data setObject:[NSNumber numberWithInt:1] forKey:SHEET_ID];
    [categoryList addObject:data];
    [data release];
    
    data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithInt:7] forKey:CATEGORY_ID];
    [data setObject:@"dev.ver02" forKey:CATEGORY_NAME];
    [data setObject:[NSNumber numberWithInt:7] forKey:CATEGORY_COLOR];
    [data setObject:[NSNumber numberWithInt:8] forKey:CARD_COUNT];
    [data setObject:[NSNumber numberWithInt:1] forKey:SHEET_ID];
    [categoryList addObject:data];
    [data release];
    */
}


@end
