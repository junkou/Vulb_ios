//
//  PhotoCameraViewController.h
//  BukurouSS
//
//  Created by 柳 英花 on 12/03/07.
//  Copyright (c) 2012 iccesoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDefine.h"
#import "DatabaseTableNmaes.h"
#import "BssAssetsController.h"
#import "AppApiDelegate.h"

@interface PhotoCameraViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
 
    id delegate;

    int     allPageCount;
    UIImagePickerController *myPicker;
    NSMutableArray  *dataList;

}

@property (nonatomic, assign) id delegate;
@property int     allPageCount;
@property (nonatomic, assign) UIImagePickerController *myPicker;
@property (nonatomic, retain) NSMutableArray  *dataList;


- (void) startUIImagePickerController ;


@end
