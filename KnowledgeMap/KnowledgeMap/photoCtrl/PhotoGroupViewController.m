#import "PhotoGroupViewController.h"
#import "PhotoSelectViewController.h"

@implementation PhotoGroupViewController

@synthesize delegate;
@synthesize allPageCount;
@synthesize groups;
@synthesize library;
@synthesize dataCnt;
@synthesize groupPotion;
@synthesize groupCnt;
@synthesize uid;
@synthesize albumId;
@synthesize searchDataStartIndex;
@synthesize friendListId;
@synthesize groups_copy;


#pragma mark Rotation support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

#pragma mark View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];	
	
    if ([Utils versionIsSeven]) {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }

//    self.tableView.backgroundView = [Utils getDataListBackView:WORK_SCREEN_RECT_SIZE];

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.searchDataStartIndex = 0;
	self.dataCnt = 0;
	self.groupPotion = 0;
	self.groupCnt = 0;
	self.groups = [[NSMutableArray alloc] init];
	self.library = [[ALAssetsLibrary alloc] init];
    self.groups_copy = [[NSMutableArray alloc] init];

    //NavigationBar Setting
    [Utils setNavigationBarLeftButton:self.navigationController.navigationBar navigationItem:self.navigationItem controller:self];

    [self viewDidLoadExtention];
}

- (void)viewDidLoadExtention {
    if ([CLLocationManager locationServicesEnabled]){
        
        // 別スレッドでフォトライブラリ内のグループを検索 
        [self performSelectorInBackground:@selector(searchPhotoGroups) withObject:nil];	
        
    }else{
        // アラートを表示
        UIAlertView* alert=[[[UIAlertView alloc] initWithTitle:@"" message:[Utils MultiLangStringInfomation:@"I0010"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        [alert show];
        
    }
	// 写真選択のキャンセルボタンを生成
	//UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(photoSelectCancel)];
	//[self.navigationItem setRightBarButtonItem:cancelButton];
	//[cancelButton release];

}

//NavigationBar CancelButton
-(void)clickCancelButton:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
    
}

-(void)searchPhotoGroups {	
 	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // フォトライブラリから取得したグループをgroupsに追加。追加するたびに画面のリロードを行う。 	
        void (^groupBlock)(ALAssetsGroup *, BOOL *) = ^(ALAssetsGroup *group, BOOL *stop) {
            if(group != nil){
                
                NSInteger photoCnt = [(ALAssetsGroup*)group numberOfAssets];
                if (photoCnt > 0) {
                    [self.groups addObject:group];
                    [self performSelectorOnMainThread:@selector(reload) withObject:nil waitUntilDone:NO];
                }
                //[self getGroupInfo:group];
            }
        };
        
        // フォトライブラリ内へアクセスし、引数のブロックを実行する。
        [self.library enumerateGroupsWithTypes:ALAssetsGroupAll
                                    usingBlock:groupBlock
                                  failureBlock:^(NSError *error) {
                                      NSLog(@"ーーーーーError!ーーーーー");
                                       // アラートを表示
                                      UIAlertView* alert=[[[UIAlertView alloc] initWithTitle:@"" message:[Utils MultiLangStringInfomation:@"I0010"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
                                      [alert show];

                                  }];		
    });
 	[pool release];		

}

-(void)getGroupInfo:(id)inGroup{
	self.groupCnt++;	
	self.groupPotion = [self.groups count];
	NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
	UIImage *image = [UIImage imageWithCGImage:[(ALAssetsGroup *)inGroup posterImage]];
	NSString *groupName = [(ALAssetsGroup *)inGroup valueForProperty:ALAssetsGroupPropertyName];
	NSInteger photoCnt = [(ALAssetsGroup *)inGroup numberOfAssets];	
	
	[data setObject:[NSNumber numberWithInt:self.groupCnt] forKey:KEY_IDENTI];
	[data setObject:KEY_GROUP forKey:KEY_DATA_TYPE];
	[data setObject:image forKey:KEY_THUMBNAIL_IMG];
	[data setObject:groupName forKey:KEY_GROP_NAME];	
	[data setObject:[NSNumber numberWithInt:photoCnt] forKey:KEY_DATA_CNT];
	[self.groups addObject:data];
	[data release];
	
	[self searchInGroupInfo:inGroup];

}

- (void)searchInGroupInfo:(id)inGroup {
	// フォトライブラリから取得した写真をphotosに追加。
	void (^assetBlock)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
		if(result != nil) {
			// ALAssetのタイプが写真を追加
			if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
				[self setInGroupPhotoInfo:result];
			}			
		}
	};
	// フォトライブラリへアクセスし、引数のブロックを実行する。
	[(ALAssetsGroup *)inGroup enumerateAssetsUsingBlock:assetBlock];		
}

- (void)setInGroupPhotoInfo:(id)inALAsset {
	NSDate *date = [(ALAsset *)inALAsset valueForProperty:ALAssetPropertyDate];
	NSString *strTime = [self dateFormatChangeStrYYYY:date];	
	NSString *key = [NSString stringWithFormat:@"%d_%@", self.groupCnt, strTime];
		
    //グループ一覧表示用データを作成 
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    UIImage *image = [UIImage imageWithCGImage:[inALAsset thumbnail]];
    [data setObject:KEY_PHOTO forKey:KEY_DATA_TYPE];
    [data setObject:image forKey:KEY_THUMBNAIL_IMG];
    [data setObject:strTime forKey:KEY_GROP_NAME];
    [data setObject:key forKey:KEY_IDENTI];
    [data setObject:[NSNumber numberWithInt:self.dataCnt] forKey:KEY_DATA_CNT];
    [self.groups addObject:data];
    [data release];			
	

	[self performSelectorOnMainThread:@selector(reload) withObject:nil waitUntilDone:NO];

}

-(void)reload {
	[self.tableView reloadData];
}

#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.groups count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {    
    NSString *CellIdentifier = [NSString stringWithFormat: @"Cell_%d",[indexPath row]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	//cell.selectionStyle = UITableViewCellSelectionStyleBlue;

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    
	// フォトライブラリ内のポスター画像・グループ名・写真数を取得してセルに表示する。
	UIImage *image = [UIImage imageWithCGImage:[(ALAssetsGroup*)[self.groups objectAtIndex:indexPath.row] posterImage]];
	NSString *groupName = [(ALAsset*)[self.groups objectAtIndex:indexPath.row] valueForProperty:ALAssetsGroupPropertyName];
	NSInteger photoCnt = [(ALAssetsGroup*)[self.groups objectAtIndex:indexPath.row] numberOfAssets];
    
    CGFloat delta = 5;
    UIImageView *imgeView = [[[UIImageView alloc] initWithFrame:CGRectMake(delta, delta, PHOT_LIBARY_THUMBNAIL_SIZE-delta*2, PHOT_LIBARY_THUMBNAIL_SIZE-delta*2)] autorelease];
    imgeView.image = image;
    [cell.contentView addSubview:imgeView];
    
    
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(imgeView.frame.origin.x+imgeView.frame.size.width+delta, delta, SCREEN_WIDTH-imgeView.frame.origin.x-delta-20, imgeView.frame.size.height)] autorelease];
 	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont  boldSystemFontOfSize:20];
	label.textColor = [UIColor blackColor];
	label.textAlignment = UITextAlignmentLeft;
	label.numberOfLines = 1;
	label.text = [NSString stringWithFormat:@"%@ (%d)", groupName, photoCnt];
	[cell.contentView addSubview:label];

    UIView *line = [[[UIView alloc] initWithFrame:CGRectMake(0, PHOT_LIBARY_THUMBNAIL_SIZE-0.4, SCREEN_WIDTH, 0.4)] autorelease];
    line.backgroundColor  = [UIColor grayColor];
    [cell.contentView addSubview:line];
   
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;

}

#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {	
	PhotoSelectViewController *controller = nil;
    controller = [[[PhotoSelectViewController alloc] initWithConfirmButtonFlag:TRUE group:[self.groups objectAtIndex:indexPath.row] allPageCount:self.allPageCount] autorelease];

    NSString *groupName = [(ALAsset*)[self.groups objectAtIndex:indexPath.row] valueForProperty:ALAssetsGroupPropertyName];
    controller.title = groupName;
	controller.delegate = self;
	[self.navigationController pushViewController:controller animated:YES];

	[tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	/*
	CGFloat height = 0;
	NSMutableDictionary *data = [self.groups objectAtIndex:indexPath.row];
	NSString *dataType = [data objectForKey:KEY_DATA_TYPE];
	if ([dataType isEqualToString:KEY_GROUP]) {
		height = PHOT_LIBARY_THUMBNAIL_SIZE;
	}else if([dataType isEqualToString:KEY_PHOTO]){
		height = PHOT_LIBARY_THUMBNAIL_SMALL_SIZE;
	}
	 */
	return PHOT_LIBARY_THUMBNAIL_SIZE;
}

#pragma mark PhotoViewControllerDelegate Methods
- (void)photoSelectConfirm:(NSMutableArray *)dataList{
	[delegate photoSelectConfirm:dataList];
}

- (void)photoSelectCancel {
	[delegate photoSelectCancel];
}

- (NSString *) dateFormatChangeStrYYYY:(NSDate *)inData{	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	NSLocale *local = [[NSLocale alloc] initWithLocaleIdentifier:@"US"];
	[formatter setLocale:local];
	[local release];
	[formatter setDateFormat:@"YYYY"];	
	NSString *now_dateStr = [formatter stringFromDate:inData];
	[formatter release];
	return now_dateStr;
}

#pragma mark Memory management
- (void)dealloc {	
	[self.groups removeAllObjects];
	[self.groups release];
	self.groups = nil;	

	
    [self.groups_copy removeAllObjects];
	[self.groups_copy release];
	self.groups_copy = nil;	

    [self.uid release];
	[self.albumId release];
 	[self.friendListId release];
   
	[self.library release];	
	self.delegate = nil;

	[super dealloc];

	
}

@end