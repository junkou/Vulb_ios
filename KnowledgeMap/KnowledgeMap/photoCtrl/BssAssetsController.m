//
//  BssAssetsController.m
//  BukurouSS
//
//  Created by k-kin on 11/11/14.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import "BssAssetsController.h"


@implementation BssAssetsController


NSMutableDictionary *photoLibaryAssetDatas; //PhotoLibrayから取得したAssetsデータ
NSMutableDictionary *photoGpsDatas;
NSMutableDictionary *editPageChangePhotoDatas;//ページ入れ替えの時一時的に使用

BssAssetsController *bssAssetsController;

+(BssAssetsController *)getInstance{
	if (bssAssetsController == nil) {
		bssAssetsController = [[BssAssetsController alloc] init] ;		
	}
	return bssAssetsController;
}



#pragma mark getPagePhotoAsset
+(NSArray *)getALLPhotoImageKeys{
	return [photoLibaryAssetDatas allKeys];
}

+(id)getPagePhotoAsset:(NSString*)inKey {
	if (photoLibaryAssetDatas == nil) {
		return nil;
	}
	
	return [photoLibaryAssetDatas objectForKey:inKey];
}

+(void)setPagePhotoAsset:(id)data key:(NSString*)inKey {
	if (photoLibaryAssetDatas == nil) {
		photoLibaryAssetDatas = [[NSMutableDictionary alloc] initWithCapacity:24];
	}
	[photoLibaryAssetDatas removeObjectForKey:inKey];
	[photoLibaryAssetDatas setObject:data forKey:inKey];
	
	[self setPhotoGPSData:inKey];
}

#pragma mark changeEditPhotoImage
+(id)getEditPageChangePhoto {
	id photoInfo = [editPageChangePhotoDatas objectForKey:EDIT_CHANGE_PHOTO_IMG_KEY];
	return [self getPhotoImageExt:photoInfo];
}

+(NSArray *)getEditPageArray {
	return [editPageChangePhotoDatas allValues];
}

+(void)setEdetPageChangePhotoAsset:(id)data {
	if (editPageChangePhotoDatas == nil) {
		editPageChangePhotoDatas = [[NSMutableDictionary alloc] initWithCapacity:1];
	}
	[editPageChangePhotoDatas removeObjectForKey:EDIT_CHANGE_PHOTO_IMG_KEY];
	[editPageChangePhotoDatas setObject:data forKey:EDIT_CHANGE_PHOTO_IMG_KEY];	
}

+(CLLocationCoordinate2D)getEditPhotoGPSinfo{
    id resourceData = [editPageChangePhotoDatas objectForKey:EDIT_CHANGE_PHOTO_IMG_KEY];
    NSDictionary *gpsData = nil;
    if ( [resourceData isKindOfClass:[ ALAsset class]]) {
        NSDictionary *metaData = [[(ALAsset *)resourceData defaultRepresentation] metadata];
        gpsData = [metaData objectForKey:(NSString *)kCGImagePropertyGPSDictionary];
    }
    
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = 0.0;
	coordinate.longitude = 0.0;
	//NSLog(@"=======gpsData:%@", gpsData);
	if (gpsData == nil) {
		return coordinate;
	}
	
	double latitude = [(NSString *)[gpsData objectForKey:GPS_Latitude] floatValue] ;
	double longitude = [(NSString *)[gpsData objectForKey:GPS_Longitude] floatValue] ;
	coordinate.latitude = latitude;
	coordinate.longitude = longitude;
	
	return coordinate;
}

+(CLLocationCoordinate2D)changeEditPhotoGPSinfo{
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = 0.0;
	coordinate.longitude = 0.0;
	//NSLog(@"=======gpsData:%@", gpsData);
	
    id resourceData = [editPageChangePhotoDatas objectForKey:EDIT_CHANGE_PHOTO_IMG_KEY];
    if ( [resourceData isKindOfClass:[ ALAsset class]]) {
        NSDictionary *metaData = [[(ALAsset *)resourceData defaultRepresentation] metadata];
        if (metaData != nil) {
            NSDictionary *gpsData = [metaData objectForKey:(NSString *)kCGImagePropertyGPSDictionary];
            NSString *_latitude  = (NSString *)[gpsData objectForKey:GPS_Latitude];
            NSString *_longitude = (NSString *)[gpsData objectForKey:GPS_Longitude];
            if (_latitude != nil) {
                coordinate.latitude  = [_latitude floatValue];
                coordinate.longitude = [_longitude floatValue];

            }
        }

    }

	
	return coordinate;
}

+(void)deleteEdetPageChangePhotoAsset {
	if (editPageChangePhotoDatas == nil) {
		return;
	}
	[editPageChangePhotoDatas removeObjectForKey:EDIT_CHANGE_PHOTO_IMG_KEY];
}


#pragma mark getDataAsset other
+ (id)getPhotoImage:(NSString *)index {
	id photoInfo = [photoLibaryAssetDatas objectForKey:index]; 

    id ret = nil;
    ret = [self getPhotoImageExt:photoInfo];
	return ret;
	
	
}

+ (id)getPhotoImageInfo:(NSString *)index {    
	return [photoLibaryAssetDatas objectForKey:index];;
    
}

+ (id)getPhotoImageExt:(id)photoInfo {	
	[NSThread sleepForTimeInterval:0.1]; 

    UIImage *img = nil;
    id retValue = nil;
    if ( [photoInfo isKindOfClass:[ ALAsset class]]) {
        ALAssetRepresentation *representation = [(ALAsset *)photoInfo defaultRepresentation];	
        UIImageOrientation orientation = (UIImageOrientation)[representation orientation];
        //NSLog(@"imgScale:%f",imgScale);
        img = [UIImage imageWithCGImage:[representation fullResolutionImage] scale:1.0 orientation:orientation];
        CGSize reSize = [self getPhotoReSize:img.size];
        img = [self resizeToLowImage:img Size:reSize];		

        retValue = [[[NSData alloc] initWithData:UIImageJPEGRepresentation([img imageByCompositeWithImage] , 1)] autorelease] ;

    }else if([photoInfo isKindOfClass:[ NSData class]]){
        NSLog(@"retValue:%d",[retValue length]);
        retValue = [self getResizePhotoImageExt:retValue];
        NSLog(@"retValue:%d",[retValue length]);
        
    }else if([photoInfo isKindOfClass:[ NSDictionary class]]){
        NSString *imageType = [photoInfo objectForKey:IMAGE_RESOURCE_TYPE_KEY];
        if ([imageType isEqualToString:IMAGE_RESOURCE_TYPE_CAMERA_IMAGE]) {
            NSData *imgData = [photoInfo objectForKey:PHOTO_IMAGE_DATA];
            //NSLog(@"retValue:%d",[imgData length]);
            retValue = [self getResizePhotoImageExt:imgData];
            //NSLog(@"retValue:%d",[retValue length]);
       }
       //NSString *imgUrl =  [photoInfo objectForKey:@"source"];
        //NSLog(@"imgUrl:%@",imgUrl);
    }
    			
	return retValue;	
}

+ (id)getResizePhotoImageExt:(NSData *)nsData {	
    UIImage *dataImage = [[[UIImage alloc] initWithData:nsData] autorelease];
    CGSize reSize = [BssAssetsController getPhotoReSize:dataImage.size];
    dataImage = [BssAssetsController resizeToLowImage:dataImage Size:reSize];
    NSData *newDataValue = [[[NSData alloc] initWithData:UIImageJPEGRepresentation([dataImage imageByCompositeWithImage] , 1)] autorelease] ;
    return newDataValue;
}

#pragma mark getPhotoReSize other
+(CGSize)getPhotoReSize:(CGSize)inSize{
	CGSize re_size;
	//NSLog(@"::::inSize: %@", NSStringFromCGSize(inSize));
	
	CGFloat sizeWith = PHOTO_RECT_HD_WIDTH;
	
    re_size.width = sizeWith;
    re_size.height = sizeWith / inSize.width * inSize.height;

    /*
    //サイズを無条件で固定させる
//	if (inSize.height > sizeHeight && inSize.width > sizeWith) {
		//画像の縦の長さが横の長さより、大きい場合、縦を基準に縮小する、　そうでない場合、横を基準に縮小する
		if (inSize.height > inSize.width) {
			re_size.width = sizeWith;
			re_size.height = sizeWith / inSize.width * inSize.height;
		}else {
			re_size.height = sizeHeight;
			re_size.width = sizeHeight / inSize.height * inSize.width;
			if (re_size.width < sizeWith) {
				re_size.width = sizeWith;
				re_size.height = sizeWith / inSize.width * inSize.height;
			}
		}
     */
		
//	}else {
//		re_size.height = inSize.height;
//		re_size.width = inSize.width;
//	}									
	//NSLog(@"::::re_size: %@", NSStringFromCGSize(re_size));
	return re_size;
}

+(UIImage*)resizeToLowImage:(UIImage*)image Size:(CGSize)size{	
	UIImage*    shrinkedImage;
	UIGraphicsBeginImageContext(size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetInterpolationQuality(context, kCGInterpolationLow); // kCGInterpolationHigh kCGInterpolationDefault kCGInterpolationLow kCGInterpolationNone
	CGRect  rect;
	rect.origin = CGPointZero;
	rect.size = size;
	[image drawInRect:rect];
	shrinkedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return shrinkedImage;
}

+(void)delPagePhotoAsset:(NSString*)inKey {
    //NSLog(@"photoLibaryAssetDatas count:%d",[photoLibaryAssetDatas count]);
	[photoLibaryAssetDatas removeObjectForKey:inKey];	
	//NSLog(@"photoLibaryAssetDatas count:%d",[photoLibaryAssetDatas count]);
}

#pragma mark getPhotoGPSData other
+ (CLLocationCoordinate2D)setPhotoGPSData:(NSString *)index {
    id resourceData = [photoLibaryAssetDatas objectForKey:index];
    if ( [resourceData isKindOfClass:[ ALAsset class]]) {
        NSDictionary *metaData = [[(ALAsset *)resourceData defaultRepresentation] metadata];
         return [self setPhotoGPSDataExt:metaData dataKey:index];
    }else{
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = 0.0;
        coordinate.longitude = 0.0;
 
        return coordinate;
    }
	 
}

// テーブル指定行のGPSデータを設定
+ (CLLocationCoordinate2D)setPhotoGPSDataExt:(NSDictionary *)metaData dataKey:(NSString *)index{
	NSDictionary *gpsData = [metaData objectForKey:(NSString *)kCGImagePropertyGPSDictionary];
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = 0.0;
	coordinate.longitude = 0.0;
	//NSLog(@"=======gpsData:%@", gpsData);
	if (gpsData == nil) {
		return coordinate;
	}
	
	NSString *_latitude = [gpsData objectForKey:GPS_Latitude] ;
	NSString *_longitude = [gpsData objectForKey:GPS_Longitude] ;
    if (_latitude == nil) {
        return coordinate;
    }
	
	if (photoGpsDatas == nil) {
		photoGpsDatas = [[NSMutableDictionary alloc] init];
	}
	NSMutableDictionary *objData = [[NSMutableDictionary alloc] initWithCapacity:2];
	[objData setObject:_latitude forKey:GPS_Latitude];
	[objData setObject:_longitude forKey:GPS_Longitude];	
	[photoGpsDatas removeObjectForKey:index];
	[photoGpsDatas setObject:objData forKey:index];
	[objData release];
	
	double latitude = [(NSString *)[gpsData objectForKey:GPS_Latitude] floatValue] ;
	double longitude = [(NSString *)[gpsData objectForKey:GPS_Longitude] floatValue] ;
	coordinate.latitude = latitude;
	coordinate.longitude = longitude;
	
	return coordinate;
	
}

// テーブル指定行のGPSデータを取得
+ (CLLocationCoordinate2D)getPhotoGPSData:(NSString *)index {
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = 0.0;
	coordinate.longitude = 0.0;
	
	NSDictionary *gpsData = [photoGpsDatas objectForKey:index];
	//NSLog(@"=======gpsData:%@", gpsData);
	if (gpsData == nil) {
		return coordinate;
	}
	
	double _latitude = [(NSString *)[gpsData objectForKey:GPS_Latitude] floatValue] ;
	double _longitude = [(NSString *)[gpsData objectForKey:GPS_Longitude] floatValue] ;
	coordinate.latitude = _latitude;
	coordinate.longitude = _longitude;
	
	return coordinate;
}

+ (void)deletePhotoGPSData:(NSString *)index {
	if (photoGpsDatas == nil) {
		return;
	}
	[photoGpsDatas removeObjectForKey:index];
	//NSLog(@"photoGpsDatas count:%d",[photoGpsDatas count]);	

}

#pragma mark Memory other
+(void)clearDatas{	
	if (photoLibaryAssetDatas != nil) {
		[photoLibaryAssetDatas removeAllObjects];
		[photoLibaryAssetDatas release];
		photoLibaryAssetDatas = nil;		
	}
	
	if (photoGpsDatas != nil) {
		[photoGpsDatas removeAllObjects];
		[photoGpsDatas release];
		photoGpsDatas = nil;		
	}
	
	if (editPageChangePhotoDatas != nil) {
		[editPageChangePhotoDatas removeAllObjects];
		[editPageChangePhotoDatas release];
		editPageChangePhotoDatas = nil;		
	}
	
}

@end


//NSLog(@"newImage.scale:%f newImg.width:%f/%f  height:%f/%f", img.scale, img.size.width,img.size.width/2, img.size.height,img.size.height/2);
/*
 // 元から付いているメタデータも残す
 NSMutableDictionary *metadata;
 metadata = [[NSMutableDictionary alloc] initWithDictionary:[representation metadata]];	
 // CGImageDestination を利用して画像とメタデータをひ関連付ける
 NSMutableData *imageData = [[[NSMutableData alloc] init] autorelease];
 CGImageDestinationRef dest;
 dest = CGImageDestinationCreateWithData((CFMutableDataRef)imageData, kUTTypeJPEG, 1, nil);
 
 CGImageDestinationAddImage(dest, img.CGImage, (CFDictionaryRef)metadata);
 CGImageDestinationFinalize(dest);
 CFRelease(dest);
 */	
//	UIImage *reSizeImg = [Utils resizeImage:img Size:reSize];		
//	UIImage *testImg = [UIImage imageNamed:@"Forest.jpg"];
//	return testImg;
//	return [img imageByCompositeWithImage];

