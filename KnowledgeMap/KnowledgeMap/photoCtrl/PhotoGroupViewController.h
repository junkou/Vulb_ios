#import <AssetsLibrary/ALAssetsGroup.h>
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <CoreLocation/CoreLocation.h>

#import "AppApiDelegate.h"

@interface PhotoGroupViewController : UITableViewController <AppApiDelegate>{
	id delegate;
    int     allPageCount;

	NSMutableArray *groups;
	ALAssetsLibrary *library; 
	int dataCnt;
	int groupPotion;
	int groupCnt;
    
    NSString *uid;
    NSString *albumId;
    
    int         searchDataStartIndex;
    NSString *friendListId;

    NSMutableArray *groups_copy;

}

@property (nonatomic, assign) id delegate;
@property int     allPageCount;

@property (nonatomic, retain) NSMutableArray			*groups;
@property (nonatomic, retain) ALAssetsLibrary			*library;
@property int dataCnt;
@property int groupPotion;
@property int groupCnt;
@property (nonatomic, retain) NSString *uid;
@property (nonatomic, retain) NSString *albumId;
@property int         searchDataStartIndex;
@property (nonatomic, retain) NSString *friendListId;
@property (nonatomic, retain) NSMutableArray *groups_copy;

- (void)viewDidLoadExtention ;
-(void)getGroupInfo:(id)inGroup;
- (void)searchInGroupInfo:(id)inGroup;
- (void)setInGroupPhotoInfo:(id)inALAsset;
- (NSString *) dateFormatChangeStrYYYY:(NSDate *)inData;

@end