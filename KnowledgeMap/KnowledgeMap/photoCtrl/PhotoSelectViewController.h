#import <AssetsLibrary/ALAssetsGroup.h>
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import <QuartzCore/QuartzCore.h>

#import "AppDefine.h"
#import "Utils.h"
#import "AppApiDelegate.h"
#import "BssAssetsController.h"

#define photoCountInCell	3
//#define imageSize			73
#define dummyTime			@"0000:00:00 00:00:00"
#define timeLabelHeight		16
 
@interface PhotoSelectViewController : UITableViewController {
	id delegate;
	BOOL	addConfirmButtonFalg;
    int     allPageCount;
    int     cellCount;
    CGFloat thumbnailSize;
    int     initScrollFlag;
@private
	NSMutableArray			*photos; 
	id                      group;

	NSMutableArray			*selectedPhotoDataIndexs;
	NSMutableArray			*selectedPhotoMetaData;
	
    int         allRowCount;
    NSString *albumId;
    int         searchDataStartIndex;

}

@property (nonatomic, assign) id delegate;
@property BOOL	addConfirmButtonFalg;
@property int     allPageCount;
@property int     allRowCount;
@property int     cellCount;
@property CGFloat thumbnailSize;
@property int     initScrollFlag;

@property (nonatomic, retain) NSMutableArray			*photos;
@property (nonatomic, assign) ALAssetsGroup				*group;
@property (nonatomic, retain) NSMutableArray			*selectedPhotoDataIndexs;
@property (nonatomic, retain) NSMutableArray		*selectedPhotoMetaData;

@property (nonatomic, assign) NSString *albumId;
@property int         searchDataStartIndex;


- (id)initWithConfirmButtonFlag:(BOOL)inFlag group:(ALAssetsGroup*)inGroup allPageCount:(int)allpageCnt;
-(void)ceateShowPhotoMetaData;
- (void)searchPhotos ;
- (void)getPhotoImageThumnail:(NSUInteger)index imageView:(UIImageView *)imageView;
-(NSString *)dateFormatChange:(NSString *)inData;
- (NSString *)dateFormatChangeStr:(NSDate *)inData;
- (void)photoSelectConfirm:(NSMutableArray *)dataList;
-(void)setPageNumber:(int)pageNum;
-(void)terminateObjects;
-(void)saveToPhotoAlbumFinal ;


@end