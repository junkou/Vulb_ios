//
//  CardDetailViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/21.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "CardDetailViewController.h"

@interface CardDetailViewController ()
@property (nonatomic, strong) UIButton      *goodButton;
@property (nonatomic, strong) NSMutableDictionary		*filenameDic;

@end

@implementation CardDetailViewController

@synthesize delegate;
@synthesize dataList;
@synthesize cardInfo;
@synthesize cardDetailInfoList;
@synthesize currentCardIndex;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.dataList = [[[NSMutableArray alloc] init] autorelease];
        self.filenameDic = [[[NSMutableDictionary alloc] init] autorelease];
        
        self.currentCardIndex = 0;
        self.cardDetailInfoList = [[[NSMutableDictionary alloc] init] autorelease];
        self.cardThumbnailList = [BusinessDataCtrlManager getCardList];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                     name: UIKeyboardWillShowNotification object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                     name: UIKeyboardWillHideNotification object:nil];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view = [[[UIViewExt alloc] initWithFrame:self.view.frame] autorelease];
    
    self.view.backgroundColor = [Utils getBaseTextColor];

    NSMutableDictionary *newCardInfo = [[[NSMutableDictionary alloc] initWithDictionary:self.cardInfo] autorelease];
    [self.cardDetailInfoList setObject:self.cardInfo forKey:[newCardInfo objectForKey:cardId_key]];

    
    for (int i=0; i<[self.cardThumbnailList count]; i++) {
        NSMutableDictionary *carThumbnailInfo = [self.cardThumbnailList objectAtIndex:i];
        if ([[carThumbnailInfo objectForKey:cardId_key] intValue] == [[self.cardInfo objectForKey:cardId_key] intValue]) {
            self.currentCardIndex = i;
            break;
        }
    }
    
    if ([Utils versionIsSeven]) {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }

    CGFloat baseWidth = 54;
    CGFloat width = 44;
    UIView *baseView = [[[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-baseWidth*3, 0, baseWidth*3, width)] autorelease];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem  alloc] initWithCustomView:baseView];

    //違反申告ボタン
    UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, width, width)] autorelease];
    [button addTarget:self action:@selector(clickViolationButton:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_notice.png"]];
    [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    UIImage *backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_notice.png"]];
    [button setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    [baseView addSubview:button];

    //Goodボタンを生成
    self.goodButton = [[[UIButton alloc] initWithFrame:CGRectMake(baseWidth, 0, width, width)] autorelease];
    //goodButton.backgroundColor = [UIColor redColor];
    [self.goodButton addTarget:self action:@selector(clickGoodButton:) forControlEvents:UIControlEventTouchUpInside];
    backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_good_normal.png"]];
    [self.goodButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_good_touch.png"]];
    [self.goodButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    [baseView addSubview:self.goodButton];
    NSString *valuationStatus = [self.cardInfo objectForKey:valuationStatus_key];
    if ([valuationStatus isEqualToString:CARD_VALUATION_GOOD]) {
        self.goodButton.enabled = NO;
    }
    

    //commentボタンを生成
    UIButton *commentButton = [[[UIButton alloc] initWithFrame:CGRectMake(baseWidth*2, 0, width, width)] autorelease];
    [commentButton addTarget:self action:@selector(clickCommentButton:) forControlEvents:UIControlEventTouchUpInside];
    backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_comment_normal.png"]];
    [commentButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_comment_touch.png"]];
    [commentButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    [baseView addSubview:commentButton];

    NSNumber *commentCnt = [Utils parseWebDataToNumber:self.cardInfo dataKey:@"commentCount"];
    NSString *cntStr = @"(00)";
    if ([commentCnt intValue] > 0) {
        cntStr = [NSString stringWithFormat:@"(%d)",[commentCnt intValue]];
     }
    
    UIFont *fornt = [UIFont  boldSystemFontOfSize:16];
    CGSize size = [cntStr sizeWithFont:fornt constrainedToSize:CGSizeMake(width, width) lineBreakMode:NSLineBreakByWordWrapping];
    
    self.commentCountLable = [[[UILabel alloc] initWithFrame:CGRectMake(38, 0, size.width, commentButton.frame.size.height)] autorelease];
    self.commentCountLable.backgroundColor = [UIColor clearColor];
    self.commentCountLable.font = fornt;
    self.commentCountLable.textColor = [UIColor whiteColor];
    self.commentCountLable.textAlignment = UITextAlignmentCenter;
    self.commentCountLable.numberOfLines = 1;
    [commentButton addSubview:self.commentCountLable];
    self.commentCountLable.text = cntStr;
    if ([commentCnt intValue] == 0) {
        self.commentCountLable.hidden = YES;
    }
    
    CGFloat positionY =self.view.bounds.size.height-TOOL_BAR_HIGHT;
    if ([Utils versionIsSeven]) {
        positionY = positionY-20;
    }
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, positionY)] autorelease];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = [Utils getBaseTextColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableHeaderView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)]autorelease];
    
    UISwipeGestureRecognizer *swipeRightGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToRight:)] autorelease];
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	swipeRightGesture.numberOfTouchesRequired = 1;
    swipeRightGesture.delegate = self;
	[self.view addGestureRecognizer:swipeRightGesture];

    UISwipeGestureRecognizer *swipeLefGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToLeft:)] autorelease];
	swipeLefGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	swipeLefGesture.numberOfTouchesRequired = 1;
    swipeLefGesture.delegate = self;
	[self.view addGestureRecognizer:swipeLefGesture];
   
    /*
    // タップジェスチャーの作成
    UITapGestureRecognizer *tapGesture = [[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(handleTapGesture:)] autorelease];
    tapGesture.numberOfTapsRequired = 1;    // シングル
    tapGesture.numberOfTouchesRequired = 1; // ジェスチャーを認識する指の数
    [self.view addGestureRecognizer:tapGesture];
     */

}

-(void)clickGoodButton:(id)sender{
    
    [[BusinessDataCtrlManager getInstance] goodWithCardId:[self.cardInfo objectForKey:cardId_key] delegate:self];

}

- (void)goodCardSucessed{
    self.goodButton.enabled = FALSE;
}


-(void)clickCommentButton:(id)sender{
    
    [[BusinessDataCtrlManager getInstance] getCommentList:[self.cardInfo objectForKey:cardId_key] delegate:self];
    
}

-(void)clickViolationButton:(id)sender{
    ViolationInputViewController *violationInputViewController = [[[ViolationInputViewController alloc] init] autorelease];
    violationInputViewController.cardInfo = self.cardInfo;
    UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:violationInputViewController] autorelease];
    
	[self presentModalViewController:controller animated:YES];

}

- (void)commentListSucessed:(NSArray *)commentList{
    CommentListViewController *commentListViewController = [[CommentListViewController alloc] init];
    commentListViewController.delegate = self;
    commentListViewController.cardId = [self.cardInfo objectForKey:cardId_key];
    commentListViewController.dataList = [[NSMutableArray alloc] initWithArray:commentList];
    
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:commentListViewController];
    
	[self presentModalViewController:controller animated:YES];
    
    [commentListViewController release];
    [controller release];

}

-(void)resetCommnetCount:(int)cnt{
    NSString *cntStr = @"";
    if (cnt > 0) {
        cntStr = [NSString stringWithFormat:@"(%d)",cnt];
        self.commentCountLable.hidden = NO;
        self.commentCountLable.text = cntStr;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //NSLog(@"[self.dataList count]:%d",[self.dataList count]);
    return [self.dataList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);

    NSMutableDictionary *cardData = [self.dataList objectAtIndex:[indexPath row]];
    CGSize size = [Utils getCardDetailTextSize:cardData delegate:self.delegate];
    //NSLog(@"---heightForRowAtIndexPath---:%f",size.height);

	return size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *cardContent = [self.dataList objectAtIndex:[indexPath row]];
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d",  [indexPath row]] ;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
     */
   
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor =[UIColor clearColor];
        
         // Configure the cell...
        [Utils setCardDetailDataContents:cardContent view:cell.contentView  delegate:self];
        
    }
    
    return cell;
}



#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    NSMutableDictionary *infoData = [self.dataList objectAtIndex:[indexPath row]];
    
    NSString *resourceType = [Utils parseWebDataToString:infoData dataKey:resourceType_key];
    if ([resourceType isEqualToString:DB_RESOURCE_TYPE_WEB]) {
        NSString* html = [Utils parseWebDataToString:infoData dataKey:begining_key];
        
        WebServerViewController *webViewController = [[[WebServerViewController alloc] initWithNibName:@"WebServerView" bundle:nil ] autorelease];
        webViewController.delegate = self;
        webViewController.view.frame = [self.view bounds];
        [webViewController toOpenWebWithURL:html];
        
        UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:webViewController] autorelease];
        [Utils setImageToNavigationBar:controller.navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];
        
        [self presentModalViewController:controller animated:YES];
    }else if([resourceType isEqualToString:DB_RESOURCE_TYPE_BEGINING]){
        
        self.selectedIndexPath = indexPath;
        
//        CHRichTextEditorViewController *vc = [[[CHRichTextEditorViewController alloc] initWithNibName:@"CHRichTextEditorViewController" bundle:nil] autorelease];
//        vc.delegate = self;
//        vc.view.userInteractionEnabled = YES;
//        [self.navigationController pushViewController:vc animated:YES];

        
    }else{
        NSNumber *resourceId = [infoData objectForKey:resourceId_key];
        if ( resourceId != nil) {
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
            [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
            NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
            [param setObject:resourceId forKey:resourceId_key];
            [paramDic setObject:[param JSONRepresentation] forKey:PARAMS];
            [param release];
            
            [[HttpClientUtils sharedClient] postDataWithMethod:@"POST" methodName:METHOD_RESOURCE_DOWNLOAD parameters:paramDic option:nil isShowLoading:YES delegate:self];
            
            [paramDic release];
            
        }

    }
    
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

#pragma mark - AppApiDelegate delegate
- (void)endEditCardSucessed:(NSMutableDictionary *)Info{

    NSMutableDictionary *cardData = [self.dataList objectAtIndex:[self.selectedIndexPath row]];
    [cardData setObject:[Info objectForKey:begining_key] forKey:begining_key];

    NSArray* indexPaths = [NSArray arrayWithObject:self.selectedIndexPath];
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
    
}

#pragma mark - AppApiDelegate サーバー間とのデータやり取り成功時に呼ぶ
-(void)dataOperationSuccess:(id)responseObject option:(NSDictionary *)option{
    NSString *resourceType = [responseObject objectForKey:resourceType_key];
    if ([resourceType isEqualToString:DB_RESOURCE_TYPE_PHOTO] || [resourceType isEqualToString:DB_RESOURCE_TYPE_IllUSTRATION]) {
        CardPartsDetailInfoViewController *detailViewController = [[CardPartsDetailInfoViewController alloc] init];
        detailViewController.delegate = self;
        detailViewController.resourceInfo = responseObject;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
        [detailViewController release];
        
    }
}


#pragma mark - clickEditTextButton
-(void)clickEditTextButton:(UIButton *)sender{

}

#pragma mark - getHtmlHeight
-(CGFloat)getHtmlHeight:(NSString *)htmlText{
    
    return [self.delegate getHtmlHeight:htmlText];
    
}

#pragma mark - clickOpenResourceButton
-(void)clickOpenResourceButton:(UIButton *)sender{

    UILabel *label = (UILabel *)[sender viewWithTag:RESOURCE_OPEN_RESOURCE_ID_LABEL_TAG];
    NSNumber *resourceId = [NSNumber numberWithInt:[label.text intValue]];
    
    label = (UILabel *)[sender viewWithTag:RESOURCE_OPEN_RESOURCE_TYPE_LABEL_TAG];
    NSString *resourceType = label.text;
 
    if([resourceType isEqualToString:DB_RESOURCE_PERSON_IMG]){
        //Youtube
        label = (UILabel *)[sender viewWithTag:RESOURCE_OPEN_RESOURCE_YOUTUBE_MOVE_LABEL_TAG];
        NSString *youtubeMoveId = label.text;
        
        NSString *videoUrl = [NSString stringWithFormat:YouTubeMoveImageURL,youtubeMoveId];        
        NSString* html = [Utils getVideoPlayHtml:videoUrl];
        
        WebServerViewController *webViewController = [[WebServerViewController alloc] initWithNibName:@"WebServerView" bundle:nil ];
        webViewController.delegate = self;
        webViewController.view.frame = [self.view bounds];
        [webViewController loadWebContents:html];
        
        UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:webViewController];
        [Utils setImageToNavigationBar:controller.navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];
        
        [self presentModalViewController:controller animated:YES];
        [webViewController release];
        [controller release];

    }else{
        
        label = (UILabel *)[sender viewWithTag:RESOURCE_OPEN_RESOURCE_FILE_NAME_LABEL_TAG];
        NSString *fileName = label.text;
        NSRange range1 = [ fileName rangeOfString:@"." options:NSBackwardsSearch];
		if (range1.location != NSNotFound) {
            NSString *fileNameExt = [fileName substringFromIndex :range1.location + range1.length ];
            NSString *localCashFileName = [NSString stringWithFormat:@"%d.%@",[resourceId integerValue],fileNameExt];
            [self.filenameDic setObject:localCashFileName forKey:resourceId];

            NSString *appFile = [Utils getLocalCatchfilePath:localCashFileName];
            if (appFile != nil) {
                [self readApplicationFile:localCashFileName];
                
                return;
            }
        }else {
            [self.filenameDic setObject:fileName forKey:resourceId];
  
        }
        
        
        [self dataLoadingAnimation];
        
        [[BusinessDataCtrlManager getInstance] getResourceDownloadKey:resourceId delegate:self];
    }
    
}

- (void)resourceInfoSucessed:(NSMutableDictionary *)resourceInfo{
    
    
    // confirmId   confirmKey resourceId
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[resourceInfo objectForKey:confirmId_key] forKey:confirmId_key];
    [param setObject:[resourceInfo objectForKey:confirmKey_key] forKey:confirmKey_key];
    [param setObject:[resourceInfo objectForKey:resourceId_key] forKey:resourceId_key];
    
    NSString *url = [NSString stringWithFormat:@"%@resourceId=%@&confirmId=%@&confirmKey=%@&mode=2",METHOD_GET_DOWNLOAD_RESOURCE_FILE,[resourceInfo objectForKey:resourceId_key],[resourceInfo objectForKey:confirmId_key],[resourceInfo objectForKey:confirmKey_key]];
    
    [[BusinessDataCtrlManager getInstance] getResourceFileDownload:url delegate:self filename:[self.filenameDic objectForKey:[resourceInfo objectForKey:resourceId_key]]];
  
}

- (void)readApplicationFile:(NSString *)fileName{
    
    [self.maskView removeFromSuperview];
    
    NSString *appFile = [Utils getLocalCatchfilePath:fileName];
    
    if (appFile == nil || [appFile isKindOfClass:[NSNull class]] ) {
        return;
    }
    
    WebServerViewController *webViewController = [[WebServerViewController alloc] initWithNibName:@"WebServerView" bundle:nil ];
    webViewController.delegate = self;
    webViewController.view.frame = [self.view bounds];
    [webViewController toOpenLoacalFile:fileName];
    
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:webViewController];
    [Utils setImageToNavigationBar:controller.navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];
    
	[self presentModalViewController:controller animated:YES];
    [webViewController release];
    [controller release];

}



#pragma mark LoadingAnimation
- (void)dataLoadingAnimation{
	UIWindow *mainWindow;
	mainWindow = [[UIApplication sharedApplication] keyWindow];
	self.maskView = (UIButton *)[mainWindow viewWithTag:DATA_LOADIN_ANI_MASK_VIEW_TAG];
	if (self.maskView != nil) {
		//すでにアニメーション中のため、リターンする
		return;
	}
	
	self.maskView = [[UIButton alloc] initWithFrame:FULL_MAX_RECT_SIZE];
    [self.maskView addTarget:self action:@selector(clickCancelResourceDowloadButton:) forControlEvents:UIControlEventTouchUpInside];

    //    maskView.backgroundColor = [UIColor blueColor];
	[mainWindow addSubview:self.maskView];
	
	UIImageView *animationBaseView = [[UIImageView alloc] initWithFrame:CGRectMake(135, 215, 50, 50)];
	[animationBaseView setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"loadingBase.png"]]];
	[self.maskView addSubview:animationBaseView];
	[animationBaseView release];
	
	UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, 230, 20, 20)];
	indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
	[indicator startAnimating];
	[self.maskView addSubview:indicator];
	[indicator release];
    
    [self.maskView release];
    
	
}

-(void)clickCancelResourceDowloadButton:(id)sender{
    MEActionSheet *actionSheet = [[MEActionSheet alloc] initWithTitle:NSLocalizedString(@"I0039",@"I0039")];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Yes", @"Yes") onTapped:^{
        [[BusinessDataCtrlManager getInstance] getResourceFileDownloadCancel ];
    }];
    [actionSheet setCancelButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
    [actionSheet showInView:self.view];
}

#pragma mark Delegate
-(void)connectNetWorkError{

    [self.maskView removeFromSuperview];
        
}

#pragma mark - PanGestureRecognizer

-(void)viewDidDraggingToRight:(UISwipeGestureRecognizer *)swipe{
    //    NSLog(@"========handleRightSwipeGesture========");
    if (self.currentCardIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        self.leftOrRightFlag = IS_LEFT;
        [self nextCardDetailInfo:self.currentCardIndex-1];
    }
    
}

-(void)viewDidDraggingToLeft:(UISwipeGestureRecognizer *)swipe{
    //    NSLog(@"========handleRightSwipeGesture========");
    if (self.currentCardIndex == [self.cardThumbnailList count]-1) {
        [UIView animateWithDuration:0.3f
                         animations:^{
                             CGRect rect =self.tableView.frame;
                             rect.origin.x = rect.origin.x - 20;
                             self.tableView.frame = rect;
                             
                         }
                         completion:^(BOOL finished) {
                             [UIView animateWithDuration:0.3f
                                              animations:^{
                                                  CGRect rect =self.tableView.frame;
                                                  rect.origin.x = rect.origin.x + 20;
                                                  self.tableView.frame = rect;
                                                  
                                              }
                                              completion:^(BOOL finished) {
                                              }];
                         }];
        
    }else{
        self.leftOrRightFlag = IS_RIGHT;
        [self nextCardDetailInfo:self.currentCardIndex+1];

    }
}

-(void)nextCardDetailInfo:(int)idx{
    NSMutableDictionary *carThumbnailInfo = [self.cardThumbnailList objectAtIndex:idx];
    NSMutableDictionary *cardDetailInfo = [self.cardDetailInfoList objectForKey:[carThumbnailInfo objectForKey:cardId_key]];
    if (cardDetailInfo == nil) {
        [[BusinessDataCtrlManager getInstance] getCardInfoWithCardId:[carThumbnailInfo objectForKey:cardId_key] delegate:self];

    }else {
        if (self.leftOrRightFlag == IS_LEFT) {
            self.currentCardIndex--;
        }else{
            self.currentCardIndex++;
        }
        
        self.cardInfo = [[[NSMutableDictionary alloc] initWithDictionary:cardDetailInfo] autorelease];;
        
        [self showNextCardView];

    }

}

-(void)showNextCardView{
    
    NSString *contents = [self.cardInfo objectForKey:CONTENTS];
    //NSLog(contents);
    
    self.dataList = [[[NSMutableArray alloc] init] autorelease];
    [Utils changeCardInfoDetailList:contents output:self.dataList];

    self.oldTableView = self.tableView;
    
    CGRect new_rect;
    if (self.leftOrRightFlag == IS_LEFT) {
        new_rect = CGRectMake(-SCREEN_WIDTH, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    }else{
        new_rect = CGRectMake(SCREEN_WIDTH, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    }
    
    self.tableView = [[[UITableView alloc] initWithFrame:new_rect] autorelease];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [Utils getBaseTextColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    self.tableView.tableHeaderView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)]autorelease];
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         CGRect rect =self.oldTableView.frame;
                         if (self.leftOrRightFlag == IS_LEFT) {
                             rect.origin.x = SCREEN_WIDTH;
                         }else{
                             rect.origin.x = -SCREEN_WIDTH;
                        }
                         self.oldTableView.frame = rect;
                         
                         rect =self.tableView.frame;
                         rect.origin.x = 0;
                         self.tableView.frame = rect;
                         
                     }
                     completion:^(BOOL finished) {
                         [self.oldTableView removeFromSuperview];
                         
                         [self.tableView reloadData];
                     }];

}

- (void)cardInfoSucessed:(NSMutableDictionary *)newCardInfo{
    self.cardInfo = [[[NSMutableDictionary alloc] initWithDictionary:newCardInfo] autorelease];
    [self.cardDetailInfoList setObject:self.cardInfo forKey:[newCardInfo objectForKey:cardId_key]];
    
    if (self.leftOrRightFlag == IS_LEFT) {
        self.currentCardIndex--;
    }else{
        self.currentCardIndex++;
    }
    
    [self showNextCardView];
    
    [self.delegate returnFromCardInfoDetailView:[self.cardInfo objectForKey:cardId_key]];
    
}


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
     NSString *url = [[[request URL] standardizedURL] absoluteString];
     //NSLog(@"url: %@", url);
     NSRange range1 = [[url lowercaseString] rangeOfString:@"http://"];
     NSRange range2 = [[url lowercaseString] rangeOfString:@"https://"];
     if (range1.location != NSNotFound || range2.location != NSNotFound) {
         WebServerViewController *webViewController = [[[WebServerViewController alloc] initWithNibName:@"WebServerView" bundle:nil ] autorelease];
         webViewController.delegate = nil;
         webViewController.view.frame = [self.view bounds];
         [webViewController toOpenWebWithURL:url];
         
         UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:webViewController] autorelease];
         [Utils setImageToNavigationBar:controller.navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];
         
         [self presentModalViewController:controller animated:YES];

         return FALSE;
     }
	
	return TRUE;
}


/*
 *  キーボード表示
 */
- (void)keyboardWillShow:(NSNotification *)notificatioin{
//    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
//    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//    [cell.superview bringSubviewToFront:cell];
    
//    /*
    //NSLog(@"---keyboardWillShow---");
    // キーボードに合わせたアニメーション処理
    CGRect keybord = [[notificatioin.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIViewExt *extView = (UIViewExt *)self.view;
    if (extView.tapPoint.y > (extView.bounds.size.height-keybord.size.height)) {
        [UIView animateWithDuration:0.3f
                         animations:^{
                             
                             // キーボードのサイズを取得
                             //NSLog(@"contentOffset: %@", NSStringFromCGPoint(keybord.origin));
                             //NSLog(@"contentSize: %@", NSStringFromCGSize(keybord.size));
                             
                             CGRect viewRect  = self.tableView.frame;
                             //Viewの高さ - キーボードの高さ - buttonBaseの高さを引いて、その位置にする
                             viewRect.origin.y = 0 - keybord.size.height ;
                             
                             self.tableView.frame = viewRect;
                             
                         }];

    }
//     */
}

- (void)keyboardWillHide:(NSNotification *)notificatioin{
    //NSLog(@"---keyboardWillShow---");
    // キーボードに合わせたアニメーション処理
    [UIView animateWithDuration:0.3f
                     animations:^{
                         
                         CGRect viewRect  = self.tableView.frame;
                         //Viewの高さ - キーボードの高さ - buttonBaseの高さを引いて、その位置にする
                         viewRect.origin.y = 0  ;
                         
                         self.tableView.frame = viewRect;
                         
                     }];

}


-(void)clickSaveButton:(id)sender {
    
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
    
    [[BusinessDataCtrlManager getInstance] startEditCard:param delegate:self];
    
    
}

#pragma mark - AppApiDelegate delegate
//- (void)startEditCardSucessed:(NSMutableDictionary *)Info{
//    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
//    [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
//    
//    NSString *contents = [self.cardInfo objectForKey:CONTENTS];
//    contents = [contents stringByReplacingOccurrencesOfString:self.originalText withString:[self.textView.text  stringByReplacingMatchesOfPattern:@"\n" withString:@"<br>"]];
//    [param setObject:contents forKey:CONTENTS];
//    
//    [param setObject:[self.cardInfo objectForKey:RESOURCEIDS] forKey:RESOURCEIDS];
//    
//    [[BusinessDataCtrlManager getInstance] updateCard:param delegate:self];
//    
//}
//
//- (void)updateCardSucessed:(NSMutableDictionary *)Info{
//    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
//    [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
//    
//    [[BusinessDataCtrlManager getInstance] endEditCard:param delegate:self];
//    
//}
//
//- (void)endEditCardSucessed:(NSMutableDictionary *)Info{
//    
//    NSMutableDictionary *data = [[[NSMutableDictionary alloc] init] autorelease];
//    [data setObject:DB_RESOURCE_TYPE_BEGINING forKey:resourceType_key];
//    [data setObject:self.textView.text forKey:begining_key];
//    
//    [self.delegate endEditCardSucessed:data];
//    
//    [self.navigationController popViewControllerAnimated:YES];
//    
//}
//
//
#pragma mark Memory management
- (void)dealloc {
    self.delegate = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object: nil];

    [[DownLoadSimpleWebDataServerController getInstance] clearDelegate];

	[super dealloc];
    
	
}


@end
