//
//  WorkViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/02.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "WorkViewController.h"

NSString *const reroadNotification = @"reroadNotification";

@interface WorkViewController ()

@end

@implementation WorkViewController

@synthesize inputTextButton;
@synthesize inputCameraButton;
@synthesize inputPhotoLibraryButton;
@synthesize inputIllustButton;

@synthesize selectSheetButton;
@synthesize selectCategoryButton;

@synthesize categoryColorId;
@synthesize contentsMode;
@synthesize photoCameraViewController;
@synthesize clickThumbnailCardId;
@synthesize clickCardResourceType;
@synthesize clickCardBeginining;
@synthesize fromOpenURLFlag;
@synthesize htmlCreateWebView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
 
    UISwipeGestureRecognizer *swipeRightGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToRight:)] autorelease];
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	swipeRightGesture.numberOfTouchesRequired = 1;
    swipeRightGesture.delegate = self;
	[self.view addGestureRecognizer:swipeRightGesture];

    UISwipeGestureRecognizer *swipeLeftGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToLeft:)] autorelease];
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	swipeLeftGesture.numberOfTouchesRequired = 1;
    swipeLeftGesture.delegate = self;
	[self.view addGestureRecognizer:swipeLeftGesture];
//
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;

    //HeaderView
    [Utils setImageToNavigationBar:self.navigationController.navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];
    
    UIView *titleView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, TOOL_BAR_HIGHT)] autorelease];
    self.navigationItem.titleView = titleView;

    
    self.title_1_View = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, TOOL_BAR_HIGHT)] autorelease];
    [titleView addSubview:self.title_1_View];
    
    //Settingボタンを生成
    CGFloat buttonSize = 44;
    UIButton *settingButton = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize, buttonSize)] autorelease];
    [settingButton addTarget:self action:@selector(clickLogoutButton:) forControlEvents:UIControlEventTouchUpInside];
    [settingButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_setting_normal.png"]] forState:UIControlStateNormal];
    [settingButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_setting_touch.png"]] forState:UIControlStateHighlighted];
    [self.title_1_View addSubview:settingButton];

    //Add Sheetボタンを生成
    UIButton *addSheetButton = [[[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-buttonSize*3-buttonSize/2-10, 0, buttonSize, buttonSize)] autorelease];
    [addSheetButton addTarget:self action:@selector(clickAddSheetButton:) forControlEvents:UIControlEventTouchUpInside];
    [addSheetButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_add_sheet_nomral.png"]] forState:UIControlStateNormal];
    [addSheetButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_add_sheet_touch.png"]] forState:UIControlStateHighlighted];
    [self.title_1_View addSubview:addSheetButton];

    //Add Categoryボタンを生成
    UIButton *addCategoryButton = [[[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-buttonSize*2-buttonSize/2, 0, buttonSize, buttonSize)] autorelease];
    [addCategoryButton addTarget:self action:@selector(clickAddCategoryButton:) forControlEvents:UIControlEventTouchUpInside];
    [addCategoryButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_add_category_nomral.png"]] forState:UIControlStateNormal];
    [addCategoryButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_add_category_touch.png"]] forState:UIControlStateHighlighted];
    [self.title_1_View addSubview:addCategoryButton];
     
    self.title_2_View = [[[UIView alloc] initWithFrame:CGRectMake(-TITLE_VIEW_X_DELTA, 0, SCREEN_WIDTH+TITLE_VIEW_X_DELTA, TOOL_BAR_HIGHT)] autorelease];
    //self.title_2_View.backgroundColor = [UIColor blackColor];
    UIImage *image =[UIImage imageNamed:[Utils getResourceIconPath:@"header.png"]];
    UIImageView *imageView = [[[UIImageView alloc] initWithFrame:self.title_2_View.bounds] autorelease];
    imageView.image =image;
    [self.title_2_View addSubview:imageView];

    [titleView addSubview:self.title_2_View];
    
    UIImage *shadowImage = [UIImage imageNamed:[Utils getResourceIconPath:@"base_dropshadow_short.png"]];
    UIImageView *shadowImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(-shadowImage.size.width, 0, shadowImage.size.width, self.title_2_View.bounds.size.height)] autorelease];
    shadowImageView.image =shadowImage;
    [self.title_2_View addSubview:shadowImageView];

    //Edit Sheetボタンを生成
    UIButton *editSheetButton = [[[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-buttonSize-10, 0, buttonSize, buttonSize)] autorelease];
    [editSheetButton addTarget:self action:@selector(clickUserListButton:) forControlEvents:UIControlEventTouchUpInside];
    [editSheetButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_friendlist_nomral.png"]] forState:UIControlStateNormal];
    [editSheetButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_friendlist_touch.png"]] forState:UIControlStateHighlighted];
    [self.title_2_View addSubview:editSheetButton];

    
    NSMutableDictionary *sheetInfo =  [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    [self setSheetDataContents:sheetInfo view:self.title_2_View  delegate:self];

    
    if ([Utils versionIsSeven]) {
        self.workBaseView = [[[UIView alloc] initWithFrame:self.view.bounds] autorelease];
    }else{
        self.workBaseView = [[[UIView alloc] initWithFrame:CGRectMake(0, TOOL_BAR_HIGHT, self.view.bounds.size.width, self.view.bounds.size.height-TOOL_BAR_HIGHT)] autorelease];
    }
    self.workBaseView.backgroundColor = [Utils getBaseTextColor];
    [self.view addSubview:self.workBaseView];
    
    shadowImage = [UIImage imageNamed:[Utils getResourceIconPath:@"base_dropshadow_long.png"]];
    shadowImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(-shadowImage.size.width, 0, shadowImage.size.width, self.workBaseView.bounds.size.height)] autorelease];
    shadowImageView.image =shadowImage;
    [self.workBaseView addSubview:shadowImageView];
    
    
    CGFloat buttonX = 0;
    CGFloat buttonY = 0;
    CGFloat buttonWidth = 80;
    CGFloat buttonHeight = 60;
    
    //button Base
    NSMutableDictionary *categoryInfo =  [BusinessDataCtrlManager getCategoryInfo:[BusinessDataCtrlManager getUserInfo].lastCategoryId];    
    NSNumber *categoryColoer = [Utils parseWebDataToNumber:categoryInfo dataKey:colorIdx_key];
    UIView *buttonBaseView = [[[UIImageView alloc] initWithFrame:CGRectMake(buttonX, buttonY, buttonWidth*4, buttonHeight)] autorelease];
    buttonBaseView.tag = WORK_VIEW_BUTTON_BASE_TAG;
    buttonBaseView.backgroundColor = [Utils getColor:[categoryColoer intValue]];
    //[categoryIcon setImage:[UIImage imageNamed:@"bss_side_menu_icon_favorite_nomral.png"]];
    [self.workBaseView addSubview:buttonBaseView ];

    //NSLog([categoryInfo JSONRepresentation]);
    //inputTextButton
    self.inputTextButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight)];
    [self.inputTextButton addTarget:self action:@selector(clickInputTextButton:) forControlEvents:UIControlEventTouchUpInside];
    //self.inputTextButton.backgroundColor = [UIColor redColor];
    UIImage *backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_input_text_normal.png"]];
    [self.inputTextButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    UIImage *backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_input_text_touch.png"]];
    [self.inputTextButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];    
    [self.workBaseView addSubview:self.inputTextButton];
 
    //inputCameraButton
    self.inputCameraButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX+buttonWidth, buttonY, buttonWidth, buttonHeight)];
    [self.inputCameraButton addTarget:self action:@selector(clickInputCameraButton:) forControlEvents:UIControlEventTouchUpInside];
    backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_input_cammera_normal.png"]];
    [self.inputCameraButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_input_cammera_touch.png"]];
    [self.inputCameraButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    [self.workBaseView addSubview:self.inputCameraButton];

    //inputPhotoLibraryButton
    self.inputPhotoLibraryButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX+buttonWidth*2, buttonY, buttonWidth, buttonHeight)];
    [self.inputPhotoLibraryButton addTarget:self action:@selector(clickInputPhotoLibraryButton:) forControlEvents:UIControlEventTouchUpInside];
    backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_input_photo_normal.png"]];
    [self.inputPhotoLibraryButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_input_photo_touch.png"]];
    [self.inputPhotoLibraryButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    [self.workBaseView addSubview:self.inputPhotoLibraryButton];

    //inputIllustButton
    self.inputIllustButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX+buttonWidth*3, buttonY, buttonWidth, buttonHeight)];
    [self.inputIllustButton addTarget:self action:@selector(clickInputIllustButton:) forControlEvents:UIControlEventTouchUpInside];
    backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_input_illust_normal.png"]];
    [self.inputIllustButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_input_illust_touch.png"]];
    [self.inputIllustButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    [self.workBaseView addSubview:self.inputIllustButton];
    
    CGFloat positionY=buttonBaseView.frame.origin.y+buttonBaseView.frame.size.height;
    self.scrollView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, positionY, SCREEN_WIDTH, self.workBaseView.bounds.size.height-positionY)] autorelease];
    //scrollView = [UIColor redColor];
    //scrollView.contentInset = UIEdgeInsetsZero;
    [self.scrollView setContentSize:self.workBaseView.bounds.size];
    self.scrollView.backgroundColor = [Utils getBaseTextColor];
    [self.workBaseView addSubview:self.scrollView];
    
    

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(reloadCardList) name:reroadNotification object:nil];
    

    self.htmlCreateWebView = [[[UIWebView alloc] initWithFrame:self.view.bounds] autorelease];
    self.htmlCreateWebView.delegate = self;
    NSString *path = [[NSBundle mainBundle] pathForResource: @"index" ofType: @"html"];
    [self.htmlCreateWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
    

}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    /**
       WebViewロード後にカード一覧データを取得する
     **/
    
    [[BusinessDataCtrlManager getInstance] getCardList:self startIdx:0];
}

#pragma mark - getHtmlHeight
-(CGFloat)getHtmlHeight:(NSString *)inHtmlText{
    
    NSString *htmlText = [inHtmlText stringByReplacingOccurrencesOfString:@"'" withString:@"\""];

    NSString *html = [NSString stringWithFormat:@"function execCreateHtml(){$('#work1').html('%@');};execCreateHtml();", htmlText];
    //NSLog(@"html:%@",html);
    
    [self.htmlCreateWebView stringByEvaluatingJavaScriptFromString:html];
    [self.htmlCreateWebView setNeedsDisplay];
    
    NSString *htmlHeight =  [self.htmlCreateWebView stringByEvaluatingJavaScriptFromString: @"function getHtmlheight(){return $('#work1').outerHeight(); }; getHtmlheight();"];
    
    //NSLog(@"htmlHeight:%@",htmlHeight);
    
    html = [NSString stringWithFormat:@"function execCreateHtml(){$('#work1').html('');};execCreateHtml();"];
    //NSLog(@"html:%@",html);
    
    [self.htmlCreateWebView stringByEvaluatingJavaScriptFromString:html];

    
    return [htmlHeight floatValue]+10;
    
}


#pragma mark - PanGestureRecognizer
-(void)viewDidDraggingToRight:(UISwipeGestureRecognizer *)swipe{
//    NSLog(@"========handleRightSwipeGesture========");
    [self clickSetSheetButton:nil];
    
}

-(void)viewDidDraggingToLeft:(UISwipeGestureRecognizer *)swipe{
    //    NSLog(@"========handleRightSwipeGesture========");
    if (self.workBaseView.frame.origin.x > 0) {
         [self clickSetSheetButton:nil];
    }else{
        [self clickUserListButton:nil];
    }
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - setButtonProPerty
- (void)setButtonProPerty:(UIButton *)button lineColor:(int)clolerId imgName:(NSString *)imgName label:(NSString *)labelText {
    button.backgroundColor = [UIColor redColor];
    
    button.layer.cornerRadius = 5.0f;
    button.clipsToBounds = YES;
    
    NSString *backImage = @"";
    UIImage *backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:[NSString stringWithFormat:@"%@Normal.png", backImage]]];
    [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];    
    UIImage *backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:[NSString stringWithFormat:@"%@Touched.png", backImage]]];
    [button setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    
    
    CGFloat cplorBarWidth = 5;
    UIView *colorBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cplorBarWidth, button.bounds.size.height)];
    colorBar.tag = WORK_VIEW_MAINBUTTON_COLOR_BAR_TAG;
    colorBar.backgroundColor = [Utils getColor:clolerId];
    [button addSubview:colorBar];
	[colorBar release];
    
    UIImage *photoImage = [UIImage imageNamed:imgName];
    UIImageView *photoImageView = [[UIImageView alloc] initWithImage:photoImage];
    [button addSubview:photoImageView];
	[photoImageView release];
        
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(cplorBarWidth+7, 0, button.bounds.size.width-cplorBarWidth, button.bounds.size.height)];
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [Utils getBaseTextColor];
	label.textAlignment = UITextAlignmentLeft;
	label.font = [UIFont  boldSystemFontOfSize:20];
	label.text =  NSLocalizedString(labelText, labelText);
	[button addSubview:label];
	[label release];
	
}

- (void)setListButtonProPerty:(UIButton *)button {
    //button.backgroundColor = [UIColor redColor];
    NSString *imgName = @"";
    UIImage *backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:[NSString stringWithFormat:@"%@Normal.png", imgName]]];
    [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    
    UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(button.bounds.size.width-40, (button.bounds.size.height-30)/2, 30, 30)];
    [arrowImageView setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"level_arrow_normal.png"]]];
    [button addSubview:arrowImageView];
    [arrowImageView release];
}

- (void)setSheetDataContents:(NSMutableDictionary *)inData view:(UIView *)inBaseView delegate:(id)inDelegate{
    UIView *baseView = [inBaseView viewWithTag:SHEET_LIST_CONTENTS_VIEW_TAG];
    if (baseView != nil) {
        [baseView removeFromSuperview];
    }
    
    baseView = [[[UILabel alloc] initWithFrame:inBaseView.bounds] autorelease];
    baseView.tag = SHEET_LIST_CONTENTS_VIEW_TAG;
    baseView.backgroundColor = [UIColor clearColor];
    [inBaseView addSubview:baseView];
    
    CGFloat x = 0;
    
    //Sheet Select Button
    UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake(x, 0, 44, TOOL_BAR_HIGHT)] autorelease];
    //button.backgroundColor = [UIColor blueColor];
    button.tag = SELECT_SHEET_BUTTON_TAG;
    UIImage *backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_list_nomral.png"]];
    [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    UIImage *backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_list_touch..png"]];
    [button setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(clickSetSheetButton:) forControlEvents:UIControlEventTouchUpInside];
    [inBaseView addSubview:button];
    
    //Set SheetName
    CGFloat labelWidth =inBaseView.bounds.size.width-(button.frame.origin.x+button.frame.size.width)*2;
    CGFloat positonX = (inBaseView.bounds.size.width-labelWidth)/2;
    
	NSString *sheetName = [inData objectForKey:sheetName_key];
	if (sheetName == nil || [sheetName isKindOfClass:[ NSNull  class]]) {
		sheetName=@"";
	}
    //    NSNumber *categoryCnt = [Utils parseWebDataToNumber:inData dataKey:totalCategory_key];
    //    sheetName = [NSString stringWithFormat:@"%@(%d)",sheetName,[categoryCnt intValue]];
    //    sheetName = [NSString stringWithFormat:@"%@",sheetName];
    UILabel *sheetNameLable = (UILabel *)[baseView viewWithTag:SHEET_NAME_LABEL_TAG];
    if (sheetNameLable != nil) {
        [sheetNameLable removeFromSuperview];
    }
    //NSLog(@"SheetName:%@",sheetName);
//	sheetNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(arrowImageView.frame.origin.x+arrowImageView.frame.size.width, 0, SCREEN_WIDTH-x-40, 22)] autorelease];
 	sheetNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(positonX, 4, labelWidth, 22)] autorelease];
   sheetNameLable.tag = SHEET_NAME_LABEL_TAG;
	sheetNameLable.backgroundColor = [UIColor clearColor];
	sheetNameLable.font = [UIFont  boldSystemFontOfSize:18];
	sheetNameLable.textColor = [UIColor whiteColor];
	//sheetNameLable.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
	sheetNameLable.textAlignment = UITextAlignmentCenter;
	sheetNameLable.numberOfLines = 1;
	sheetNameLable.text = sheetName;
	[baseView addSubview:sheetNameLable];
	
    //Set categoryName
    NSMutableDictionary *categoryList =[BusinessDataCtrlManager getSheetIdAndCategoryList];
    NSArray *categoryDatas = [categoryList objectForKey:[inData objectForKey:sheetId_Key]];
    NSMutableDictionary *categoryInfo = nil;
    for (int j=0; j<[categoryDatas count]; j++) {
        categoryInfo = [categoryDatas objectAtIndex:j];
        int categoryId = [[categoryInfo objectForKey:categoryId_Key] intValue];
        if ([[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue] == categoryId ) {
            break;
        }
    }

//    NSMutableDictionary *categoryInfo =  [BusinessDataCtrlManager getCategoryInfo:[BusinessDataCtrlManager getUserInfo].lastCategoryId];
	NSString *categoryName = [categoryInfo objectForKey:categoryName_key];
    //    NSNumber *cardCnt = [Utils parseWebDataToNumber:categoryInfo dataKey:totalCard_key];
//    NSNumber *categoryColoer = [Utils parseWebDataToNumber:categoryInfo dataKey:colorIdx_key];
	if (categoryName == nil || [categoryName isKindOfClass:[ NSNull  class]]) {
		categoryName=@"";
	}
    //    categoryName = [NSString stringWithFormat:@"%@(%d)",categoryName,[cardCnt intValue]];
    //categoryName = [NSString stringWithFormat:@"%@",categoryName];
    
    UILabel *categoryNameLable = (UILabel *)[baseView viewWithTag:CATEGORY_NAME_LABEL_TAG];
    if (categoryNameLable != nil) {
        [categoryNameLable removeFromSuperview];
    }
	categoryNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(positonX, sheetNameLable.frame.origin.y+sheetNameLable.frame.size.height, labelWidth, baseView.frame.size.height-(sheetNameLable.frame.origin.y+sheetNameLable.frame.size.height))] autorelease];
    categoryNameLable.tag = CATEGORY_NAME_LABEL_TAG;
	categoryNameLable.backgroundColor = [UIColor clearColor];
	categoryNameLable.font = [UIFont  systemFontOfSize:15];
//	categoryNameLable.textColor = [Utils getColor:[categoryColoer intValue]];
	categoryNameLable.textColor = [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0];;
	categoryNameLable.textAlignment = UITextAlignmentCenter;
	categoryNameLable.numberOfLines = 1;
	categoryNameLable.text = categoryName;
	[baseView addSubview:categoryNameLable];
    

}

#pragma mark - clickLogout
-(void)clickLogoutButton:(id)sender{
	UIAlertView *alert = [[UIAlertView alloc]
						  initWithTitle:NSLocalizedString(@"Information",@"Information")
						  message:NSLocalizedString(@"I0001",@"I0001")
						  delegate:self
						  cancelButtonTitle:MultiLangString(@"Yes")
						  otherButtonTitles: MultiLangString(@"No"),nil];
    [alert show];
    [alert release];
    
}

#pragma mark - clickAddSheetButton
-(void)clickAddSheetButton:(id)sender{
    CreateSheetViewController *createSheetViewController = [[[CreateSheetViewController alloc] init] autorelease];
    createSheetViewController.delegate = self;
    createSheetViewController.viewTitle = @"Sheet creation";
    createSheetViewController.operationType = SHEET_OPERATION_TYPE_NEW;
    
    UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:createSheetViewController] autorelease];
    
	[self presentModalViewController:controller animated:YES];

}

#pragma mark - clickAddCategoryButton
-(void)clickAddCategoryButton:(id)sender{
    CreateCategoryViewController *createCategoryViewController = [[[CreateCategoryViewController alloc] init] autorelease];
    createCategoryViewController.delegate = self;
    
    UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:createCategoryViewController] autorelease];
    
	[self presentModalViewController:controller animated:YES];

}

#pragma mark - clickAddSheetButton
-(void)clickUserListButton:(id)sender{
    
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId  forKey:sheetId_Key];

    [[BusinessDataCtrlManager getInstance] getSheetMemberList:param delegate:self];
    
}

#pragma mark - AppApiDelegate delegate
- (void)sheetMemberListSucessed:(NSArray *)memberList{
    UserListViewController *userListViewController = [[[UserListViewController alloc] init] autorelease];
    userListViewController.delegate = self;
    userListViewController.invitedUserList = [[[NSMutableArray alloc] initWithArray:memberList] autorelease];
    
    [self.navigationController pushViewController:userListViewController animated:YES];
    
}


//create new Category
- (void)createCategory:(NSMutableDictionary *)param{
    [[BusinessDataCtrlManager getInstance] createCategory:param delegate:self];

}

#pragma mark - main Button
-(void)clickInputTextButton:(id)sender{
    InputTextViewController *inputTextViewController = [[[InputTextViewController alloc] init] autorelease];
    inputTextViewController.delegate = self;
    inputTextViewController.categoryColorId = self.categoryColorId;
    
    UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:inputTextViewController] autorelease];
    
	[self presentModalViewController:controller animated:YES];
    
}

-(void)clickInputIllustButton:(id)sender{
    IllustSelectViewController *illustSelectViewController = [[IllustSelectViewController alloc] init];
    illustSelectViewController.delegate = self;
    
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:illustSelectViewController];
    
	[self presentModalViewController:controller animated:YES];
    
    [illustSelectViewController release];
    [controller release];

}

-(void)clickInputCameraButton:(id)sender{
    
    self.contentsMode = CONTENTS_MODE_CAMERA;

    self.photoCameraViewController = [[PhotoCameraViewController alloc] init] ;
    self.photoCameraViewController.delegate = self;
    [self.photoCameraViewController startUIImagePickerController];

}

-(void)clickInputPhotoLibraryButton:(id)sender{
    
    self.contentsMode = CONTENTS_MODE_PHOTOLIBRARY;
    
    PhotoGroupViewController *photoGroupViewController = [[PhotoGroupViewController alloc] init];
    photoGroupViewController.delegate = self;
   
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:photoGroupViewController];
    controller.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    

	[self presentModalViewController:controller animated:YES];
    
    [photoGroupViewController release];
    [controller release];

}

#pragma mark - Select Sheet
-(void)clickSetSheetButton:(id)sender{
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         CGFloat positionX = SCREEN_WIDTH-44;
                         CGRect rect =self.workBaseView.frame;
                         CGRect rect2 = self.title_2_View.frame;
                        if (rect.origin.x == 0) {
                             rect.origin.x = positionX;
                             rect2.origin.x = positionX-TITLE_VIEW_X_DELTA/2-1;
                         }else{
                             rect.origin.x = 0;
                             rect2.origin.x = -TITLE_VIEW_X_DELTA;
                         }
                         self.workBaseView.frame = rect;
                         self.title_2_View.frame = rect2;
                         
                    }
                     completion:^(BOOL finished) {
                         CGRect rect =self.workBaseView.frame;
                         if (rect.origin.x == 0) {
                             [self.dummyButton removeFromSuperview];
                         }else{
                             [self resetDummyButton];
                         }
                     }];

}

-(void)resetDummyButton
{
    [self.dummyButton removeFromSuperview];
    
    self.dummyButton = [[[UIButton alloc] initWithFrame:self.workBaseView.bounds] autorelease];
    
    [self.dummyButton addTarget:self action:@selector(clickSetSheetButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.workBaseView addSubview:self.dummyButton];

}

-(void)selectedNewCategoryExt{
    [UIView animateWithDuration:0.3f
                     animations:^{
                         CGFloat positionX = SCREEN_WIDTH-44;
                         CGRect rect =self.workBaseView.frame;
                         CGRect rect2 = self.title_2_View.frame;
                         if (rect.origin.x == 0) {
                             rect.origin.x = positionX;
                             rect2.origin.x = positionX-TITLE_VIEW_X_DELTA/2-1;
                         }else{
                             rect.origin.x = 0;
                             rect2.origin.x = -TITLE_VIEW_X_DELTA;
                         }
                         self.workBaseView.frame = rect;
                         self.title_2_View.frame = rect2;
                         
                     }
                     completion:^(BOOL finished) {
                         [self.dummyButton removeFromSuperview];
                     }];

}

#pragma mark - WorkDataControllerDelegate Delegate
- (void)newSheetIDAndCategoryID:(int)sheetId categoryId:(int)categoryId{
    //最新データ反映
    NSMutableDictionary *sheetInfo =  [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
     if (sheetInfo != nil) {
        [self setSheetDataContents:sheetInfo view:self.title_2_View  delegate:self];
    }
    
    NSMutableDictionary *info =[[NSMutableDictionary alloc] init];
    [[BusinessDataCtrlManager getUserInfo] toCreateDictionaryValues:info];
    [[DataRegistry getInstance] updateSheetIdAndCategoryId:info];
    [info release];
  
    
    NSMutableDictionary *categoryList =[BusinessDataCtrlManager getSheetIdAndCategoryList];
    NSArray *categoryDatas = [categoryList objectForKey:[sheetInfo objectForKey:sheetId_Key]];
    NSMutableDictionary *categoryInfo = nil;
    for (int j=0; j<[categoryDatas count]; j++) {
        categoryInfo = [categoryDatas objectAtIndex:j];
        int categoryId = [[categoryInfo objectForKey:categoryId_Key] intValue];
        if ([[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue] == categoryId ) {
            break;
        }
        
    }
    NSNumber *categoryColoer = [Utils parseWebDataToNumber:categoryInfo dataKey:colorIdx_key];
    [self resetMainButtonColorBar:[categoryColoer intValue]];
    
    //CardDataをクリア
    [[BusinessDataCtrlManager getCardList] removeAllObjects];

    //Card一覧を再取得する
    [[BusinessDataCtrlManager getInstance] getCardList:self startIdx:0];


}


-(void)resetMainButtonColorBar:(int)colorIdx{
    UIView *buttonBaseView = [self.workBaseView viewWithTag:WORK_VIEW_BUTTON_BASE_TAG];
    buttonBaseView.backgroundColor = [Utils getColor:colorIdx];
        
}
#pragma mark - WorkDataControllerDelegate Delegate
- (void)photoSelectConfirm:(NSMutableArray *)dataList{
    //バッググラウンドでImageデータを作ってサーバーに送る 
    [Utils sendDataToLocalServer:[dataList JSONRepresentation]];

    
    if (self.contentsMode == CONTENTS_MODE_CAMERA) {
        [self.photoCameraViewController release];
        self.photoCameraViewController = nil;
    }else if(self.contentsMode == CONTENTS_MODE_PHOTOLIBRARY){
        [self dismissModalViewControllerAnimated:YES];
        
    }

}

-(void)reloadCardList{
    [self newCategoryID:[[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue]];
}

- (void)cardListSucessed{
    
    [super cardListSucessed];
    
    
    UIImage *shadowImage = [UIImage imageNamed:[Utils getResourceIconPath:@"back_image_gradation.png"]];
    CGFloat startHeight = shadowImage.size.height ;
    
    UIView *baseView = [self.workBaseView viewWithTag:CARD_THUMBNAIL_BASE_VIEW_TAG];
    [baseView removeFromSuperview];
    
    baseView = [[[UIView alloc] initWithFrame:self.scrollView.bounds] autorelease];
    //baseView = [[[UIView alloc] initWithFrame:self.workBaseView.bounds] autorelease];
    baseView.tag = CARD_THUMBNAIL_BASE_VIEW_TAG;
    CGRect rect = baseView.frame;
    rect.origin.y = startHeight;
    baseView.frame = rect;
    //baseView.backgroundColor = [UIColor redColor];
    [self.scrollView addSubview:baseView ];
    
    NSMutableDictionary *categoryInfo =  [BusinessDataCtrlManager getCategoryInfo:[BusinessDataCtrlManager getUserInfo].lastCategoryId];
    NSMutableArray *cardList =[BusinessDataCtrlManager getCardList];

    if ([cardList count] < CARD_ROW_COUNT) {
        [categoryInfo setObject:[NSNumber numberWithInt:[cardList count]] forKey:totalCard_key];
        
        //Cateogry 情報を更新（カード件数）
        UIView *categoryView = [self.workBaseView viewWithTag:SELECT_CATEGORY_BUTTON_TAG];
        UILabel *nameLabel = (UILabel *)[categoryView viewWithTag:CATEGORY_NAME_LABEL_TAG];
        NSString *categoryName = [categoryInfo objectForKey:categoryName_key];
        NSString *categoryNameText = [NSString stringWithFormat:@"%@(%d)",categoryName,[cardList count]];
        nameLabel.text = categoryNameText;

    }


    CGFloat cardWidth = CARD_WIDTH;
    CGFloat cardHeight = CARD_HEIGHT;
    
    NSNumber *categoryColoer = [Utils parseWebDataToNumber:categoryInfo dataKey:colorIdx_key];

    for (int i=0; i<[cardList count] && i<=SHOW_CARD_LIST_INIT_COUNT; i++) {
        NSMutableDictionary *cardInfo = [cardList objectAtIndex:i];
        UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-cardWidth)/2, (cardHeight+THUMBNAIL_LIST_DELTA)*i, cardWidth, cardHeight)] autorelease];
        NSNumber *cardId = [cardInfo objectForKey:cardId_key];
        button.tag = [cardId intValue];
        [button addTarget:self action:@selector(clickThumbnailButton:) forControlEvents:UIControlEventTouchUpInside];
        button.backgroundColor = [UIColor whiteColor];
        [baseView addSubview:button];
        
        [Utils setCardListDataContents:cardInfo view:button colorBar:[categoryColoer intValue] delegate:self];
      
        rect = baseView.frame;
        rect.size.height = (cardHeight+THUMBNAIL_LIST_DELTA)*(i+1);
        baseView.frame = rect;

        startHeight = startHeight + cardHeight+THUMBNAIL_LIST_DELTA;
                
      }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width, startHeight+65);

    
    CGFloat moreButtonHeight=30;
    //CGFloat moreButtonWidth=260;
    if ([cardList count] > SHOW_CARD_LIST_INIT_COUNT+1) {
         UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-CARD_WIDTH)/2, (cardHeight+THUMBNAIL_LIST_DELTA)*(SHOW_CARD_LIST_INIT_COUNT+1), CARD_WIDTH, moreButtonHeight)] autorelease];
        [button addTarget:self action:@selector(clickMoreButton:) forControlEvents:UIControlEventTouchUpInside];
        [Utils addCommonButtonProPerty:button imgName:@"" label:@"More"];
        //button.backgroundColor = [UIColor whiteColor];
        [baseView addSubview:button];
         
        rect = baseView.frame;
        rect.size.height = (cardHeight+THUMBNAIL_LIST_DELTA)*(SHOW_CARD_LIST_INIT_COUNT+1)+moreButtonHeight+THUMBNAIL_LIST_DELTA;
        baseView.frame = rect;

         self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width, self.scrollView.contentSize.height+moreButtonHeight+THUMBNAIL_LIST_DELTA);
        
    }
    
    [self.scrollView setContentOffset:CGPointMake(0.0f, 0.0f) animated:NO];
    
    UIView *backView = [self.workBaseView viewWithTag:WORK_VIEW_BASE_BACK_VIEW_TAG];
    rect = backView.frame;
    rect.size.height = self.scrollView.contentSize.height;
    backView.frame = rect;
    
    
    if (self.fromOpenURLFlag) {
        self.fromOpenURLFlag = FALSE;
        
        //起動時の１回のみ実行する
        NSMutableDictionary *option = [[BusinessDataCtrlManager getInstance] getGlobalOptionData] ;
        
        NSString *cardId = [option objectForKey:@"card_id"];
        if (cardId != nil) {
            self.clickThumbnailCardId = [NSNumber numberWithInt:[cardId intValue]];
            self.clickCardResourceType = DB_RESOURCE_TYPE_PHOTO;
            [[BusinessDataCtrlManager getInstance] getCardInfoWithCardId:self.clickThumbnailCardId delegate:self];
        }
    }
}


- (void)clickScrollToTopButton:(UIButton *)button{
    UIScrollView *scrollView = (UIScrollView *)self.workBaseView;
    [scrollView setContentOffset:CGPointMake(0, -TOOL_BAR_HIGHT) animated:YES];
    
        
}

- (void)clickThumbnailButton:(UIButton *)button{
    //NSLog(@"-------clickThumbnailButton---------");
   
    self.clickedCardThumbnailView = button;
    self.clickThumbnailCardId = [NSNumber numberWithInt:button.tag];
   [[BusinessDataCtrlManager getInstance] getCardInfoWithCardId:self.clickThumbnailCardId delegate:self];
    
}

- (void)cardInfoSucessed:(NSMutableDictionary *)cardInfo{
    
    UIView *readStatusView = [self.clickedCardThumbnailView viewWithTag:READ_STATUS_TAG];
    [readStatusView removeFromSuperview];
    
    NSString *contents = [cardInfo objectForKey:CONTENTS];
    //NSLog(contents);

    NSMutableArray *cardInfoDetailList = [[[NSMutableArray alloc] init] autorelease];
    [Utils changeCardInfoDetailList:contents output:cardInfoDetailList];
    
    CardDetailViewController *cardDetailViewController = [[[CardDetailViewController alloc] init] autorelease];
    cardDetailViewController.delegate = self;
    [cardDetailViewController.dataList addObjectsFromArray:cardInfoDetailList];
    cardDetailViewController.cardInfo = cardInfo;
    [self.navigationController pushViewController:cardDetailViewController animated:YES];
    
 
}

-(void)returnFromCardInfoDetailView:(NSNumber *)cardId{
    UIView *buttonBaseView = [self.scrollView viewWithTag:CARD_THUMBNAIL_BASE_VIEW_TAG];
    NSArray *subViews = [buttonBaseView subviews];
    for (UIView *subView in subViews ) {
        if ([subView isKindOfClass:[UIButton class]]) {
             if (subView.tag == [cardId intValue]) {
                UIView *readStatusView = [subView viewWithTag:READ_STATUS_TAG];
                [readStatusView removeFromSuperview];
            }
        }
    }
}

- (void)createNewSheetOrUpdateSheetSucessed:(NSMutableDictionary *)Info{
    
    [super initWorkData];
    
    [self.tableView reloadData];
    
    [self selectedNewCategoryExt];
    
    //NaivicationBar
    NSMutableDictionary *sheetInfo =  [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    if (sheetInfo != nil) {
        [self setSheetDataContents:sheetInfo view:self.title_2_View  delegate:self];
    }
    
    //MainButtonエリア
    NSMutableDictionary *categoryList =[BusinessDataCtrlManager getSheetIdAndCategoryList];
    NSArray *categoryDatas = [categoryList objectForKey:[sheetInfo objectForKey:sheetId_Key]];
    NSMutableDictionary *categoryInfo = nil;
    for (int j=0; j<[categoryDatas count]; j++) {
        categoryInfo = [categoryDatas objectAtIndex:j];
        int categoryId = [[categoryInfo objectForKey:categoryId_Key] intValue];
        if ([[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue] == categoryId ) {
            break;
        }
        
    }
    NSNumber *categoryColoer = [Utils parseWebDataToNumber:categoryInfo dataKey:colorIdx_key];
    [self resetMainButtonColorBar:[categoryColoer intValue]];

    
    [[BusinessDataCtrlManager getInstance] getCardList:self startIdx:0];

}

- (void)clickMoreButton:(UIButton *)button{
    //NSLog(@"-------clickMoreButton---------");
    CardListViewController *cardListViewController = [[CardListViewController alloc] init];
    cardListViewController.delegate = self;
    cardListViewController.dataList = [BusinessDataCtrlManager getCardList];
    cardListViewController.lastPosition = [cardListViewController.dataList count];
    cardListViewController.title = [NSString stringWithFormat:@"%@(%d)", MultiLangString(@"Card List") ,[cardListViewController.dataList count]];
    [self.navigationController pushViewController:cardListViewController animated:YES];
    [cardListViewController release];

}

- (void)photoSelectCancel {
//	[delegate photoSelectCancel];
    if (self.photoCameraViewController != nil) {
        [self.photoCameraViewController release];
        self.photoCameraViewController = nil;
    }

}


#pragma mark - alert Delegate
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0:
			//Yes
			;
            
            [[BusinessDataCtrlManager getInstance] logOut];
            
            [self.delegate logOut];
            
            [self dismissModalViewControllerAnimated:NO];
            
			break;
		case 1:
			//No;
			;
            
            
			break;
	}
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
     return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

#pragma mark - Memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
 	[self.inputTextButton release];
 	[self.inputCameraButton release];
 	[self.inputPhotoLibraryButton release];
 	[self.inputIllustButton release];

 	[self.selectSheetButton release];
	[self.selectCategoryButton release];
    
    [super dealloc];
}



@end
