//
//  CommentListViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/07/30.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "CommentListViewController.h"

@interface CommentListViewController ()

@end

@implementation CommentListViewController
@synthesize delegate;
@synthesize dataList;
@synthesize cardId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    //HeaderView
    [Utils setImageToNavigationBar:self.navigationController.navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];

    //キャンセルボタンを生成
    [Utils setNavigationBarLeftButton:self.navigationController.navigationBar navigationItem:self.navigationItem controller:self];


    //Add WriteButton
    [Utils setNavigationBarRightWriteButton:self.navigationItem controller:self];

	// Do any additional setup after loading the view.
}

#pragma mark - NavicationBar CancelButton Call Method
-(void)clickCancelButton:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(resetCommnetCount:)]) {
 		[self.delegate resetCommnetCount:[self.dataList count]];
	}

    [self dismissModalViewControllerAnimated:YES];
    
}

#pragma mark - NavicationBar SaveButton Call Method
-(void)clickWriteButton:(id)sender {
    InputCommentViewController *inputCommentViewController = [[[InputCommentViewController alloc] init] autorelease];
    inputCommentViewController.delegate = self;
    inputCommentViewController.cardId = self.cardId;
    inputCommentViewController.commnetInfo = nil;
    
    UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:inputCommentViewController] autorelease ];

    [self presentModalViewController:controller animated:YES];
 
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dataList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
    NSMutableDictionary *info = [self.dataList objectAtIndex:[indexPath row]];
    NSString *commentDetail = [info objectForKey:content_key];
	if (commentDetail == nil || [commentDetail isKindOfClass:[ NSNull  class]]) {
		commentDetail=@"";
	}
    CGSize size = [commentDetail sizeWithFont:[UIFont boldSystemFontOfSize:CommentDetailSize] constrainedToSize:CGSizeMake(tableView.bounds.size.width-70, tableView.bounds.size.height*3) lineBreakMode:NSLineBreakByWordWrapping];
    
    NSString *toUserName = [Utils parseWebDataToString:info dataKey:toUserName_key];
    if (![toUserName isEqualToString:@""]) {
        return 74+size.height;

    }else{
        return 54+size.height;

    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    static NSString *CellIdentifier = @"Cell";

    NSMutableDictionary *info = [self.dataList objectAtIndex:[indexPath row]];
    NSNumber *commentId = [info objectForKey:commentId_key];

    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d",[commentId intValue]] ;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
     */
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        CGRect rect = cell.contentView.frame;
        rect.size.height = CATEGORY_LIST_HEIGHT;
        cell.contentView.frame = rect;
        
        // Configure the cell...
        // Configure the cell...
        UIImageView *coverImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
        [coverImg setImage: [UIImage imageNamed:[Utils getResourceIconPath:@"defaultProfImg.png"]]];
        //[[DownLoadThumbnailImageServerController getInstance] getImageData:shadowImg];
        [cell.contentView addSubview:coverImg ];
        [coverImg release];
        NSNumber *userId = [info objectForKey:userId_key];
        if (userId != nil) {
            [Utils setUserIconWith:[userId intValue] imageView:coverImg];
         }
        
        CGFloat y = 14;
        NSString *authName = [Utils parseWebDataToString:info dataKey:userName_Key];
        UILabel *authNameLable = [[UILabel alloc] initWithFrame:CGRectMake(70, y, tableView.bounds.size.width-70, 20)];
        authNameLable.backgroundColor = [UIColor clearColor];
        authNameLable.font = [UIFont  boldSystemFontOfSize:CommentAuthNameSize];
        authNameLable.textColor = [UIColor blackColor];
        authNameLable.textAlignment = UITextAlignmentLeft;
        authNameLable.numberOfLines = 1;
        authNameLable.text = authName;
        [cell.contentView addSubview:authNameLable];
        [authNameLable release];
  
        y+=20;
        
        NSString *toUserName = [Utils parseWebDataToString:info dataKey:toUserName_key];
        if (![toUserName isEqualToString:@""]) {
            UILabel *toUserNameLable = [[UILabel alloc] initWithFrame:CGRectMake(70, y, tableView.bounds.size.width-70, 20)];
            toUserNameLable.backgroundColor = [UIColor clearColor];
            toUserNameLable.font = [UIFont  boldSystemFontOfSize:CommentDetailSize];
            toUserNameLable.textColor = [UIColor blueColor];
            toUserNameLable.textAlignment = UITextAlignmentLeft;
            toUserNameLable.numberOfLines = 1;
            toUserNameLable.text = [NSString stringWithFormat:@"to %@",toUserName];
            [cell.contentView addSubview:toUserNameLable];
            [toUserNameLable release];
            
            y+= 20;
        }

        NSString *regsitTime = [Utils parseWebDataToString:info dataKey:updateDate_key];
        UILabel *registTimeLable = [[UILabel alloc] initWithFrame:CGRectMake(70, y, tableView.bounds.size.width-70, 16)];
        registTimeLable.backgroundColor = [UIColor clearColor];
        registTimeLable.font = [UIFont  systemFontOfSize:CommentDetailSize];
        registTimeLable.textColor = [UIColor grayColor];
        registTimeLable.textAlignment = UITextAlignmentLeft;
        registTimeLable.numberOfLines = 1;
        registTimeLable.text = regsitTime;
        [cell.contentView addSubview:registTimeLable];
        [registTimeLable release];
        y+= 16;
        
        y+= 4;
    
        NSString *commentDetail = [Utils parseWebDataToString:info dataKey:content_key];
        CGSize size = [commentDetail sizeWithFont:[UIFont boldSystemFontOfSize:CommentDetailSize] constrainedToSize:CGSizeMake(tableView.bounds.size.width-70, tableView.bounds.size.height*3) lineBreakMode:NSLineBreakByWordWrapping];
        
        UILabel *commentDetailLable = [[UILabel alloc] initWithFrame:CGRectMake(authNameLable.frame.origin.x, y, tableView.bounds.size.width-70, size.height)];
        commentDetailLable.backgroundColor = [UIColor clearColor];
        commentDetailLable.font = [UIFont  systemFontOfSize:CommentDetailSize];
        commentDetailLable.textColor = [UIColor blackColor];
        commentDetailLable.textAlignment = UITextAlignmentLeft;
        commentDetailLable.numberOfLines = size.height/CommentDetailSize;
        commentDetailLable.text = commentDetail;
        [cell.contentView addSubview:commentDetailLable];
        [commentDetailLable release];
        
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
    
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     NSMutableDictionary *info = [self.dataList objectAtIndex:[indexPath row]];
    
    InputCommentViewController *inputCommentViewController = [[[InputCommentViewController alloc] init] autorelease];
    inputCommentViewController.delegate = self;
    inputCommentViewController.cardId = self.cardId;
    inputCommentViewController.commnetInfo = info;
    
    UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:inputCommentViewController] autorelease ];
    
    [self presentModalViewController:controller animated:YES];
    */
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark - WorkDataControllerDelegate Delegate
- (void)newCommnetSucessed{

    [[BusinessDataCtrlManager getInstance] getCommentList:self.cardId delegate:self];
    
}

- (void)commentListSucessed:(NSArray *)commentList{
    self.dataList = [[NSMutableArray alloc] initWithArray:commentList];
    
    [self.tableView reloadData];

    
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
