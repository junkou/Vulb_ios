//
//  SheetListExtViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/09/27.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "SheetListExtViewController.h"

@interface SheetListExtViewController ()

@end

@implementation SheetListExtViewController

@synthesize delegate;
@synthesize tableView = _tableView;

@synthesize currentSheetId;
@synthesize categoryLists;
@synthesize sheetList;

#pragma mark - lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.sheetList = [[[NSMutableArray alloc] init] autorelease];
        self.openedSectionIds =[[[NSMutableArray alloc] init] autorelease];
        self.sectionOpenedFlag = TRUE;
        
        [self initWorkData];

     }
    return self;
}

-(void) initWorkData{
    self.sheetList = [[[NSMutableArray alloc] initWithArray:[BusinessDataCtrlManager getSheetList]] autorelease];
    
    self.categoryLists = [[[NSMutableDictionary alloc] init] autorelease];
    NSMutableArray *list = [[[NSMutableArray alloc] initWithArray:[BusinessDataCtrlManager getCategoryList]] autorelease];
    [self.categoryLists setObject:list forKey:[BusinessDataCtrlManager getUserInfo].lastSheetId];

    self.categoryStatisticsLists = [[[NSMutableDictionary alloc] init] autorelease];
    NSMutableArray *csList = [BusinessDataCtrlManager getGlobalCategoryStatisticsList];
    [self.categoryStatisticsLists setObject:csList forKey:[BusinessDataCtrlManager getUserInfo].lastSheetId];

    self.currentSheetId = [[BusinessDataCtrlManager getUserInfo].lastSheetId intValue];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([Utils versionIsSeven]) {
        self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-TOOL_BAR_HIGHT-20)] autorelease];
    }else{
        self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, TOOL_BAR_HIGHT, self.view.bounds.size.width, self.view.bounds.size.height-TOOL_BAR_HIGHT)] autorelease];
    }

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];

}

-(int)getCurrentSheetRowNum{
    int sheetRowNumber = 0;
    for (int i=0; i<[self.sheetList count]; i++) {
        NSMutableDictionary *sheetInfo =[self.sheetList objectAtIndex:i];
        NSNumber *sheetId = [sheetInfo objectForKey: sheetId_Key];
        if ([sheetId intValue] == self.currentSheetId ) {
            sheetRowNumber = i;
            break;
        }
    }

    return sheetRowNumber;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    int sectionNum = [self getCurrentSheetRowNum];
    
    [self.openedSectionIds addObject:[NSNumber numberWithInt:sectionNum]];
    

}

- (void)viewDidAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    
    int categoryRow = 0;
    int sectionNum = [self getCurrentSheetRowNum];
    NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:sectionNum];
    NSArray *categoryDatas = [self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key]];
    for (int j=0; j<[categoryDatas count]; j++) {
        NSMutableDictionary *categoryData = [categoryDatas objectAtIndex:j];
        int categoryId = [[categoryData objectForKey:categoryId_Key] intValue];
        if ([[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue] == categoryId ) {
            categoryRow = j;
            break;
        }
    }
    
    if (categoryRow == 0 && sectionNum == 0) {
        CGPoint point =CGPointMake(0, 0);
        [self.tableView setContentOffset:point animated:YES];
    }else{
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:categoryRow inSection:sectionNum];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];

    }
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)getSectionHeaderHeight:(NSMutableDictionary *)infoData {
    NSNumber *newCardCount = [infoData objectForKey:newCardCount_key];
    NSNumber *updateCardCount = [infoData objectForKey:upCardCount_key];
    if ( ( newCardCount != nil && [newCardCount intValue] > 0 )
        || (updateCardCount != nil && [updateCardCount intValue] > 0) ) {
        return SHEET_LIST_LARGE_HEIGHT;
    }else{
        return SHEET_LIST_HEIGHT;
    }
    
}


-(CGFloat)getCellHeaderHeight:(NSMutableDictionary *)infoData {
    NSNumber *newCardCount = [infoData objectForKey:newCardCount_key];
    NSNumber *updateCardCount = [infoData objectForKey:updateCardCount_key];
    if ( ( newCardCount != nil && [newCardCount intValue] > 0 )
        || (updateCardCount != nil && [updateCardCount intValue] > 0) ) {
        return SHEET_LIST_LARGE_HEIGHT;
    }else{
        return SHEET_LIST_HEIGHT;
    }

}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sheetList count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.sheetList objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	//NSLog(@"====indexPath:%d",indexPath.row);
     NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:section];
    return  [self getSectionHeaderHeight:sheetData ];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSNumber *secNum = [NSNumber numberWithInt:section];
    if ([self.openedSectionIds containsObject:secNum]) {
        NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:section];
        int cnt = [[self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key] ] count];
        return cnt;
    }else{
        return 0;
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:section];
    
    CGFloat sheetHeight = [self getSectionHeaderHeight:sheetData ];
    
    UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, sheetHeight)] autorelease];
    button.tag =section;
    [button addTarget:self action:@selector(clickSelectSheetButton:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor whiteColor];
    UIView *lineView = [[[UIView alloc] initWithFrame:CGRectMake(0, sheetHeight-0.5, SCREEN_WIDTH, 0.5)] autorelease];
    lineView.backgroundColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:0.4];
    [button addSubview:lineView];
    
    NSString *openIcon = @"icon_close_normal.png";
    for (int i=0; i<[self.openedSectionIds count]; i++) {
        if ([[self.openedSectionIds objectAtIndex:i] intValue] == section) {
            openIcon = @"icon_open_normal.png";
        }
    }

    NSString *sampleSheetFlag = [Utils parseWebDataToString:sheetData dataKey:sampleSheetFlg_key];
    if ([sampleSheetFlag isEqualToString:@""]) {
        sampleSheetFlag = @"0";
    }

    [Utils setSheetDataContents:sheetData sampleSheetFlag:sampleSheetFlag view:button iconImage:openIcon delegate:self];
    
    return button;
}

-(void)clickSelectSheetButton:(UIButton *)sender{
    
    int sectionNum = [self getCurrentSheetRowNum];
    if (sectionNum == sender.tag ) {
 		NSNumber *secNum = [NSNumber numberWithInt:sender.tag];
        if ([self.openedSectionIds containsObject:secNum]) {
            [self.openedSectionIds removeObject:secNum];
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sectionNum] withRowAnimation:UITableViewRowAnimationNone];
            
            return;

        }
    }
    

    
    NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:sender.tag];

    self.clickedSectionRow =sender.tag;
    
    self.currentSheetId = [[sheetData objectForKey:sheetId_Key] intValue];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:[sheetData objectForKey:sheetId_Key] forKey:sheetId_Key];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    self.methodName = METHOD_CATEGORY_LIST;
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];

    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
    // セクションを取得する
    NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:indexPath.section];
    
    // セクション名をキーにしてそのセクションの項目をすべて取得
    NSArray *items = [self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key]];
    NSMutableDictionary *categoryData = [items objectAtIndex:indexPath.row];
    
	return [self getCellHeaderHeight:categoryData ];
}


- (UITableViewCell *)tableView:(UITableView *)loacltableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //static NSString *CellIdentifier = @"Cell";
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d_%d",indexPath.section, [indexPath row]] ;
    
    UITableViewCell *cell = [loacltableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
     */
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        // Configure the cell...
        // セクションを取得する
        NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:indexPath.section];
        
        // セクション名をキーにしてそのセクションの項目をすべて取得
        NSArray *items = [self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key]];
        NSMutableDictionary *categoryData = [items objectAtIndex:indexPath.row];

        CGRect rect = cell.contentView.frame;
        rect.size.height = [self getCellHeaderHeight:categoryData];
        cell.contentView.frame = rect;

        
    }
    
    [self setCellContent:cell IndexPath:indexPath];

    
    return cell;
}

-(void)setCellContent:(UITableViewCell *)cell IndexPath:(NSIndexPath *)indexPath{
    // セクションを取得する
    NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:indexPath.section];
    
    // セクション名をキーにしてそのセクションの項目をすべて取得
    NSArray *items = [self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key]];
    NSMutableDictionary *categoryData = [items objectAtIndex:indexPath.row];
    
    BOOL checkedFlag = FALSE;
    NSNumber *localCategoryId = [categoryData objectForKey:categoryId_Key];
    if ([[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue] == [localCategoryId intValue]  ) {
        if (self.beforeIndexPath == nil ) {
            //最初はbeforeIndexPathがヌルのため、ここで設定する
            self.clickedindexPath = indexPath;
        }
        checkedFlag = TRUE;
        
    }
    
    NSMutableArray *categoryStatusList = [self.categoryStatisticsLists objectForKey:[sheetData objectForKey:sheetId_Key]];
    NSMutableDictionary *categoryStatus = nil;
    for (NSMutableDictionary *info in categoryStatusList) {
        if ([localCategoryId intValue] == [[info objectForKey:categoryId_Key] intValue] ) {
            categoryStatus = info ;
            break;
        }
    }

    //NSLog(@"height:%f",cell.contentView.bounds.size.height);
    NSString *sampleSheetFlag = [Utils parseWebDataToString:sheetData dataKey:sampleSheetFlg_key];
    if ([sampleSheetFlag isEqualToString:@""]) {
        sampleSheetFlag = @"0";
    }

    [Utils setCategoryDataContents:categoryData status:categoryStatus checked:checkedFlag sampleSheetFlag:sampleSheetFlag view:cell.contentView];
    

}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)loacltableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // セクションを取得する
    NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:indexPath.section];
    
    // セクション名をキーにしてそのセクションの項目をすべて取得
    NSMutableArray *items = [self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key]];
    NSMutableDictionary *categoryData = [items objectAtIndex:indexPath.row];

    
    //すべてのカテゴリーのカード一覧を見る場合
    id allCategoryFlag = [categoryData objectForKey:ALL_CATEGORY_SHOW_FLAG_KEY];
    if (allCategoryFlag != nil) {
        
        AllCardListViewController *allCardListViewController = [[[AllCardListViewController alloc] init] autorelease];
        allCardListViewController.delegate = self;
        
        NSMutableArray *categoryList = [[[NSMutableArray alloc] initWithArray:items] autorelease];
        
        //すべてのカテゴリーのカード一覧選択用データは削除しておく
        [categoryList removeObjectAtIndex:0];
        allCardListViewController.categoryList = categoryList;
        
        NSMutableDictionary *categoryData = [categoryList objectAtIndex:0];
        allCardListViewController.currentCategoryId = [[categoryData objectForKey:categoryId_Key] intValue];
        allCardListViewController.title = [sheetData objectForKey:sheetName_key];
        
        [self.navigationController pushViewController: allCardListViewController animated:YES];
        
        [loacltableView deselectRowAtIndexPath:indexPath animated:YES];

        return;
    }

    
    //old IndexPath
    self.beforeIndexPath = self.clickedindexPath;
    

    //new IndexPath
    self.clickedindexPath = indexPath;
    
    
    int currentCategoryId = [[categoryData objectForKey:categoryId_Key] intValue];
    
    [BusinessDataCtrlManager getUserInfo].lastSheetId = [sheetData objectForKey:sheetId_Key];
    [BusinessDataCtrlManager getUserInfo].lastCategoryId = [NSNumber numberWithInt:currentCategoryId];
    
    self.currentSheetId = [[sheetData objectForKey:sheetId_Key] intValue];
    
    
    //old Cell
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.beforeIndexPath];
    [self setCellContent:cell IndexPath:self.beforeIndexPath];
    [self.tableView setNeedsDisplay];

    [self newCategoryID:currentCategoryId];
    
    [self selectedNewCategoryExt];
    
    [loacltableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
}

-(void)selectedNewCategoryExt{
    
}


#pragma mark - WorkDataControllerDelegate Delegate
- (void)newCategoryID:(int)categoryId{
    
    [[BusinessDataCtrlManager getCategoryList] removeAllObjects];
    NSArray *categoryDatas = [self.categoryLists objectForKey:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    [[BusinessDataCtrlManager getCategoryList] addObjectsFromArray:categoryDatas];
    
    [[BusinessDataCtrlManager getSheetIdAndCategoryList] removeObjectForKey:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    [[BusinessDataCtrlManager getSheetIdAndCategoryList] setObject:categoryDatas forKey:[BusinessDataCtrlManager getUserInfo].lastSheetId];

    
    [self newSheetIDAndCategoryID:[[BusinessDataCtrlManager getUserInfo].lastSheetId intValue] categoryId:categoryId];
}

-(void)createCategorySucessed{
    
    self.createCategoryFlag = TRUE;
    
    self.currentSheetId = [[BusinessDataCtrlManager getUserInfo].lastSheetId intValue];

    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId forKey:sheetId_Key];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    self.methodName = METHOD_CATEGORY_LIST;
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];

}


// Categori一覧データを受信したとき
- (void)recieveOprationData:(NSString *)inData{
    //NSLog(@"%@",inData);
	NSMutableDictionary *recieveData = [inData JSONValue];
    
    NSMutableDictionary *result = [recieveData objectForKey:RESULT_KEY];
    
    NSString *resultStatus = [result objectForKey:RESULT_STATUS_KEY];
    
    if (resultStatus != nil && [resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {
        
        if ([self.methodName isEqualToString:METHOD_CATEGORY_LIST]) {
            // セクション名をキーにしてそのセクションの項目をすべて取得
            NSNumber *sheetId = [NSNumber numberWithInt:self.currentSheetId];
            
            NSMutableArray *newList = [[[NSMutableArray alloc] init] autorelease];
            NSMutableDictionary *data = [recieveData objectForKey:RESULT_DATA_VALUE_KEY];
            //NSLog([data JSONRepresentation]);
            NSMutableArray *categoryList = [data objectForKey:UNCATEGORYIZED];
            [newList addObjectsFromArray:categoryList];
            categoryList = [data objectForKey:CATEGORYIZED0];
            [newList addObjectsFromArray:categoryList];
            categoryList = [data objectForKey:CATEGORYIZED1];
            [newList addObjectsFromArray:categoryList];
            
            [self.categoryLists setObject:newList forKey:sheetId];
            
            
            if (self.createCategoryFlag) {
                //新規カテゴリーを作成
                self.createCategoryFlag = FALSE;
                
                [self newCategoryID:[[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue]];
                
            }else{
                
                int sectionNum = [self getCurrentSheetRowNum];
                [self.openedSectionIds addObject:[NSNumber numberWithInt:sectionNum]];
                
                if ([self.categoryStatisticsLists objectForKey:sheetId] == nil) {
                    NSMutableArray *cateoryIds = [[[NSMutableArray alloc] init] autorelease];
                    NSMutableArray *categoryList = [self.categoryLists objectForKey:sheetId];
                    
                    for (NSMutableDictionary *categoryInfo in categoryList ) {
                        [cateoryIds addObject:[categoryInfo objectForKey:categoryId_Key]];
                    }
                    
                    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
                    [param setObject:cateoryIds forKey:@"categoryIds"];

                    NSMutableDictionary *paramDic = [[[NSMutableDictionary alloc] init] autorelease];
                    [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
                    [paramDic setObject:param forKey:PARAMS];
                    
                    self.methodName = METHOD_CATEGORY_STATISTICS;
                    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
                    
                    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
  
                }else{
                    
                    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sectionNum] withRowAnimation:UITableViewRowAnimationNone];

                }
                
            }

        }else if([self.methodName isEqualToString:METHOD_CATEGORY_STATISTICS]){
            NSMutableDictionary *data = [recieveData objectForKey:RESULT_DATA_VALUE_KEY];
            //NSLog([data JSONRepresentation]);
            NSMutableArray *categoryStatisticsList = [data objectForKey:CATEGORYIES];
            NSNumber *sheetId = [NSNumber numberWithInt:self.currentSheetId];
            [self.categoryStatisticsLists setObject:categoryStatisticsList forKey:sheetId];
            
            int sectionNum = [self getCurrentSheetRowNum];
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sectionNum] withRowAnimation:UITableViewRowAnimationNone];

            
        }
     }else{
        NSString *errMsg = [result objectForKey:RESULT_ERROR_MESSGATE_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle:MultiLangString(@"Information")
							  message:errMsg
							  delegate:nil
							  cancelButtonTitle:MultiLangString(@"Yes")
							  otherButtonTitles: nil];
		[alert show];
		[alert release];
        
        [self.delegate loginFaild];
        
    }
    
}

- (void)cardListSucessed{
    int sectionNum = [self getCurrentSheetRowNum];

    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sectionNum] withRowAnimation:UITableViewRowAnimationNone];

}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}


#pragma mark Memory management
- (void)dealloc {
    self.delegate = nil;
    
    self.categoryLists = nil;
    
    [[DownLoadSimpleWebDataServerController getInstance] clearDelegate];
    
    
	[super dealloc];
    
	
}





@end
