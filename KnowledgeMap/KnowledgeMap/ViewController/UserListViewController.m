//
//  UserListViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/20.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "UserListViewController.h"

@interface UserListViewController ()

@end

@implementation UserListViewController

@synthesize delegate;
@synthesize invitedUserList;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [Utils getBaseTextColor];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    if ([Utils versionIsSeven]) {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
    
    
    UIButton *inviteUserButton = [[[UIButton alloc] initWithFrame:NAVICATION_BAR_RIGHT_BUTTON_LARGE_RECT] autorelease];
    [inviteUserButton addTarget:self action:@selector(clickInviteUserButton:) forControlEvents:UIControlEventTouchUpInside];
    [inviteUserButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_add_friend_nomral.png"]] forState:UIControlStateNormal];
    [inviteUserButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_add_friend_touch.png"]] forState:UIControlStateHighlighted];
    UIBarButtonItem *doneButton= [[[UIBarButtonItem alloc] initWithCustomView:inviteUserButton] autorelease];
	[self.navigationItem setRightBarButtonItem:doneButton];

    UISwipeGestureRecognizer *swipeRightGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToRight:)] autorelease];
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	swipeRightGesture.numberOfTouchesRequired = 1;
    swipeRightGesture.delegate = self;
	[self.view addGestureRecognizer:swipeRightGesture];

}

#pragma mark - clickInviteUserButton
-(void)clickInviteUserButton:(id)sender{
    
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId  forKey:sheetId_Key];
    
    [[BusinessDataCtrlManager getInstance] getReferenceSheetSetting:param delegate:self];

    
    
}

- (void)referenceSheetSettingSucessed:(NSMutableDictionary *)Info{
    
    CreateSheetViewController *createSheetViewController = [[[CreateSheetViewController alloc] init] autorelease];
    createSheetViewController.delegate = self;
    createSheetViewController.operationType = SHEET_OPERATION_TYPE_EDIT;
    
    NSMutableDictionary *sheetInfo =  [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    createSheetViewController.viewTitle = [sheetInfo objectForKey:sheetName_key];
    
    NSString *shareFlag = [Info objectForKey:shareFlg_key];
    if (shareFlag != nil && [shareFlag isEqualToString:@"1"]) {
        createSheetViewController.shareFlag = TRUE;
    }
    
    //招待リスト
    createSheetViewController.invitedUserList = [[[NSMutableArray alloc] initWithArray:self.invitedUserList] autorelease];
    
    //シート設定情報
    createSheetViewController.sheetSettingInfo = [[[NSMutableDictionary alloc] initWithDictionary:Info] autorelease];

    
    UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:createSheetViewController] autorelease];
    
	[self presentModalViewController:controller animated:YES];
    
    
    
    
}

#pragma mark - AppApiDelegate delegate
- (void)createNewSheetOrUpdateSheetSucessed:(NSMutableDictionary *)Info{
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId  forKey:sheetId_Key];
    
    [[BusinessDataCtrlManager getInstance] getSheetMemberList:param delegate:self];

}

- (void)sheetMemberListSucessed:(NSArray *)memberList{
    self.invitedUserList = [[[NSMutableArray alloc] initWithArray:memberList] autorelease];
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.invitedUserList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
	return USER_ICON_SIZE+8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *info = [self.invitedUserList objectAtIndex:[indexPath row]];
    // NSNumber *resourceId = [Utils parseWebDataToNumber:cardData dataKey:resourceId_key];
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d",[[info objectForKey:userId_key] intValue] ] ;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
     */
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView *baseView = [[[UIView alloc] initWithFrame:cell.contentView.bounds] autorelease];
        //baseView.backgroundColor = [UIColor whiteColor];
        [cell.contentView addSubview:baseView];
        cell.backgroundColor = [UIColor clearColor];
        
        // Configure the cell...
        [self setCellDataContents:info view:baseView];
        
    }
    
    
    return cell;
    
}

- (void)setCellDataContents:(NSMutableDictionary *)inData view:(UIView *)baseView {
    
    CGFloat x = 16;
    
    //User Icon
    UIImageView *UserIconView;
    CGFloat iconSize = USER_ICON_SIZE;
    UserIconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, (baseView.frame.size.height-iconSize)/2, iconSize, iconSize)] autorelease];
    UserIconView.layer.cornerRadius = 5.0f;
    UserIconView.clipsToBounds = YES;
    UserIconView.image = [UIImage imageNamed:[Utils getResourceIconPath:@"defaultProfImg.png"]];
    NSNumber *userId = [inData objectForKey:userId_key];
    [baseView addSubview:UserIconView];
    [Utils setUserIconWith:[userId intValue] imageView:UserIconView];
    
    x += iconSize +10 ;
    
    NSString *notJoinedStasus = [inData objectForKey:memberStatus_Key];
    if (notJoinedStasus != nil && ![notJoinedStasus isKindOfClass:[ NSNull class]] && [notJoinedStasus isEqualToString:@"1"]) {
        
        //Set UserName
        NSString *userName = [inData objectForKey:userName_Key];
        if (userName == nil || [userName isKindOfClass:[ NSNull  class]]) {
            userName=@"";
        }
        UILabel *userNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(x, UserIconView.frame.origin.y, SCREEN_WIDTH-x-40, iconSize/2)] autorelease];
        userNameLable.backgroundColor = [UIColor clearColor];
        userNameLable.font = [UIFont  boldSystemFontOfSize:20];
        userNameLable.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
        userNameLable.textAlignment = UITextAlignmentLeft;
        userNameLable.numberOfLines = 1;
        userNameLable.text = userName;
        [baseView addSubview:userNameLable];

        //is Jointed ?
        UILabel *jointedLable = (UILabel *)[baseView viewWithTag:MEMBER_LIST_DATA_TAG];
        jointedLable = [[[UILabel alloc] initWithFrame:CGRectMake(x, UserIconView.frame.origin.y+iconSize/2, SCREEN_WIDTH-x-40, iconSize/2)] autorelease];
        jointedLable.backgroundColor = [UIColor clearColor];
        jointedLable.font = [UIFont  systemFontOfSize:10];
        jointedLable.textColor = [UIColor blueColor];
        jointedLable.textAlignment = UITextAlignmentLeft;
        jointedLable.numberOfLines = 1;
        jointedLable.text = MultiLangString(@"Not joined");
        [baseView addSubview:jointedLable];

    }else{
        //Set UserName
        NSString *userName = [inData objectForKey:userName_Key];
        if (userName == nil || [userName isKindOfClass:[ NSNull  class]]) {
            userName=@"";
        }
        UILabel *userNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(x, UserIconView.frame.origin.y, SCREEN_WIDTH-x-40, iconSize)] autorelease];
        userNameLable.backgroundColor = [UIColor clearColor];
        userNameLable.font = [UIFont  boldSystemFontOfSize:20];
        userNameLable.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
        userNameLable.textAlignment = UITextAlignmentLeft;
        userNameLable.numberOfLines = 1;
        userNameLable.text = userName;
        [baseView addSubview:userNameLable];
    }
	
    
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - PanGestureRecognizer
-(void)viewDidDraggingToRight:(UISwipeGestureRecognizer *)swipe{
    //    NSLog(@"========handleRightSwipeGesture========");
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
