//
//  ViolationInputViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/09/23.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "ViolationInputViewController.h"

@interface ViolationInputViewController ()

@end

@implementation ViolationInputViewController
@synthesize cardInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor blackColor];
    //HeaderView
    [Utils setImageToNavigationBar:self.navigationController.navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];
    
    UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, TOOL_BAR_HIGHT)] autorelease];
    self.navigationItem.titleView = view;

    //キャンセルボタンを生成
    [Utils setNavigationBarLeftButton:self.navigationController.navigationBar navigationItem:self.navigationItem controller:self];
    

    UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = UITextAlignmentCenter;
    label.font = [UIFont  boldSystemFontOfSize:18];
    label.text =  NSLocalizedString(@"Violation report",@"Violation report");
    [view addSubview:label];
    [label release];
    
    
    self.sendButton = [[[UIButton alloc] initWithFrame:NAVICATION_BAR_RIGHT_BUTTON_LARGE_RECT] autorelease];
    [self.sendButton addTarget:self action:@selector(clickSaveButton:) forControlEvents:UIControlEventTouchUpInside];
    //[Utils setNaviBarButtonProPerty:saveButton title:@"Save"];
    UIImage *backgoundImageNormal = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_write_normal.png"]];
    [self.sendButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    UIImage *backgoubdImageHilight = [UIImage imageNamed:[Utils getResourceIconPath:@"icon_write_touch.png"]];
    [self.sendButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    UIBarButtonItem *doneButton= [[[UIBarButtonItem alloc] initWithCustomView:self.sendButton] autorelease];
	[self.navigationItem setRightBarButtonItem:doneButton];
    self.sendButton.enabled = NO;
    

    
    //TextView
    self.txtView = [[[UITextView alloc] initWithFrame:CGRectMake(10, 5, 300, SCREEN_HEIGHT-TOOL_BAR_HIGHT- KEY_BOARD_HEIGHT)] autorelease];
    self.txtView.backgroundColor = [UIColor whiteColor];
    self.txtView.font = [UIFont systemFontOfSize:16];
    self.txtView.layer.cornerRadius = 5.0f;
    self.txtView.clipsToBounds = YES;
    self.txtView.delegate = self;
    [self.view addSubview:self.txtView];
    [self.txtView becomeFirstResponder];


}


-(void)clickCancelButton:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
    
}

-(void)clickSaveButton:(id)sender {
	UIAlertView *alert = [[UIAlertView alloc]
						  initWithTitle:NSLocalizedString(@"Information",@"Information")
						  message:NSLocalizedString(@"I0030",@"I0030")
						  delegate:self
						  cancelButtonTitle:MultiLangString(@"Yes")
						  otherButtonTitles: MultiLangString(@"No"),nil];
    [alert show];
    [alert release];

}


#pragma mark - alert Delegate
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0:
			//Yes
			;
            
            [self sendViolationData];
            
			break;
		case 1:
			//No;
			;
            
            
			break;
	}
}

-(void)sendViolationData {
    
    //暫定として、メール直接送信にする
	NSString* mailtoURI = [NSString stringWithFormat:@"mailto:%@?Subject=%@&body=\n%@\n\n\nId:%d\n\n" , MAIL_NUMBER, NSLocalizedString(@"Violation report",@"Violation report"), self.txtView.text, [[self.cardInfo objectForKey:cardId_key] integerValue] ];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[mailtoURI stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    [self dismissModalViewControllerAnimated:YES];

    /*
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
        [param setObject:self.txtView.text forKey:content_key];
        [param setObject:[BusinessDataCtrlManager getUserInfo].userId  forKey:userId_key];
    
        [[BusinessDataCtrlManager getInstance] sendViolationData:param delegate:self];
   */
        
    
}


#pragma mark - WorkDataControllerDelegate Delegate
- (void)sendViolationSucessed{
    
    [self dismissModalViewControllerAnimated:YES];
    
}

#pragma mark  UITextViewのデリゲートメソッド
- (void)textViewDidChange:(UITextView *)textView{
    if (self.txtView.text != nil && ![self.txtView.text isEqualToString:@""]) {
        self.sendButton.enabled = YES;
    }else{
        self.sendButton.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
