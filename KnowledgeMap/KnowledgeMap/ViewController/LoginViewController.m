    //
//  CreateNewBookTitleViewController.m
//  BukurouSS
//
//  Created by k-kin on 11/09/28.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import "LoginViewController.h"


@implementation LoginViewController

@synthesize delegate;
@synthesize txtMailAddress;
@synthesize txtPassword;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
    self.view.backgroundColor = [Utils getBaseTextColor];

    //HeaderView
    [Utils setImageToNavigationBar:self.navigationController.navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];

    UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, TOOL_BAR_HIGHT)] autorelease];
    self.navigationItem.titleView = view;
    
    //キャンセルボタンを生成
    [Utils setNavigationBarLeftButton:self.navigationController.navigationBar navigationItem:self.navigationItem controller:self];

    UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = UITextAlignmentCenter;
    label.font = [UIFont  boldSystemFontOfSize:24];
    label.text =  NSLocalizedString(@"Login",@"Login");
    [view addSubview:label];
    [label release];
    
    CGFloat buttonHeight = 30;
    CGFloat buttonWidth = 260;
    CGFloat y=10;
    CGFloat x=(SCREEN_WIDTH-buttonWidth)/2;

    [Utils addShadowLine:self.navigationItem.titleView];
    
    //MailAddress Label
    [self createDetailLabel:self.view title:NSLocalizedString(@"MailAddress",@"MailAddress") rect:CGRectMake(x, y-5, buttonWidth, buttonHeight)];
    
    //MailAddress TextFiled
    self.txtMailAddress = [[UITextField alloc] initWithFrame:CGRectMake(x, y+buttonHeight-10, buttonWidth, buttonHeight)];
    [self setTextFiledProperty:self.txtMailAddress  ];
    self.txtMailAddress.keyboardType = UIKeyboardTypeEmailAddress;
    //self.txtMailAddress.placeholder = @"Mail Address";
    self.txtMailAddress.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.txtMailAddress becomeFirstResponder];
    self.txtMailAddress.text = [BusinessDataCtrlManager getUserInfo].mailAddress;

    //Password Label
    [self createDetailLabel:self.view title:NSLocalizedString(@"Password",@"Password") rect:CGRectMake(x, y*2+buttonHeight*2-5, buttonWidth, buttonHeight)];

    
    //Password TextFiled
    self.txtPassword = [[UITextField alloc] initWithFrame:CGRectMake(x, y*2+buttonHeight*3-10, buttonWidth, buttonHeight)];
    [self setTextFiledProperty:self.txtPassword ];
    self.txtPassword.keyboardType = UIKeyboardTypeASCIICapable;
    self.txtPassword.secureTextEntry = YES;
    //self.txtPassword.placeholder = @"Password";

    
    //self.txtPassword.text = @"jin751207";
    
    //LoginButton
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(x, y+buttonHeight*5, buttonWidth, buttonHeight);
    [button addTarget:self action:@selector(clickLoginButton:) forControlEvents:UIControlEventTouchUpInside];
    //button.backgroundColor = [UIColor blueColor];
    [button setImage:[Utils getBlueButtonNormalImage:button.bounds] forState:UIControlStateNormal];
    [button setImage:[Utils getBlueButtonSelectedImage:button.bounds] forState:UIControlStateHighlighted];
    [self.view addSubview:button];
    
    label = [[UILabel alloc] initWithFrame:button.bounds];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = UITextAlignmentCenter;
    label.font = [UIFont  boldSystemFontOfSize:BUTTON_TEXT_INIT_SIZE];
    label.text =  NSLocalizedString(@"Login",@"Login");
    [button addSubview:label];
    [label release];

    

}

-(void)createDetailLabel:(UIView *)baseView title:(NSString *)labelText rect:(CGRect)rect{
    //NSLog(@"---------");
    int fontSize = 14;
    CGSize size = [labelText sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] constrainedToSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT) lineBreakMode:NSLineBreakByCharWrapping];
    
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
	label.backgroundColor = [UIColor clearColor];
    label.lineBreakMode = UILineBreakModeCharacterWrap;
	label.font = [UIFont  boldSystemFontOfSize:fontSize];
	label.textColor = [UIColor grayColor];
	label.textAlignment = UITextAlignmentLeft;
	label.numberOfLines = size.height/fontSize;
	label.text = labelText;
	[baseView addSubview:label];
	[label release];
    
}

-(void)setTextFiledProperty:(UITextField *)textFiled{
  	textFiled.delegate = self;
    textFiled.backgroundColor = [UIColor whiteColor];
    //textFiled.borderStyle = UITextBorderStyleBezel;
	textFiled.clearButtonMode = UITextFieldViewModeWhileEditing;
    textFiled.autocapitalizationType = UITextAutocapitalizationTypeSentences;
	[self.view addSubview:textFiled ];

}

-(void)clickCancelButton:(id)sender{
	[self.txtMailAddress resignFirstResponder];
 	[self.txtPassword resignFirstResponder];
   [self dismissModalViewControllerAnimated:YES];

}

-(void)clickLoginButton:(id)sender{
    if (self.txtMailAddress.text == nil || [self.txtMailAddress.text isEqualToString:@""]) {
        [self.txtMailAddress becomeFirstResponder];
        return;
    }
    
    if (self.txtPassword.text == nil || [self.txtPassword.text isEqualToString:@""]) {
        [self.txtPassword becomeFirstResponder];
        return;
    }
   
	[self.txtMailAddress resignFirstResponder];
 	[self.txtPassword resignFirstResponder];

    [[BusinessDataCtrlManager getInstance]loginWithUserIdAndDelegete:self.txtMailAddress.text pass:[[self.txtPassword.text MD5String] lowercaseString] delegate:self];
    
    /*
    WorkViewController *workViewController = [[WorkViewController alloc] init];
    
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:workViewController];
    
	[self presentModalViewController:controller animated:YES];
    
    [workViewController release];
    [controller release];
     */

}

#pragma mark - WorkDataControllerDelegate Delegate
- (void)CategoryListSucessed{
    
    WorkViewController *workViewController = nil;
    workViewController = [[[WorkViewController alloc] init] autorelease];
    workViewController.delegate = self;
    //workViewController.title = [NSString stringWithFormat:@"%@(%d)", MultiLangString(@"Sheet Select") ,[workViewController.sheetList count]];
    workViewController.currentSheetId = [[BusinessDataCtrlManager getUserInfo].lastSheetId intValue];

    
    UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:workViewController] autorelease];
    
	[self presentModalViewController:controller animated:YES];
    

}

- (void)logOut{
    [self dismissModalViewControllerAnimated:NO];

    
    if ([self.delegate respondsToSelector:@selector(logOut)]) {
        [self.delegate logOut];
    }
}

- (void)loginFaild{
     
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	self.txtMailAddress.delegate = nil;
	[self.txtMailAddress release];

    self.txtPassword.delegate = nil;
	[self.txtPassword release];

    [super dealloc];
}


@end
