//
//  CommentListViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/07/30.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "InputCommentViewController.h"

#define CommentAuthNameSize         18
#define CommentDetailSize           14

@interface CommentListViewController : UITableViewController{
    id delegate;
    
    NSMutableArray		*dataList;
    NSNumber *cardId;


}

@property (nonatomic, assign) id delegate;

@property (nonatomic, strong) NSMutableArray		*dataList;
@property (nonatomic, strong) NSNumber *cardId;

@end
