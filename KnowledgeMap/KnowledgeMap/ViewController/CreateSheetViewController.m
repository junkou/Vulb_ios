//
//  CreateSheetViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/09.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "CreateSheetViewController.h"

@interface CreateSheetViewController ()

@end

@implementation CreateSheetViewController
@synthesize delegate;
@synthesize operationType;
@synthesize shareFlag;
@synthesize viewTitle;
@synthesize invitedUserList;
@synthesize sheetSettingInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.historyUserDataList = [[[NSMutableArray alloc] init] autorelease];
        self.fbFreindsDataList = [[[NSMutableArray alloc] init] autorelease];
        self.listDataType = LIST_DATA_TYPE_INVITED;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = [Utils getBaseTextColor];
    
    //NavigationBar + LeftButton
    [Utils setNavigationBarLeftButton:self.navigationController.navigationBar navigationItem:self.navigationItem controller:self];
    
    //Add NavigationBar Title
    [Utils setNavigationBarTitleLabel:self.navigationItem.titleView text:self.viewTitle];

    //add NavicationBar RightButton
    [Utils setNavigationBarRightSaveButton:self.navigationItem controller:self];

    [self createMainWorkView];

}

-(void)createMainWorkView{
    
    CGFloat y = y_delta;
    
    if (self.operationType == SHEET_OPERATION_TYPE_NEW) {
        //txtCagegoryName TextFiled
        self.txtSheetName = [[[UITextField alloc] initWithFrame:CGRectMake(x_delta, y, SCREEN_WIDTH-x_delta*2, BUTTON_HEIGHT)] autorelease];
        self.txtSheetName.backgroundColor = [UIColor whiteColor];
        //self.txtSheetName.borderStyle = UITextBorderStyleBezel;
        self.txtSheetName.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.txtSheetName.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        self.txtSheetName.placeholder = MultiLangString(@"Sheet Name");
        self.txtSheetName.delegate = self;
        [self.view addSubview:self.txtSheetName ];
        y = self.txtSheetName.frame.origin.y + self.txtSheetName.frame.size.height +y_delta;
    }

    //Not shere Button
    self.notsharedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.notsharedButton.tag = NOT_SHARE_BUTTON_TAG;
    self.notsharedButton.frame = CGRectMake(x_delta, y, SCREEN_WIDTH/2-x_delta*2, BUTTON_TEXT_INIT_SIZE*1.5);
    [self.notsharedButton addTarget:self action:@selector(clickShareButton:) forControlEvents:UIControlEventTouchUpInside];
    [self setShereButtonPropertie:self.notsharedButton status:TRUE label:@"Not Share"];
    [self.view addSubview:self.notsharedButton];
    
    //shere Button
    self.sharedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sharedButton.tag = SHARE_BUTTON_TAG;
    self.sharedButton.frame = CGRectMake(SCREEN_WIDTH/2+x_delta, y, SCREEN_WIDTH/2-x_delta*2, BUTTON_TEXT_INIT_SIZE*1.5);
    [self.sharedButton addTarget:self action:@selector(clickShareButton:) forControlEvents:UIControlEventTouchUpInside];
    [self setShereButtonPropertie:self.sharedButton status:FALSE label:@"Share"];
    [self.view addSubview:self.sharedButton];


    if(self.shareFlag){
        [self clickShareButton:self.sharedButton];
    }
 
}

-(void)createMainWork_Detail_View{
    
    if (self.shareDetalBaseView != nil) {
        self.shareDetalBaseView.hidden = NO;
        return;
    }
    
    self.shareDetalBaseView = [[[UIView alloc] initWithFrame:CGRectMake(0, self.sharedButton.frame.origin.y + self.sharedButton.frame.size.height , SCREEN_WIDTH, self.view.bounds.size.height-self.sharedButton.frame.origin.y - self.sharedButton.frame.size.height )] autorelease];
    [self.view addSubview:self.shareDetalBaseView ];
    //self.shareDetalBaseView.backgroundColor = [UIColor redColor];
   
    
    //txtCertificationCode TextFiled
    self.txtCertificationCode = [[[UITextField alloc] initWithFrame:CGRectMake(x_delta, y_delta, SCREEN_WIDTH-x_delta*2, BUTTON_HEIGHT)] autorelease];
    self.txtCertificationCode.backgroundColor = [UIColor whiteColor];
    //self.txtCertificationCode.borderStyle = UITextBorderStyleBezel;
	self.txtCertificationCode.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.txtCertificationCode.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.txtCertificationCode.placeholder = NSLocalizedString(@"Set Certification code",@"Set Certification code");
    self.txtCertificationCode.delegate = self;
    [self.shareDetalBaseView addSubview:self.txtCertificationCode ];
    if (self.sheetSettingInfo != nil) {
        NSString *confirmCode = [self.sheetSettingInfo objectForKey:confirmCd_key];
        if (confirmCode != nil && ![confirmCode isKindOfClass:[ NSNull class]]) {
            self.txtCertificationCode.text = confirmCode;
        }
        
    }
    
    
    // セグメンテッドコントロール
    NSArray *arr = [NSArray arrayWithObjects:MultiLangString(@"History Users"), MultiLangString(@"Facebook Users"),  nil];
    self.memberListTypeSeg =[[[UISegmentedControl alloc] initWithItems:arr] autorelease];
    self.memberListTypeSeg.frame = CGRectMake(x_delta, self.txtCertificationCode.frame.origin.y + self.txtCertificationCode.frame.size.height +y_delta, SCREEN_WIDTH-x_delta*2, 32);
    self.memberListTypeSeg.segmentedControlStyle = UISegmentedControlStyleBezeled; //スタイルの設定
    self.memberListTypeSeg.selectedSegmentIndex = 0; //
    //self.memberListTypeSeg.tintColor = HEXCOLOR(0x593D00);
    [self.shareDetalBaseView addSubview:self.memberListTypeSeg];
    //値が変更された時にメソッドを呼び出す
    [self.memberListTypeSeg addTarget:self action:@selector(changeMemberListType:) forControlEvents:UIControlEventValueChanged];
    
    //MemberListView
    CGFloat t_y =self.memberListTypeSeg.frame.origin.y + self.memberListTypeSeg.frame.size.height +y_delta/2;
    self.memberListTableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, t_y, SCREEN_WIDTH, self.view.bounds.size.height-t_y-BUTTON_HEIGHT*5) style:UITableViewStylePlain] autorelease];
    self.memberListTableView.delegate = self;
    self.memberListTableView.dataSource = self;
    [self.shareDetalBaseView addSubview:self.memberListTableView];
    //    UIImageView *backImageView = [[[UIImageView alloc] initWithFrame:memberListTableView.bounds] autorelease];
    //    [backImageView setImage:[UIImage imageNamed:[Utils getRourcesFilePath:@"bss_side_menu_back_image.png"]]];
    //    memberListTableView.backgroundView = backImageView;
    
    
    //Input MailAdress
    self.mailAddressButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.mailAddressButton.frame = CGRectMake(x_delta, self.memberListTableView.frame.origin.y + self.memberListTableView.frame.size.height +y_delta/2, SCREEN_WIDTH-x_delta*2, BUTTON_HEIGHT);
    [self.mailAddressButton addTarget:self action:@selector(clickInputMailAddressButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.mailAddressButton setImage:[Utils getWhiteButtonNormalImage:self.mailAddressButton.bounds] forState:UIControlStateNormal];
    [self.mailAddressButton setImage:[Utils getWhiteButtonSelectedImage:self.mailAddressButton.bounds] forState:UIControlStateHighlighted];
    [self.shareDetalBaseView addSubview:self.mailAddressButton];
    
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(x_delta, 0, self.mailAddressButton.bounds.size.width-x_delta*2, self.mailAddressButton.bounds.size.height)] autorelease];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = UITextAlignmentLeft;
    label.font = [UIFont  boldSystemFontOfSize:BUTTON_TEXT_INIT_SIZE];
    label.text =  NSLocalizedString(@"Invite Users by e-mail",@"Invite Users by e-mail");
    [self.mailAddressButton addSubview:label];
    
    
    
    if ([[self getTableListData] count] == 0) {
        [[BusinessDataCtrlManager getInstance] getInvitedList:self];
    }

}

-(void)setShereButtonPropertie:(UIButton *)button status:(BOOL)status label:(NSString *)title{
    
    UIView *baseView = [button viewWithTag:CHECK_MARK_VIEW_TAG];
    [baseView removeFromSuperview];
    
    baseView = [[[UIView alloc] initWithFrame:button.bounds] autorelease];
    baseView.userInteractionEnabled = NO;
    baseView.tag = CHECK_MARK_VIEW_TAG;
    [button addSubview:baseView];
    
    UIImage *image = nil;
    if (status) {
        image = [UIImage imageNamed:[Utils getResourceIconPath:@"BssRadioButtonOn.png"]];
    }else{
        image = [UIImage imageNamed:[Utils getResourceIconPath:@"BssRadioButtonOff.png"]];
    }
    UIImageView *imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, (button.bounds.size.height - image.size.height)/2, image.size.width, image.size.height)] autorelease];
    imageView.image =image;
    [baseView addSubview:imageView];
    
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(image.size.width+5, 0, button.bounds.size.width-image.size.width-5, button.bounds.size.height)] autorelease];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = UITextAlignmentLeft;
    label.font = [UIFont  boldSystemFontOfSize:BUTTON_TEXT_INIT_SIZE];
    label.text =  NSLocalizedString(title,title);
    [baseView addSubview:label];

    
}

#pragma mark - NavicationBar CancelButton Call Method
-(void)clickCancelButton:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
    
}

#pragma mark - NavicationBar SaveButton Call Method
-(void)clickSaveButton:(id)sender {
    if (self.operationType == SHEET_OPERATION_TYPE_NEW) {
        [self registNewSheet];
    }else if (self.operationType == SHEET_OPERATION_TYPE_EDIT){
        [self updateSheet];

    }

}

-(void)registNewSheet{
    if (self.txtSheetName.text != nil && ![self.txtSheetName.text isEqualToString:@""]) {
        if (self.shareFlag) {
            if (self.txtCertificationCode.text != nil && ![self.txtCertificationCode.text isEqualToString:@""]) {
                //認証コードは４桁以上である必要がある
                if ([self.txtCertificationCode.text length] < 4) {
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle:MultiLangString(@"Information")
                                          message:NSLocalizedString(@"I0038",@"I0038")
                                          delegate:nil
                                          cancelButtonTitle:MultiLangString(@"Yes")
                                          otherButtonTitles: nil];
                    [alert show];
                    [alert release];
                    
                    return;
                    
                }
                
            }
        }
        
        NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
        [param setObject:self.txtSheetName.text forKey:sheetName_key];
        if (self.shareFlag) {
            [param setObject:@"1"  forKey:shareFlg_key];
            
            if (self.txtCertificationCode.text != nil  && ![self.txtCertificationCode.text isEqualToString:@""]) {
                [param setObject:self.txtCertificationCode.text  forKey:confirmCd_key];
            }
            
        }else{
            [param setObject:@"0"  forKey:shareFlg_key];
        }
        
        [param setObject:@""  forKey:sheetDescription_key];
        
        [self dataLoadingAnimation];
        
        [[BusinessDataCtrlManager getInstance] createNewSheet:param delegate:self];
        
    }
}

-(void)updateSheet{
    if (self.sheetSettingInfo != nil) {
        BOOL sheetInfoUpdateFlag = FALSE;
        NSString *oldShareFlag = [self.sheetSettingInfo objectForKey:shareFlg_key];
        if (([oldShareFlag isEqualToString:@"0"] && self.shareFlag)
            || ([oldShareFlag isEqualToString:@"1"] && !self.shareFlag)) {
            sheetInfoUpdateFlag = TRUE;
        }
        
        if (self.shareFlag) {
            NSString *confirmCode = [self.sheetSettingInfo objectForKey:confirmCd_key];
            if (confirmCode == nil || [confirmCode isKindOfClass:[ NSNull class]]) {
                if (self.txtCertificationCode.text != nil && ![self.txtCertificationCode.text isEqualToString:@""]) {
                    sheetInfoUpdateFlag = TRUE;
                }
             }else{
                 if (![confirmCode isEqualToString:self.txtCertificationCode.text]) {
                     sheetInfoUpdateFlag = TRUE;
                 }

            }
        }
        
        if (sheetInfoUpdateFlag) {
            NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
            
            [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId  forKey:sheetId_Key];

            if (self.txtCertificationCode.text != nil && ![self.txtCertificationCode.text isEqualToString:@""]) {
                //認証コードは４桁以上である必要がある
                if ([self.txtCertificationCode.text length] < 4) {
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle:MultiLangString(@"Information")
                                          message:NSLocalizedString(@"I0038",@"I0038")
                                          delegate:nil
                                          cancelButtonTitle:MultiLangString(@"Yes")
                                          otherButtonTitles: nil];
                    [alert show];
                    [alert release];
                    
                    return;
                    
                }
                [param setObject:self.txtCertificationCode.text  forKey:confirmCd_key];
            }
 
            if (self.shareFlag) {
                [param setObject:@"1"  forKey:shareFlg_key];
            }else{
                [param setObject:@"0"  forKey:shareFlg_key];
            }
            
            [[BusinessDataCtrlManager getInstance] updateReferenceSheetSetting:param delegate:self];

        }else{
            [self createNewSheetOrUpdateSheetSucessed:nil];
        }
    }
}

-(void)clickShareButton:(UIButton *)sender{
    
    [self.txtSheetName resignFirstResponder];
    [self.txtCertificationCode resignFirstResponder];
   
    if (sender.tag == SHARE_BUTTON_TAG) {
        [self setShereButtonPropertie:self.sharedButton status:TRUE label:@"Share"];
        [self setShereButtonPropertie:self.notsharedButton status:FALSE label:@"Not Share"];
        
        self.shareFlag = TRUE;
        
        [self createMainWork_Detail_View];
        
    }else{
        [self setShereButtonPropertie:self.sharedButton status:FALSE label:@"Share"];
        [self setShereButtonPropertie:self.notsharedButton status:TRUE label:@"Not Share"];
        
        self.shareFlag = FALSE;

        self.shareDetalBaseView.hidden = YES;
 
    }
}

#pragma mark - clickInputMailAddressButton Call Method
-(void)clickInputMailAddressButton:(id)sender {
    InputTextfiledViewController *inputTextfiledViewController = [[[InputTextfiledViewController alloc] init] autorelease];
    inputTextfiledViewController.delegate = self;
    inputTextfiledViewController.txtFieldPlaceholder = NSLocalizedString(@"MailAddress",@"MailAddress");
    
    [self.navigationController pushViewController:inputTextfiledViewController animated:YES];
    
}


-(void)textValueConfirm:(NSString *)text{
    self.listDataType = LIST_DATA_TYPE_INVITED;
    
    NSMutableDictionary *data = [[[NSMutableDictionary alloc] init] autorelease];
    [data setObject:[Utils getRanDomCode] forKey:RANDOM_CODE_KEY];
    [data setObject:MEMBER_SELECTED_FLAG forKey:MEMBER_SELECTED_FLAG];
    [data setObject:text forKey:userName_Key];
    [self.historyUserDataList insertObject:data atIndex:0];
    
    self.memberListTypeSeg.selectedSegmentIndex = 0 ;
    
    [self.memberListTableView reloadData];
    
    
}

#pragma mark UISegmentedControl changeMemberListType Methods
-(void)changeMemberListType:(UISegmentedControl*)seg{
    //    NSLog(@"selectedSegmentIndex:%d",seg.selectedSegmentIndex);
    [self.txtSheetName resignFirstResponder];
    [self.txtCertificationCode resignFirstResponder];

    if (seg.selectedSegmentIndex == 0) {
        //from History
        self.listDataType = LIST_DATA_TYPE_INVITED;
        
        if ([[self getTableListData] count] == 0) {
            [[BusinessDataCtrlManager getInstance] getInvitedList:self];
        }else{
            [self.memberListTableView reloadData];
        }
 
    }else if (seg.selectedSegmentIndex == 1) {
        //from Facebook
        self.listDataType = LIST_DATA_TYPE_FB_FREINDS;
      
        if ([[self getTableListData] count] == 0) {
            [[BusinessDataCtrlManager getInstance] getFbFreindsList:self];
        }else{
            [self.memberListTableView reloadData];
        }

    }

}


-(NSMutableArray *)getTableListData{
    if (self.listDataType == LIST_DATA_TYPE_INVITED) {
        return self.historyUserDataList;
    }else if(self.listDataType == LIST_DATA_TYPE_FB_FREINDS){
        return self.fbFreindsDataList;
    }else{
        return [[[NSMutableArray alloc] init] autorelease];
    }
}

-(BOOL)isInvitedUserList:(NSMutableDictionary *)inData{
    BOOL invitedUserFlag = FALSE;
    if (self.invitedUserList != nil && [self.invitedUserList count] > 0 && [inData objectForKey:userId_key] != nil) {
        for (NSMutableDictionary *invitedUserInfo in self.invitedUserList) {
            if ([[invitedUserInfo objectForKey:userId_key] intValue] == [[inData objectForKey:userId_key] intValue]) {
                NSString *notJoinedStasus = [invitedUserInfo objectForKey:memberStatus_Key];
                if (!(notJoinedStasus != nil && ![notJoinedStasus isKindOfClass:[ NSNull class]] && [notJoinedStasus isEqualToString:@"1"])) {
                    //未参加も再招待できるようにする
                    invitedUserFlag = TRUE;
                    break;

                }
            }
        }
    }
    
    return invitedUserFlag;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    return [[self getTableListData] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {    
    return MEMBER_TABLE_CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableDictionary *memberInfo = [[self getTableListData] objectAtIndex:[indexPath row]];
    
    NSNumber *userId = [memberInfo objectForKey:userId_key];
    NSString *CellIdentifier = @"";
    if (userId != nil) {
        CellIdentifier = [NSString stringWithFormat:@"Cell-%d-%d", [indexPath row], [userId intValue] ];
    }else{
        NSString *randomCode = [memberInfo objectForKey:RANDOM_CODE_KEY];
        CellIdentifier = [NSString stringWithFormat:@"Cell-%d-%@", [indexPath row], randomCode ];
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        

   }
    
    if ([self isInvitedUserList:memberInfo]) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    NSString *seletedFlag = [memberInfo objectForKey:MEMBER_SELECTED_FLAG];
    if (seletedFlag != nil) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    [self setCellDataContents:memberInfo view:cell.contentView];

    
    return cell;
}

- (void)setCellDataContents:(NSMutableDictionary *)inData view:(UIView *)inBaseView {
    UIView *baseView = [inBaseView viewWithTag:MEMBER_CELL_TAG];
    if (baseView != nil) {
        [baseView removeFromSuperview];
    }
    
    BOOL invitedUserFlag = [self isInvitedUserList:inData];

    baseView = [[[UILabel alloc] initWithFrame:inBaseView.bounds] autorelease];
    baseView.tag = MEMBER_CELL_TAG;
    if (invitedUserFlag) {
        baseView.backgroundColor = [UIColor grayColor];
    }else{
        baseView.backgroundColor = [UIColor clearColor];
    }
    [inBaseView addSubview:baseView];
    
    CGFloat x = 10;
    
    //User Icon
    UIImageView *UserIconView;
    CGFloat iconSize = USER_ICON_SIZE;
    UserIconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, (baseView.frame.size.height-iconSize)/2, iconSize, iconSize)] autorelease];
    UserIconView.layer.cornerRadius = 5.0f;
    UserIconView.clipsToBounds = YES;
    UserIconView.image = [UIImage imageNamed:[Utils getResourceIconPath:@"defaultProfImg.png"]];
    [baseView addSubview:UserIconView];
    NSNumber *userId = [inData objectForKey:userId_key];
    if (userId != nil) {
        [Utils setUserIconWith:[userId intValue] imageView:UserIconView];
    }
    
    x = UserIconView.frame.origin.x + UserIconView.frame.size.width + x;
    
    //Set UserName
	NSString *userName = [inData objectForKey:userName_Key];
	if (userName == nil || [userName isKindOfClass:[ NSNull  class]]) {
		userName=@"";
	}
	UILabel *userNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(x, UserIconView.frame.origin.y, SCREEN_WIDTH-x-40, 26)] autorelease];
	userNameLable.backgroundColor = [UIColor clearColor];
	userNameLable.font = [UIFont  boldSystemFontOfSize:20];
	userNameLable.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
	userNameLable.textAlignment = UITextAlignmentLeft;
	userNameLable.numberOfLines = 1;
	userNameLable.text = userName;
	[baseView addSubview:userNameLable];
	
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *memberInfo = [[self getTableListData] objectAtIndex:[indexPath row]];
    
    if ([self isInvitedUserList:memberInfo]) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    
    
    UITableViewCell *newCell  = [tableView cellForRowAtIndexPath:indexPath];

    NSString *seletedFlag = [memberInfo objectForKey:MEMBER_SELECTED_FLAG];
    if (seletedFlag == nil) {
        [memberInfo setObject:MEMBER_SELECTED_FLAG forKey:MEMBER_SELECTED_FLAG];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        [memberInfo removeObjectForKey:MEMBER_SELECTED_FLAG];
        newCell.accessoryType = UITableViewCellAccessoryNone;

    }

	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark - AppApiDelegate delegate
- (void)invitedListSucessed:(NSArray *)memberList{
    self.historyUserDataList = [[[NSMutableArray alloc] initWithArray:memberList] autorelease];
    [self.memberListTableView reloadData];
    
}

- (void)fbFreindsListSucessed:(NSArray *)memberList{
    self.fbFreindsDataList = [[[NSMutableArray alloc] initWithArray:memberList] autorelease];
    [self.memberListTableView reloadData];
    
}

#pragma mark - UITextField delegate
-(BOOL)textFieldShouldReturn:(UITextField*)textField{
    [self.txtSheetName resignFirstResponder];
    [self.txtCertificationCode resignFirstResponder];
    
    return TRUE;
}

#pragma mark - alert Delegate
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0:
			//Yes
			;
            
             
			break;
		case 1:
			//No;
			;
            
            
			break;
	}
}


#pragma mark - AppApiDelegate delegate
- (void)createNewSheetOrUpdateSheetSucessed:(NSMutableDictionary *)Info{
    
    if (Info == nil ||  [Info isKindOfClass:[ NSNull class]]) {
        self.theNewSheetId = [BusinessDataCtrlManager getUserInfo].lastSheetId ;
    }else{
        NSLog(@"%@",[Info description]);
        self.theNewSheetId = [Info objectForKey:sheetId_Key];
    }
    
    if (self.shareFlag) {

        //履歴から
        NSMutableArray *invitedList = [[[NSMutableArray alloc] init] autorelease];
        for (NSMutableDictionary *data in self.historyUserDataList) {
            id userId = [data objectForKey:userId_key];
            if (userId != nil) {
                NSString *seletedFlag = [data objectForKey:MEMBER_SELECTED_FLAG];
                if (seletedFlag != nil) {
                    [invitedList addObject:userId];
                }
            }
        }
        
        //FB Friendsから
        for (NSMutableDictionary *data in self.fbFreindsDataList) {
            id userId = [data objectForKey:userId_key];
            if (userId != nil) {
                NSString *seletedFlag = [data objectForKey:MEMBER_SELECTED_FLAG];
                if (seletedFlag != nil) {
                    [invitedList addObject:userId];
                }
            }
        }

        
        if ([invitedList count] > 0) {
            NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
            [param setObject:self.theNewSheetId forKey:sheetId_Key];            
            [param setObject:invitedList  forKey:@"userIds"];
            
            [[BusinessDataCtrlManager getInstance] inviteWithUserid:param delegate:self];

        }else{
            
            //Maill Adress登録による招待
            [self inviteWithUseridSucessed];
            
        }
    }else{
        
        [self createSheetSucessFinal];

    }
    
}


#pragma mark - WorkDataControllerDelegate Delegate
-(void)inviteWithUseridSucessed{
    //New MailAddressから
    NSMutableArray *invitedList = [[[NSMutableArray alloc] init] autorelease];
    for (NSMutableDictionary *data in self.historyUserDataList) {
        NSString *seletedFlag = [data objectForKey:MEMBER_SELECTED_FLAG];
        if (seletedFlag != nil) {
            id userId = [data objectForKey:userId_key];
            if (userId == nil) {
                //Mail address
                [invitedList addObject:[data objectForKey:userName_Key]];
            }
        }
    }
    
    
    if ([invitedList count] > 0) {
        NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
        [param setObject:self.theNewSheetId forKey:sheetId_Key];
        [param setObject:invitedList  forKey:@"mailAddresses"];
        
        [[BusinessDataCtrlManager getInstance] inviteWithMailaddress:param delegate:self];
        
    }else{
        [self createSheetSucessFinal];
    }
}

-(void)inviteWithMailAddressSucessed{
    
    [self createSheetSucessFinal];

}

-(void)createSheetSucessFinal{
    if (self.operationType == SHEET_OPERATION_TYPE_NEW) {
        [BusinessDataCtrlManager getUserInfo].lastSheetId = self.theNewSheetId ;
        
        [[BusinessDataCtrlManager getInstance] reloadSheetList:self];

    }else if(self.operationType == SHEET_OPERATION_TYPE_EDIT){
        
        [self.maskView removeFromSuperview];
        
        [self dismissModalViewControllerAnimated:YES];
        
        [self.delegate createNewSheetOrUpdateSheetSucessed:nil];

    }

}

- (void)CategoryListSucessed{
    
    [self.maskView removeFromSuperview];
    
    [self dismissModalViewControllerAnimated:YES];
    
    [self.delegate createNewSheetOrUpdateSheetSucessed:nil];
    
    
}


#pragma mark LoadingAnimation
- (void)dataLoadingAnimation{
	UIWindow *mainWindow;
	mainWindow = [[UIApplication sharedApplication] keyWindow];
	
	self.maskView = [[UIButton alloc] initWithFrame:FULL_MAX_RECT_SIZE];
    //    maskView.backgroundColor = [UIColor blueColor];
	[mainWindow addSubview:self.maskView];
	
	UIImageView *animationBaseView = [[UIImageView alloc] initWithFrame:CGRectMake(135, 215, 50, 50)];
	[animationBaseView setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"loadingBase.png"]]];
	[self.maskView addSubview:animationBaseView];
	[animationBaseView release];
	
	UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, 230, 20, 20)];
	indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
	[indicator startAnimating];
	[self.maskView addSubview:indicator];
	[indicator release];
    
    [self.maskView release];
    
	
}

#pragma mark Delegate
-(void)connectNetWorkError{
    
    [self.maskView removeFromSuperview];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
