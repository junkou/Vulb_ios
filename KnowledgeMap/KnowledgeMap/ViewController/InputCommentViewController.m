//
//  InputCommentViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/07/31.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "InputCommentViewController.h"

@interface InputCommentViewController ()

@end

@implementation InputCommentViewController
@synthesize delegate;
@synthesize cardId;
@synthesize commnetInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    //BackgroundImage
//    [Utils setBackgroundBaseImage:self.view];
    
     self.view.backgroundColor = [UIColor blackColor];

    [Utils setImageToNavigationBar:self.navigationController.navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];
    
    UIView *titleView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, TOOL_BAR_HIGHT)] autorelease];
    self.navigationItem.titleView = titleView;

    
    //キャンセルボタンを生成
    [Utils setNavigationBarLeftButton:self.navigationController.navigationBar navigationItem:self.navigationItem controller:self];
    
    
    //Add WriteButton
    self.saveButton = [Utils setNavigationBarRightSaveButton:self.navigationItem controller:self];
    self.saveButton.enabled = NO;
    
    //TextView
    self.txtView = [[[UITextView alloc] initWithFrame:CGRectMake(5, 5, 310, SCREEN_HEIGHT-TOOL_BAR_HIGHT- KEY_BOARD_HEIGHT)] autorelease];
    self.txtView.tag = TEXT_INPUT_VIEW_TAG;
    self.txtView.backgroundColor = [UIColor whiteColor];
    self.txtView.font = [UIFont systemFontOfSize:16];
    self.txtView.layer.cornerRadius = 5.0f;
    self.txtView.clipsToBounds = YES;
    self.txtView.delegate = self;
    [self.view addSubview:self.txtView];
    [self.txtView becomeFirstResponder];

}

- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    
}


#pragma mark - NavicationBar CancelButton Call Method
-(void)clickCancelButton:(id)sender{
    
    [self dismissModalViewControllerAnimated:YES];
    
}

#pragma mark - NavicationBar SaveButton Call Method
-(void)clickSaveButton:(id)sender {
        
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:self.cardId forKey:cardId_key];
    [param setObject:self.txtView.text forKey:content_key];
    if (self.commnetInfo != nil) {
        [param setObject:[self.commnetInfo objectForKey:commentId_key] forKey:@"toCommentId"];
    }
    
    [[BusinessDataCtrlManager getInstance] writeCommentData:param delegate:self];
    
}

- (void)newCommnetSucessed{
    [self dismissModalViewControllerAnimated:YES];
    
    [self.delegate newCommnetSucessed];
    
    
}

#pragma mark  UITextViewのデリゲートメソッド
- (void)textViewDidChange:(UITextView *)textView{
    if (self.txtView.text != nil && ![self.txtView.text isEqualToString:@""]) {
        self.saveButton.enabled = YES;
    }else{
        self.saveButton.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
