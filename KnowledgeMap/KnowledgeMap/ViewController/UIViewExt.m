//
//  UIViewExt.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/12/20.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "UIViewExt.h"

@implementation UIViewExt

@synthesize tapPoint;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    
    // タッチされたビューを取得する
    UIView *hitView = [super hitTest:point withEvent:event];
    
    self.tapPoint = point;
    
    /*
    // タッチされたものがselfだったらイベントを発生させない
    if ( self == hitView )
    {
        return nil;
    }
    */
    
    // それ意外だったらイベント発生させる
    return hitView;
}


@end
