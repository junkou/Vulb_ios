//
//  CardPartsDetailInfoViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/22.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "CardPartsDetailInfoViewController.h"

@interface CardPartsDetailInfoViewController ()

@end

@implementation CardPartsDetailInfoViewController

@synthesize delegate;
@synthesize resourceInfo;

CGPoint offset;
CGFloat baseHight;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];

    NSString *resourceType = [Utils parseWebDataToString:self.resourceInfo dataKey:resourceType_key];
    if ([resourceType isEqualToString:DB_RESOURCE_TYPE_PHOTO]
        || [resourceType isEqualToString:DB_RESOURCE_TYPE_IllUSTRATION]) {
        
        NSString *resourceData = [self.resourceInfo objectForKey:RESOURCEDATA];
        NSData *imageData = [NSData dataFromBase64String:resourceData];


        baseHight = SCREEN_HEIGHT-TOOL_BAR_HIGHT;
        if ([Utils versionIsSeven]) {
            self.scrollView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH, baseHight)] autorelease];
        }else{
             self.scrollView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, TOOL_BAR_HIGHT,SCREEN_WIDTH, baseHight)] autorelease];

        }

        //scrollView.backgroundColor = [UIColor redColor];
        [self.scrollView setMinimumZoomScale:1.0];
        [self.scrollView setMaximumZoomScale:5.0];
        [self.scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, baseHight)];
        self.scrollView.delegate = self;
        [self.view addSubview:self.scrollView];

        UIImage *image = [UIImage imageWithData:imageData];
        self.imageView = [[[UIImageView alloc] initWithImage:image] autorelease];
        [self.scrollView addSubview:self.imageView];

        // Update image view
        [self updateImageViewSize];
        [self updateImageViewOrigin];
        
    }
     
}


- (void)updateImageViewSize
{
    
    // Get image size
    CGSize  imageSize;
    imageSize = _imageView.image.size;
    
    // Decide image view size
    CGRect  bounds;
    CGRect  rect;
    bounds = _scrollView.bounds;
    rect.origin = CGPointZero;
    if (imageSize.width / imageSize.height > CGRectGetWidth(bounds) / CGRectGetHeight(bounds)) {
        rect.size.width = CGRectGetWidth(bounds);
        rect.size.height = floor(imageSize.height / imageSize.width * CGRectGetWidth(rect));
    }
    else {
        rect.size.height = CGRectGetHeight(bounds);
        rect.size.width = imageSize.width / imageSize.height * CGRectGetHeight(rect);
    }
    
    // Set image view frame
    _imageView.frame = rect;
     
    
}

- (void)updateImageViewOrigin
{
    // Get image view frame
    CGRect  rect;
    rect = _imageView.frame;
    
    // Get scroll view bounds
    CGRect  bounds;
    bounds = _scrollView.bounds;
    
    // Compare image size and bounds
    rect.origin = CGPointZero;
    if (CGRectGetWidth(rect) < CGRectGetWidth(bounds)) {
        rect.origin.x = floor((CGRectGetWidth(bounds) - CGRectGetWidth(rect)) * 0.5f);
    }
    if (CGRectGetHeight(rect) < CGRectGetHeight(bounds)) {
        rect.origin.y = floor((CGRectGetHeight(bounds) - CGRectGetHeight(rect)) * 0.5f);
    }
    
    // Set image view frame
    _imageView.frame = rect;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)_scrollView {
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView*)scrollView
{
    // Update image view origin
    [self updateImageViewOrigin];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
