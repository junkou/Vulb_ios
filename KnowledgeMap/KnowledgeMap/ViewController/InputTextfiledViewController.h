//
//  InputTextfiledViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/10.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Utils.h"

@interface InputTextfiledViewController : UIViewController{
    id delegate;
	UITextField	*txtField;
    NSString *txtFieldPlaceholder;
    NSString *txtFieldValue;

}

@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) UITextField	*txtField;
@property (nonatomic, strong) NSString *txtFieldPlaceholder;
@property (nonatomic, strong) NSString *txtFieldValue;

@end
