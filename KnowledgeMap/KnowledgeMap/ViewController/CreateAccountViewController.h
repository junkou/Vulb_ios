//
//  CreateAccountViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/13.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "AppApiDelegate.h"

@interface CreateAccountViewController : UIViewController < UITextFieldDelegate>{
    id delegate;

}

@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) UIButton	*saveButton;
@property (nonatomic, strong) UIScrollView	*scrollView;
@property (nonatomic, strong) UITextField	*txtMailAddress;
@property (nonatomic, strong) UITextField	*txtMailAddressConfirm;
@property (nonatomic, strong) UITextField	*txtPassword;
@property (nonatomic, strong) UITextField	*txtPasswordConfirm;
@property (nonatomic, strong) UITextField	*txtName;

@end
