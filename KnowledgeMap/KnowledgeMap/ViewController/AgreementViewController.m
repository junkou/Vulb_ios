//
//  AgreementViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/09/22.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "AgreementViewController.h"

@interface AgreementViewController ()

@end

@implementation AgreementViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.agreementFlag = FALSE;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = [Utils getBaseTextColor];
    
    //HeaderView
    [Utils setImageToNavigationBar:self.navigationController.navigationBar imageName:[Utils getResourceIconPath:@"header.png"]];
    
    UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, TOOL_BAR_HIGHT)] autorelease];
    self.navigationItem.titleView = view;
    
    //キャンセルボタンを生成
    [Utils setNavigationBarLeftButton:self.navigationController.navigationBar navigationItem:self.navigationItem controller:self];

    //Add NavigationBar Title
    [Utils setNavigationBarTitleLabel:self.navigationItem.titleView text:@"Agreement"];
    
    if ([Utils versionIsSeven]) {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
    

    //CGFloat x = 10;
    CGFloat y = 10;
    CGFloat buttonHeight = 30;
    CGFloat buttonWidth = 260;
    
  
    //NextButton positionY
    CGFloat positionY =self.view.bounds.size.height-buttonHeight-y*2-TOOL_BAR_HIGHT;
    if ([Utils versionIsSeven]) {
        positionY = positionY-20;
    }

    //NextButton
    self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextButton.frame = CGRectMake((SCREEN_WIDTH-buttonWidth)/2, positionY, buttonWidth, buttonHeight);
    [self.nextButton addTarget:self action:@selector(clickNextButton:) forControlEvents:UIControlEventTouchUpInside];
    //self.nextButton.backgroundColor = [UIColor blueColor];
    [self.nextButton setImage:[Utils getBlueButtonNormalImage:self.nextButton.bounds] forState:UIControlStateNormal];
    [self.nextButton setImage:[Utils getBlueButtonSelectedImage:self.nextButton.bounds] forState:UIControlStateHighlighted];
    self.nextButton.enabled = NO;
    [self.view addSubview:self.nextButton];
    
    UILabel *label = [[[UILabel alloc] initWithFrame:self.nextButton.bounds] autorelease];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = UITextAlignmentCenter;
    label.font = [UIFont  boldSystemFontOfSize:BUTTON_TEXT_INIT_SIZE];
    label.text =  NSLocalizedString(@"Next",@"Next");
    [self.nextButton addSubview:label];

    //Agree Checkbox
    self.checkBox = [UIButton buttonWithType:UIButtonTypeCustom];
    self.checkBox.frame = CGRectMake((SCREEN_WIDTH-buttonWidth)/2, positionY - BUTTON_TEXT_INIT_SIZE*1.5 - y*1.5, buttonWidth, BUTTON_TEXT_INIT_SIZE*1.5);
    [self.checkBox addTarget:self action:@selector(clickcheckBoxButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.checkBox];
    
    UIImage *image = [UIImage imageNamed:[Utils getResourceIconPath:@"checkbox_normal.png"]];
    self.checkMark = [[[UIImageView alloc] initWithFrame:CGRectMake(0, (self.checkBox.bounds.size.height - image.size.height)/2, image.size.width, image.size.height)] autorelease];
    self.checkMark.image =image;
    [self.checkBox addSubview:self.checkMark];
    
    label = [[[UILabel alloc] initWithFrame:CGRectMake(image.size.width+5, 0, self.checkBox.bounds.size.width-image.size.width-5, self.checkBox.bounds.size.height)] autorelease];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = UITextAlignmentLeft;
    label.font = [UIFont  boldSystemFontOfSize:BUTTON_TEXT_INIT_SIZE];
    label.text =  NSLocalizedString(@"Agree",@"Agree");
    [self.checkBox addSubview:label];

    //Agree View
    self.agreementView = [[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.checkBox.frame.origin.y-y*3 )] autorelease];
//    self.agreementView.layer.cornerRadius = 5.0f;
//    self.agreementView.clipsToBounds = YES;
    NSString *path = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat:@"agreement_%@",[Utils getSystemLangCode]]  ofType: @"htm"];;
    [self.agreementView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path] ]];
    [self.view addSubview: self.agreementView];
    
    
 


}

-(void)clickCancelButton:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
    
}

-(void)clickcheckBoxButton:(id)sender{
    if (self.agreementFlag) {
        self.agreementFlag = FALSE;
        UIImage *image = [UIImage imageNamed:[Utils getResourceIconPath:@"checkbox_normal.png"]];
        self.checkMark.image =image;
        self.nextButton.enabled = NO;

    }else{
        self.agreementFlag = TRUE;
        UIImage *image = [UIImage imageNamed:[Utils getResourceIconPath:@"checkbox_active.png"]];
        self.checkMark.image =image;
        self.nextButton.enabled = YES;

    }
}

-(void)clickNextButton:(id)sender{
    if (self.loginType == LOGIN_TYPE_MAIL) {
        LoginViewController *loginViewController = [[[LoginViewController alloc] init] autorelease];
        loginViewController.delegate = self;
        
        UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:loginViewController] autorelease ];
        
        [self presentModalViewController:controller animated:YES];
    }else if(self.loginType == LOGIN_TYPE_FACEBOOK){
        [[BusinessDataCtrlManager getInstance] loginWithFacebook:self];
        
    }else if(self.loginType == CREATE_NEW_ACCOUNT){
        CreateAccountViewController *createAccountViewController = [[[CreateAccountViewController alloc] init] autorelease];
        createAccountViewController.delegate = self;
        
        UINavigationController *controller = [[[UINavigationController alloc] initWithRootViewController:createAccountViewController] autorelease ];
        
        [self presentModalViewController:controller animated:YES];

    }
    
}


#pragma mark - WorkDataControllerDelegate Delegate
- (void)createUserSucessed{
    
    [self dismissModalViewControllerAnimated:NO];
    
}

- (void)facebookUrlSucessed:(NSMutableDictionary *)facebookInfo{
    
    NSString *urlStr = [facebookInfo objectForKey:@"url"];
    //NSLog(@"%@",urlStr);
    NSURL* url = [NSURL URLWithString:urlStr];
	[[UIApplication sharedApplication] openURL:url];

}

- (void)logOut{
    [self clickcheckBoxButton:nil];
    
    [self dismissModalViewControllerAnimated:YES];
    
}

- (void)loginFaild{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
