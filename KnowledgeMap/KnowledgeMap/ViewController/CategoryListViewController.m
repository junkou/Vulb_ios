//
//  CategoryListViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/01.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "CategoryListViewController.h"

@interface CategoryListViewController ()

@end

@implementation CategoryListViewController

@synthesize delegate;
@synthesize dataList;
@synthesize lastIndexPath;
@synthesize currentCategoryId;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.dataList = [[NSMutableArray alloc] init];
         
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //add NavicationBar RightButton
   // [Utils setNavigationBarRightButton:self.navigationItem controller:self];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

#pragma mark - NavicationBar SaveButton Call Method
-(void)clickSaveButton:(id)sender {
    [self newCategoryID:self.currentCategoryId];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dataList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
	return CATEGORY_LIST_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"Cell";
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d",[indexPath row]] ;

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
     */
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        CGRect rect = cell.contentView.frame;
        rect.size.height = CATEGORY_LIST_HEIGHT;
        cell.contentView.frame = rect;

        // Configure the cell...
        NSMutableDictionary *categoryData = [self.dataList objectAtIndex:[indexPath row]];
//        [Utils setCategoryDataContents:categoryData view:cell.contentView];
        
        NSNumber *localCategoryId = [categoryData objectForKey:categoryId_Key];
        if (self.currentCategoryId == [localCategoryId intValue]  ) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.lastIndexPath = indexPath;
        }else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    

    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *data = [self.dataList objectAtIndex:[indexPath row]];
    
    self.currentCategoryId = [[data objectForKey:categoryId_Key] intValue];
    
    if (self.lastIndexPath != nil ) {
		//NSLog(@"self.lastIndexPath:%d",[self.lastIndexPath row]);
		UITableViewCell *oldCell  = [tableView cellForRowAtIndexPath:self.lastIndexPath];
		oldCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    UITableViewCell *newCell  = [tableView cellForRowAtIndexPath:indexPath];
    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    
    self.lastIndexPath = indexPath;

    [self newCategoryID:self.currentCategoryId];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark - WorkDataControllerDelegate Delegate
- (void)newCategoryID:(int)categoryId{
    NSMutableArray *categoryList = [BusinessDataCtrlManager getCategoryList];
    [categoryList removeAllObjects];
    [categoryList addObjectsFromArray:self.dataList];
    
    [BusinessDataCtrlManager getUserInfo].lastCategoryId = [NSNumber numberWithInt:categoryId];
    
    [self.delegate newCategoryID:categoryId];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Memory management
- (void)dealloc {
    self.delegate = nil;

	[self.dataList release];;
    
	[super dealloc];
    
	
}


@end
