//
//  CardListViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/21.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "AppApiDelegate.h"
#import "CardDetailViewController.h"

@interface CardListViewController : UITableViewController <UIGestureRecognizerDelegate, AppApiDelegate>{
    
    id delegate;
    
    NSMutableArray		*dataList;
    
    int					lastPosition ;

}

@property (nonatomic, assign) id delegate;

@property (nonatomic, assign) NSMutableArray		*dataList;
@property (nonatomic, strong) UIView    *clickedCardThumbnailView;

@property int					lastPosition ;

-(void)addMoreBooksButton;

@end
