//
//  EditTextViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/12/03.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Utils.h"
#import "AppApiDelegate.h"

@interface EditTextViewController : UIViewController< AppApiDelegate,UITextViewDelegate>{
    id delegate;
    NSMutableDictionary		*cardInfo;
    NSString                *originalText;

}

@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) NSMutableDictionary		*cardInfo;
@property (nonatomic, strong) NSString                *originalText;

@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, strong) UITextView *textView;


@end
