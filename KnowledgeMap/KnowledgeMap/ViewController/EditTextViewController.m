//
//  EditTextViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/12/03.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "EditTextViewController.h"

@interface EditTextViewController ()

@end

@implementation EditTextViewController

@synthesize delegate;
@synthesize cardInfo;
@synthesize originalText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if ([Utils versionIsSeven]) {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
    
    //HeaderView
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    
    //add NavicationBar RightButton
    [Utils setNavigationBarRightSaveButton:self.navigationItem controller:self];
    
    self.view.backgroundColor = [Utils getBaseTextColor];

    //TextFiled
    self.textView = [[[UITextView alloc] initWithFrame:CGRectMake(10, 10, 300, SCREEN_HEIGHT-TOOL_BAR_HIGHT- KEY_BOARD_HEIGHT - 20)] autorelease];
    self.textView.backgroundColor = [UIColor whiteColor];
    self.textView.font = [UIFont systemFontOfSize:16];
    self.textView.text = self.originalText;
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    [self.textView becomeFirstResponder];

}

#pragma mark - NavicationBar SaveButton Call Method
-(void)clickSaveButton:(id)sender {
    
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
    
    [[BusinessDataCtrlManager getInstance] startEditCard:param delegate:self];

    
}


#pragma mark - AppApiDelegate delegate
- (void)startEditCardSucessed:(NSMutableDictionary *)Info{
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
    
    NSString *contents = [self.cardInfo objectForKey:CONTENTS];
    contents = [contents stringByReplacingOccurrencesOfString:self.originalText withString:[self.textView.text  stringByReplacingMatchesOfPattern:@"\n" withString:@"<br>"]];
    [param setObject:contents forKey:CONTENTS];
  
    [param setObject:[self.cardInfo objectForKey:RESOURCEIDS] forKey:RESOURCEIDS];

    [[BusinessDataCtrlManager getInstance] updateCard:param delegate:self];

}

- (void)updateCardSucessed:(NSMutableDictionary *)Info{
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
    
    [[BusinessDataCtrlManager getInstance] endEditCard:param delegate:self];

}

- (void)endEditCardSucessed:(NSMutableDictionary *)Info{
    
    NSMutableDictionary *data = [[[NSMutableDictionary alloc] init] autorelease];
    [data setObject:DB_RESOURCE_TYPE_BEGINING forKey:resourceType_key];
    [data setObject:self.textView.text forKey:begining_key];
    
    [self.delegate endEditCardSucessed:data];
    
    [self.navigationController popViewControllerAnimated:YES];

}


#pragma mark  UITextViewのデリゲートメソッド
- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text != nil && ![textView.text isEqualToString:@""]) {
        self.saveButton.enabled = YES;
    }else{
        self.saveButton.enabled = NO;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
