//
//  CardDetailViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/21.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "CardPartsDetailInfoViewController.h"
#import "CommentListViewController.h"
#import "WebServerViewController.h"
#import "HttpClientUtils.h"
#import "ViolationInputViewController.h"
#import "EditTextViewController.h"
#import "MEActionSheet.h"
#import "UIViewExt.h"

#define IS_LEFT     1
#define IS_RIGHT    2

@interface CardDetailViewController : UIViewController <UITableViewDataSource,UITableViewDelegate, UIGestureRecognizerDelegate, UIWebViewDelegate>{
    id delegate;
    
    NSMutableArray		*dataList;
    NSMutableDictionary		*cardInfo;
 
    NSMutableDictionary		*cardDetailInfoList;
    int currentCardIndex;

}

@property (nonatomic, assign) id delegate;

@property (nonatomic, strong) NSMutableArray		*dataList;
@property (nonatomic, strong) NSMutableDictionary		*cardInfo;
@property (nonatomic, strong) UIButton *maskView;
@property (nonatomic, strong) UILabel *commentCountLable;

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;


@property (nonatomic, assign) NSMutableArray            *cardThumbnailList;
@property (nonatomic, strong) NSMutableDictionary		*cardDetailInfoList;
@property (nonatomic) int currentCardIndex;

@property (nonatomic) int leftOrRightFlag;

@property (nonatomic, assign) UITableView            *tableView;
@property (nonatomic, assign) UITableView            *oldTableView;

@end
