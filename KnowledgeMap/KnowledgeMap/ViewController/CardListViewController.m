//
//  CardListViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/21.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "CardListViewController.h"

@interface CardListViewController ()

@end

@implementation CardListViewController

@synthesize delegate;
@synthesize dataList;
@synthesize lastPosition;



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([Utils versionIsSeven]) {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.view.backgroundColor = [Utils getBaseTextColor];

    [self addMoreBooksButton];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    UISwipeGestureRecognizer *swipeRightGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToRight:)] autorelease];
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	swipeRightGesture.numberOfTouchesRequired = 1;
    swipeRightGesture.delegate = self;
	[self.tableView addGestureRecognizer:swipeRightGesture];

}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:[self.dataList count]-1 inSection:0];
	[self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];

    
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    
	
}



-(void)addMoreBooksButton{
    if ([self.dataList count] == 0) {
        return;
    }
    
            
    if ([self.dataList count] >= CARD_ROW_COUNT) {
        UIView *footerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, TOOL_BAR_HIGHT)] autorelease];
        //footerView.backgroundColor = [UIColor whiteColor];
        self.tableView.tableFooterView = footerView;
        
        UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake(10, 6, CARD_WIDTH, 32)] autorelease];
        [button addTarget:self action:@selector(searchCardListDatas) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:button];
        CGRect rect = CGRectMake(0, 0, CARD_WIDTH, 32);
        [Utils setCommonButtonProperti:button rect:rect title:@"More"];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dataList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
	return CARD_HEIGHT+10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSMutableDictionary *cardData = [self.dataList objectAtIndex:[indexPath row]];
   // NSNumber *resourceId = [Utils parseWebDataToNumber:cardData dataKey:resourceId_key];

    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d",[indexPath row]] ;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
     */
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        UIView *baseView = [[[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-CARD_WIDTH)/2, 0, CARD_WIDTH, CARD_HEIGHT)] autorelease];
        baseView.backgroundColor = [UIColor whiteColor];
        [cell.contentView addSubview:baseView];
        cell.backgroundColor = [UIColor clearColor];
        
        NSMutableDictionary *categoryInfo =  [BusinessDataCtrlManager getCategoryInfo:[BusinessDataCtrlManager getUserInfo].lastCategoryId];
        NSNumber *categoryColoer = [Utils parseWebDataToNumber:categoryInfo dataKey:colorIdx_key];

        // Configure the cell...
        [Utils setCardListDataContents:cardData view:baseView colorBar:[categoryColoer intValue] delegate:self];

    }
    
    
    return cell;

}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSMutableDictionary *data = [self.dataList objectAtIndex:[indexPath row]];

    NSNumber *cardId = [data objectForKey:cardId_key];

    self.clickedCardThumbnailView = [tableView cellForRowAtIndexPath:indexPath].contentView;

    [[BusinessDataCtrlManager getInstance] getCardInfoWithCardId:cardId delegate:self];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (void)cardInfoSucessed:(NSMutableDictionary *)cardInfo{
    
    UIView *readStatusView = [self.clickedCardThumbnailView viewWithTag:READ_STATUS_TAG];
    [readStatusView removeFromSuperview];
    

    NSString *contents = [cardInfo objectForKey:CONTENTS];
    // NSLog(contents);
    
    NSMutableArray *cardInfoDetailList = [[NSMutableArray alloc] init];
    [Utils changeCardInfoDetailList:contents output:cardInfoDetailList];
    
    CardDetailViewController *cardDetailViewController = [[CardDetailViewController alloc] init];
    cardDetailViewController.delegate = self;
    [cardDetailViewController.dataList addObjectsFromArray:cardInfoDetailList];
    cardDetailViewController.cardInfo = cardInfo;
     [self.navigationController pushViewController:cardDetailViewController animated:YES];
    [cardDetailViewController release];
    
    [cardInfoDetailList release];
    
    
}


-(void)returnFromCardInfoDetailView:(NSNumber *)cardId{
    for (int i=0; i<[self.dataList count]; i++) {
        NSMutableDictionary *cardInfo = [self.dataList objectAtIndex:i];
        if ([[cardInfo objectForKey:cardId_key] intValue] == [cardId intValue]) {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            UIView *readStatusView = [cell viewWithTag:READ_STATUS_TAG];
            [readStatusView removeFromSuperview];
            break;
        }
    }
}


-(void)searchCardListDatas{
    //Card一覧を取得する
    self.lastPosition = [self.dataList count];
    [[BusinessDataCtrlManager getInstance] getCardList:self startIdx:self.lastPosition];
}

#pragma mark Cardlist Delegate
- (void)cardListSucessed{
    
    self.title = [NSString stringWithFormat:@"%@(%d)", MultiLangString(@"Card List") ,[self.dataList count]];

    NSMutableArray *insertArray = [[NSMutableArray alloc] init];
    NSIndexPath *insertIndexPath = nil;
    int addPotion = lastPosition;
    for (int i=0; i < [self.dataList count]-self.lastPosition; i++) {
        insertIndexPath = [NSIndexPath indexPathForRow:addPotion+i inSection:0];
        [insertArray addObject:insertIndexPath];
    }
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:insertArray withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    [insertArray release];
    
    
    NSMutableDictionary *categoryInfo = [BusinessDataCtrlManager  getCategoryInfo:[BusinessDataCtrlManager getUserInfo].lastCategoryId ];
    
    NSNumber *totalCardCount = [categoryInfo objectForKey:totalCard_key];
    if ([self.dataList count] >= [totalCardCount intValue]) {
        self.tableView.tableFooterView = nil;
    }

}


#pragma mark - getHtmlHeight
-(CGFloat)getHtmlHeight:(NSString *)htmlText{
    
    return [self.delegate getHtmlHeight:htmlText];
    
}


#pragma mark - PanGestureRecognizer
-(void)viewDidDraggingToRight:(UISwipeGestureRecognizer *)swipe{
    //    NSLog(@"========handleRightSwipeGesture========");
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark Memory management
- (void)dealloc {
    self.delegate = nil;
    
    [[DownLoadSimpleWebDataServerController getInstance] clearDelegate];
    
	[super dealloc];
    
	
}


@end
