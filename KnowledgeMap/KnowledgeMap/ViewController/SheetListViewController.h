//
//  SheetListViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/01.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "CategoryListViewController.h"
#import "TestDataController.h"
#import "AppApiDelegate.h"
#import "DownLoadSimpleWebDataServerController.h"


@interface SheetListViewController : UITableViewController <AppApiDelegate>{
    id delegate;
	
    int currentSheetId;
    int currentCategoryId;
    NSMutableArray		*sheetList;
	NSIndexPath	*lastIndexPath;

}

@property (nonatomic, assign) id delegate;

@property (nonatomic) int currentSheetId;
@property (nonatomic) int currentCategoryId;
@property (nonatomic, assign) NSMutableArray		*sheetList;
@property (nonatomic, retain) NSIndexPath	*lastIndexPath;


@end
