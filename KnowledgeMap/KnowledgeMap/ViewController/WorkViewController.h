//
//  WorkViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/02.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "InputTextViewController.h"
#import "PhotoCameraViewController.h"
#import "PhotoGroupViewController.h"
#import "CardListViewController.h"
#import "CardDetailViewController.h"
#import "IllustSelectViewController.h"
#import "SheetListExtViewController.h"

#import "CreateSheetViewController.h"
#import "CreateCategoryViewController.h"

#import "UserListViewController.h"

#define TITLE_VIEW_X_DELTA  8

@interface WorkViewController : SheetListExtViewController <UIGestureRecognizerDelegate, UIWebViewDelegate>{
    UIButton	*inputTextButton;
    UIButton	*inputCameraButton;
    UIButton	*inputPhotoLibraryButton;
    UIButton	*inputIllustButton;
  
    UIButton	*selectSheetButton;
    UIButton	*selectCategoryButton;
    
    int currentCategoryId;
    int categoryColorId;
    
    int contentsMode;  //1:Camera 2:PhotoLaibray
    
    PhotoCameraViewController *photoCameraViewController;
    
    NSNumber *clickThumbnailCardId;
    NSString *clickCardResourceType;
    NSString *clickCardBeginining;
   
    BOOL    fromOpenURLFlag;

    UIWebView *htmlCreateWebView;
}

@property (nonatomic, strong) UIView    *title_1_View;
@property (nonatomic, strong) UIView    *title_2_View;
@property (nonatomic, strong) UIView    *workBaseView;
@property (nonatomic, strong) UIButton	*inputTextButton;
@property (nonatomic, strong) UIButton	*inputCameraButton;
@property (nonatomic, strong) UIButton	*inputPhotoLibraryButton;
@property (nonatomic, strong) UIButton	*inputIllustButton;
@property (nonatomic, strong) UIButton	*selectSheetButton;
@property (nonatomic, strong) UIButton	*selectCategoryButton;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton	*dummyButton;

@property (nonatomic) int     categoryColorId;
@property (nonatomic) int     contentsMode;
@property (nonatomic, assign) PhotoCameraViewController *photoCameraViewController;
@property (nonatomic, strong) NSNumber *clickThumbnailCardId;
@property (nonatomic, strong) NSString *clickCardResourceType;
@property (nonatomic, strong) NSString *clickCardBeginining;
@property (nonatomic, strong) UIView    *clickedCardThumbnailView;
@property (nonatomic) BOOL    fromOpenURLFlag;

@property (nonatomic, strong) UIWebView *htmlCreateWebView;

- (void)setButtonProPerty:(UIButton *)button lineColor:(int)clolerId imgName:(NSString *)imgName label:(NSString *)labelText ;

- (void)setListButtonProPerty:(UIButton *)button ;
- (void)resetMainButtonColorBar:(int)colorIdx;
-(CGFloat)getHtmlHeight:(NSString *)htmlText;


@end
