//
//  CardPartsDetailInfoViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/22.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"

@interface CardPartsDetailInfoViewController : UIViewController<UIScrollViewDelegate>{
    id delegate;
    
    NSMutableDictionary		*resourceInfo;

}

@property (nonatomic, assign) id delegate;

@property (nonatomic, strong) NSMutableDictionary		*resourceInfo;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView    *imageView;

@end
