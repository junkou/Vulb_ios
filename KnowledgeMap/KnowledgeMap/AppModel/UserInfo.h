//
//  UserInfo.h
//  BookMaker
//
//  Created by k-kin on 10/07/05.
//  Copyright 2010 ICCESOFT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseTableNmaes.h"

@interface UserInfo : NSObject {
	NSNumber *userId;
	NSString *mailAddress;
	NSString *pass;
    NSString *tokenId;
    NSString *sessionId;
    NSString *userName;
	NSNumber *cardSize;
	NSString *local;
    NSString *personImg;
  	NSNumber *lastSheetId;
  	NSNumber *lastCategoryId;

}

@property  (nonatomic,retain) NSNumber *userId;
@property  (nonatomic,retain) NSString *mailAddress;
@property  (nonatomic,retain) NSString *pass;
@property  (nonatomic,retain) NSString *tokenId;
@property  (nonatomic,retain) NSString *sessionId;
@property  (nonatomic,retain) NSString *userName;
@property  (nonatomic,retain) NSNumber *cardSize;
@property  (nonatomic,retain) NSString *local;
@property  (nonatomic,retain) NSString *personImg;
@property  (nonatomic,retain) NSNumber *lastSheetId;
@property  (nonatomic,retain) NSNumber *lastCategoryId;

- (void)toChangeObjectValues:(NSDictionary *)indata;
- (void)toCreateDictionaryValues:(NSDictionary *)indata;
- (void)clearData;

@end
