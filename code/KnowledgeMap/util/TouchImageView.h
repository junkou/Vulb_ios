//
//  TouchImageView.h
//  KnowledgeMap
//
//  Created by KouJun on 5/17/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TouchImageView <NSObject>
-(void)tappedOnImageView:(UIImageView*)imageView;
@end
@interface TouchImageView : UIImageView
@property(nonatomic,assign) id delegate;
@end
