//
//  NSString+RegExp.m
//  Memogram
//
//  Created by 雅彦 竹本 on 12/06/19.
//  Copyright (c) 2012年 小麦 株式会社. All rights reserved.
//

#import "NSString+RegExp.h"

@implementation NSString (RegExp)

- (NSString *)stringByReplacingMatchesOfPattern:(NSString *)pattern withString:(NSString *)replacement
{
    NSMutableString *str = [self mutableCopy];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionDotMatchesLineSeparators error:NULL];
    [regex replaceMatchesInString:str options:0 range:NSMakeRange(0, [str length]) withTemplate:replacement];
    
    return str;
}

- (NSArray *)matchedStringsByMatchesOfPattern:(NSString *)pattern
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionDotMatchesLineSeparators error:NULL];
    NSArray *matches = [regex matchesInString:self options:0 range:NSMakeRange(0, [self length])];
    
    for (NSTextCheckingResult *result in matches) {
        for (int i = 0; i < [result numberOfRanges]; i++) {
            NSRange r = [result rangeAtIndex:i];
            [results addObject:[self substringWithRange:r]];
        }
    }
    return results;
}

@end
