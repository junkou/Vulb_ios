//
//  ExtImageView.h
//  ThreadBunkatu
//
//  Created by k-kin on 11/11/15.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@interface ImagePartitionView : UIView {
	NSArray *images;
    BOOL    verticalDrawFlag;
}

@property (nonatomic, retain) NSArray *images; 
@property BOOL verticalDrawFlag;

-(void)drawHorizonRect:(CGRect)inRect yPotion:(CGFloat)inY leftImage:(UIImage *)imageLeft centerImage:(UIImage *)imageCenter rightImage:(UIImage *)imageRight;

-(UIImage*)trimImage:(UIImage*)image  frameRect:(CGRect)frameRect;

-(UIImage *)getScreenShotImage;

@end
