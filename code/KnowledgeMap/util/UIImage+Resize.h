//
//  UIImage+Resize.h
//  Memogram
//
//  Created by 雅彦 竹本 on 12/06/20.
//  Copyright (c) 2012年 小麦 株式会社. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

- (UIImage *)imageFitInSize:(CGSize)size;
- (UIImage *)imageFilledInSize:(CGSize)size;

@end
