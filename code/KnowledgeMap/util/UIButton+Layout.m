//
//  UIButton+Layout.m
//  BaccaratClient
//
//  Created by jun kou on 11/6/12.
//  Copyright (c) 2012 wicresoft. All rights reserved.
//

#import "UIButton+Layout.h"

@implementation UILayButton
- (void)update
{
    CGFloat spacing = 8.0;
    if (self.layout == ImageLeftTextRight)
    {
        self.imageView.frame = CGRectMake(10, self.imageView.frame.origin.y, self.imageView.frame.size.width, self.imageView.frame.size.height);
        self.titleLabel.frame = CGRectMake(10+self.imageView.frame.size.width+spacing, self.titleLabel.frame.origin.y, self.titleLabel.frame.size.width, self.titleLabel.frame.size.height);
    }
    else if(self.layout == ImageUpTextDown)
    {
        //        self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, (self.bounds.size.width-titleSize.width)/2 + imageSize.width + spacing);
        //        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, self.bounds.size.width - titleSize.width - spacing);
    }
    else if (self.layout == TextLeftImageRight)
    {
        //        self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, (self.bounds.size.width-titleSize.width)/2 + imageSize.width + spacing);
        //        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, self.bounds.size.width - titleSize.width - spacing);
    }
    else
    {
        //        self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, (self.bounds.size.width-titleSize.width)/2 + imageSize.width + spacing);
        //        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, self.bounds.size.width - titleSize.width - spacing);
    }
}

-(void)setLayout:(EContentLayout)layout
{
    _layout = layout;
    [self update];
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    [self setLayout:self.layout];
}
@end

