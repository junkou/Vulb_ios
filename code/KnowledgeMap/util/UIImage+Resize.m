//
//  UIImage+Resize.m
//  Memogram
//
//  Created by 雅彦 竹本 on 12/06/20.
//  Copyright (c) 2012年 小麦 株式会社. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UIImage+Resize.h"

@implementation UIImage (Resize)

/*
 * 任意のサイズにリサイズする。（縦横比は維持しない）
 */
- (UIImage *)imageResizedUsingGPU:(CGSize)size scale:(CGFloat)screenScale
{
    CIImage *sourceImage = [[CIImage alloc] initWithCGImage:self.CGImage];
    
    CGRect imageRect = [sourceImage extent];
    CIImage *filteredImage = sourceImage;
    
    switch (self.imageOrientation) {
        
        case UIImageOrientationUp:
        {
            // 処理なし
            break;
        }   
        case UIImageOrientationLeft:
        {
            CGAffineTransform translate = CGAffineTransformMakeTranslation(imageRect.size.height, 0.0);
            CGAffineTransform rotate = CGAffineTransformRotate(translate, M_PI_2);
            filteredImage = [filteredImage imageByApplyingTransform:rotate];
            break;
        }   
        case UIImageOrientationDown:
        {
            CGAffineTransform translate = CGAffineTransformMakeTranslation(imageRect.size.width, imageRect.size.height);
            CGAffineTransform rotate = CGAffineTransformRotate(translate, M_PI);
            filteredImage = [filteredImage imageByApplyingTransform:rotate];
            break;
        }   
        case UIImageOrientationRight:
        {
            CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, imageRect.size.width);
            CGAffineTransform rotate = CGAffineTransformRotate(translate, M_PI + M_PI_2);
            filteredImage = [filteredImage imageByApplyingTransform:rotate];
            break;
        }   
        default:
            break;
    }
    
    CGPoint scale = CGPointMake(size.width / self.size.width,
                                size.height / self.size.height);
    
    filteredImage = [filteredImage imageByApplyingTransform:CGAffineTransformMakeScale(scale.x, scale.y)];
    filteredImage = [filteredImage imageByCroppingToRect:CGRectMake(0, 0, size.width, size.height)];
    
    CIContext *ciContext = [CIContext contextWithOptions:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:kCIContextUseSoftwareRenderer]];
    
    CGImageRef imageRef = [ciContext createCGImage:filteredImage fromRect:[filteredImage extent]];
    UIImage *outputImage = [UIImage imageWithCGImage:imageRef scale:screenScale orientation:UIImageOrientationUp];
    CGImageRelease(imageRef);
    
    return outputImage;
}

/*
 * 任意のサイズにリサイズする。（縦横比は維持しない）
 */
- (UIImage *)imageResized:(CGSize)size scale:(CGFloat)scale
{
    size = CGSizeMake(size.width / scale, size.height / scale);
    
    if (UIGraphicsBeginImageContextWithOptions != NULL) {
        UIGraphicsBeginImageContextWithOptions(size, NO, scale);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    
    [self drawInRect:CGRectMake(0.0, 0.0, size.width, size.height)];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}

/*
 * 縦横比を維持して、領域に収まる最大サイズにリサイズする。
 */
- (UIImage *)imageFitInSize:(CGSize)size
{
    BOOL retina = [UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0;
    
    CGSize targetSize;
    CGFloat scale;
    if (retina == YES) {
        scale = 2.0;
        targetSize = CGSizeMake(size.width * scale, size.height * scale);
    } else {
        scale = 1.0;
        targetSize = size;
    }
    
    CGFloat width = 0;
    CGFloat height = 0;
    
    CGFloat ratio = self.size.height / self.size.width;
    width = targetSize.width;
    height = targetSize.width * ratio;
    
    if (height > targetSize.height) {
        CGFloat ratio = self.size.width / self.size.height;
        width = targetSize.height * ratio;
        height = targetSize.height;
    }
    
    return [self imageResized:CGSizeMake(width, height) scale:scale];
}

/*
 * 縦横比を維持して、領域を埋める最小サイズにリサイズする。
 */
- (UIImage *)imageFilledInSize:(CGSize)size
{
    BOOL retina = NO; // [UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0;
    
    CGSize targetSize;
    CGFloat scale;
    if (retina == YES) {
        scale = 2.0;
        targetSize = CGSizeMake(size.width * scale, size.height * scale);
    } else {
        scale = 1.0;
        targetSize = size;
    }
    
    CGFloat width = 0;
    CGFloat height = 0;
    
    CGFloat ratio = self.size.height / self.size.width;
    width = targetSize.width;
    height = targetSize.width * ratio;
    
    if (height < targetSize.height) {
        CGFloat ratio = self.size.width / self.size.height;
        width = targetSize.height * ratio;
        height = targetSize.height;
    }
    
    return [self imageResized:CGSizeMake(width, height) scale:scale];
}

@end
