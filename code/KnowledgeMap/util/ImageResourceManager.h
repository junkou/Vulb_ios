//
//  ImageResourceManager.h
//  BukurouSS
//
//  Created by k-kin on 11/10/30.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataRegistry.h"
#import "Utils.h"

@interface ImageResourceManager : NSObject {

}

+(ImageResourceManager *)getInstance;

-(void)getLatestImageResourceInfo:(int)type  pageCode:(NSString *)pageCode index:(int)index result:(NSMutableDictionary *)outData;
-(NSString *)getLatestImageResourcePath:(int)type  pageCode:(NSString *)pageCode index:(int)index;
-(NSData *)getLatestImageResource:(int)type  pageCode:(NSString *)pageCode index:(int)index;
-(NSString *)getLatestSavedImageResourcePath:(int)type  pageCode:(NSString *)pageCode index:(int)index;
-(NSData *)getLatestSavedImageResource:(int)type  pageCode:(NSString *)pageCode index:(int)index;
-(NSString *)saveImageResourceData:(int)type  pageCode:(NSString *)pageCode index:(int)index assetsIdx:(NSString *)in_assets_idx imageData:(NSData *)imageData bookCode:(NSString *)bookCode;
-(NSString *)saveImageResourceDataWithSync:(NSMutableDictionary *)imageDataDicn ;
-(void)updateLatestImageResourceSaveStatus:(int)type  pageCode:(NSString *)pageCode index:(int)index saveStatus:(int)saveStatus;
-(void)updateImageResourceSyncStatus:(NSMutableDictionary *)param;
-(void)clearUnusedImageResource:(int)type  pageCode:(NSString *)pageCode index:(int)index;
-(void)clearLatestImageResource:(int)type  pageCode:(NSString *)pageCode index:(int)index;
-(void)clearImageResource:(int)type  pageCode:(NSString *)pageCode index:(int)index;
-(void)clearImageResourceWithBookCode:(NSString *)bookCode ;

/*
-(NSData *)getLatestSavedMapCaptureImage:(NSString *)pageCode index:(int)index;
-(void)saveMapCaptureImage:(NSString *)pageCode index:(int)index imageData:(NSData *)inImageData;
-(void)updateLatestMapCaptureImageSaveStatus:(NSString *)pageCode index:(int)index saveStatus:(int)saveStatus;
-(void)updateMapCaptureImageSyncStatus:(NSMutableDictionary *)param;
-(void)clearUnusedMapCaptureImage:(NSString *)pageCode index:(int)index;
-(void)clearMapCaptureImage:(NSString *)pageCode index:(int)index;
*/


@end
