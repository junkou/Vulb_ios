//
//  NSString+RegExp.h
//  Memogram
//
//  Created by 雅彦 竹本 on 12/06/19.
//  Copyright (c) 2012年 小麦 株式会社. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RegExp)

- (NSString *)stringByReplacingMatchesOfPattern:(NSString *)pattern withString:(NSString *)replacement;
- (NSArray *)matchedStringsByMatchesOfPattern:(NSString *)pattern;

@end
