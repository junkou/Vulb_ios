//
//  ExtImageView.m
//  ThreadBunkatu
//
//  Created by k-kin on 11/11/15.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import "ImagePartitionView.h"


@implementation ImagePartitionView

@synthesize images;
@synthesize verticalDrawFlag;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
		self.backgroundColor = [UIColor clearColor];
        self.verticalDrawFlag = FALSE;
   }
    return self;
}

- (void)drawRect:(CGRect)rect {
	int imageCount = [self.images count];
	if (imageCount <= 4) {
		//３分割処理
		// Drawing Top Area.
        if (self.verticalDrawFlag) {
            //Vertical
            UIImage *topImage = [UIImage imageNamed:[images objectAtIndex:0]];
            if (topImage == nil) {
                return;
            }
            [topImage drawInRect:CGRectMake(0, 0, topImage.size.width, topImage.size.height)];
            
 
            UIImage *buttomImage = [UIImage imageNamed:[images objectAtIndex:2]];

            CGFloat maxY = rect.size.height;
            UIImage *stampImage = nil;
            if ([images count] == 4) {
                stampImage = [UIImage imageNamed:[images objectAtIndex:3]];
                if (stampImage != nil) {
                    maxY = maxY - (stampImage.size.height - buttomImage.size.height);
                }
            }
            CGFloat targetDrawCenterHeight = maxY - topImage.size.height - buttomImage.size.height;
            if (stampImage != nil) {
                if (targetDrawCenterHeight < stampImage.size.height) {
                    targetDrawCenterHeight = stampImage.size.height-topImage.size.height-buttomImage.size.height;
                    maxY = topImage.size.height + targetDrawCenterHeight + buttomImage.size.height;
                }
            }
           
            // Drawing Buttom Area.
             [buttomImage drawInRect:CGRectMake(0, maxY - buttomImage.size.height, buttomImage.size.width, topImage.size.height)];

            // Drawing Center Area.
           UIImage *centerImage = [UIImage imageNamed:[images objectAtIndex:1]];
            BOOL drawFla = TRUE;
            CGFloat drawStartPotion = 0;
            while (drawFla) {
                //NSLog(@"maxY:%f",maxY);
                //NSLog(@":centerImage: %@", NSStringFromCGSize(centerImage.size));
                if (drawStartPotion  + centerImage.size.height <= targetDrawCenterHeight) {
                    [centerImage drawInRect:CGRectMake(0, drawStartPotion + topImage.size.height, centerImage.size.width, centerImage.size.height)];
                     drawStartPotion += centerImage.size.height;
                }else {
                    CGFloat delta = targetDrawCenterHeight - drawStartPotion ;
                    if (delta > 0) {
                        UIImage *deltaCenterImage = [self trimImage:[UIImage imageNamed:[images objectAtIndex:1]] frameRect:CGRectMake(0, 0, centerImage.size.width, delta)];
                        
                        [deltaCenterImage drawInRect:CGRectMake(0, drawStartPotion  + topImage.size.height, deltaCenterImage.size.width, deltaCenterImage.size.height)];
                        //NSLog(@"delta:%f",delta);
                    }
                    drawFla = FALSE;
                }
            }

            if ([images count] == 4) {
                 if (stampImage != nil) {
                    [stampImage drawInRect:CGRectMake(0, 0, stampImage.size.width, stampImage.size.height)];
                }
            }
        }else{
            //Horizon
            [self drawHorizonRect:rect yPotion:0 
                        leftImage:[UIImage imageNamed:[images objectAtIndex:0]] 
                      centerImage:[UIImage imageNamed:[images objectAtIndex:1]]
                       rightImage:[UIImage imageNamed:[images objectAtIndex:2]]
             ];
        }
	}else if (imageCount == 9) {
		//９分割処理
		// Drawing Top Area.
		UIImage *topImage = [UIImage imageNamed:[images objectAtIndex:0]];
        if (topImage == nil) {
            return;
        }
        
		[self drawHorizonRect:rect yPotion:0 
					leftImage:[UIImage imageNamed:[images objectAtIndex:0]] 
				  centerImage:[UIImage imageNamed:[images objectAtIndex:1]]
				   rightImage:[UIImage imageNamed:[images objectAtIndex:2]]
		 ];
		
		// Drawing Buttom Area.
		UIImage *buttomImage = [UIImage imageNamed:[images objectAtIndex:6]];
		[self drawHorizonRect:rect yPotion:rect.size.height - buttomImage.size.height 
					leftImage:[UIImage imageNamed:[images objectAtIndex:6]] 
				  centerImage:[UIImage imageNamed:[images objectAtIndex:7]]
				   rightImage:[UIImage imageNamed:[images objectAtIndex:8]]	 
		 ];
		
		// Drawing Center Area.
		UIImage *centerImage = [UIImage imageNamed:[images objectAtIndex:3]];
		CGFloat maxY = rect.size.height;
		CGFloat targetDrawCenterHeight = maxY - topImage.size.height - buttomImage.size.height;
		BOOL drawFla = TRUE;
		CGFloat drawStartPotion = 0;
		while (drawFla) {
			if (drawStartPotion  + centerImage.size.height <= targetDrawCenterHeight) {
				[self drawHorizonRect:rect yPotion:drawStartPotion + topImage.size.height
							leftImage:[UIImage imageNamed:[images objectAtIndex:3]] 
						  centerImage:[UIImage imageNamed:[images objectAtIndex:4]]
						   rightImage:[UIImage imageNamed:[images objectAtIndex:5]]	 			 
				 ];
				drawStartPotion += centerImage.size.height;
			}else {
				CGFloat delta = targetDrawCenterHeight - drawStartPotion ;
				if (delta > 0) {
					UIImage *deltaLeftImage = [self trimImage:[UIImage imageNamed:[images objectAtIndex:3]] frameRect:CGRectMake(0, 0, centerImage.size.width, delta)];
					UIImage *deltaCenterImage = [self trimImage:[UIImage imageNamed:[images objectAtIndex:4]] frameRect:CGRectMake(0, 0, centerImage.size.width, delta)];
					UIImage *deltaRightImage = [self trimImage:[UIImage imageNamed:[images objectAtIndex:5]] frameRect:CGRectMake(0, 0, centerImage.size.width, delta)];
					[self drawHorizonRect:rect yPotion:drawStartPotion  + topImage.size.height
								leftImage:deltaLeftImage 
							  centerImage:deltaCenterImage
							   rightImage:deltaRightImage	 			 
					 ];
					//NSLog(@"delta:%f",delta);
				}
				drawFla = FALSE;
			}
		}
	}
}

-(void)drawHorizonRect:(CGRect)inRect yPotion:(CGFloat)inY leftImage:(UIImage *)imageLeft centerImage:(UIImage *)imageCenter rightImage:(UIImage *)imageRight{	
	CGFloat maxX = inRect.size.width;
	
    //NSLog(@"inY:%f",inY);
    //NSLog(@"maxX:%f",maxX);
    //NSLog(@":leftImage: %@", NSStringFromCGSize(imageLeft.size));
    //NSLog(@":centerImage: %@", NSStringFromCGSize(imageCenter.size));
    //NSLog(@":rightImage: %@", NSStringFromCGSize(imageRight.size));
    
	//draw Left
    if (imageLeft == nil) {
        return;
    }
	[imageLeft drawInRect:CGRectMake(0, inY, imageLeft.size.width, imageLeft.size.height)];
	
	//draw Right
	[imageRight drawInRect:CGRectMake(maxX - imageRight.size.width, inY, imageRight.size.width, imageRight.size.height)];
	
	//draw Center
	CGFloat targetDrawCenterWidth = maxX - imageLeft.size.width - imageRight.size.width;
    //NSLog(@"targetDrawCenterWidth:%f",targetDrawCenterWidth);
   
	BOOL drawFla = TRUE;
	CGFloat drawStartPotion = 0;
	
	while (drawFla) {
		if (drawStartPotion + imageCenter.size.width <= targetDrawCenterWidth ) { 
			[imageCenter drawInRect:CGRectMake(drawStartPotion+imageLeft.size.width, inY, imageCenter.size.width, imageCenter.size.height)];
			drawStartPotion = drawStartPotion + imageCenter.size.width;
			//NSLog(@"drawStartPotion:%f",drawStartPotion);
		}else {
			//NSLog(@"targetDrawCenterWidth:%f",targetDrawCenterWidth);
			//NSLog(@"drawStartPotion:%f",drawStartPotion);
			CGFloat delta = targetDrawCenterWidth - drawStartPotion ;
            //NSLog(@"delta:%f",delta);
			if (delta > 0) {
				UIImage *deltaImage = [self trimImage:imageCenter frameRect:CGRectMake(0, 0, delta, imageCenter.size.height)];
			    //NSLog(@":deltaImage: %@", NSStringFromCGSize(deltaImage.size));

                [deltaImage drawInRect:CGRectMake(drawStartPotion+imageLeft.size.width, inY, delta, imageCenter.size.height)];
			}
			drawFla = FALSE;
		}
		
	}
}


-(UIImage*)trimImage:(UIImage*)image  frameRect:(CGRect)frameRect {
    UIImage*    shrinkedImage;
    UIGraphicsBeginImageContext(frameRect.size);
    [image drawAtPoint:frameRect.origin];
    shrinkedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	return shrinkedImage;

}


-(UIImage *)getScreenShotImage{		
	UIGraphicsBeginImageContext(self.bounds.size); 
	[self.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return viewImage;
}

- (void)dealloc {
	[self.images release];
	
    [super dealloc];
}


@end
