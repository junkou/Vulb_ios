//
//  UIImage+Mask.h
//  MemogramTest
//
//  Created by 雅彦 竹本 on 12/06/11.
//  Copyright (c) 2012年 小麦 株式会社. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Mask)

- (UIImage *)trimImageInRect:(CGRect)rect;

@end
