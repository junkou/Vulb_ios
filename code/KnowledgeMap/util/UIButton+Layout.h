//
//  UIButton+Layout.h
//  BaccaratClient
//
//  Created by jun kou on 11/6/12.
//  Copyright (c) 2012 wicresoft. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    ImageLeftTextRight = 0,
    TextLeftImageRight,
    ImageUpTextDown,
    TextUpImageDown,
}EContentLayout;

@interface UILayButton : UIButton
@property(nonatomic,assign) EContentLayout layout;
@end