//
//  MainViewController.m
//  KnowledgeMap
//
//  Created by KouJun on 3/30/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import "MainViewController.h"
#import "WorkViewController.h"
#import "UserListViewController.h"

#import "AppHelper.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    workController = [[[WorkViewController alloc] init] autorelease];
    
//    WorkViewController *v1 = [[[WorkViewController alloc] init] autorelease];
    
    userListController = [[[UserListViewController alloc] init] autorelease];
    
    navigation = [[[BaseNavigationController alloc] initWithRootViewController:workController] autorelease];
//    navigation1 = [[[BaseNavigationController alloc] initWithRootViewController:v1] autorelease];
    navigation2 = [[[BaseNavigationController alloc] initWithRootViewController:userListController] autorelease];
    NSArray *vcs = @[navigation,/*navigation1,*/navigation2];
    [self setViewControllers:vcs];
    [self setNormalImages:@[[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_footer_list_normal.png"]],/*[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_footer_timeline_normal.png"]],*/[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_footer_friend_normal.png"]]] selectedImages:@[[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_footer_list_selected.png"]],/*[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_footer_timeline_selected.png"]],*/[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_footer_friend_selected.png"]]] highlightedImages:@[[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_footer_list_touch.png"]],/*[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_footer_timeline_touch.png"]],*/[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_footer_friend_touch.png"]]]];
    self.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setTabbarHidden:NO animated:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AppApiDelegate delegate
- (void)sheetMemberListSucessed:(NSArray *)memberList
{
    [SVProgressHUD dismiss];
    
    userListController.invitedUserList = [[[NSMutableArray alloc] initWithArray:memberList] autorelease];
    [self setSelectedIndex:1];
}

#pragma mark - UITabBarControllerDelegate
- (BOOL)mh_tabBarController:(MHTabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController atIndex:(NSUInteger)index
{
    if (viewController == navigation2 && userListController.invitedUserList == nil)
    {
        [SVProgressHUD show];
        
        NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
        [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId  forKey:sheetId_Key];
        
        [[BusinessDataCtrlManager sharedInstance] requestSheetMemberList:param delegate:self];
        return NO;
    }
    return YES;
}
@end
