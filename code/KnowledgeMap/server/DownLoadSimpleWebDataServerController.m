//
//  DownLoadTextDataServerController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/10.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "DownLoadSimpleWebDataServerController.h"

@implementation DownLoadSimpleWebDataServerController

@synthesize delegate;
@synthesize imageEditFlag;
@synthesize resizeRate;

NSLock				*downLoadLockData;

DownLoadSimpleWebDataServerController	*ctrl;
NSMutableDictionary	*filesCtrl;

+(DownLoadSimpleWebDataServerController *)getInstance{
	if (ctrl == nil) {
		ctrl = [[DownLoadSimpleWebDataServerController alloc] init] ;
		filesCtrl = [[NSMutableDictionary alloc] init];
        downLoadLockData = [[NSLock alloc] init];
        
	}
	return ctrl;
}

-(void)clearThread{
    NSArray *keys = [filesCtrl allKeys];
	for (int i=0; i<[keys count]; i++) {
		NSThread *thread = [filesCtrl objectForKey:[keys objectAtIndex:i]];
		if (thread != nil && ![thread isCancelled] && ![thread isFinished] && [thread isExecuting]) {
			//NSLog(@"=====nowThread Cenceled======");
			[thread cancel];
		}
	}
	
	[filesCtrl removeAllObjects];
}

-(void)clearDelegate{
    [downLoadLockData lock];
    self.delegate = nil;
    [downLoadLockData unlock];

}

-(void)getDataFromWebServer:(NSMutableDictionary *)param view:(UIView *)inView delegate:(id)inDelgate{
    /*
     NSArray *allkey = [filesCtrl allKeys];
     for (int i; i<[allkey count]; i++) {
     NSLog([allkey objectAtIndex:i]);
     }
     */
    NSString *key = [param objectForKey:data_identity_key];
	if (key != nil && [filesCtrl objectForKey:key] != nil ) {
		//すでにダウンロード中になので、スキップする
        //NSLog(@"return  filePath:%@", filePath);
		return;
	}
	
    [param setValue:inView forKey:OUPUT_VIEW_KEY];
    
	NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(handleRequest:) object:param ];
    [downLoadLockData lock];
    self.delegate = inDelgate;
    self.imageEditFlag = FALSE;
    [filesCtrl setObject:thread forKey:key];
    [downLoadLockData unlock];
	[thread start];
    
    
	[thread release];

}


-(void)handleRequest:(NSMutableDictionary *)params{
    
	NSAutoreleasePool *serverPool = [[NSAutoreleasePool alloc] init];
        
    NSString *key = [params objectForKey:data_identity_key];
    NSString *methodName = [params objectForKey:METHOD_NAME_KEY];
    NSData *data = nil;
    NSString *resourceFileName = [NSString stringWithFormat:@"%@_%@.png",key,RESOURCEDATA];

    if ([methodName isEqualToString:METHOD_DOWNLOAD_THUMBNAIL]) {
        data = [AppHelper readFileData:[NSString stringWithFormat:@"%@.png",key]];
        
    }else if ([methodName isEqualToString:METHOD_RESOURCE_DOWNLOAD]) {
        data = [AppHelper readFileData:resourceFileName];
    }
    
    
    //NSLog(@"filePath:%@",filePath);
    //NSLog(@"data length:%d",[data length]);
    if (data != nil) {
        if ([methodName isEqualToString:METHOD_DOWNLOAD_THUMBNAIL]) {
            UIImageView *imageView = [params objectForKey:OUPUT_VIEW_KEY];
            UIImage* image = [UIImage imageWithData:data];
            //CGSize size = CGSizeMake(imageView.frame.size.width, imageView.frame.size.height);
            image = [image imageFilledInSize:CGSizeMake(imageView.frame.size.width, imageView.frame.size.height)];
            image = [image trimImageInRect:imageView.bounds];
            //size = image.size;
           [imageView setImage:image];
      
        }else if ([methodName isEqualToString:METHOD_RESOURCE_DOWNLOAD]) {
            UIImageView *imageView = [params objectForKey:OUPUT_VIEW_KEY];
            UIImage* image = [UIImage imageWithData:data];
            
            if (image.size.height > imageView.bounds.size.height) {
                //CGSize resize = CGSizeMake(image.size.width*imageView.bounds.size.height/image.size.height, imageView.bounds.size.height);
                //image = [Utils resizeImage:image Size:resize];
                image = [image imageFilledInSize:CGSizeMake(imageView.frame.size.width, imageView.frame.size.height)];
                image = [image trimImageInRect:imageView.bounds];

            }            
            [imageView setImage:image];
            
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            NSString *resourceType = [ud stringForKey:resourceFileName];
            if (resourceType != nil && [resourceType isEqualToString:DB_RESOURCE_TYPE_PDF]) {
                UIView *view = [[imageView superview] viewWithTag:RESOURCE_OPEN_BUTTON_TAG];
                view.hidden = NO;
                UILabel *label = (UILabel *)[view viewWithTag:RESOURCE_OPEN_LABEL_TAG];
                label.text =  NSLocalizedString(@"開く",@"開く");
            }

       }

 
    }else{
        if (![Utils checkWebServerStatus]) {
            return ;
        }
        
        NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,[params objectForKey:METHOD_NAME_KEY]];
        
        NSURL *cgiUrl = [NSURL URLWithString: url];
        //NSLog([WebServerImgDataDownloadServerURLAdress stringByAppendingString:file_name]);
        NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:cgiUrl];
        NSMutableData* postData = [[NSMutableData alloc] init];
        
        NSArray *key_list = [params allKeys];
        for (int i=0; i < [key_list count] ; i++) {
            NSString *key = [key_list objectAtIndex:i];
            id valueData = [params objectForKey:key];
            NSString *value = @"";
            if ( [valueData isKindOfClass:[ NSString class]]) {
                value = valueData;
            }else if ( [valueData isKindOfClass:[ NSMutableDictionary class]]) {
                value = [valueData JSONRepresentation];
            }else if ( [valueData isKindOfClass:[ NSMutableArray class]]) {
                value = [valueData JSONRepresentation];
            }else{
                value = valueData;
            }
            
            //NSLog(@"key: %@", key);
            //NSLog(@"value: %@", value);
            [postData appendData:[[NSString stringWithFormat:@"%@=%@&", key,value] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        
        [postRequest setHTTPMethod:@"POST"];
        [postRequest setHTTPBody:postData];
        [postRequest setTimeoutInterval:SYN_SEVER_CONNECT_SEND_DATA_INTERVAL_10];
        if ([NSURLConnection canHandleRequest:postRequest]) {
            NSURLResponse *response = nil;
            NSError*error=nil;
            //NSLog(@"file_name length = %@  : ", file_name);
            data = [NSURLConnection sendSynchronousRequest:postRequest returningResponse:&response error:&error];
            //NSLog(@"file_name length = %@  : %d", file_name, [data length]);
        }
        [postData release];
        
        
        [downLoadLockData lock];
        if ([filesCtrl count] == 0) {
            [serverPool release];

            return;
        }
        [downLoadLockData unlock];

        if (data != nil && [data length] > 0) {
            NSString *ret_str = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
            //NSLog(ret_str);
            
            NSMutableDictionary *recieveData = [ret_str JSONValue];
            
            NSMutableDictionary *result = [recieveData objectForKey:RESULT_KEY];
            
            NSString *resultStatus = [result objectForKey:RESULT_STATUS_KEY];
            
            if (resultStatus != nil && [resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {
                //正常
                NSMutableDictionary *resultDatas = [recieveData objectForKey:RESULT_DATA_VALUE_KEY];
                
                 if ([methodName isEqualToString:METHOD_SHEET_MEMBER_LIST]) {
                    NSMutableArray *memberList = [resultDatas objectForKey:MEMBERS];
                    NSString *oner = @"";
                    NSString *otherMember = @"";
                    for (int i=0; i < [memberList count]; i++) {
                        NSMutableDictionary *memberItem = [memberList objectAtIndex:i];
                        NSString *userName = [memberItem objectForKey:userName_Key];
                        if (i == 0) {
                            oner = userName;
                        }else{
                            otherMember = [NSString stringWithFormat:@"%@,%@", otherMember,userName];
                        }
                        
                    }
                    
                    //                membersStr = [membersStr stringByReplacingOccurrencesOfString:@"_DUMMY_," withString:@""];
                    // NSLog(@"otherMember:%@",otherMember);
                     if (self.delegate != nil) {
                         UILabel *label = [params objectForKey:OUPUT_VIEW_KEY];
                         if (label != nil ) {
                             [downLoadLockData lock];
                             CGSize size = [AppHelper sheetNameLabelProperty:oner view:label startX:0 color:[UIColor blueColor]];
                             if (![otherMember isEqualToString:@""]) {
                                 //NSLog(@"otherMember:%@",otherMember);
                                 [AppHelper sheetNameLabelProperty:otherMember view:label startX:size.width color:[UIColor grayColor]];
                                 
                             }
                             [downLoadLockData unlock];
                             
                         }
                     }
                }else if ([methodName isEqualToString:METHOD_CATEGORY_LIST]) {
                    
                }else if ([methodName isEqualToString:METHOD_DOWNLOAD_THUMBNAIL]) {
                    NSString *resourceData = [resultDatas objectForKey:RESOURCEDATA];
                    NSData *imageData = [NSData dataFromBase64String:resourceData];
                   //NSLog(personImgStr);
                     if (self.delegate != nil) {
                         UIImageView *imageView = [params objectForKey:OUPUT_VIEW_KEY];
                         UIImage *image = [UIImage imageWithData:imageData];
                         image = [image imageFilledInSize:CGSizeMake(imageView.frame.size.width, imageView.frame.size.height)];
                         image = [image trimImageInRect:imageView.bounds];

                         [imageView setImage:image];
                         
                         NSString *key = [params objectForKey:data_identity_key];                         
                         [AppHelper writeFileData:[NSString stringWithFormat:@"%@.png",key] fileData:imageData];

                     }
                     
                }else if ([methodName isEqualToString:METHOD_RESOURCE_DOWNLOAD]) {
                    NSString *resourceType = [resultDatas objectForKey:resourceType_key];
                    NSString *resourceData = [resultDatas objectForKey:RESOURCEDATA];
                    NSData *imageData = [NSData dataFromBase64String:resourceData];
                    
                   if ([resourceType isEqualToString:DB_RESOURCE_TYPE_PHOTO]
                       || [resourceType isEqualToString:DB_RESOURCE_TYPE_IllUSTRATION]
                       || [resourceType isEqualToString:DB_RESOURCE_TYPE_PDF]) {
                        //NSLog(personImgStr);
                        UIImage *image = [UIImage imageWithData:imageData];
                       if (self.delegate != nil) {
                           UIImageView *imageView = [params objectForKey:OUPUT_VIEW_KEY];
                           if (image.size.height > imageView.bounds.size.height) {
                               CGSize resize = CGSizeMake(imageView.bounds.size.width, imageView.bounds.size.height);
                               image = [image imageFilledInSize:resize];
                               image = [image trimImageInRect:imageView.bounds];

                           }
                           
                           CGRect rect = imageView.frame;
                           rect.size.width =image.size.width;
                           rect.origin.x =(imageView.frame.size.width-image.size.width)/2;
                           imageView.frame = rect;
                           [imageView setImage:image];
                           
                           NSString *key = [params objectForKey:data_identity_key];
                           NSString *resourceFileName = [NSString stringWithFormat:@"%@_%@.png",key,RESOURCEDATA];
                           
                           if ([resourceType isEqualToString:DB_RESOURCE_TYPE_PDF]) {
                               
                               UIView *view = [[imageView superview] viewWithTag:RESOURCE_OPEN_BUTTON_TAG];
                               view.hidden = NO;
                               UILabel *label = (UILabel *)[view viewWithTag:RESOURCE_OPEN_LABEL_TAG];
                               label.text =  NSLocalizedString(@"開く",@"開く");
                               
                              //regist resource type of The File
                               NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                               [ud setObject:resourceType forKey:resourceFileName];

                           }
                           
                           NSData*newData = [[NSData alloc] initWithData:UIImagePNGRepresentation(image)]  ;                           
                           [AppHelper writeFileData:resourceFileName fileData:newData];                           
                           [AppHelper writeFileData:[NSString stringWithFormat:@"%@_%@_%@.png",key,RESOURCEDATA,ORIGINAL] fileData:imageData];

                       }
                      

                   }else if([resourceType isEqualToString:DB_RESOURCE_TYPE_SOUND]) {
                    
                    }
                    
 
 
                }else if ([methodName isEqualToString:METHOD_PERSON_IMG]) {
                    NSString *personImgStr = [resultDatas objectForKey:personImg_Key];
                    //NSLog(personImgStr);
                    UIImageView *imageView = [params objectForKey:OUPUT_VIEW_KEY];
                    NSData *imageData = [NSData dataFromBase64String:personImgStr];
                    
                    [imageView setImage:[UIImage imageWithData:imageData]];
                    
                }
                
                
                //NSLog([data JSONRepresentation]);
            }
            
        }

    }
    
    
    [downLoadLockData lock];
    if (key != nil) {
        [filesCtrl removeObjectForKey:key];
    }
     [downLoadLockData unlock];
    
	
    //NSLog(@"[allFilePath]:%@",allFilePath);
	//NSLog(@"------[filesCtrl count]:%d",[filesCtrl count]);
	//[data release];
	[serverPool release];
}

@end
