//
//  WebServerSYNController.h
//  BookMaker
//
//  Created by k-kin on 10/06/13.
//  Copyright 2010 ICCESOFT. All rights reserved.
//

#import "ServerParamDef.h"
#import "DataRegistry.h"
#import "BusinessDataCtrlManager.h"
#import "NSData+Base64.h"
#import "SRWebSocket.h"
#import "AppDefine.h"

#define CONTENT_IMAGE_KEY @"image"
#define CONTENT_TEXT_KEY  @"title"

@interface UploadToWebServerSYNController : NSObject <SRWebSocketDelegate>{
	
    id delegate;
	BOOL isDataUploadLoading;
	NSMutableData	*receiveData;
	DataRegistry	*dataRegistry;
	UserInfo		*userinfo;
	
	NSTimer				*timer; 
	NSLock				*lockData;
	NSMutableDictionary *dictionaryData;
    
    NSNumber *currentSheetId;
   
    NSString *currentMethodName;
    NSString *currentFileName;
    NSString *currentCardBeging;
   
    SRWebSocket *web_socket;
    
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign)	DataRegistry	*dataRegistry;
@property (nonatomic, assign)	UserInfo		*userinfo;

@property (nonatomic, retain) NSLock		*lockData;
@property (nonatomic, retain) NSTimer       *timer;
@property (nonatomic, retain) NSMutableDictionary *dictionaryData;
@property (nonatomic, retain) NSMutableData	*receiveData;
@property					  BOOL isDataUploadLoading;
@property (nonatomic, retain) NSString *currentMethodName;
@property (nonatomic, retain) NSString *currentFileName;
@property (nonatomic, retain) NSString *currentCardBeging;
@property (nonatomic, retain) NSNumber *currentSheetId;
@property (nonatomic, retain) SRWebSocket *web_socket;

+(UploadToWebServerSYNController *)getUploadServerInstance;
//-(UploadToWebServerSYNController *)initData:(id)inDelegate;
//- (void)startUpload;
//-(void)stopUpLoad;
//- (void)clearDatas;
//- (void)toStartSYNCwithWebServer;
//- (void)stopSYNCTimer;
//-(void) startWebsocket;
- (void)sendWorkDataToWebServer:(NSMutableDictionary *)params photoData:(NSData *)photoData fileName:(NSString *)photoFileName serverUrl:(NSString *)inUrl ;
//- (void)searchLocalSyncData;
//-(void)sheetEntryOrExit:(NSString *)inAction;
//-(void)sheetEntryFinal:(NSString *)inAction sheetId:(NSNumber *)inSheetId;

- (BOOL)publishCard:(NSArray*)dataList;
- (BOOL)updateCard:(NSNumber*)cardInfo data:(NSArray*)datalist;
@end

