//
//  Server.m
//  BukurouSS
//
//  Created by k-kin on 11/11/14.
//  Copyright 2011 iccesoft. All rights reserved. 
//

#import "WebServerDownLoadController.h"

@implementation WebTask
+(id)TaskWithUrl:(NSString*)url params:(NSDictionary*)params delegate:(id)delegate showIndicator:(bool)show
{
    WebTask *wt = [[[WebTask alloc] init] autorelease];
    wt.url = url;
    wt.params = params;
    wt.delegate = delegate;
    wt.showIndicator = show;
    return wt;
}

-(void)dealloc
{
    [_url release];
    [_params release];
    [_response release];
    
    [super dealloc];
}
@end

@interface WebServerDownLoadController()
@property (nonatomic, retain) NSString			*fileMIMEType;
@property (nonatomic, retain) NSMutableData		*receiveData;

@end
@implementation WebServerDownLoadController

//@synthesize delegate;
//@synthesize urlConnection;
@synthesize fileMIMEType;
@synthesize receiveData;

WebServerDownLoadController	*server;
NSLock			*lockData;

+(WebServerDownLoadController *)getInstance{
	if (server == nil) {
		server = [[WebServerDownLoadController alloc] init] ;
		lockData = [[NSLock alloc] init];
	}
	return server;
}

- (id)init
{
    if (self = [super init])
    {
        tasks = [[NSMutableArray alloc] init];
    }
    return self;
}
- (bool)beforeCheck:(NSDictionary*)params url:(NSString*)url delegate:(id)delegate
{
    for (WebTask *wt in tasks)
    {
        if ([wt.url isEqualToString:url] && (wt.params != nil && [wt.params isEqualToDictionary:params]))
        {
            if (wt.delegate == delegate)
            {
                return NO;
            }
        }
    }
    return YES;
}
#pragma mark -
#pragma mark sendWorkDataToWebServer
- (void) sendWorkDataToWebServer:(NSMutableDictionary *)params 
			  showConnectionFlag:(BOOL)showConnectionIndicator 
					   serverUrl:(NSString *)inUrl
						delegate:(id)inDelegate
{
    if ([self beforeCheck:params url:inUrl delegate:inDelegate])
    {
        [lockData lock];
        [tasks addObject:[WebTask TaskWithUrl:inUrl params:params delegate:inDelegate showIndicator:showConnectionIndicator]];
        [lockData unlock];
        
        [self tick];
    }
}

#pragma mark -
#pragma mark sendOtherApplicationSerivice
- (void) sendOtherApplicationSerivice:(NSString *)inUrl delegate:(id)inDelegate
{
    if ([self beforeCheck:nil url:inUrl delegate:inDelegate])
    {
        [lockData lock];
        [tasks addObject:[WebTask TaskWithUrl:inUrl params:nil delegate:inDelegate showIndicator:NO]];
        [lockData unlock];
        
        [self tick];
    }
}

- (void)next
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    [lockData lock];
    [tasks removeObjectAtIndex:0];
    [lockData unlock];
    
    currentTask = nil;
    urlConnection = nil;
    [self tick];
}

- (void)tick
{
    if (![Utils checkWebServerStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:MultiLangString(@"Information")
                              message:[Utils MultiLangStringInfomation:@"I0006"]
                              delegate:nil
                              cancelButtonTitle:MultiLangString(@"Yes")
                              otherButtonTitles: nil];
        [alert show];
        [alert release];
		return;
	}
    
    if ([tasks count] > 0 && currentTask == nil)
    {
        WebTask *task = [tasks firstObject];
        currentTask = task;
        
        float timeout = 60.0;
        
        NSMutableURLRequest *postRequest = [[[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:task.url]] autorelease];
        if ([task.params count])
        {
            NSMutableData* postData = [[NSMutableData alloc] init];
            timeout = SYN_SEVER_CONNECT_SEND_DATA_INTERVAL_10;
            
            NSArray *key_list = [task.params allKeys];
            for (int i=0; i < [key_list count] ; i++) {
                NSString *key = [key_list objectAtIndex:i];
                id valueData = [task.params objectForKey:key];
                NSString *value = @"";
                if ( [valueData isKindOfClass:[ NSString class]]) {
                    value = valueData;
                }else if ( [valueData isKindOfClass:[ NSMutableDictionary class]]) {
                    value = [valueData JSONRepresentation];
                }else if ( [valueData isKindOfClass:[ NSMutableArray class]]) {
                    value = [valueData JSONRepresentation];
                }else{
                    value = valueData;
                }
                
                [postData appendData:[[NSString stringWithFormat:@"%@=%@&", key,value] dataUsingEncoding:NSUTF8StringEncoding]];
                
            }
            //[postData appendData:[[NSString stringWithFormat:@"--%@--\r\n",BOUNDARY] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [postRequest setHTTPMethod:@"POST"];
            [postRequest setHTTPBody:postData];		
            
        }
            
        [postRequest setTimeoutInterval:timeout];
        
        urlConnection = [NSURLConnection connectionWithRequest:postRequest delegate:self];
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:task.showIndicator];
    }
}
#pragma mark -
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)i_connection didReceiveResponse:(NSURLResponse *)i_response{	
	//NSLog(@"---download is didReceiveResponse--- !");
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
//	self.fileMIMEType = [i_response MIMEType];
//    NSLog(@"%@",[i_response description]);
//    NSString *fileName = [i_response suggestedFilename];
//    NSLog(@"fileName:%@",fileName);
    
    // (NSHTTPURLResponse *)にキャストする
    NSHTTPURLResponse *httpResopnse = (NSHTTPURLResponse *)i_response;
    // レスポンスヘッダを列挙
    NSDictionary *headers = httpResopnse.allHeaderFields;
    for (id key in headers) {
        if ([[key lowercaseString] isEqualToString:@"content-type"]) {
            self.fileMIMEType = [headers objectForKey:key];
        }
        //NSLog(@"%@: %@", key, [headers objectForKey:key]);
    }
    
    // データ蓄積用に NSMutableData を初期化
    self.receiveData = [[NSMutableData alloc] init];
}

- ( void ) connection:( NSURLConnection *) connection didReceiveData:( NSData *) data {	
	
	[self.receiveData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
	
    
	[self connectNetWorkError];
    
	NSLog(@"-- didFailWithError --:%@",[error localizedDescription]);
    
    [self next];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)i_connection{
	//NSLog(@"---download is connectionDidFinishLoading--- !");
    
    [AppHelper endDataLoadingAnimation];
    

	NSRange fileTypeImage = [self.fileMIMEType rangeOfString:@"image/"];
    NSRange fileTypeApplication = [self.fileMIMEType rangeOfString:@"application/"];

	if (fileTypeImage.location != NSNotFound || fileTypeApplication.location != NSNotFound)
    {
		[self recieveApplicationData:self.receiveData fileType:self.fileMIMEType];
	}
	else
    {
		NSString *ret_str = [[[NSString alloc] initWithData:self.receiveData encoding:NSUTF8StringEncoding] autorelease];
		//NSLog(@"--recieveData :%@",ret_str);
		//NSMutableDictionary *dictData = [ret_str JSONValue];
		//データ処理、拡張
		[self recieveOprationData:ret_str];
	}
		
    [self next];
}


#pragma mark -
#pragma mark recieveData Delegate
//データ通信をキャンセルしたとき
-(void)cancelOptarion
{
    if (currentTask)
    {
        [urlConnection cancel];
        urlConnection = nil;
        
        self.receiveData = nil;
        
        if ([currentTask.delegate respondsToSelector:@selector(cancelOptarion)]) {
            [currentTask.delegate cancelOptarion];
        }
    }
}

//データ通信エラーが発生したとき
-(void)connectNetWorkError{
    
	self.receiveData = nil;

	if ([currentTask.delegate respondsToSelector:@selector(connectNetWorkError)]) {
 		[currentTask.delegate performSelectorOnMainThread:@selector(connectNetWorkError) withObject:nil waitUntilDone:NO];
	}
}

// 画像データを受信したとき
- (void)recieveApplicationData:(NSMutableData*)data fileType:(NSString *)fileType{
    if ([currentTask.delegate respondsToSelector:@selector(recieveApplicationData:)]) {
		[currentTask.delegate performSelectorOnMainThread:@selector(recieveApplicationData:) withObject:@{fileType:data} waitUntilDone:NO];
    }
}

// 処理データを受信したとき
- (void)recieveOprationData:(NSString *)data
{
    currentTask.response = data;
	if ([currentTask.delegate respondsToSelector:@selector(recieveOprationData:)]) {
		[currentTask.delegate performSelectorOnMainThread:@selector(recieveOprationData:) withObject:currentTask waitUntilDone:NO];
	}
}


#pragma mark -
- (void)dealloc {

    urlConnection = nil;
	
	[lockData release];
	[self.fileMIMEType release];
	[self.receiveData release];
	
    [tasks release];
	[super dealloc];
	
}

@end
