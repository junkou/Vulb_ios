//
//  DownLoadTextDataServerController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/10.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppHelper.h"
#import "NSData+Base64.h"
#import "UIImage+Resize.h"
#import "UIImage+Mask.h"

@interface DownLoadSimpleWebDataServerController : NSObject{
    id delegate;

    BOOL imageEditFlag;
    int     resizeRate;
}

@property (nonatomic, assign) id delegate;

@property       BOOL imageEditFlag;
@property       int     resizeRate;

+(DownLoadSimpleWebDataServerController *)getInstance;
-(void)clearThread;
-(void)clearDelegate;
-(void)getDataFromWebServer:(NSMutableDictionary *)param view:(UIView *)inView delegate:(id)inDelgate;

@end
