//
//  DownLoadImageResourceController.m
//  BukurouSS
//
//  Created by k-kin on 11/12/19.
//  Copyright 2011 iccesoft. All rights reserved.
//


#import "DownLoadImageResourceController.h"
#import "UploadToWebServerSYNController.h"

@implementation DownLoadImageResourceController

@synthesize listeningSocket;
@synthesize portNumber;
@synthesize isWorking;
@synthesize progress;
@synthesize pageCodeList;
@synthesize lockData;

DownLoadImageResourceController *downLoadImageResourceController;

+(DownLoadImageResourceController *)getServerInstance{
	if (downLoadImageResourceController == nil) {
		downLoadImageResourceController = [[DownLoadImageResourceController alloc] init] ;		
	}
	return downLoadImageResourceController;
}

-(DownLoadImageResourceController *)initData{	
	self.isWorking = FALSE;
	self.progress = 0;
    if (self.lockData == nil) {
        self.lockData = [[NSLock alloc] init];
        self.pageCodeList = [[NSMutableArray alloc] init];
    }
	
	return downLoadImageResourceController;
	
}

#pragma mark -
#pragma mark Service Handling
- (void) startService{		
	[NSThread detachNewThreadSelector:@selector(serverHTTPThreadEntry) toTarget:self withObject:NULL];		
}

- (int) getPort{		
	return self.portNumber;		
}

- (void) stopService{
	close(listeningSocket);
}


#pragma mark -
#pragma mark Web Service Thread
- (void) serverHTTPThreadEntry{
	NSAutoreleasePool *serverPool = [[NSAutoreleasePool alloc] init];
	
	int connectionSocket;
	socklen_t length;
	struct sockaddr_in client_address; 
	struct sockaddr_in server_address;
	
	// リスニングソケットの生成
	if((listeningSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		return;
	}
	
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);
	server_address.sin_port = htons(HTTP_PORT);
	
	// 待ち受けポートのバインド
	self.portNumber = HTTP_PORT ;
	for (int i= 0; i < 1000; i++) {
		int ret = bind(listeningSocket, (struct sockaddr *)&server_address,sizeof(server_address));
		if( ret < 0) {
			self.portNumber += 1 ;
			server_address.sin_port = htons(self.portNumber);
			[NSThread sleepForTimeInterval:0.1];
			
		}else {
			break;
		}
	}
	
	//NSLog(@"start listeningSocket: %d",listeningSocket);
	//NSLog(@"---init--http_port--:%@", [NSString stringWithFormat:@"%d/", self.portNumber]);
	
	int set = 1;
	setsockopt(listeningSocket, SOL_SOCKET, SO_NOSIGPIPE, (void *)&set, sizeof(int));
	
	// 待ち受け開始
	if(listen(listeningSocket, QUEUE_SIZE) < 0) {
		return;
	}
	
	// リクエスト待ちループ	
	while (TRUE) {
		length = sizeof(client_address);
		if((connectionSocket = accept(listeningSocket, (struct sockaddr *)&client_address, &length)) < 0) {
			break;
		}
		[NSThread sleepForTimeInterval:0.1];
		// 接続ソケットのリクエストを処理する
		//NSLog(@"start connectionSocket: %d",connectionSocket);
		[NSThread detachNewThreadSelector:@selector(handleRequest:) toTarget:self withObject:[NSNumber numberWithInt:connectionSocket] ];
		//[self handleRequest:connectionSocket];
	}
	
    //NSLog(@"--Server End ---");
	[serverPool release];
	
}

/** HTTP-GETリクエストに対してのサービス **/
- (void) handleRequest:(NSNumber *)inSocket{
	//- (void) handleRequest:(int)inSocket{
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	int connectionSocket = [inSocket intValue];
	//	int connectionSocket = inSocket;
	char buffer[BUFFER_SIZE+1];
	int len = recv(connectionSocket, buffer, BUFFER_SIZE,0);
	
	buffer[len] = '\0';
	//NSLog(@"recev size: %d",len);
	
	NSString *request = [NSString stringWithCString:buffer encoding: NSUTF8StringEncoding];	
	//NSLog(@"request:%@",request);

	NSString *url_part = [[request substringToIndex:[request rangeOfString:@"HTTP/"].location] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	NSString *data_part = nil;
	NSMutableArray *parameter = nil;
	NSRange cmd_type_post = [url_part rangeOfString:@"POST"];
	if (cmd_type_post.location != NSNotFound) {
		NSRange data_range = [request rangeOfString:@"\r\n\r\n"];
		data_part = [request substringFromIndex :data_range.location + data_range.length ];
		
		//NSLog(@"data_part:%@",data_part);
		parameter = [data_part JSONValue] ;
		//NSLog(parameter);		
	}
	
	//ダウンロード対象に追加
	[self.lockData lock];
	[self.pageCodeList addObjectsFromArray:parameter];
	[self.lockData unlock];

	//NSLog(@"self.pageCodeList cnt:%d",[self.pageCodeList count]);
	//NSLog(@"parameter cnt:%d",[parameter count]);
	//NSLog(@"self.pageCodeList cnt:%d",[self.pageCodeList count]);

	//ダウンロード開始
	[AppHelper performSelectorInBackground:@selector(startDataLoadingProgressAnimation:) withObject:[NSNumber numberWithInt:[self.pageCodeList count]]];
	[NSThread sleepForTimeInterval:0.1]; 

	//プログレスバーを０から再スタートさせる
	[self.lockData lock];
	self.progress = 0;
	[self.lockData unlock];

	// 接続ソケットのクローズ
	close(connectionSocket);
		
	if (self.isWorking) {
		return;
	}
	
	[self.lockData lock];
	self.isWorking = TRUE;
	[self.lockData unlock];
	
	[self mainDownLoadImageResourceMethod];
	
	[AppHelper endDataLoadingProgressAnimation];

	[self.lockData lock];
	self.isWorking = FALSE;
	[self.lockData unlock];
	
	//NSLog(@"--end --");
	[pool release];
}


-(void)mainDownLoadImageResourceMethod{	
	while ([self.pageCodeList count] > 0) {
		NSMutableDictionary *info = [self.pageCodeList objectAtIndex:0];
		//NSLog(@"pageCode:%@",pageCode);

        [self insertPhotoLibraryDataToDataBase:info];
 		
	
		//[Utils setDataLoadingProgressStatus:[NSNumber numberWithInt:progress]];
 
		[AppHelper performSelectorInBackground:@selector(setDataLoadingProgressStatus:) withObject:[NSNumber numberWithInt:progress]];
        [NSThread sleepForTimeInterval:0.1]; 


		//取得済みデータは削除とプログレスアップ
		[self.lockData lock];
		self.progress++;
		[self.pageCodeList removeObjectAtIndex:0];
		[self.lockData unlock];
		
//        //データをサーバーへのアップ処理を起動する
//        UploadToWebServerSYNController *uploadToWebServerSYNController = [[UploadToWebServerSYNController getUploadServerInstance] initData:nil];
//        [uploadToWebServerSYNController performSelectorOnMainThread:@selector(startUpload) withObject:nil waitUntilDone:NO];

	}

 }


-(void)insertPhotoLibraryDataToDataBase:(NSMutableDictionary *)param{

	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSString *assets_data_key	= [param objectForKey:ASSETS_DATA_KEY];
	NSData *pngData = [BssAssetsController getPhotoImageData:assets_data_key];
    
    //NSLog(@"insert imageData pageCode:%@ length:%d", pageCode, [imageData length]);
    if (pngData != nil) {
        NSDate* now_date = [NSDate date];
        NSNumber *nowTime = [NSNumber numberWithInt:[Utils getNowTimeIntervalWithDate:now_date]];
        NSString *createTime = [Utils getNowTimeWithDate:now_date];
        NSString *fileName = [NSString stringWithFormat:@"%@.%@",createTime,IMG_FILE_EXT_JPG];

        [AppHelper writeFileData:fileName fileData:pngData];
                
        //NSLog(@"dataSize:%d; imgUrl:%@",[pngData length], imgUrl);
        NSMutableDictionary *imageDataDicn = [[NSMutableDictionary alloc] initWithCapacity:1];
        [imageDataDicn setObject:fileName forKey:fileName_key];
        [imageDataDicn setObject:DB_RESOURCE_TYPE_PHOTO forKey:resourceType_key];
        [imageDataDicn setObject:@"" forKey:illustrationId_key];
        [imageDataDicn setObject:@"" forKey:longitute_key];
        [imageDataDicn setObject:@"" forKey:latitute_key];
        [imageDataDicn setObject:[NSNumber numberWithInt:0] forKey:zoom_key];
        [imageDataDicn setObject:nowTime forKey:TIME];
        [imageDataDicn setObject:@"" forKey:urlAddress_key];
        [imageDataDicn setObject:[param objectForKey:sheetId_Key] forKey:sheetId_Key];
      
        [[DataRegistry getInstance] saveImageResourceData:imageDataDicn];
        [imageDataDicn release];
        
        //Cardデータも作成し、DBに保存する。
        [AppHelper careateCardDataToSave:param contents:CONTENTS_META_PHOTO sync_status:SYNC_STATUS_OFF option:fileName];
        
    }
     
    //NSLog(@"insertPhotoLibraryDataToDataBase: [data length]:%d",[pngData length]);

	[pool release];	
	
	//Assetsデータを取り出した後、削除する
	[BssAssetsController delPagePhotoAsset:[param objectForKey:ASSETS_DATA_KEY]];
	
	//NSLog(@"---insertImgDataToDataBase End---");
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
	NSLog(@"---- MainViewController MemoryWarning !!! ----");
}


- (void)dealloc {
	//	NSLog(@"---MainViewController : dealloc--!!!-------");	
    [self.lockData release];
	[self.pageCodeList removeAllObjects];
	[self.pageCodeList release];
	
	[super dealloc];
    
}


@end
