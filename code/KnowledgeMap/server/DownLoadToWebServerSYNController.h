//
//  DownLoadToWebServerSYNController.h
//  BukurouSS
//
//  Created by k-kin on 11/12/13.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import "UploadToWebServerSYNController.h"

#define  MAX_ownLoadingReturnCount  5

@interface DownLoadToWebServerSYNController : UploadToWebServerSYNController {
	NSString	*fileMIMEType;
	NSString	*bookCode;
	NSThread	*thread;
	NSMutableArray	*bookCodeList;
	NSMutableArray	*downLoadMetaDatas;
	
}

@property (nonatomic, retain) NSString	*fileMIMEType;
@property (nonatomic, retain) NSString	*bookCode;
@property (nonatomic, retain) NSThread	*thread;
@property (nonatomic, retain) NSMutableArray	*bookCodeList;
@property (nonatomic, retain) NSMutableArray	*downLoadMetaDatas;

+(DownLoadToWebServerSYNController *)getDownLoadServerInstance;
-(DownLoadToWebServerSYNController *)initDownLoadData:(id)inDelegate;
- (void)startDownload:(NSString *)bookCode;
-(void)cancelDownload;
-(void)stopDownLoad;
-(void) toStartDownLoadWebServer:(NSString *)bookCode;
-(void)downLoadServerSyncData:(NSMutableDictionary *)param;
@end

