//
//  Server.h
//  BukurouSS
//
//  Created by k-kin on 11/11/14.
//  Copyright 2011 iccesoft. All rights reserved.
//

@class WebTask;
@protocol WebServerDelegate
@optional

//データ通信をキャンセルしたとき
-(void)cancelOptarion;

//データ通信エラーが発生したとき
-(void)connectNetWorkError;

// Fileデータを受信したとき
//a dictionary of format {fileType:data}
- (void)recieveApplicationData:(NSDictionary*)fileData;

// 処理データを受信したとき
- (void)recieveOprationData:(WebTask *)request;
@end

@interface WebTask : NSObject
@property(nonatomic,assign) id delegate;
@property(nonatomic,assign) bool showIndicator;
@property(nonatomic,copy) NSString *url;
@property(nonatomic,retain) NSDictionary *params;
@property(nonatomic,copy) NSString* response;
+(id)TaskWithUrl:(NSString*)url params:(NSDictionary*)params delegate:(id)delegate showIndicator:(bool)show;
@end

#import "ServerParamDef.h"

@interface WebServerDownLoadController : NSObject{
    NSMutableArray *tasks;
    WebTask *currentTask;
    NSURLConnection *urlConnection;
}

//@property (nonatomic, assign) id delegate;

+ (WebServerDownLoadController *)getInstance;
- (void) sendWorkDataToWebServer:(NSMutableDictionary *)params 
			  showConnectionFlag:(BOOL)showConnectionIndicator 
					   serverUrl:(NSString *)inUrl
						delegate:(id)inDelegate;


- (void) sendOtherApplicationSerivice:(NSString *)inUrl delegate:(id)inDelegate;

- (void)cancelOptarion;
@end
