//
//  WebServerSYNController.m
//  BookMaker
//
//  Created by k-kin on 10/06/13.
//  Copyright 2010 ICCESOFT. All rights reserved.
//

#import "UploadToWebServerSYNController.h"

@interface UploadToWebServerSYNController()
@property(strong) NSMutableArray *currentCardContentList;
@property(strong) NSMutableDictionary *currentCardInfo;
@property(assign) NSMutableDictionary *currentUploadItem;
@end

@implementation UploadToWebServerSYNController

@synthesize delegate;
@synthesize userinfo;
@synthesize dataRegistry;
@synthesize lockData;
@synthesize receiveData;
@synthesize timer;
@synthesize dictionaryData;
@synthesize isDataUploadLoading;
@synthesize currentMethodName;
@synthesize currentFileName;
@synthesize currentCardBeging;
@synthesize currentSheetId;
@synthesize currentCardContentList;
@synthesize web_socket;


UploadToWebServerSYNController *uploadToWebServerSYNController;

+(UploadToWebServerSYNController *)getUploadServerInstance{
	if (uploadToWebServerSYNController == nil) {
		uploadToWebServerSYNController = [[UploadToWebServerSYNController alloc] init] ;		
	}
	return uploadToWebServerSYNController;
}

//-(UploadToWebServerSYNController *)initData:(id)inDelegate{
//	if (self.dataRegistry != nil) {
//        self.delegate = inDelegate;
//		return uploadToWebServerSYNController;
//	}
//	
//    self.delegate = inDelegate;
//	self.dataRegistry = [DataRegistry getInstance];
//	self.userinfo = [BusinessDataCtrlManager getUserInfo];	
//	self.lockData = [[NSLock alloc] init];
//    self.currentSheetId = [NSNumber numberWithInt:0];
//	return uploadToWebServerSYNController;
//
//}

-(id)init
{
    if(self = [super init])
    {
        self.dataRegistry = [DataRegistry getInstance];
        self.userinfo = [BusinessDataCtrlManager getUserInfo];
        self.lockData = [[NSLock alloc] init];
        self.currentSheetId = [NSNumber numberWithInt:0];
    }
    return self;
}

-(void)stopUpLoad{
    [self clearDatas];
}

-(void)clearDatas{
	[lockData lock];
    self.delegate = nil;
	[receiveData release];
	receiveData = nil;
	isDataUploadLoading = FALSE;
    if (self.web_socket != nil) {
        [self.web_socket close];
        [self.web_socket release];
        self.web_socket = nil;
    }
	//Timerをストップし、MainViewController.mで定期的にチェックし、ネットが繋がったら再会する。 
	[self stopSYNCTimer];
	[lockData unlock];
	
}

// 非同期のスレッドエントリー（TCP/IP）
- (void) startUpload{	
	//NSLog(@"--------startUpload----------");
	//NSLog(@"======= LOG OUT ========");
	//if( self.userinfo == nil || [USER_ACTIVE_OFF isEqualToString:self.userinfo.activeFlg]){
	if( self.userinfo == nil ){
		//NSLog(@"======= LOG OUT ========");
		return;	
	}	

	//ネット接続エラーの場合：　スキップする
	if (![Utils checkWebServerStatus]) {
		return;
	} 

	//データダウンロード中の場合：　スキップする
	if (isDataUploadLoading) {
		return;
	}
	
	//Upload開始
	self.timer =[NSTimer scheduledTimerWithTimeInterval:SYNC_WEBSEVER_TIME_INTERVAL target:self selector: @selector(toStartSYNCwithWebServer) userInfo: nil repeats: NO];	
	//[self performSelectorInBackground:@selector(toStartSYNCwithWebServer) withObject:nil];

	//NSLog(@"------getServerData-------");
}


-(void) toStartSYNCwithWebServer{
	//NSLog(@"======= LOG OUT ========");
	if( self.userinfo == nil || self.userinfo.sessionId == nil || [self.userinfo.sessionId isEqualToString:@""]){
		return;	
	}	
	
	//データUPロード中の場合：　スキップする
	if (isDataUploadLoading) {
		return;
	}
	
	//同期が始まる前に同期中にステータスを変更する
	[lockData lock];
	isDataUploadLoading = TRUE;
	[lockData unlock];
		
    //WebSockt通信を確立する
    [self startWebsocket];
 
//	[self searchLocalSyncData];
}


-(void)stopSYNCTimer{
	if (self.timer != nil && [self.timer isValid]) {
		[self.timer invalidate];
	}	
}

- (NSString*)cardContents:(NSMutableArray*)resourceIds
{
    static NSString *cardBegin = @"<?xml version=\"1.0\" encoding=\"UTF-8\"?><card>";
    static NSString *cardEnd = @"</card>";
    
    NSString *ret = [NSString stringWithString:cardBegin];
    
    for (NSMutableDictionary *item in currentCardContentList)
    {
        if ([item objectForKey:resourceId_key])
        {
            ret = [ret stringByAppendingFormat:@"<resource id=\"%@\" />",[item objectForKey:resourceId_key]];
            [resourceIds addObject:@([[item objectForKey:resourceId_key] intValue])];
        }
        else if([item objectForKey:illustrationId_key])
        {
             ret = [ret stringByAppendingFormat:@"<illustration id=\"%@\" />",[item objectForKey:illustrationId_key]];
        }
        else
        {
            NSString *text = [item objectForKey:CONTENT_TEXT_KEY];
            if ([text length])
            {
                ret = [ret stringByAppendingFormat:@"%@",text];
            }
        }
    }
    
    ret = [ret stringByAppendingString:cardEnd];
    return ret;
}

- (void)prepareCardEdit
{
    if (self.web_socket == nil)
    {
        [self startWebsocket];
        return;
    }
    
    
    if (self.currentSheetId != [BusinessDataCtrlManager getUserInfo].lastSheetId)
    {
        self.currentSheetId = [BusinessDataCtrlManager getUserInfo].lastSheetId;
        [self sheetEntryOrExit:ACTION_CODE_VALUE_ENTRY_SHEET];
        return;
    }
    
    if (self.currentCardInfo)
    {
        [[BusinessDataCtrlManager sharedInstance] startEditCard:self.currentCardInfo delegate:self];
    }
    else
    {
        [[BusinessDataCtrlManager sharedInstance] createCard:self];
    }
}

- (void)uploadResource
{
    self.currentUploadItem = nil;

    for (NSMutableDictionary *item in currentCardContentList)
    {
        if ([item objectForKey:CONTENT_IMAGE_KEY] && ([item objectForKey:resourceId_key] == nil || [[item objectForKey:resourceId_key] intValue] == 0))
        {
            self.currentUploadItem = item;
            break;
        }
    }
    
    if (self.currentUploadItem)
    {
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        
        [param setObject:self.userinfo.sessionId forKey:JSESSIONID];
        [param setObject:[self.currentUploadItem objectForKey:resourceType_key] forKey:resourceType_key];
        if ([self.currentUploadItem objectForKey:illustrationId_key])
        {
            [param setObject:[self.currentUploadItem objectForKey:illustrationId_key] forKey:illustrationId_key];
        }
        
		NSData* imageNSData = UIImageJPEGRepresentation([self.currentUploadItem objectForKey:CONTENT_IMAGE_KEY], 0.8);
		
		self.currentMethodName = METHOD_RESOURCE_UPLOAD;
        NSString *filename = [self.currentUploadItem objectForKey:fileName_key];
        if([filename length] == 0)
        {
            filename = @"no name.jpg";
        }
        NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,METHOD_RESOURCE_UPLOAD];
		[self sendWorkDataToWebServer:param photoData:imageNSData fileName:filename serverUrl:url];
        
		[param release];
    }
    else
    {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        NSMutableArray *resourceIds = [NSMutableArray array];
        NSString* contents = [self cardContents:resourceIds];
        [params setObject:contents forKey:CONTENTS];
        [params setObject:resourceIds forKey:RESOURCEIDS];
        [params setObject:[self.currentCardInfo objectForKey:cardId_key] forKey:cardId_key];
        [[BusinessDataCtrlManager sharedInstance] updateCard:params delegate:self];
    }
}

- (BOOL)publishCard:(NSArray*)dataList
{
	//ネット接続エラーの場合：　スキップする
	if (![Utils checkWebServerStatus]) {
		return false;
	}
    
	//データダウンロード中の場合：　スキップする
	if (isDataUploadLoading) {
		return false;
	}
    self.currentCardInfo = nil;
    self.currentCardContentList = [NSMutableArray arrayWithArray:dataList];
    
    [self prepareCardEdit];
    
    return true;
}

- (BOOL)updateCard:(NSNumber*)cardId data:(NSArray*)datalist
{
//    if( self.userinfo == nil ){
//		//NSLog(@"======= LOG OUT ========");
//		return false;
//	}
    
	//ネット接続エラーの場合：　スキップする
	if (![Utils checkWebServerStatus]) {
		return false;
	}
    
	//データダウンロード中の場合：　スキップする
	if (isDataUploadLoading) {
		return false;
	}
    
    self.currentCardInfo = [NSMutableDictionary dictionaryWithObject:cardId forKey:cardId_key];
    self.currentCardContentList = [NSMutableArray arrayWithArray:datalist];
    
    [self prepareCardEdit];
    return true;
}

#pragma mark - AppApiDelegate delegate
- (void)createCardSuccessed:(NSMutableDictionary*)info{
    self.currentCardInfo = [NSMutableDictionary dictionaryWithObject:[info objectForKey:cardId_key] forKey:cardId_key];
    
    [[BusinessDataCtrlManager sharedInstance] startEditCard:self.currentCardInfo delegate:self];
}

- (void)startEditCardSucessed:(NSMutableDictionary *)Info{
    NSLog(@"startEditCardSucessed");
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[self.currentCardInfo objectForKey:cardId_key] forKey:cardId_key];
    [self uploadResource];
}

- (void)updateCardSucessed:(NSMutableDictionary *)Info{
    NSLog(@"updateCardSucessed");
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[self.currentCardInfo objectForKey:cardId_key] forKey:cardId_key];
    
    [[BusinessDataCtrlManager sharedInstance] endEditCard:param delegate:self];
    
}

- (void)endEditCardSucessed:(NSMutableDictionary *)Info{
    NSLog(@"endEditCardSucessed");
    NSMutableDictionary *data = [[[NSMutableDictionary alloc] init] autorelease];
    [data setObject:DB_RESOURCE_TYPE_BEGINING forKey:resourceType_key];
    
    [self.delegate endEditCardSucessed:data];
    [self stopUpLoad];
}

#pragma mark 同期業務開始
/** ローカルの同期対象データを検索し、サーバーに送信: 同期順番： 画像データ、Cardデータ **/
-(void)searchLocalSyncData{
    
	/*  Cardデータの同期処理  */
	NSMutableArray		*cardDataList = [[NSMutableArray alloc] init];
	int cnt = [self.dataRegistry selectCardData:cardDataList];
    //NSLog(@"Card count:%d",cnt);
	if (cnt > 0 ) {
		//NSLog(@"-------PageData------ start");
 		NSMutableDictionary *dataInfo = [cardDataList objectAtIndex:0];
        NSNumber *sheetId = [dataInfo objectForKey:sheetId_Key];

        if ([sheetId intValue] != [self.currentSheetId intValue]) {
            
            //同期後に各ステータスをクリアする
            //[self searchLocalSyncDataEndClearStatus];

            //途中でSheetIdが変わった場合、新しいSheetIdのソケットをEntryする
            self.currentSheetId = sheetId;
            [self sheetEntryOrExit:ACTION_CODE_VALUE_ENTRY_SHEET];
            
            //WebSocketを新しいシートに切り替えて、戻りメソッドで再度searchLocalSyncDataを呼ぶ。
            //return;

        }

        //同じシート内なので、そのまま同期処理を継続する
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        [paramDic setObject:self.userinfo.sessionId forKey:JSESSIONID];
        
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        //[param addEntriesFromDictionary:dataInfo];
        [param setObject:[dataInfo objectForKey:categoryId_Key] forKey:categoryId_Key];
        [param setObject:[dataInfo objectForKey:cardContents_key] forKey:CONTENTS];
        NSMutableArray *resourceArray = [[NSMutableArray alloc] init];
        NSString *resourceId = [dataInfo objectForKey:resourceId_key];
        if (resourceId != nil && ![resourceId isEqualToString:@""]) {
            [resourceArray addObject:[NSNumber numberWithInt:[resourceId intValue]]];
        }
        
        [param setObject:resourceArray  forKey:RESOURCEIDS];
        [resourceArray release];        
        [paramDic setObject:param forKey:PARAMS];
        [param release];
        
        self.currentMethodName = METHOD_CREATE_CARD;
        self.currentCardBeging = [dataInfo objectForKey:begining_key];
        
        NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,METHOD_CREATE_CARD];
        //NSLog([paramDic JSONRepresentation]);
        [self sendWorkDataToWebServer:paramDic photoData:nil fileName:nil serverUrl:url];
        
        [paramDic release];        
        [cardDataList release];
        return;
	}
    [cardDataList release];

    //reSourceData
    NSMutableArray *imgDataInfo =[[NSMutableArray alloc] init];
	cnt = [self.dataRegistry loadImageResourceData:imgDataInfo];
	if (cnt > 0 ) {
		//NSLog(@"-----ImageResourceData----- start");
        
 		NSMutableDictionary *dataInfo = [imgDataInfo objectAtIndex:0];
       NSNumber *sheetId = [dataInfo objectForKey:sheetId_Key];
        if ([sheetId intValue] != [self.currentSheetId intValue]) {
            
            //同期後に各ステータスをクリアする
            //[self searchLocalSyncDataEndClearStatus];

            //途中でSheetIdが変わった場合、新しいSheetIdのソケットをEntryする
            self.currentSheetId = sheetId;
             [self sheetEntryOrExit:ACTION_CODE_VALUE_ENTRY_SHEET];
            
            //WebSocketを新しいシートに切り替えて、戻りメソッドで再度searchLocalSyncDataを呼ぶ。
            //return;
            
        }

		NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        
        [param setObject:self.userinfo.sessionId forKey:JSESSIONID];
        [param setObject:[dataInfo objectForKey:resourceType_key] forKey:resourceType_key];
        [param setObject:[dataInfo objectForKey:illustrationId_key] forKey:illustrationId_key];
        
		NSString *filename =  [dataInfo objectForKey:fileName_key];
		NSData* imageNSData = [AppHelper readFileData:filename];
		
		self.currentMethodName = METHOD_RESOURCE_UPLOAD;
        self.currentFileName = [dataInfo objectForKey:fileName_key];
        
        NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,METHOD_RESOURCE_UPLOAD];
		[self sendWorkDataToWebServer:param photoData:imageNSData fileName:filename serverUrl:url];
        
		[param release];
		[imgDataInfo release];
		return;
	}
	[imgDataInfo release];
	
		
    //同期後に各ステータスをクリアする
    [self searchLocalSyncDataEndClearStatus];
	
}

-(void)searchLocalSyncDataEndClearStatus{
    /**  ここで１回目のアップロードが終わったので、ダウンロードフラグをFalseにリセットする      **/
    [lockData lock];
    isDataUploadLoading = FALSE;
    self.dictionaryData = nil;
    [lockData unlock];
    
    //今のSheetIdのエントリをクローズする
    [self sheetEntryOrExit:ACTION_CODE_VALUE_EXIT_SHEET];

}

#pragma mark Websocket
//WebSockt通信を確立する
-(void) startWebsocket{
    
    if (self.web_socket == nil) {
        NSString *url = [NSString stringWithFormat:@"%@%@",BssBookWsURLAdress,METHOD_SESSTION];
        //NSLog([paramDic JSONRepresentation]);
        
        self.web_socket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        [self.web_socket setDelegate:self];
        [self.web_socket open];

   }
//    else{
//        [self sheetEntryOrExit:ACTION_CODE_VALUE_ENTRY_SHEET];
//
//        [self searchLocalSyncData];
//        
//    }
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket{
    NSLog(@"------- webSocket  webSocketDidOpen-------  ");
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *actions = [[NSMutableDictionary alloc] initWithCapacity:2];
    [actions setObject:ACTION_CODE_VALUE_LOGIN forKey:ACTION_CODE_KEY];
    [actions setObject:ACTION_TYPE_VALUE_REQ forKey:ACTION_TYPE_KEY];
    [paramDic setObject:actions forKey:ACTION];
    [actions release];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:self.userinfo.sessionId forKey:SESSIONID];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
//    self.currentSheetId = self.userinfo.lastSheetId;

    //NSLog([paramDic JSONRepresentation]);
    [self.web_socket send:[paramDic JSONRepresentation]];
    
    [paramDic release];

}


-(void)sheetEntryOrExit:(NSString *)inAction{
    if ([self.currentSheetId intValue] > 0) {
        [self sheetEntryFinal:inAction sheetId:self.currentSheetId];
        
    }
}

-(void)sheetEntryFinal:(NSString *)inAction sheetId:(NSNumber *)inSheetId{
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *actions = [[NSMutableDictionary alloc] initWithCapacity:2];
    [actions setObject:inAction forKey:ACTION_CODE_KEY];
    [actions setObject:ACTION_TYPE_VALUE_REQ forKey:ACTION_TYPE_KEY];
    [paramDic setObject:actions forKey:ACTION];
    [actions release];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:inSheetId forKey:sheetId_Key];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    
    //NSLog([paramDic JSONRepresentation]);
    [self.web_socket send:[paramDic JSONRepresentation]];
    
    [paramDic release];
    
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message{
//    NSLog(@"------- webSocket  didReceiveMessage-------: %@ ",message);
    NSMutableDictionary *recieve_data = [message JSONValue];
    NSMutableDictionary *result = [recieve_data objectForKey:RESULT_KEY];
    
    NSString  *resultStatus = [result objectForKey:RESULT_STATUS_KEY];
        
    if ([resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {
        //NSLog(@"recive mothodName:%@",mothodName);
        NSMutableDictionary *resultParam =[recieve_data objectForKey:ACTION];
        NSString *code = [resultParam objectForKey:ACTION_CODE_KEY];
        
        if ([code isEqualToString:ACTION_CODE_VALUE_LOGIN] ) {//Login
            
            //Sheetをエントリーする　エントリーシートは戻り値が帰って来ない！！！
//            self.web_socket.delegate = nil;
//            [self sheetEntryOrExit:ACTION_CODE_VALUE_ENTRY_SHEET];
//            
//
//            //EntrySheet
//             NSMutableArray		*resourceDataList = [[NSMutableArray alloc] init];
//            int cnt = [self.dataRegistry loadImageResourceData:resourceDataList];
//            if (cnt > 0 ) {
//                 NSMutableDictionary *dataInfo = [resourceDataList objectAtIndex:0];
//                self.currentSheetId = [dataInfo objectForKey:sheetId_Key];
//                
//
//                [self searchLocalSyncData];
//                [resourceDataList release];
//              
//                return;
//            }
//            [resourceDataList release];
//
//            /*  Cardデータの同期処理  */
//            NSMutableArray		*cardDataList = [[NSMutableArray alloc] init];
//            cnt = [self.dataRegistry selectCardData:cardDataList];
//            //NSLog(@"Card count:%d",cnt);
//            if (cnt > 0 ) {
//                NSMutableDictionary *dataInfo = [cardDataList objectAtIndex:0];
//                self.currentSheetId = [dataInfo objectForKey:sheetId_Key];
//                
//                [self searchLocalSyncData];
//                [cardDataList release];
//                
//                return;
//
//            }
//            [cardDataList release];
//            
//  
//            /**  ここで１回目のアップロードが終わったので、ダウンロードフラグをFalseにリセットする      **/
//            [lockData lock];
//            isDataUploadLoading = FALSE;
//            self.dictionaryData = nil;
//            [lockData unlock];
            [self prepareCardEdit];
            
        }else if([code isEqualToString:ACTION_CODE_VALUE_ENTRY_SHEET]){
             /** Sheetをエントリーする　エントリーシートは戻り値が帰って来ない！！！  **/
            [self prepareCardEdit];
        }

        
    }
    
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
    NSLog(@"------- webSocket  didFailWithError-------  ");

}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{

    NSLog(@"------- webSocket  didCloseWithCode-------  ");

}



#pragma mark Workデータ
// Workデータをサーバーに送る
- (void) sendWorkDataToWebServer:(NSMutableDictionary *)params photoData:(NSData *)photoData fileName:(NSString *)photoFileName serverUrl:(NSString *)inUrl{
	if (![Utils checkWebServerStatus]) {
		return;
	} 
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	//setting up the body:
    NSURL *cgiUrl = [NSURL URLWithString:inUrl];
	NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:cgiUrl];
	[postRequest setHTTPMethod:@"POST"];
	[postRequest setTimeoutInterval:60];

	NSMutableData* postData = [[NSMutableData alloc] init];
    
    
    NSArray *key_list = [params allKeys];
	for (int i=0; i < [key_list count] ; i++) {
		NSString *key = [key_list objectAtIndex:i];
        id valueData = [params objectForKey:key];
		NSString *value = @"";
        if ( [valueData isKindOfClass:[ NSString class]]) {
			value = valueData;
		}else if ( [valueData isKindOfClass:[ NSMutableDictionary class]]) {
            value = [valueData JSONRepresentation];
		}else if ( [valueData isKindOfClass:[ NSMutableArray class]]) {
            value = [valueData JSONRepresentation];
        }else{
            value = valueData;
        }
        
        //value = [value stringByReplacingOccurrencesOfString:@"%@" withString:<#(NSString *)#>]
        value = [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        NSLog(@"key: %@", key);
//        NSLog(@"value: %@", value);

        if (photoData != nil) {
            [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", BOUNDARY] dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[[NSString stringWithFormat:@"%@", value ] dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

        }else{
            //NSLog(@"%@",[NSString stringWithFormat:@"%@=%@&", key,value]);
            [postData appendData:[[NSString stringWithFormat:@"%@=%@&", key,value] dataUsingEncoding:NSUTF8StringEncoding]];
 
        }
     }
	
    if (photoData != nil) {
        
        [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", BOUNDARY] dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n", photoFileName] dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[[NSString stringWithFormat:@"Content-Type: image/%@\r\n\r\n", IMG_FILE_EXT_JPG] dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:photoData];
        [postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postData appendData:[[NSString stringWithFormat:@"--%@--\r\n",BOUNDARY] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BOUNDARY];
        [postRequest setValue: contentType forHTTPHeaderField: @"Content-Type"];
    }
    
	// POST実行
    //NSString *ret_str = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",ret_str);

	[postRequest setHTTPBody:postData];
	
	//NSLog(@"--exec Post ---");
	[NSURLConnection connectionWithRequest:postRequest delegate:self] ;
	
	[postData release];
}


- (void)connection:(NSURLConnection *)i_connection didReceiveResponse:(NSURLResponse *)i_response{		
   // データ蓄積用に NSMutableData を初期化
	receiveData = nil;
    receiveData = [[NSMutableData alloc] init];
}

- ( void ) connection:( NSURLConnection *) connection didReceiveData:( NSData *) data {		
	[receiveData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{	
	[self clearDatas];
	
	NSLog(@"-- didFailWithError --");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)i_connection{
	//NSLog(@"---download is complted--- !");
    
    /*****  ---------------------------------------------------    *****/
        
	NSString *ret_str = [[NSString alloc] initWithData:receiveData encoding:NSUTF8StringEncoding];
	//NSLog(ret_str);
	[receiveData release];
	receiveData = nil ;
	NSMutableDictionary *recieve_data = nil;
	NSString *resultStatus = nil;
	NSMutableDictionary *resutlData = nil;
	if ([ret_str isKindOfClass:[ NSNull class]]  || ret_str == nil  || [ret_str isEqualToString:@""]) {
		/****   何かの原因で受信データが空きダッタ場合、各変数を初期化し、処理を再スタートする。   ***/
		NSLog(@"------SYNC---Error--- EmptyDatas!--- ");
		[self clearDatas];
	}else {
		recieve_data = [ret_str JSONValue];
        NSMutableDictionary *result = [recieve_data objectForKey:RESULT_KEY];

		if (result == nil || [result isKindOfClass:[ NSNull class]] ) {
			NSLog(@"------SYNC---Error--- ReCeiveData Error!--- ");
			[self clearDatas];				
		}else {
			resultStatus = [result objectForKey:RESULT_STATUS_KEY];
            
			resutlData = [recieve_data objectForKey:RESULT_DATA_VALUE_KEY];							
		}
	}
	
	[ret_str release];
    
	if([resultStatus isKindOfClass:[ NSNull class]]){
        [self clearDatas];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        return;
    }else if ([resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {
        //NSLog(@"recive mothodName:%@",mothodName);
        if ([self.currentMethodName isEqualToString:METHOD_RESOURCE_UPLOAD] ) {
            //対象画像データを削除する。
            //CardテーブルのリソースIDを更新する
            NSNumber *resourceId = [resutlData objectForKey:resourceId_key];
//            NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
//            [param setObject:[resourceId stringValue] forKey:resourceId_key];
//            NSString *contents = [CONTENTS_META_PHOTO stringByReplacingOccurrencesOfString:CONTENTS_META_DUMMY withString:[NSString stringWithFormat:@"%d",[resourceId intValue]]];
//            [param setObject:contents forKey:cardContents_key];
//            [param setObject:self.currentFileName forKey:begining_key];
//            [self.dataRegistry updateResourceId:param];
//            [param release];
//
//                        
//            [self.dataRegistry deleteImageResourceData:self.currentFileName];
//            [AppHelper removeFile:self.currentFileName];
            [self.currentUploadItem setObject:resourceId forKey:resourceId_key];
            [self uploadResource];
            
         }else if ([self.currentMethodName isEqualToString:METHOD_CREATE_CARD] ) {
          //NSLog(@"-----METHOD_CREATE_CARD------");
          //対象Cardデータを削除す。
            [self.dataRegistry deleteCardData:self.currentCardBeging];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:reroadNotification object:self userInfo:nil];

        }
 

    }else  {
		//NSLog(@"--upload error ---");
		[lockData lock];
		isDataUploadLoading = FALSE;
		self.dictionaryData = nil;
		[lockData unlock];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
		return;
	}

	//NSLog(@"--upload error ---");			
	[lockData lock];
	self.dictionaryData = nil;
	[lockData unlock];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

	//次のローカルデータをアップロードする。
	[self searchLocalSyncData];
	
	
}

@end
