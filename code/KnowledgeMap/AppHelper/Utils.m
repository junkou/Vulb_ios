//
//  Utils.m
//  KnowledgeMap
//
//  Created by KouJun on 4/12/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import "Utils.h"
#import "AppDefine.h"
#import "Reachability.h"

@implementation Utils
#pragma mark checkVersion
+(BOOL)isIOS5orLater{
	NSString *versions = [[UIDevice currentDevice] systemVersion];
	//NSLog(@"versions:%@",versions);
	if ([versions compare:@"5.0"] != NSOrderedAscending) {
		return TRUE;
	}else {
		return FALSE;
	}
}

+(BOOL)isIOS6orLater{
	NSString *versions = [[UIDevice currentDevice] systemVersion];
	//NSLog(@"versions:%@",versions);
	if ([versions compare:@"6.0"] != NSOrderedAscending) {
		return TRUE;
	}else {
		return FALSE;
	}
}

+(BOOL)isIOS7orLater
{
	NSString *versions = [[UIDevice currentDevice] systemVersion];
	//NSLog(@"versions:%@",versions);
	if ([versions compare:@"7.0"] != NSOrderedAscending) {
		return TRUE;
	}else {
		return FALSE;
	}
}

+(BOOL)deviceIs1136H
{
    return [UIScreen mainScreen].bounds.size.height == 568;
}

#pragma mark -
#pragma mark checkWebServerStatus
+(BOOL)checkWebServerStatus{
	Reachability *rb = [Reachability reachabilityForInternetConnection];
	if ([rb currentReachabilityStatus] == NotReachable) {
		return FALSE;
	} else {
		return TRUE;
	}
}

+(BOOL)checkWiFiServerStatus{
	Reachability *rb = [Reachability reachabilityForLocalWiFi];
	if ([rb currentReachabilityStatus] == NotReachable) {
		return FALSE;
	} else {
		return TRUE;
	}
}

+(NSString *) getSystemLangCode{
    NSArray *langs = [NSLocale preferredLanguages];
    NSString *langID = [langs objectAtIndex:0];
    NSString *ret;
    if ([[langID lowercaseString] isEqualToString:@"ja"]) {
        ret = langID;
    }else if([[langID lowercaseString] isEqualToString:@"en"]){
        ret = langID;
    }else{
        ret = @"en";
    }
    return ret;
}

+ (NSString *)MultiLangStringInfomation:(NSString *)key{
	NSString *value = MultiLangString(key);
	return value;
}

+(UIImage*)resizeImage:(UIImage*)image Size:(CGSize) size{
	UIImage*    shrinkedImage;
    //	CGSize  size = { 320, 480 };
	UIGraphicsBeginImageContext(size);
	CGRect  rect;
	rect.origin = CGPointZero;
	rect.size = size;
	[image drawInRect:rect];
	shrinkedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return shrinkedImage;
}

+(UIImage*)trimImageToCenterPos:(UIImage*)image trimSize:(CGSize)trimSize {
    
    CGFloat x = 0;
    CGFloat y = 0;
    
    if (image.size.width <= trimSize.width || image.size.height <= trimSize.height) {
        return image;
    }
    
    if(image.size.width > trimSize.width){
        x = (image.size.width-trimSize.width)/2;
    }
    
    if(image.size.height > trimSize.height){
        y = (image.size.height-trimSize.height)/2;
    }
    UIImage *trimImage = [self trimImage:image frameRect:CGRectMake(x, y, trimSize.width, trimSize.height)];
    //    if (imgThumb.size.width > SCREEN_WIDTH/2) {
    //        imgThumb = [self resizeImage:imgThumb Size:CGSizeMake(SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
    //    }
    
	return trimImage;
    
    /*
     if(re_size.width > trimSize){
     x = (re_size.width-trimSize)/2;
     re_size.width = trimSize;
     }
     if(re_size.height > trimSize){
     y = (re_size.height-trimSize)/2;
     re_size.height = trimSize;
     }
     */
    //    NSLog(@"x:%f, y:%f",x,y);
    //    NSLog(@"re_size: %@", NSStringFromCGSize(re_size));
    
    
}

+(UIImage*)trimImage:(UIImage*)image  frameRect:(CGRect)frameRect {
    /*
     UIImage*    shrinkedImage;
     UIGraphicsBeginImageContext(frameRect.size);
     [image drawAtPoint:frameRect.origin];
     shrinkedImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     return shrinkedImage;
     */
	UIImage*    shrinkedImage;
	CGImageRef cgimage = CGImageCreateWithImageInRect(image.CGImage, frameRect);
	shrinkedImage = [UIImage imageWithCGImage:cgimage];
	CGImageRelease(cgimage);
	return shrinkedImage;
    
}

#pragma mark -
#pragma mark getTimeData With Formate
+ (NSString *) getNowTimeMill{
    //	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setDateFormat:@"ssSSS"];
	NSDate* now_date = [NSDate date];
	NSString *now_dateStr = [formatter stringFromDate:now_date] ;
	[formatter release];
	return now_dateStr;
}

+ (NSString *) getYYYYMM{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
	[formatter setDateFormat:@"YYYYMM"];
	NSDate* now_date = [NSDate date];
	NSString *now_dateStr = [formatter stringFromDate:now_date];
	[formatter release];
	return now_dateStr;
}

+ (NSString *) getNowTime{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
	[formatter setDateFormat:@"YYMMddHHmmssSSS"];
	NSDate* now_date = [NSDate date];
	NSString *now_dateStr = [formatter stringFromDate:now_date];
	[formatter release];
	return now_dateStr;
}

+ (NSString *) getNowTimeWithDate:(NSDate *)inDate{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
	[formatter setDateFormat:@"YYMMddHHmmssSSS"];
	NSString *now_dateStr = [formatter stringFromDate:inDate];
	[formatter release];
	return now_dateStr;
}

+ (NSTimeInterval) getNowTimeInterval{
	NSDate* now_date = [NSDate date];
    //NSLog(@"timeIntervalSince1970:%f",(double)[now_date timeIntervalSince1970]);
	return [now_date timeIntervalSince1970];
}

+ (NSTimeInterval) getNowTimeIntervalWithDate:(NSDate *)inDate{
	return [inDate timeIntervalSince1970];
}

+(NSString *)changeToDateString:(NSTimeInterval)dateTimeInterval{
    //NSTimeInterval pastSeconds = dateTimeInterval / (60 * 60 * 24);
    //NSLog(@"pastSeconds:%f",pastSeconds);
    // NSDate *newDate =  [[NSDate date] dateByAddingTimeInterval:(0 - pastSeconds)];
    NSDate* now_date = [NSDate date];
    NSDate *newDate =  [[NSDate date] initWithTimeInterval:dateTimeInterval-[now_date timeIntervalSince1970] sinceDate:now_date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
	[formatter setDateFormat:@"YYYY/MM/dd"];
	NSString *ret = [formatter stringFromDate:newDate];
    [newDate release];
	[formatter release];
    //NSLog(@"ret:%@",ret);
    
    return ret;
}

+ (NSString *) getDataCreateTime{
    //	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"US"] autorelease]];
	[formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	NSDate* now_date = [NSDate date];
	NSString *now_dateStr = [formatter stringFromDate:now_date];
	//NSLog(now_dateStr);
	[formatter release];
	return now_dateStr;
}

+ (NSString *)getShowTime:(NSString*)strTime
{
    NSDate *dateNow = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
	NSDate *dateTime = [formatter dateFromString:strTime];

    
    NSUInteger now = [dateNow timeIntervalSince1970];
    NSUInteger time = [dateTime timeIntervalSince1970];
//    NSUInteger diffTime = now > time ? now - time : 0;
    
    NSString* strRet = nil;
    
    NSCalendar* calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSDateComponents* components_now = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:dateNow];
    NSDateComponents* components_time = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[NSDate dateWithTimeIntervalSince1970:time]];
    
    static const NSUInteger aday = 60*60*24;
    NSUInteger today_begin = (now/aday)*aday;
    
    if (time > today_begin)
    {
        //modified by Changbin,remove 今天
        strRet = MultiLangString(@"Today");
    }
    else if (time > today_begin-aday)
    {
        strRet = MultiLangString(@"Yesterday");
    }
    else
    {
        if (components_now.year == components_time.year)
        {
            strRet = [NSString stringWithFormat:@"%d/%d", components_time.month, components_time.day];
        }
        else
        {
            strRet = [NSString stringWithFormat:@"%d/%d/%d", components_time.year, components_time.month, components_time.day];
        }
    }
    return strRet;
//    if (diffTime < 10)
//    {
//        strTime = @"刚刚";
//    }
//    else if(diffTime < 60)
//    {
//        strTime = [NSString stringWithFormat:@"%d秒前", diffTime];
//    }
//    else if(diffTime < 3600)
//    {
//        strTime = [NSString stringWithFormat:@"%d分钟前", diffTime / 60];
//    }
//    else
//    {
//        NSCalendar* calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
//        NSDateComponents* components_now = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:dateNow];
//        NSDateComponents* components_time = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[NSDate dateWithTimeIntervalSince1970:time]];
//        if (diffTime >= 3600 && diffTime < 7200)
//        {//1-2小时之间
//            if (components_now.year == components_time.year && components_now.month == components_time.month && components_now.day == components_time.day)
//            {
//                strRet = @"1小时前";
//            }
//            else
//            {
//                strRet = [NSString stringWithFormat:@"昨天 %02d:%02d", components_time.hour, components_time.minute];
//            }
//        }
//        else
//        {//2小时之后
//            NSString* am = components_time.hour < 12 ? @"上午":@"下午";
//
//            static const NSUInteger aday = 60*60*24;
//            NSUInteger today_begin = (now/aday)*aday;
//            
//            if (time > today_begin)
//            {
//                //modified by Changbin,remove 今天
//                strRet = [NSString stringWithFormat:@"%@ %02d:%02d", am, components_time.hour, components_time.minute];
//            }
//            else if (time > today_begin-aday)
//            {
//                strRet = [NSString stringWithFormat:@"昨天  %@ %02d:%02d", am, components_time.hour, components_time.minute];
//            }
//            else if (time > today_begin-aday*2)
//            {
//                strRet = [NSString stringWithFormat:@"前天  %@ %02d:%02d", am, components_time.hour, components_time.minute];
//            }
//            else
//            {
//                if (components_now.year == components_time.year)
//                {
//                    strRet = [NSString stringWithFormat:@"%d月%d日  %@ %02d:%02d", components_time.month, components_time.day, am, components_time.hour, components_time.minute];
//                }
//                else
//                {
//                    strRet = [NSString stringWithFormat:@"%d年%d月%d日  %@ %02d:%02d", components_time.year, components_time.month, components_time.day, am, components_time.hour, components_time.minute];
//                }
//            }
//        }
//    }
//    return strRet;
}

static UIWebView *htmlWebView;
+(CGFloat)getHtmlHeight:(NSString *)inHtmlText inWidth:(CGFloat)width
{
    NSString *htmlText = [inHtmlText stringByReplacingOccurrencesOfString:@"'" withString:@"\""];
    
    NSString *html = [NSString stringWithFormat:@"function execCreateHtml(){$('#work1').html('%@');};execCreateHtml();", htmlText];
    
    if (htmlWebView == nil)
    {
        htmlWebView = [[UIWebView alloc] initWithFrame:CGRectZero];
        NSString *path = [[NSBundle mainBundle] pathForResource: @"index" ofType: @"html"];
        [htmlWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
    }
    htmlWebView.frame = CGRectMake(0, 0, width == 0 ? [UIScreen mainScreen].bounds.size.width : width, 100);
    [htmlWebView stringByEvaluatingJavaScriptFromString:html];
    [htmlWebView setNeedsDisplay];
    
    NSString *htmlHeight =  [htmlWebView stringByEvaluatingJavaScriptFromString: @"function getHtmlheight(){return $('#work1').outerHeight(); }; getHtmlheight();"];

    return [htmlHeight floatValue] + 20;

}
@end