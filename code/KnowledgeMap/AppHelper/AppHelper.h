//
//  Utils.h
//  BookMaker
//
//  Created by k-kin on 10/06/03.
//  Copyright 2010 ICCESOFT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "sqlite3.h"
#import "EnvDefine.h"
#import "AppDefine.h"
#import "DatabaseTableNmaes.h"
#import "Reachability.h"
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "ImagePartitionView.h"
#import "ServerParamDef.h"
#import "DownLoadImageResourceController.h"
#import "BusinessDataCtrlManager.h"
#import "DownLoadSimpleWebDataServerController.h"
#import "DatabaseTableNmaes.h"
#import "NSString+RegExp.h"
#import "GTMDefines.h"
#import "GTMNSString+HTML.h"
#import "HttpClientUtils.h"
#import "UnderLineLabel.h"
#import "Utils.h"
//#define NAME_VALUE_SPECT @"_@_"
//#define OBJECT_VALUE_SPECT @"_@@_"
@interface AppHelper : NSObject {
	
}

+(natural_t)get_active_memory ;

+ (void)addCommonButtonProPerty:(UIButton *)button imgName:(NSString *)imgName label:(NSString *)labelText ;
+ (void)setNaviBarButtonProPerty:(UIButton *)button title:(NSString *)title;
+(void)setImageToNavigationBar:(UINavigationBar *)navigationBar imageName:(NSString *)imageName;
+(void)setNavigationBarLeftCancelButton:(UINavigationItem *)navigationItem controller:(id)controller;
+(void)setNavigationBarTitleLabel:(UIView *)view text:(NSString *)title;
+(UIButton *)setNavigationBarRightSaveButton:(UINavigationItem *)navigationItem controller:(id)controller;
+(UIButton *)setNavigationBarRightWriteButton:(UINavigationItem *)navigationItem controller:(id)controller;

+ (NSString *)getResourceIconPath:(NSString *)fileName;
+ (NSString *)getResourceIllustPath:(NSString *)fileName;
+ (NSString *) getRanDomCode;

+ (void)setSheetDataContents:(NSMutableDictionary *)inData sampleSheetFlag:(NSString *)sampleFlag view:(UIView *)superview iconImage:(NSString *)openIcon delegate:(id)inDelegate;

+ (void)setCategoryDataContents:(NSMutableDictionary *)inData status:(NSMutableDictionary *)statusData checked:(BOOL)checkedFlag  sampleSheetFlag:(NSString *)sampleFlag view:(UIView *)superview;

+ (void)setCardInfo:(NSDictionary *)inData superview:(UIView *)superview size:(CGSize)size colorBar:(int)colorIdx delegate:(id)inDelegate;

+ (void)setCardDetailDataContents:(NSMutableDictionary *)inData  view:(UIView *)superview delegate:(id)inDelegate;

+ (CGSize)getCardDetailTextSize:(NSMutableDictionary *)inData;

+(CGSize)sheetNameLabelProperty:(NSString *)inString view:(UIView *)superview startX:(CGFloat)inX color:(UIColor *)inColor;

+(UIImage *)getListFourceImage:(CGRect)rect;

+(void)sendDataToLocalServer:(NSString *)inData;

+(BOOL)writeFileData:(NSString *)dataKey fileData:(NSData *)data ;
+(NSString *)writeMovieData:(NSString *)dataKey movie_data:(NSData *)movie;
+(NSData *)readFileData:(NSString *)fileName ;
+(void)removeFile:(NSString*)path ;
+(BOOL)existsFile:(NSString*)path ;
+ (BOOL)existsFileDocumentDirectory:(NSString*)fileName ;
+ (BOOL)existsFileNSCachesDirectory:(NSString*)fileName ;
+(void)makeDir:(NSString*)path ;
+ (void)clearTempFileDirectory:(NSString*)path ;
+(NSString *)getLocalCatchfilePath:(NSString *)fileName;


+ (void) startDataLoadingAnimation;
+ (void) endDataLoadingAnimation;
+ (void) startDataLoadingAnimationAndSetTimeInterVal;
+(void)startDataLoadingProgressAnimation:(NSNumber *)dataCount;
+(void)setDataLoadingProgressStatus:(NSNumber *)dataIndex;
+(void)endDataLoadingProgressAnimation;

+(NSString *) getSNSImageSourceUrl:(NSDictionary *)dataInfo;
+(NSString *) getVideoImgUrl:(NSDictionary *)dataInfo;

+ (NSString *) parseWebDataToString:(NSDictionary *)dataInfo dataKey:(NSString *)dataKey;
+ (NSNumber *) parseWebDataToNumber:(NSDictionary *)dataInfo dataKey:(NSString *)dataKey;


+(void)setStringDataExt:(char *)inData key:(NSString *)key dict:(NSMutableDictionary *)dict;
+(void)setSqlStatmentExt:(sqlite3_stmt *)stmt data:(NSMutableDictionary *)inData key:(NSString *)key index:(int)index;
+(void)setSqlStatmentExtWithNumber:(sqlite3_stmt *)stmt data:(NSMutableDictionary *)inData key:(NSString *)key index:(int)index;

+(void)careateCardDataToSave:(NSMutableDictionary *)param contents:(NSString *)contents sync_status:(NSString *)sync_status option:(NSString *)fileName;

+(NSString *)htmlEscape:(NSString *)inStr;
+(void)getResourceList:(NSString *)inStr output:(NSMutableArray *)outputList;
+(void)changeCardInfoDetailList:(NSString *)inStr output:(NSMutableArray *)outputList;

+(void) setCommonButtonProperti:(UIButton *)button rect:(CGRect)rect title:(NSString *)title;
+(UIImage *)getWhiteButtonNormalImage:(CGRect)rect;
+(UIImage *)getWhiteButtonSelectedImage:(CGRect)rect;
+(UIImage *)getBlueButtonNormalImage:(CGRect)rect;
+(UIImage *)getBlueButtonSelectedImage:(CGRect)rect;

+(BOOL)isNewCardInSheet:(NSDictionary *)inData;

+ (UIColor *) getCategoryColor:(NSDictionary*)categoryInfo;
+ (UIColor *) getColor:(int)idx;
+ (UIColor *) getBaseTextColor;
+(void)addShadowLine:(UIView *)superview;

+(void)setUserIconWith:(int)userId imageView:(UIView *)imageView;
+ (NSMutableArray *)splitParameterString:(NSString *)inParameterString;
+(NSString *) getVideoPlayHtml:(NSString *)videoUrl;

+(void)createColorMetaDatas:(NSMutableArray *)globalColorMasterList;
+(UIImage *)getFileThumbnailIconImage:(NSString *)fileName;
+(UIImage *)getFileIconImage:(NSString *)fileName;
+(BOOL)isShowFileName:(NSString *)fileName;
+(BOOL)isOfficeFile:(NSString *)fileName;

+(UIImage *)getEditTextToolAreaBaseButtonBaseImage:(CGRect)inRect;
+(UIImage *)getEditTextToolAreaBaseImage:(CGRect)inRect;
+(UIImage *)getEditTextToolAreaBaseShadowImage:(CGRect)inRect;
+ (NSString *) getColorTypeString:(int)idx;

@end

@interface BaseViewController : UIViewController

@end

@interface BaseTableViewController : UITableViewController

@end

@interface BaseNavigationController : UINavigationController

@end