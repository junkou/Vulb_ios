//
//  HttpClientUtils.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/09/11.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AppDefine.h"
#import "AppApiDelegate.h"
#import "ServerParamDef.h"
#import "AFHTTPClient.h"
#import "SVProgressHUD.h"

@interface HttpClientUtils : AFHTTPClient

+ (instancetype)sharedClient;

- (void) getServerDataSetToView:(NSString *)methodName
                      parameter:(NSMutableDictionary *)parameters
                           view:(UIView*)inView
                       delegate:(id)inDelegate
                         option:(id)option
;

- (void)cancelServerDataWithView:(UIView*)inView;

- (void) postDataWithMethod:(NSString *)method
                    methodName:(NSString *)webPath
                 parameters:(NSMutableDictionary *)parameters
                     option:(NSMutableDictionary *)option
              isShowLoading:(BOOL)isShow
                   delegate:(id)delegate
;


@end
