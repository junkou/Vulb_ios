//
//  DataRegistry.h
//  BookMaker
//
//  Created by k-kin on 10/05/06.
//  Copyright 2010 iccesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
#import "AppDefine.h"
#import "AppHelper.h"
#import "UserInfo.h"

#define kFilename					@"bss.BookDataRegistry"
#define delFlgDeleted				1 //削除されたデータ
#define PAGE_TEMPLATE_DATA_COUNTS	57
#define SAVE_TEMP_IMG_DATA_COUNTS	400

@interface DataRegistry : NSObject { 
	sqlite3    *database;
	NSLock *lockData;
}

+(DataRegistry *)getInstance;
-(void)LockDatas;
-(void)unLockDatas;
- (NSString *)dataFilePath;
-(void)reSetSyncStatus;

//会員情報関連
- (int)saveUserDataWithUserInfo:(NSMutableDictionary *)userInfo;
- (int)loadUserMasterData:(NSMutableDictionary *)userInfo;
- (int)updateSheetIdAndCategoryId:(NSMutableDictionary *)inUserData ;
- (void)changeUserMstDataToDicon:(sqlite3_stmt  *)statement outData:(NSMutableDictionary *)outData;
- (void)setUserMstDataToStatement:(sqlite3_stmt  *)stmt userInfo:(NSMutableDictionary *)userInfo;
- (int)deleteUserMasterData;

//Cardデータ関連
- (int)selectCardData:(NSMutableArray *)outDataArray;
- (int)saveCardData:(NSMutableDictionary *)inCardData;
- (int)updateResourceId:(NSMutableDictionary *)inCardData ;
- (int)deleteCardData:(NSString *)inBegining;
- (int)deleteAllCardData;
- (void)changeCardDataToDicon:(sqlite3_stmt  *)statement cardData:(NSMutableDictionary *)dataDicon;
- (void)setBookCardDataToStatement:(sqlite3_stmt  *)stmt cardData:(NSMutableDictionary *)inData;

//画像関連
-(int)loadImageResourceData:(NSMutableArray *)dataResult;
-(void)saveImageResourceData:(NSMutableDictionary *)dataDicon; 
-(void)deleteImageResourceData:(NSString *)fileName;
-(void)deleteAllImageResourceData;
-(void)changeImageResourceDataToDicon:(sqlite3_stmt  *)statement result:(NSMutableDictionary *)dataDicon;


@end


