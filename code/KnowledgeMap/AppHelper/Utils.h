//
//  Utils.h
//  KnowledgeMap
//
//  Created by KouJun on 4/12/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CreateView(class,x,y,w,h) [[class alloc] initWithFrame:CGRectMake(x,y,w,h)]
#define CreateViewWithRect(class,rect) [[class alloc] initWithFrame:rect]

@interface Utils : NSObject
+(BOOL)isIOS5orLater;
+(BOOL)isIOS6orLater;
+(BOOL)isIOS7orLater;
+(BOOL)checkWebServerStatus;
+(BOOL)checkWiFiServerStatus;
+(BOOL)deviceIs1136H;
+(NSString *)getSystemLangCode;

+ (NSString *)MultiLangStringInfomation:(NSString *)key;

+(UIImage*)resizeImage:(UIImage*)image Size:(CGSize) size;
+(UIImage*)trimImageToCenterPos:(UIImage*)image trimSize:(CGSize)trimSize ;

+ (NSString *) getNowTimeMill;
+ (NSString *) getYYYYMM;
+ (NSString *) getNowTime;
+ (NSString *) getNowTimeWithDate:(NSDate *)inDate;
+ (NSTimeInterval) getNowTimeInterval;
+ (NSTimeInterval) getNowTimeIntervalWithDate:(NSDate *)inDate;
+ (NSString *)changeToDateString:(NSTimeInterval)dateTimeInterval;
+ (NSString *) getDataCreateTime;
+ (NSString *)getShowTime:(NSString*)time;

+(CGFloat)getHtmlHeight:(NSString *)htmlText inWidth:(CGFloat)width;
@end
