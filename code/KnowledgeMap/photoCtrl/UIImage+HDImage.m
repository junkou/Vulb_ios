//
//  UIImageHD.m
//  AssetsTest1
//
//  Created by k-kin on 11/09/15.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import "UIImage+HDImage.h"


@implementation UIImage (HDImage)

- (UIImage *)imageByCompositeWithImage {
	NSString* model = [[UIDevice currentDevice] model];
	BOOL isPad = [model rangeOfString:@"iPad"].location != NSNotFound;
	
    CGFloat sizeWith = PHOTO_RECT_HD_WIDTH ;
	
    //CGSize size = self.size;
    //NSLog(@"::size: %@", NSStringFromCGSize(size));
	if (self.size.width >= sizeWith) {
        //画像の縦の長さが横の長さより、大きい場合、縦を基準に縮小する、　そうでない場合、横を基準に縮小する
        UIImage *newImage = nil;
        if (!isPad && [UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
            newImage = [UIImage imageWithCGImage:self.CGImage scale:2.0 orientation:self.imageOrientation];
        }else {
            newImage = [Utils resizeImage:self Size:CGSizeMake(self.size.width/2, self.size.height/2)];
            //newImage = [UIImage imageWithCGImage:self.CGImage scale:1.0 orientation:self.imageOrientation];
        }
        return newImage;
    }else{
        return self;
    }
}


- (UIImage *)imageByCompositeWithContentsOfFile:(NSString *)path {
	NSString* model = [[UIDevice currentDevice] model];
	BOOL isPad = [model rangeOfString:@"iPad"].location != NSNotFound;
	
	if (!isPad && [UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
		NSString *path2x = [[NSBundle mainBundle] pathForResource: [path stringByDeletingPathExtension] ofType: [path pathExtension]];		
		return [UIImage imageWithContentsOfFile:path2x];
	}else {
		return [UIImage imageWithContentsOfFile:path];
	}

}


@end
