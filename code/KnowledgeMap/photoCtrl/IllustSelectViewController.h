//
//  IllustSelectViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/07/11.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDefine.h"
#import "AppHelper.h"
#import "UploadToWebServerSYNController.h"

@interface IllustSelectViewController : BaseTableViewController{
    id delegate;

    int     cellCount;
    CGFloat thumbnailSize;

	NSMutableArray			*photos;

}

@property (nonatomic, assign) id delegate;

@property int     cellCount;
@property CGFloat thumbnailSize;

@property (nonatomic, strong) NSMutableArray			*photos;

@end
