//
//  BssAssetsController.h
//  BukurouSS
//
//  Created by k-kin on 11/11/14.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/ALAssetsGroup.h>
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>
#import "UIImage+HDImage.h"
#import "AppDefine.h"
#import "DatabaseTableNmaes.h"
#import "AppHelper.h"

@interface BssAssetsController : NSObject {

}

+(BssAssetsController *)getInstance;
+(NSArray *)getALLPhotoImageKeys;
+(id)getPagePhotoAsset:(NSString*)inKey ;
+(void)setPagePhotoAsset:(id)data key:(NSString*)inKey ;
+(void)delPagePhotoAsset:(NSString*)inKey ;

+(id)getEditPageChangePhoto ;
+(NSArray *)getEditPageArray;
+(CLLocationCoordinate2D)getEditPhotoGPSinfo;
+(void)setEdetPageChangePhotoAsset:(id)data ;
+(CLLocationCoordinate2D)changeEditPhotoGPSinfo;
+(void)deleteEdetPageChangePhotoAsset ;

+ (UIImage*)getPhotoImage:(NSString*)index;
+ (id)getPhotoImageData:(NSString *)index ;
+ (id)getPhotoImageInfo:(NSString *)index ;
+ (id)getPhotoImageFileName:(NSString*)index;
+ (id)getPhotoImageExt:(id)photoInfo ;
+ (id)getResizePhotoImageExt:(NSData *)nsData ;

+ (CLLocationCoordinate2D)setPhotoGPSData:(NSString *)index ;
+ (CLLocationCoordinate2D)setPhotoGPSDataExt:(NSDictionary *)metaData dataKey:(NSString *)index;
+ (CLLocationCoordinate2D)getPhotoGPSData:(NSString *)index ;
+ (void)deletePhotoGPSData:(NSString *)index ;

+(CGSize)getPhotoReSize:(CGSize)inSize;
+(UIImage*)resizeToLowImage:(UIImage*)image Size:(CGSize)size;

+ (void)clearDatas;

@end
