#import "PhotoSelectViewController.h"

@implementation PhotoSelectViewController

@synthesize delegate;
@synthesize addConfirmButtonFalg;
@synthesize allPageCount;
@synthesize photos;
@synthesize group;
@synthesize selectedPhotoDataIndexs;
@synthesize selectedPhotoMetaData;
@synthesize albumId;
@synthesize searchDataStartIndex;
@synthesize allRowCount;
@synthesize cellCount;
@synthesize thumbnailSize;
@synthesize initScrollFlag;

int pageNumber;
NSMutableArray *checkBocks ;

#pragma mark Rotation support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (id)initWithConfirmButtonFlag:(BOOL)inFlag group:(id)inGroup allPageCount:(int)allpageCnt{
    self.initScrollFlag = 1;
    self.cellCount = photoCountInCell;
    self.thumbnailSize = PHOT_LIBARY_THUMBNAIL_SIZE;
    self.addConfirmButtonFalg = inFlag;
    self.allPageCount = allpageCnt;
	self.group = inGroup;
	pageNumber = 0;
    self.searchDataStartIndex = 0;
    self.allRowCount = 0;
	self.photos = [[NSMutableArray alloc] init];
	checkBocks = [[NSMutableArray alloc] init];
	self.selectedPhotoDataIndexs = [[NSMutableArray alloc] init];
	self.selectedPhotoMetaData = [[NSMutableArray alloc] init];
	
        
	[self searchPhotos];
	
	return self;
}


#pragma mark View lifecycle
- (void)viewDidLoad {
	[super viewDidLoad];
    
		
    [self.navigationItem setTitle:[NSString stringWithFormat:@"%d/%d", self.allPageCount,MAX_UPLOAD_PHTO_COUNT] ];

    //add NavicationBar RightButton
    [AppHelper setNavigationBarRightSaveButton:self.navigationItem controller:self];


}

- (void)searchPhotos {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
	// フォトライブラリから取得した写真をphotosに追加。
	void (^assetBlock)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
		if(result != nil) {
			if(![photos containsObject:result]) {
				// ALAssetのタイプが写真を追加
				if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
					[self.photos addObject:result];					
				}
			}
		}else {
			//NSLog(@"photos count:%d",[photos count]);
			[self ceateShowPhotoMetaData];			
		}		
	};
	
	// フォトライブラリへアクセスし、引数のブロックを実行する。 
	[(ALAssetsGroup *)group enumerateAssetsUsingBlock:assetBlock];	
	
	//[self ceateShowPhotoMetaData];
	
	[pool release];
}

-(void)ceateShowPhotoMetaData{
//	[Utils endDataLoadingAnimation];
	//NSLog(@"---cnt1+cnt2-1:%d",cnt1+cnt2-1);
	[self.tableView reloadData];	
	[NSThread sleepForTimeInterval:0.1];
    
    if ([self.photos count]==0 || self.initScrollFlag==0) {
        return;
    }
    
	int cnt1 = [self.photos count] / self.cellCount;
	int cnt2 = 0;
	if ([self.photos count] % self.cellCount > 0) {
		cnt2 = 1;
	};
	NSIndexPath* indexPath = [NSIndexPath indexPathForRow:cnt1+cnt2-1 inSection:0];
	[self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];

}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    
	
}

// テーブル指定行の画像データを取得
- (void)getPhotoImageThumnail:(NSUInteger)index imageView:(UIImageView *)imageView{
	//NSLog(@"index:%d",index);
	ALAsset *aseet = (ALAsset *)[self.photos objectAtIndex:index];
	imageView.image = [UIImage imageWithCGImage:[aseet thumbnail]];
}


#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {	
	int cnt1 = [self.photos count] / self.cellCount;
	int cnt2 = 0;
	if ([self.photos count] % self.cellCount > 0) {
		cnt2 = 1;
	};	
	//NSLog(@"cnt1+cnt2:%d",cnt1+cnt2);

	return cnt1+cnt2;
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//	NSLog(@"[indexPath row]:%d",[indexPath row]);
	NSString *SimpleTableIdentifier = [NSString stringWithFormat:@"Cell%d", [indexPath row]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SimpleTableIdentifier];
    if (cell != nil){
		return cell;
	}
	
	cell = [[[UITableViewCell alloc] initWithStyle:(UITableViewCellStyle)UITableViewCellSelectionStyleNone reuseIdentifier:SimpleTableIdentifier] autorelease];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	CGSize re_size;
	re_size.width = self.thumbnailSize;
	re_size.height = self.thumbnailSize;
	
	//UIImage		*image = nil;	
	UIImageView *imageView = nil;
	UIButton	*c_checkBoxButton;
	UIImageView *c_checkBox = nil;

	for (int j=0; j<self.cellCount; j++) {
		if ([indexPath row]*self.cellCount+j >= [self.photos count]) {
			break;
		}
		CGFloat x = j*self.thumbnailSize ;
		imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x+inCellImgOffsetX, inCellImgOffsetY, self.thumbnailSize-inCellImgOffsetX*2, self.thumbnailSize-inCellImgOffsetY*2)];
		[cell.contentView addSubview:imageView];
		[imageView release];
		[self getPhotoImageThumnail:[indexPath row]*self.cellCount+j imageView:imageView];	
		
		c_checkBoxButton = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, self.thumbnailSize, self.thumbnailSize)];
		c_checkBoxButton.tag = [indexPath row]*self.cellCount+j;
		c_checkBoxButton.backgroundColor = [UIColor clearColor];
		[c_checkBoxButton addTarget:self action:@selector(clickedCheckBox:) forControlEvents:UIControlEventTouchUpInside];
		[cell.contentView addSubview:c_checkBoxButton];
		[c_checkBoxButton release];
        
        UIView *playButton = [cell.contentView viewWithTag:MEDIA_PLAY_BUTTON_TAG];
        if (playButton != nil) {
            [cell.contentView bringSubviewToFront:playButton];
        }
        UIView *playButton2 = [cell.contentView viewWithTag:MEDIA_PLAY_BUTTON_TAG_2];
        if (playButton2 != nil) {
            [cell.contentView bringSubviewToFront:playButton2];
        }
		
		//NSLog(@"i*4+j:%d",[imgIdx intValue]);
		c_checkBox = [[UIImageView alloc] initWithFrame:CGRectMake(-4, self.thumbnailSize - 33, 46, 35)];
		c_checkBox.tag = CHECK_TAG;
		c_checkBox.hidden = YES;
		[c_checkBox setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"mainMenuCount1.png"]]];
		[c_checkBoxButton addSubview:c_checkBox];
		[c_checkBox release];
		
		UILabel *pageNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(-4, self.thumbnailSize - 37, 46, 35)];
		pageNumberLabel.backgroundColor = [UIColor clearColor];
		pageNumberLabel.tag = CHECK_NUMBER_TAG;
		pageNumberLabel.text = @"";
		pageNumberLabel.font = [UIFont  boldSystemFontOfSize:INIT_FONT_SIZE]  ;
		pageNumberLabel.textColor = [UIColor whiteColor] ;
		pageNumberLabel.textAlignment = NSTextAlignmentCenter;
		[c_checkBoxButton addSubview:pageNumberLabel];
		[pageNumberLabel release];			
		
		
	}
	
    self.allRowCount ++;
    
	return cell;
}

#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {		
//	NSLog(@"===============indexPath.row:%d", indexPath.row);	
//[tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];	
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
	return self.thumbnailSize;	
}

#pragma mark PhotoViewControllerDelegate Methods
- (void)photoSelectConfirm:(NSMutableArray *)dataList{

	[delegate photoSelectConfirm:dataList];
}

- (void)photoSelectCancel {

	[delegate photoSelectCancel];
}


#pragma mark Gyoumu Methods
-(void)clickedCheckBox:(id) inSender{
	UIButton	*button = (UIButton *)inSender;
	UIImageView	*checkView = (UIImageView *)[button viewWithTag:CHECK_TAG];
	UILabel		*pageNumberLabel = (UILabel *)[button viewWithTag:CHECK_NUMBER_TAG];
	NSNumber	*value = [NSNumber numberWithInt:button.tag];
	//NSLog(@"value:%d",[value intValue]);
	for (int i=0; i<[self.selectedPhotoDataIndexs count]; i++) {
		NSNumber *oldValue = [self.selectedPhotoDataIndexs objectAtIndex:i];
		if ([oldValue intValue] == button.tag) {
			[self.selectedPhotoDataIndexs removeObjectAtIndex:i];
			break;
		}
	}
	
	if (checkView.hidden) {
        if (self.allPageCount+pageNumber >= MAX_UPLOAD_PHTO_COUNT) {
            UIAlertView *alert = [[UIAlertView alloc]  
                                  initWithTitle:MultiLangString(@"Information")  
                                  message:[Utils MultiLangStringInfomation:@"I0029"]
                                  delegate:nil 
                                  cancelButtonTitle:MultiLangString(@"Yes") 
                                  otherButtonTitles: nil];  
            [alert show];  
            [alert release]; 

            return;
        }
		checkView.hidden = NO;
		pageNumberLabel.hidden = NO;
		[checkBocks addObject:button];
		[self.selectedPhotoDataIndexs insertObject:value atIndex:pageNumber];
		pageNumber ++;
		if (pageNumber < 10) {
			checkView.image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"mainMenuCount1.png"]];
		}else {
			checkView.image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"mainMenuCount2.png"]];
		}
		pageNumberLabel.text = [NSString stringWithFormat:@"%d", pageNumber];
	}else {
		checkView.hidden = YES;
		pageNumberLabel.hidden = YES;
		pageNumber --;
		[self setPageNumber:[pageNumberLabel.text intValue]];		
	}	
	
    [self.navigationItem setTitle:[NSString stringWithFormat:@"%d/%d",self.allPageCount+pageNumber,MAX_UPLOAD_PHTO_COUNT] ];

	
}

-(void)setPageNumber:(int)pageNum{
	for (int i=0; i< [checkBocks count]; i++) {
		UIButton	*button = [checkBocks objectAtIndex:i];
		UIImageView	*checkView = (UIImageView *)[button viewWithTag:CHECK_TAG];
		UILabel		*pageNumberLabel = (UILabel *)[button viewWithTag:CHECK_NUMBER_TAG];
		int myPageNum = [pageNumberLabel.text intValue];
		if (myPageNum > pageNum) {
			myPageNum--;
			if (myPageNum < 10) {
				checkView.image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"mainMenuCount1.png"]];
			}else {
				checkView.image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"mainMenuCount2.png"]];
			}
			pageNumberLabel.text = [NSString stringWithFormat:@"%d", myPageNum];
		} 
	}
}


#pragma mark - NavicationBar SaveButton Call Method
-(void)clickSaveButton:(id)sender {
	if ([self.selectedPhotoDataIndexs count] == 0) {
		UIAlertView *alert = [[UIAlertView alloc]  
							  initWithTitle:MultiLangString(@"Information")    
							  message:[Utils MultiLangStringInfomation:@"I0003"]  
							  delegate:nil 
							  cancelButtonTitle:MultiLangString(@"Yes") 
							  otherButtonTitles: nil];  
		[alert show];  
		[alert release];  		
		
		return;		
	}
    
    [self saveToPhotoAlbumFinal];
	
}

-(void)saveToPhotoAlbumFinal {    
	for (int i=0; i < [self.selectedPhotoDataIndexs count] ; i++) {
		NSNumber *value = [self.selectedPhotoDataIndexs objectAtIndex:i];
        id photoInfo = [self.photos objectAtIndex:[value intValue]];
		NSString *uKey = [AppHelper getRanDomCode];
        
		[BssAssetsController setPagePhotoAsset:photoInfo key:uKey];
        
        NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
        [info setObject:IMAGE_RESOURCE_TYPE_LIBRARY forKey:IMAGE_RESOURCE_TYPE_KEY];
        [info setObject:uKey forKey:ASSETS_DATA_KEY];
        [info setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId forKey:sheetId_Key];
        [info setObject:[BusinessDataCtrlManager getUserInfo].lastCategoryId forKey:categoryId_Key];
      
        [self.selectedPhotoMetaData addObject:info];
        [info release];
	}
    
	[self photoSelectConfirm:self.selectedPhotoMetaData] ;

}


-(NSString *)dateFormatChange:(NSString *)inData{
	if (inData == nil) {
		return @"";
	}
	NSRange range = [ inData rangeOfString:@" " ];
	NSString *subData = [inData substringToIndex :range.location ];
	subData = [ subData stringByReplacingOccurrencesOfString:@":" withString:@"/" options:0 range:NSMakeRange(0, [subData length])];	
	//NSLog(subData);
	
	return subData;
}

- (NSString *) dateFormatChangeStr:(NSDate *)inData{	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	NSLocale *local = [[NSLocale alloc] initWithLocaleIdentifier:@"US"];
	[formatter setLocale:local];
	[local release];
	[formatter setDateFormat:@"YYYY"];	
	NSString *now_dateStr = [formatter stringFromDate:inData];
	[formatter release];
	return now_dateStr;
}

#pragma mark Memory management
-(void)terminateObjects{
	[checkBocks removeAllObjects];
	[checkBocks release];
	checkBocks = nil;
	
	self.group  = nil;

	[self.photos removeAllObjects];
	[self.photos release];
	self.photos = nil;
	
	[self.selectedPhotoDataIndexs removeAllObjects];
	[self.selectedPhotoDataIndexs release];
	self.selectedPhotoDataIndexs = nil;
	
	[self.selectedPhotoMetaData removeAllObjects];
	[self.selectedPhotoMetaData release];
	self.selectedPhotoMetaData = nil;
	
}

- (void)dealloc {
//	NSLog(@"---PhotoSelectViewController -- dealloc ----"); 
	[self terminateObjects];
	
	self.delegate = nil;
    [super dealloc];

}

@end
