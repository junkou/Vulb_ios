//
//  UIImageHD.h
//  AssetsTest1
//
//  Created by k-kin on 11/09/15.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppHelper.h"

@interface UIImage (HDImage)


- (UIImage *)imageByCompositeWithContentsOfFile:(NSString *)path ;

- (UIImage *)imageByCompositeWithImage ;


@end
