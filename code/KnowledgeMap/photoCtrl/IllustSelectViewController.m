//
//  IllustSelectViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/07/11.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "IllustSelectViewController.h"

@interface IllustSelectViewController ()

@end

@implementation IllustSelectViewController
@synthesize delegate;
@synthesize cellCount;
@synthesize photos;
@synthesize thumbnailSize;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.cellCount = 1;
        self.thumbnailSize = 250;
        self.photos = [[NSMutableArray alloc] init];
        [self.photos addObject:@"01Wonderful.png"];
        [self.photos addObject:@"02Idea.png"];
        [self.photos addObject:@"03make.png"];
        [self.photos addObject:@"04break.png"];
        [self.photos addObject:@"05Thinking.png"];
        [self.photos addObject:@"06Confusion.png"];
        [self.photos addObject:@"07wants.png"];
        [self.photos addObject:@"08Not_needed.png"];
        [self.photos addObject:@"09Hard.png"];
        [self.photos addObject:@"10Up.png"];
        [self.photos addObject:@"11Down.png"];
        [self.photos addObject:@"12Think1.png"];
        [self.photos addObject:@"13Hard2.png"];
        [self.photos addObject:@"14goodidea.png"];
        [self.photos addObject:@"15F_LetsStart.png"];
        [self.photos addObject:@"16F_Good.png"];
        [self.photos addObject:@"17F_GREAT.png"];
        [self.photos addObject:@"18F_Yeah.png"];
        [self.photos addObject:@"19Question.png"];
        [self.photos addObject:@"20F_hmm.png"];
        [self.photos addObject:@"21F_difficult.png"];
        [self.photos addObject:@"22F_Oh.png"];
        [self.photos addObject:@"23F_Cry.png"];
        [self.photos addObject:@"24F_hi01.png"];
        [self.photos addObject:@"25Discovery.png"];
        [self.photos addObject:@"26PC.png"];
        [self.photos addObject:@"27Telephone.png"];
        [self.photos addObject:@"28F_LetsDrink.png"];
        [self.photos addObject:@"29F_LetsDrink01.png"];
        [self.photos addObject:@"30F_LetsDrink02.png"];
        [self.photos addObject:@"31F_LetsDrink03.png"];
        [self.photos addObject:@"32F_LetsDrink04.png"];
 
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [AppHelper setImageToNavigationBar:self.navigationController.navigationBar imageName:[AppHelper getResourceIconPath:@"header.png"]];
    
    UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, TOOL_BAR_HIGHT)] autorelease];
    self.navigationItem.titleView = view;
    
    UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont  boldSystemFontOfSize:24];
    label.text =  NSLocalizedString(@"Illust",@"Illust");
    [view addSubview:label];
    [label release];
    
	//キャンセルボタンを生成
    [AppHelper setNavigationBarLeftCancelButton:self.navigationItem controller:self];

}

-(void)clickCancelButton:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
	return [self.photos count];
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
	return self.thumbnailSize;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *CellIdentifier = @"Cell";
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d",[indexPath row]] ;
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    

    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        // Configure the cell...
        NSString *fileName = [AppHelper getResourceIllustPath:[self.photos objectAtIndex:[indexPath row]]];
        CGRect rect = cell.contentView.frame;
        rect.size.height = thumbnailSize;
        cell.contentView.frame = rect;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((cell.contentView.frame.size.width-self.thumbnailSize)/2, 0, self.thumbnailSize, self.thumbnailSize)];
        imageView.image = [UIImage imageNamed:fileName];
        [cell.contentView addSubview:imageView];
        
        
    }
	
	return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"dataSize:%d; imgUrl:%@",[pngData length], imgUrl);
    NSDate* now_date = [NSDate date];
    NSNumber *nowTime = [NSNumber numberWithInt:[Utils getNowTimeIntervalWithDate:now_date]];
    NSString *createTime = [Utils getNowTimeWithDate:now_date];
    NSString *fileName = [NSString stringWithFormat:@"%@.%@",createTime,IMG_FILE_EXT_JPG];

    NSMutableDictionary *imageDataDicn = [[NSMutableDictionary alloc] initWithCapacity:1];
    [imageDataDicn setObject:fileName forKey:fileName_key];
    [imageDataDicn setObject:DB_RESOURCE_TYPE_IllUSTRATION forKey:resourceType_key];
    [imageDataDicn setObject:[NSString stringWithFormat:@"%d",[indexPath row]+1] forKey:illustrationId_key];
    [imageDataDicn setObject:[self.photos objectAtIndex:indexPath.row] forKey:@"image"];
    [imageDataDicn setObject:@"" forKey:longitute_key];
    [imageDataDicn setObject:@"" forKey:latitute_key];
    [imageDataDicn setObject:[NSNumber numberWithInt:0] forKey:zoom_key];
    [imageDataDicn setObject:nowTime forKey:TIME];
    [imageDataDicn setObject:@"" forKey:urlAddress_key];
    [imageDataDicn setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId forKey:sheetId_Key];
    
    
    [[DataRegistry getInstance] saveImageResourceData:imageDataDicn];
    
    //add by kj
    [self.delegate performSelector:@selector(illustSelected:) withObject:imageDataDicn];
    
    [imageDataDicn release];
    
//    //Cardデータも作成し、DBに保存する。
//    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
//    [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId forKey:sheetId_Key];
//    [param setObject:[BusinessDataCtrlManager getUserInfo].lastCategoryId forKey:categoryId_Key];
//    
//    [AppHelper careateCardDataToSave:param contents:CONTENTS_META_PHOTO sync_status:SYNC_STATUS_OFF option:fileName];
//
//    
//    
//    //データをサーバーへのアップ処理を起動する
//    UploadToWebServerSYNController *uploadToWebServerSYNController = [[UploadToWebServerSYNController getUploadServerInstance] initData:nil];
//    [uploadToWebServerSYNController performSelectorOnMainThread:@selector(startUpload) withObject:nil waitUntilDone:NO];

	[tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


#pragma mark Gyoumu Methods
-(void)clickedCheckBox:(id) inSender{
	UIButton	*button = (UIButton *)inSender;
	NSNumber	*value = [NSNumber numberWithInt:button.tag];
	NSLog(@"value:%d",[value intValue]);

    
	
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
