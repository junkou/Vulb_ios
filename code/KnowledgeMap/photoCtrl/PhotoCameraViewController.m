//
//  PhotoCameraViewController.m
//  BukurouSS
//
//  Created by 柳 英花 on 12/03/07.
//  Copyright (c) 2012 iccesoft. All rights reserved.
//

#import "PhotoCameraViewController.h"

@implementation PhotoCameraViewController

@synthesize delegate;
@synthesize allPageCount;
@synthesize myPicker;
@synthesize dataList;

#pragma mark - View lifecycle
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad{
    [super viewDidLoad];
    
    /*
    self.dataList = [[NSMutableArray alloc] init];
    self.myPicker = [[UIImagePickerController alloc] init];
    self.myPicker.delegate = self;	
    self.myPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self.view addSubview:self.myPicker.view];
     */
 
}



- (void) startUIImagePickerController {
    self.dataList = [[NSMutableArray alloc] init];
    self.myPicker = [[UIImagePickerController alloc] init];
    self.myPicker.delegate = self;
    
   if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        self.myPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
   } else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
       self.myPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
   } 
    
    [self.delegate presentViewController:self.myPicker animated:YES completion:nil];

}



#pragma mark -
#pragma mark imagePickerController & delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {	
	//NSLog(@"---didFinishPickingMediaWithInfo---");
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];		
    NSData *retValue = [[[NSData alloc] initWithData:UIImageJPEGRepresentation([image imageByCompositeWithImage] , 1)] autorelease] ;
    NSDictionary *metadata = [info objectForKey:UIImagePickerControllerMediaMetadata];
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];;
    NSURL *assetURL = [NSURL URLWithString:[info objectForKey:UIImagePickerControllerReferenceURL]] ;
    __block NSString *fileName = @"";
    [library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
        fileName = asset.defaultRepresentation.filename;
    } failureBlock:nil];
    // カメラの場合、写真を写真ライブラリに保存する
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        [library writeImageToSavedPhotosAlbum:image.CGImage
                                     metadata:metadata
                              completionBlock:^(NSURL *url, NSError *error) {
                              }];
    }

    //**** CameraTypeとして設定する
    NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
    [item setObject:IMAGE_RESOURCE_TYPE_CAMERA_IMAGE forKey:IMAGE_RESOURCE_TYPE_KEY];
    [item setObject:retValue forKey:PHOTO_IMAGE_DATA];
    [item setObject:fileName forKey:PHOTO_FILE_NAME];
    NSString *uKey = [AppHelper getRanDomCode];    
    [BssAssetsController setPagePhotoAsset:item key:uKey];    
    [item release];
    
    NSMutableDictionary *assetsInfo = [[NSMutableDictionary alloc] init];
    [assetsInfo setObject:IMAGE_RESOURCE_TYPE_LIBRARY forKey:IMAGE_RESOURCE_TYPE_KEY];
    [assetsInfo setObject:uKey forKey:ASSETS_DATA_KEY];
    [assetsInfo setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId forKey:sheetId_Key];
    [assetsInfo setObject:[BusinessDataCtrlManager getUserInfo].lastCategoryId forKey:categoryId_Key];
    [self.dataList addObject:assetsInfo];
    [assetsInfo release];

    
    [picker dismissViewControllerAnimated:YES completion:nil];

    [self.delegate photoSelectConfirm:self.dataList];
	
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController*)picker {  	
    //キャンセル時の処理  
	//NSLog(@"---imagePickerControllerDidCancel---");
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [self.delegate photoSelectCancel];

}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark Memory management
- (void)dealloc {
    
    NSLog(@"---PhotoCameraViewController dealloc---");
    [self.dataList release];
    self.delegate = nil;
    self.myPicker.delegate = nil;
//    [self.myPicker.view removeFromSuperview];
    [self.myPicker release];
    
	[super dealloc];
}





/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (void)viewDidUnload{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
