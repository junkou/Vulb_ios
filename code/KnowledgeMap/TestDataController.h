//
//  TestDataController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/02.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppHelper.h"

@interface TestDataController : NSObject{
    
}

+ (void)createSheetListTestData:(NSMutableArray *)dataList;
+ (void)createCategoryListTestData:(NSMutableDictionary *)inputData;

@end
