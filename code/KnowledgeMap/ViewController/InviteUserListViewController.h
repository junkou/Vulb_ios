//
//  InviteUserListViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/20.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppHelper.h"
#import "AppApiDelegate.h"
#import "CreateSheetViewController.h"

@interface InviteUserListViewController : BaseViewController <UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate>{
    id delegate;
    
    int listDataType;
    
    UIButton    *rightButton;
    UIButton    *sendMailButton;
    
    UISegmentedControl *memberListTypeSeg;
    UITableView *memberListTableView;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) BOOL shareMode;

@property (nonatomic, strong) NSMutableArray *historyUserDataList;
@property (nonatomic, strong) NSMutableArray *fbFreindsDataList;
@property (nonatomic, strong) NSMutableArray *invitedUserList;
@end
