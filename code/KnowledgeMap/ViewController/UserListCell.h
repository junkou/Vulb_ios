//
//  UserListCell.h
//  KnowledgeMap
//
//  Created by KouJun on 4/3/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UserListCell : UITableViewCell
{
    UIImageView *userLogo;
    UILabel *userName;
    UILabel *notJoined;
    UIImageView *selected;
    
    UIImage *defaultLogo;
    
    
}
@property(nonatomic, strong, readonly) NSDictionary *userInfo;

- (void)setUserInfo:(NSDictionary*)userInfo selected:(bool)selected;
@end