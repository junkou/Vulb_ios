//
//  SheetListExtViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/09/27.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "SheetListExtViewController.h"
#import "CreateSheetViewController.h"
#import "AppDelegate.h"
#import "EditProfileViewController.h"

@interface SheetListExtViewController ()

@end

@implementation SheetListExtViewController

@synthesize delegate;
@synthesize tableView;

@synthesize currentSheetId;
@synthesize categoryLists;
@synthesize sheetList;

#pragma mark - lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.sheetList = [[[NSMutableArray alloc] init] autorelease];
        self.openedSectionIds =[[[NSMutableArray alloc] init] autorelease];
        self.sectionOpenedFlag = TRUE;
        
        [self initWorkData];
        
    }
    return self;
}

-(void) initWorkData{
    self.sheetList = [[[NSMutableArray alloc] initWithArray:[BusinessDataCtrlManager getSheetList]] autorelease];
    
    self.categoryLists = [[[NSMutableDictionary alloc] init] autorelease];
    NSMutableArray *list = [[[NSMutableArray alloc] initWithArray:[BusinessDataCtrlManager getCategoryList]] autorelease];
    [self.categoryLists setObject:list forKey:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    
    self.categoryStatisticsLists = [[[NSMutableDictionary alloc] init] autorelease];
    NSArray *csList = [BusinessDataCtrlManager getCategoryStatisticsList];
    [self.categoryStatisticsLists setObject:csList forKey:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    
//    self.currentSheetId = [[BusinessDataCtrlManager getUserInfo].lastSheetId intValue];
    currentGroupIndex = 0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [AppHelper getBaseTextColor];
    //Settingボタンを生成
    CGFloat buttonSize = 36;
    profileButton = [[[UIButton alloc] initWithFrame:CGRectMake(0, 8, buttonSize, buttonSize)] autorelease];
    [profileButton addTarget:self action:@selector(clickProfileButton:) forControlEvents:UIControlEventTouchUpInside];
    [profileButton setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"defaultProfImg.png"]] forState:UIControlStateNormal];
    //    [profileButton setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_setting_touch.png"]] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:profileButton] autorelease];
    
    editButton = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)] autorelease];
    [editButton setTitle:NSLocalizedString(@"Edit", @"Edit") forState:UIControlStateNormal];
    [editButton addTarget:self action:@selector(clickEditButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:editButton] autorelease];
    
    //Add Sheetボタンを生成
    addSheetButton = [[[UILayButton alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, buttonSize)] autorelease];
    addSheetButton.backgroundColor = [UIColor whiteColor];
    addSheetButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    [addSheetButton setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Sheet creation", @"Create Sheet")] forState:UIControlStateNormal];
    [addSheetButton setTitleColor:COLOR_ADD_BUTTON_TEXT forState:UIControlStateNormal];
    [addSheetButton addTarget:self action:@selector(clickAddSheetButton:) forControlEvents:UIControlEventTouchUpInside];
    [addSheetButton setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_edit_addsheet_normal.png"]] forState:UIControlStateNormal];
    [addSheetButton setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_edit_addsheet_touch.png"]] forState:UIControlStateNormal];
//    [addSheetButton setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_add_sheet_touch.png"]] forState:UIControlStateHighlighted];
    addSheetButton.layout = ImageLeftTextRight;
    [self.view addSubview:addSheetButton];
    
    //    //Add Categoryボタンを生成
    //    UIButton *addCategoryButton = [[[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-buttonSize*2-buttonSize/2, 0, buttonSize, buttonSize)] autorelease];
    //    [addCategoryButton addTarget:self action:@selector(clickAddCategoryButton:) forControlEvents:UIControlEventTouchUpInside];
    //    [addCategoryButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_add_category_nomral.png"]] forState:UIControlStateNormal];
    //    [addCategoryButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_add_category_touch.png"]] forState:UIControlStateHighlighted];
    
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)] autorelease];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
}

-(int)getSheetIndex:(NSNumber*)sheetId
{
    int sheetRowNumber = 0;
    for (int i = 0; i < [self.sheetList count]; i++)
    {
        NSDictionary *sheetInfo =[self.sheetList objectAtIndex:i];
        if ([[sheetInfo objectForKey:sheetId_Key] isEqualToNumber:sheetId])
        {
            sheetRowNumber = i;
            break;
        }
    }
    return sheetRowNumber;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (![self.openedSectionIds containsObject:@(currentGroupIndex)])
    {
        [self.openedSectionIds addObject:@(currentGroupIndex)];
    }
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    
//    int categoryRow = 0;
//    int sectionNum = [self getSheetIndex:@(self.currentSheetId)];
//    NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:sectionNum];
//    NSArray *categoryDatas = [self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key]];
//    for (int j=0; j<[categoryDatas count]; j++) {
//        NSMutableDictionary *categoryData = [categoryDatas objectAtIndex:j];
//        int categoryId = [[categoryData objectForKey:categoryId_Key] intValue];
//        if ([[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue] == categoryId ) {
//            categoryRow = j;
//            break;
//        }
//    }
//    
//    if (categoryRow == 0 && sectionNum == 0) {
//        CGPoint point =CGPointMake(0, 0);
//        [self.tableView setContentOffset:point animated:YES];
//    }else{
//        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:categoryRow inSection:sectionNum];
//        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
//        
//    }
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)getSectionHeaderHeight:(NSMutableDictionary *)infoData {
//    NSNumber *newCardCount = [infoData objectForKey:newCardCount_key];
//    NSNumber *updateCardCount = [infoData objectForKey:upCardCount_key];
//    if ( ( newCardCount != nil && [newCardCount intValue] > 0 )
//        || (updateCardCount != nil && [updateCardCount intValue] > 0) ) {
//        return SHEET_LIST_LARGE_HEIGHT;
//    }else{
//        return SHEET_LIST_HEIGHT;
//    }
    return SHEET_LIST_HEIGHT;
}


-(CGFloat)getCellHeaderHeight:(NSMutableDictionary *)infoData {
//    NSNumber *newCardCount = [infoData objectForKey:newCardCount_key];
//    NSNumber *updateCardCount = [infoData objectForKey:updateCardCount_key];
//    if ( ( newCardCount != nil && [newCardCount intValue] > 0 )
//        || (updateCardCount != nil && [updateCardCount intValue] > 0) ) {
        return SHEET_LIST_LARGE_HEIGHT;
//    }else{
//        return SHEET_LIST_HEIGHT;
//    }
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sheetList count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[BusinessDataCtrlManager sharedInstance] getSheetCreator:[[self.sheetList objectAtIndex:section] objectAtIndex:0]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	//NSLog(@"====indexPath:%d",indexPath.row);
//    NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:section];
    return 44;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.openedSectionIds containsObject:@(section)])
    {
        return [[self.sheetList objectAtIndex:section] count];
    }
    else
    {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)table viewForHeaderInSection:(NSInteger)section
{
//    NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:section];
//    NSNumber *sheetID = [sheetData objectForKey:sheetId_Key];
    CGFloat sheetHeight = 44;
    
    UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, sheetHeight)] autorelease];
    button.backgroundColor = [UIColor darkGrayColor];
    button.tag = section;
    [button addTarget:self action:@selector(clickSelectSheetButton:) forControlEvents:UIControlEventTouchUpInside];
    UIView *lineView = [[[UIView alloc] initWithFrame:CGRectMake(0, sheetHeight-0.5, SCREEN_WIDTH, 0.5)] autorelease];
    lineView.backgroundColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:0.4];
    [button addSubview:lineView];
    
    NSString *openIcon = @"icon_close_normal.png";
    if ([self.openedSectionIds containsObject:@(section)])
    {
        openIcon = @"icon_open_normal.png";
    }
    
    //open Icon
    UIImage *iconImage =[UIImage imageNamed:[AppHelper getResourceIconPath:openIcon]];
    UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, iconImage.size.width, iconImage.size.height)] autorelease];
    iconView.image = iconImage;
    [button addSubview:iconView];
    
	UILabel *sheetGroupLable = [[[UILabel alloc] initWithFrame:CGRectMake(iconView.frame.size.width, 0, SCREEN_WIDTH-iconView.frame.origin.x-40, button.bounds.size.height)] autorelease];
    sheetGroupLable.tag = SHEET_NAME_LABEL_TAG;
	sheetGroupLable.backgroundColor = [UIColor clearColor];
	sheetGroupLable.font = [UIFont  systemFontOfSize:18];
	sheetGroupLable.textColor = [UIColor whiteColor];
	sheetGroupLable.textAlignment = NSTextAlignmentLeft;
	sheetGroupLable.numberOfLines = 1;
	sheetGroupLable.text = [self tableView:table titleForHeaderInSection:section];
	[button addSubview:sheetGroupLable];

    return button;
}

-(void)clickSelectSheetButton:(UIButton *)sender
{
    NSNumber *section = [NSNumber numberWithInt:sender.tag];
    if ([self.openedSectionIds containsObject:section])
    {
        [self.openedSectionIds removeObject:section];
    }
    else
    {
        [self.openedSectionIds addObject:section];
    }
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationNone];
    
    self.clickedSectionRow = sender.tag;
    
//    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
//    [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
//    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
//    [param setObject:sheetId forKey:sheetId_Key];
//    [paramDic setObject:param forKey:PARAMS];
//    [param release];
//    
//    self.methodName = METHOD_CATEGORY_LIST;
//    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
//    
//    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
//    [paramDic release];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
    // セクションを取得する
    NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:indexPath.section];
    
    // セクション名をキーにしてそのセクションの項目をすべて取得
//    NSArray *items = [self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key]];
//    NSMutableDictionary *categoryData = [items objectAtIndex:indexPath.row];
    
	return [self getCellHeaderHeight:sheetData];
}


- (UITableViewCell *)tableView:(UITableView *)loacltableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //static NSString *CellIdentifier = @"Cell";
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d_%d",indexPath.section, [indexPath row]] ;
    
    UITableViewCell *cell = [loacltableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
     */
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        // Configure the cell...
        // セクションを取得する
        cell.backgroundColor = [UIColor clearColor];
        
//        NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:indexPath.section];
//        // セクション名をキーにしてそのセクションの項目をすべて取得
//        NSArray *items = [self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key]];
//        NSMutableDictionary *categoryData = [items objectAtIndex:indexPath.row];
//        
//        CGRect rect = cell.contentView.frame;
//        rect.size.height = [self getCellHeaderHeight:categoryData];
//        cell.contentView.frame = rect;
        
        UILabel* sheetNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-40, 26)] autorelease];
        sheetNameLable.tag = SHEET_NAME_LABEL_TAG;
        sheetNameLable.backgroundColor = [UIColor clearColor];
        sheetNameLable.font = [UIFont  systemFontOfSize:18];
        sheetNameLable.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
        sheetNameLable.textAlignment = NSTextAlignmentLeft;
        sheetNameLable.numberOfLines = 1;
        [cell.contentView addSubview:sheetNameLable];
        
        UILabel* categoryCountLabel = [[[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-50, 0, 40, 26)] autorelease];
        categoryCountLabel.tag = CATEGORY_COUNT_TAG;
        categoryCountLabel.backgroundColor = [UIColor clearColor];
        categoryCountLabel.font = [UIFont  systemFontOfSize:18];
        categoryCountLabel.textColor = [UIColor grayColor];
        categoryCountLabel.textAlignment = NSTextAlignmentRight;
        categoryCountLabel.numberOfLines = 1;
        [cell.contentView addSubview:categoryCountLabel];
        
        int x = SCREEN_WIDTH-100;
        int y = 35;
        UIImage *iconImage =[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_sheetlist_new.png"]];
        UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, y, iconImage.size.width, iconImage.size.height)] autorelease];
        iconView.image = iconImage;
        iconView.tag = SHEET_LIST_NEW_IMAGE_TAG;
        [cell.contentView addSubview:iconView];
        x += iconView.frame.size.width;
        
        UILabel *countLabel = [[[UILabel alloc] initWithFrame:CGRectMake(x, y, 40, iconView.frame.size.height)] autorelease];
        countLabel.tag = SHEET_LIST_NEW_TAG;
        countLabel.backgroundColor = [UIColor clearColor];
        countLabel.font = [UIFont  systemFontOfSize:16];
        countLabel.textColor = [UIColor grayColor];
        countLabel.textAlignment = NSTextAlignmentLeft;
        countLabel.numberOfLines = 1;
        [cell.contentView addSubview:countLabel];
        x += countLabel.frame.size.width + 5;
        
        iconImage =[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_sheetlist_update.png"]];
        iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-50, y, iconImage.size.width, iconImage.size.height)] autorelease];
        iconView.image = iconImage;
        iconView.tag = SHEET_LIST_UPDATE_IMAGE_TAG;
        [cell.contentView addSubview:iconView];
        x += iconView.frame.size.width + 5;
        
        countLabel = [[[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-30, y, 20, iconView.frame.size.height)] autorelease];
        countLabel.tag = SHEET_LIST_UPDATE_TAG;
        countLabel.backgroundColor = [UIColor clearColor];
        countLabel.font = [UIFont systemFontOfSize:16];
        countLabel.textColor = [UIColor grayColor];
        countLabel.textAlignment = NSTextAlignmentRight;
        countLabel.numberOfLines = 1;
        [cell.contentView addSubview:countLabel];
        
        x = 10;
        y = 25;
        CGFloat iconSize = USER_ICON_SIZE;
        for (int i = 0; i < 6; i++)
        {
            if (i < 5)
            {
                UIImageView *UserIconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, y, iconSize, iconSize)] autorelease];
                UserIconView.tag = SHEET_LIST_UESR_ICON_START+i;
                UserIconView.image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"defaultProfImg.png"]];
                [cell.contentView addSubview:UserIconView];
                x += iconSize;
            }
            else
            {
                UILabel *labelddd = [[[UILabel alloc] initWithFrame:CGRectMake(x, 20, 20, 20)] autorelease];
                labelddd.tag = SHEET_LIST_UESR_ICON_START+5;
                labelddd.text = @"...";
                labelddd.font = [UIFont systemFontOfSize:10];
                labelddd.textColor = [UIColor whiteColor];
                labelddd.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:labelddd];
            }
        }
    }
    
    [self setCellContent:cell IndexPath:indexPath];
    
    
    return cell;
}

-(void)setCellContent:(UITableViewCell *)cell IndexPath:(NSIndexPath *)indexPath{
    // セクションを取得する
    NSMutableDictionary *inData = [[self.sheetList objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
//    // セクション名をキーにしてそのセクションの項目をすべて取得
//    NSArray *items = [self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key]];
//    NSMutableDictionary *categoryData = [items objectAtIndex:indexPath.row];
    
//    BOOL checkedFlag = FALSE;
//    NSNumber *localCategoryId = [categoryData objectForKey:categoryId_Key];
//    if ([[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue] == [localCategoryId intValue]  ) {
//        if (self.beforeIndexPath == nil ) {
//            //最初はbeforeIndexPathがヌルのため、ここで設定する
//            self.clickedindexPath = indexPath;
//        }
//        checkedFlag = TRUE;
//        
//    }
    
//    NSMutableArray *categoryStatusList = [self.categoryStatisticsLists objectForKey:[sheetData objectForKey:sheetId_Key]];
//    NSMutableDictionary *categoryStatus = nil;
//    for (NSMutableDictionary *info in categoryStatusList) {
//        if ([localCategoryId intValue] == [[info objectForKey:categoryId_Key] intValue] ) {
//            categoryStatus = info ;
//            break;
//        }
//    }
//    
//    //NSLog(@"height:%f",cell.contentView.bounds.size.height);
//    NSString *sampleSheetFlag = [AppHelper parseWebDataToString:sheetData dataKey:sampleSheetFlg_key];
//    if ([sampleSheetFlag isEqualToString:@""]) {
//        sampleSheetFlag = @"0";
//    }
//    
//    [AppHelper setCategoryDataContents:categoryData status:categoryStatus checked:checkedFlag sampleSheetFlag:sampleSheetFlag view:cell.contentView];
    
    NSString *sampleFlag = [AppHelper parseWebDataToString:inData dataKey:sampleSheetFlg_key];
    if ([sampleFlag isEqualToString:@""])
    {
        sampleFlag = @"0";
    }
    //Set SheetName
	NSString *sheetName = [inData objectForKey:sheetName_key];
	if (sheetName == nil || [sheetName isKindOfClass:[ NSNull  class]]) {
		sheetName=@"";
	}
    NSNumber *categoryCnt = [AppHelper parseWebDataToNumber:inData dataKey:totalCategory_key];
    UILabel *sheetNameLable = (UILabel *)[cell.contentView viewWithTag:SHEET_NAME_LABEL_TAG];
	sheetNameLable.text = sheetName;
    UILabel *categoryCntLabel = (UILabel*)[cell.contentView viewWithTag:CATEGORY_COUNT_TAG];
    categoryCntLabel.text = [categoryCnt description];
    
    NSArray *memberList = [inData objectForKey:MEMBERS];
    
    NSNumber *newCardCount = [inData objectForKey:newCardCount_key];
    NSNumber *updateCardCount = [inData objectForKey:upCardCount_key];
    
    //User Icon
    UIImageView *UserIconView;
    
//    UserIconView.layer.cornerRadius = 5.0f;
//    UserIconView.clipsToBounds = YES;
    
    int i = 0;
    for (; i < [memberList count] && i < 5; i++)
    {
        NSMutableDictionary *memberInfo = [memberList objectAtIndex:i];
//        NSString *ownFlag = [memberInfo objectForKey:@"ownFlg"];
//        if ([ownFlag isEqualToString:@"1"]) {
            UserIconView = (UIImageView*)[cell.contentView viewWithTag:SHEET_LIST_UESR_ICON_START+i];
            UserIconView.hidden = NO;
            NSNumber *userId = [memberInfo objectForKey:userId_key];
            //UserIcon
            [AppHelper setUserIconWith:[userId intValue] imageView:UserIconView];
    }
    
    for (; i < 5; i++)
    {
        UserIconView = (UIImageView*)[cell.contentView viewWithTag:SHEET_LIST_UESR_ICON_START+i];
        UserIconView.hidden = YES;
    }
    
    UILabel * label = (UILabel*)[cell.contentView viewWithTag:SHEET_LIST_UESR_ICON_START+5];
    if ([memberList count] < 6)
    {
        label.hidden = YES;
    }
    else
    {
        label.hidden = NO;
    }
    
    UILabel *newLabel = (UILabel*)[cell.contentView viewWithTag:SHEET_LIST_NEW_TAG];
    UILabel *updateLabel = (UILabel*)[cell.contentView viewWithTag:SHEET_LIST_UPDATE_TAG];
    UIImageView *newImage = (UIImageView*)[cell.contentView viewWithTag:SHEET_LIST_NEW_IMAGE_TAG];
    UIImageView *updateImage = (UIImageView*)[cell.contentView viewWithTag:SHEET_LIST_UPDATE_IMAGE_TAG];
    
    newLabel.hidden = YES;
    newImage.hidden = YES;
    updateLabel.hidden = YES;
    updateImage.hidden = YES;
    
//    if ([sampleFlag isEqualToString:@"0"] )
    {
        int x = SCREEN_WIDTH - 30;
        //UpdateCount
        if ([updateCardCount intValue] > 0)
        {
            updateLabel.hidden = NO;
            updateImage.hidden = NO;
            //update Icon
            
            NSString *count = [NSString stringWithFormat:@"(%d)",[updateCardCount intValue]];
            updateLabel.text = count;
            updateLabel.frame = CGRectMake(x, updateLabel.frame.origin.y, updateLabel.frame.size.width, updateLabel.frame.size.height);
            updateImage.frame = CGRectMake(updateLabel.frame.origin.x-20, updateImage.frame.origin.y, updateImage.frame.size.width, updateImage.frame.size.height);
            x -= updateLabel.frame.size.width - 30;
        }
        
        //NewCardCount
        if ([newCardCount intValue] > 0)
        {
            newLabel.hidden = NO;
            newImage.hidden = NO;
            //new Icon
            
            NSString *count = [NSString stringWithFormat:@"(%d)",[newCardCount intValue]];
            newLabel.text = count;
            
            newLabel.frame = CGRectMake(x, newLabel.frame.origin.y, newLabel.frame.size.width, newLabel.frame.size.height);
            newImage.frame = CGRectMake(newLabel.frame.origin.x-20, newImage.frame.origin.y, newImage.frame.size.width, newImage.frame.size.height);
            
        }
    }
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)loacltableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // セクションを取得する
    NSMutableDictionary *sheetData = [[self.sheetList objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
//    // セクション名をキーにしてそのセクションの項目をすべて取得
//    NSMutableArray *items = [self.categoryLists objectForKey:[sheetData objectForKey:sheetId_Key]];
//    NSMutableDictionary *categoryData = [items objectAtIndex:indexPath.row];
    
    
    //    //すべてのカテゴリーのカード一覧を見る場合
    //    id allCategoryFlag = [categoryData objectForKey:ALL_CATEGORY_SHOW_FLAG_KEY];
    //    if (allCategoryFlag != nil) {
    //
    //        AllCardListViewController *allCardListViewController = [[[AllCardListViewController alloc] init] autorelease];
    //        allCardListViewController.delegate = self;
    //
    //        NSMutableArray *categoryList = [[[NSMutableArray alloc] initWithArray:items] autorelease];
    //
    //        //すべてのカテゴリーのカード一覧選択用データは削除しておく
    //        [categoryList removeObjectAtIndex:0];
    //        allCardListViewController.categoryList = categoryList;
    //
    //        NSMutableDictionary *categoryData = [categoryList objectAtIndex:0];
    //        allCardListViewController.currentCategoryId = [[categoryData objectForKey:categoryId_Key] intValue];
    //        allCardListViewController.title = [sheetData objectForKey:sheetName_key];
    //
    //        [self.navigationController pushViewController: allCardListViewController animated:YES];
    //
    //        [loacltableView deselectRowAtIndexPath:indexPath animated:YES];
    //
    //        return;
    //    }
    
    
    //old IndexPath
//    self.beforeIndexPath = self.clickedindexPath;
    
    
    //new IndexPath
//    self.clickedindexPath = indexPath;
    
    
    //    int currentCategoryId = [[categoryData objectForKey:categoryId_Key] intValue];
    NSNumber *sheetID = [sheetData objectForKey:sheetId_Key];
    [[BusinessDataCtrlManager sharedInstance] goSheet:sheetID];
    
//    self.currentSheetId = [[sheetData objectForKey:sheetId_Key] intValue];
//    
//    //old Cell
//    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.beforeIndexPath];
//    [self setCellContent:cell IndexPath:self.beforeIndexPath];
//    [self.tableView setNeedsDisplay];
    
    [self performSelector:@selector(back2MainView) withObject:nil];
    
    [loacltableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - WorkDataControllerDelegate Delegate
- (void)back2MainView
{
    [[[AppDelegate sharedInstance] mainViewController] dismissViewControllerAnimated:NO completion:nil];
}

-(void)createCategorySucessed{
    
    self.createCategoryFlag = TRUE;
    
    self.currentSheetId = [[BusinessDataCtrlManager getUserInfo].lastSheetId intValue];
    
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId forKey:sheetId_Key];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    self.methodName = METHOD_CATEGORY_LIST;
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
}


// Categori一覧データを受信したとき
- (void)recieveOprationData:(WebTask *)task
{
    //NSLog(@"%@",inData);
	NSMutableDictionary *recieveData = [task.response JSONValue];
    
    NSMutableDictionary *result = [recieveData objectForKey:RESULT_KEY];
    
    NSString *resultStatus = [result objectForKey:RESULT_STATUS_KEY];
    
    if (resultStatus != nil && [resultStatus isEqualToString:RESULT_STATUS_VALUE_0])
    {
        if ([self.methodName isEqualToString:METHOD_CATEGORY_LIST])
        {
            // セクション名をキーにしてそのセクションの項目をすべて取得
            NSNumber *sheetId = [[[task params] objectForKey:PARAMS] objectForKey:sheetId_Key];
            
            NSMutableArray *newList = [[[NSMutableArray alloc] init] autorelease];
            NSMutableDictionary *data = [recieveData objectForKey:RESULT_DATA_VALUE_KEY];
            //NSLog([data JSONRepresentation]);
            NSMutableArray *categoryList = [data objectForKey:UNCATEGORYIZED];
            [newList addObjectsFromArray:categoryList];
            categoryList = [data objectForKey:CATEGORYIZED0];
            [newList addObjectsFromArray:categoryList];
            categoryList = [data objectForKey:CATEGORYIZED1];
            [newList addObjectsFromArray:categoryList];
            
            [self.categoryLists setObject:newList forKey:sheetId];
            
            
            if (self.createCategoryFlag)
            {
                //新規カテゴリーを作成
                self.createCategoryFlag = FALSE;
                
                [self newCategoryID:[[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue]];
                
            }
            else
            {
//                if (![self.openedSectionIds containsObject:sheetId])
//                {
//                    [self.openedSectionIds addObject:sheetId];
//                }
                
                if ([self.categoryStatisticsLists objectForKey:sheetId] == nil) {
                    NSMutableArray *cateoryIds = [[[NSMutableArray alloc] init] autorelease];
                    NSMutableArray *categoryList = [self.categoryLists objectForKey:sheetId];
                    
                    for (NSMutableDictionary *categoryInfo in categoryList ) {
                        [cateoryIds addObject:[categoryInfo objectForKey:categoryId_Key]];
                    }
                    
                    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
                    [param setObject:cateoryIds forKey:@"categoryIds"];
                    
                    NSMutableDictionary *paramDic = [[[NSMutableDictionary alloc] init] autorelease];
                    [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
                    [paramDic setObject:param forKey:PARAMS];
                    
                    self.methodName = METHOD_CATEGORY_STATISTICS;
                    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,self.methodName];
                    
                    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
                    
                }
                else
                {
                    
                    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:[self getSheetIndex:sheetId]] withRowAnimation:UITableViewRowAnimationNone];
                }
            }
            
        }else if([self.methodName isEqualToString:METHOD_CATEGORY_STATISTICS]){
            NSMutableDictionary *data = [recieveData objectForKey:RESULT_DATA_VALUE_KEY];
            //NSLog([data JSONRepresentation]);
            NSMutableArray *categoryStatisticsList = [data objectForKey:CATEGORYIES];
            NSNumber *sheetId = [NSNumber numberWithInt:self.currentSheetId];
            [self.categoryStatisticsLists setObject:categoryStatisticsList forKey:sheetId];
            
            int sectionNum = [self getSheetIndex:sheetId];
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sectionNum] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
    else
    {
        NSString *errMsg = [result objectForKey:RESULT_ERROR_MESSGATE_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle:MultiLangString(@"Information")
							  message:errMsg
							  delegate:nil
							  cancelButtonTitle:MultiLangString(@"Yes")
							  otherButtonTitles: nil];
		[alert show];
		[alert release];
        
        [[AppDelegate sharedInstance] loginFaild];
        
    }
}

- (void)cardListSucessed
{
    //    int sectionNum = [self getCurrentSheetIndex];
    
    //    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sectionNum] withRowAnimation:UITableViewRowAnimationNone];
    //
    [self back2MainView];
}

- (void)createNewSheetOrUpdateSheetSucessed:(NSMutableDictionary *)Info
{
    [self dismissViewControllerAnimated:NO completion:nil];
    
    [self initWorkData];
    
    [[BusinessDataCtrlManager sharedInstance] requestLastCardList:self startIdx:0];
    
}
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}


#pragma mark Memory management
- (void)dealloc {
    self.delegate = nil;
    
    self.categoryLists = nil;
    
    [[DownLoadSimpleWebDataServerController getInstance] clearDelegate];
    
    
	[super dealloc];
    
	
}

- (void)logOut
{
    [[BusinessDataCtrlManager sharedInstance] logOut];
    
    [[AppDelegate sharedInstance] logOut];
}

- (void)goEditProfile
{
    EditProfileViewController *controller = [[EditProfileViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - WorkDataControllerDelegate Delegate
- (void)newSheetIDAndCategoryID:(int)sheetId categoryId:(int)categoryId{
    //最新データ反映
    NSDictionary *sheetInfo =  [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    if (sheetInfo != nil) {
        //        [self setSheetDataContents:sheetInfo delegate:self];
    }
    
    NSMutableDictionary *info =[[NSMutableDictionary alloc] init];
    [[BusinessDataCtrlManager getUserInfo] toCreateDictionaryValues:info];
    [[DataRegistry getInstance] updateSheetIdAndCategoryId:info];
    [info release];
    
    
    NSDictionary *categoryList =[BusinessDataCtrlManager getSheetIdAndCategoryList];
    NSArray *categoryDatas = [categoryList objectForKey:[sheetInfo objectForKey:sheetId_Key]];
    NSMutableDictionary *categoryInfo = nil;
    for (int j=0; j<[categoryDatas count]; j++) {
        categoryInfo = [categoryDatas objectAtIndex:j];
        int categoryId = [[categoryInfo objectForKey:categoryId_Key] intValue];
        if ([[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue] == categoryId ) {
            break;
        }
        
    }
    
    //CardDataをクリア
    
    //Card一覧を再取得する
    [[BusinessDataCtrlManager sharedInstance] requestLastCardList:self startIdx:0];
}


#pragma mark - alert Delegate
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0:
			//Yes
			;
            
            [[BusinessDataCtrlManager sharedInstance] logOut];
            
            [self.delegate logOut];
            
            [self dismissViewControllerAnimated:NO completion:nil];
            
			break;
		case 1:
			//No;
			;
            
            
			break;
	}
}

#pragma mark - clickLogout
-(void)clickLogoutButton:(id)sender{
	UIAlertView *alert = [[UIAlertView alloc]
						  initWithTitle:NSLocalizedString(@"Information",@"Information")
						  message:NSLocalizedString(@"I0001",@"I0001")
						  delegate:self
						  cancelButtonTitle:MultiLangString(@"Yes")
						  otherButtonTitles: MultiLangString(@"Cancel"),nil];
    [alert show];
    [alert release];
    
}

#pragma mark - clickAddSheetButton
-(void)clickAddSheetButton:(id)sender
{
    CreateSheetViewController *createSheetViewController = [[[CreateSheetViewController alloc] init] autorelease];
    createSheetViewController.delegate = self;
    createSheetViewController.viewTitle = @"Sheet creation";
    createSheetViewController.operationType = SHEET_OPERATION_TYPE_NEW;
    
    BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:createSheetViewController] autorelease];
    
	[self presentViewController:controller animated:YES completion:nil];
    
}

-(void)clickAddCategoryButton:(id)sender
{
    
}

-(void)clickProfileButton:(id)sender
{
    MEActionSheet *actionSheet = [[[MEActionSheet alloc] initWithTitle:nil] autorelease];
//    [actionSheet addButtonWithTitle:MultiLangString(@"Edit Profile") onTapped:^{
//        [self goEditProfile];
//    }];
    [actionSheet addButtonWithTitle:MultiLangString(@"Logout") onTapped:^{
        [self logOut];
    }];
    [actionSheet setCancelButtonWithTitle:MultiLangString(@"Cancel") onTapped:^{
        
    }];
    [actionSheet showInView:self.view];
}

-(void)clickEditButton:(id)sender
{
    [tableView setEditing:!tableView.editing];
    if (tableView.isEditing)
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            addSheetButton.frame = CGRectMake(0, self.view.bounds.size.height - addSheetButton.frame.size.height, addSheetButton.frame.size.width, addSheetButton.frame.size.height);
            
            tableView.frame = CGRectMake(0, 0, tableView.frame.size.width, self.view.frame.size.height-addSheetButton.frame.size.height);

        }];
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            addSheetButton.frame = CGRectMake(0, self.view.bounds.size.height, addSheetButton.frame.size.width, addSheetButton.frame.size.height);

            tableView.frame = CGRectMake(0, 0, tableView.frame.size.width, self.view.frame.size.height);
        }];
    }
}
@end
