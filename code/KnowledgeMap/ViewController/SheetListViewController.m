//
//  SheetListViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/01.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "SheetListViewController.h"

@interface SheetListViewController ()

@end

@implementation SheetListViewController

@synthesize delegate;

@synthesize currentSheetId;
@synthesize sheetList;
@synthesize lastIndexPath;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
 
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //ShadowLine
    [Utils addShadowLine:self.navigationItem.titleView];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.sheetList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
	return SHEET_LIST_HEIGHT;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //static NSString *CellIdentifier = @"Cell";
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d",[indexPath row]] ;

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    */
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        // Configure the cell...
        NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:[indexPath row]];
        CGRect rect = cell.contentView.frame;
        rect.size.height = SHEET_LIST_HEIGHT;
        cell.contentView.frame = rect;
//        [Utils setSheetDataContents:sheetData view:cell.contentView delegate:self];
        NSNumber *localSheetId = [sheetData objectForKey:sheetId_Key];
        if (self.currentSheetId == [localSheetId intValue]  ) {
            self.lastIndexPath = indexPath;
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }


    
    return cell;
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     
    NSMutableDictionary *data = [self.sheetList objectAtIndex:[indexPath row]];
    
    self.currentSheetId = [[data objectForKey:sheetId_Key] intValue];
    
    if (self.lastIndexPath != nil ) {
		//NSLog(@"self.lastIndexPath:%d",[self.lastIndexPath row]);
		UITableViewCell *oldCell  = [tableView cellForRowAtIndexPath:self.lastIndexPath];
        oldCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    UITableViewCell *newCell  = [tableView cellForRowAtIndexPath:indexPath];
    newCell.contentView.backgroundColor = [UIColor blueColor];
    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    
    self.lastIndexPath = indexPath;
        
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:[data objectForKey:sheetId_Key] forKey:sheetId_Key];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,METHOD_CATEGORY_LIST];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    


}

#pragma mark - WorkDataControllerDelegate Delegate
- (void)newCategoryID:(int)categoryId{
    [BusinessDataCtrlManager getUserInfo].lastSheetId = [NSNumber numberWithInt:self.currentSheetId];

    [self.delegate newSheetIDAndCategoryID:self.currentSheetId categoryId:categoryId];
}


// 処理データを受信したとき
- (void)recieveOprationData:(NSString *)inData{
    //NSLog(@"%@",inData);
	NSMutableDictionary *recieveData = [inData JSONValue];
    
    NSMutableDictionary *result = [recieveData objectForKey:RESULT_KEY];
    
    NSString *resultStatus = [result objectForKey:RESULT_STATUS_KEY];
    
    if (resultStatus != nil && [resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {

        CategoryListViewController *categoryListViewController = [[CategoryListViewController alloc] init];
        categoryListViewController.delegate = self;
        NSMutableDictionary *sheetData = [self.sheetList objectAtIndex:[self.lastIndexPath row]];
        categoryListViewController.currentCategoryId = [[BusinessDataCtrlManager getUserInfo].lastCategoryId intValue];
        NSMutableDictionary *data = [recieveData objectForKey:RESULT_DATA_VALUE_KEY];

        //NSLog([data JSONRepresentation]);
        NSMutableArray *categoryList = [data objectForKey:UNCATEGORYIZED];
        [categoryListViewController.dataList addObjectsFromArray:categoryList];
        categoryList = [data objectForKey:CATEGORYIZED0];
        [categoryListViewController.dataList addObjectsFromArray:categoryList];
        categoryList = [data objectForKey:CATEGORYIZED1];
        [categoryListViewController.dataList addObjectsFromArray:categoryList];
        
        categoryListViewController.title = [NSString stringWithFormat:@"%@(%d)", [sheetData objectForKey:sheetName_key] ,[categoryListViewController.dataList count]];

        [self.navigationController pushViewController:categoryListViewController animated:YES];
        [categoryListViewController release];

        
    }else{
        NSString *errMsg = [result objectForKey:RESULT_ERROR_MESSGATE_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle:MultiLangString(@"Information")
							  message:errMsg
							  delegate:nil
							  cancelButtonTitle:MultiLangString(@"Yes")
							  otherButtonTitles: nil];
		[alert show];
		[alert release];
        
        [self.delegate loginFaild];
        
    }
    
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}


#pragma mark Memory management
- (void)dealloc {
    self.delegate = nil;
    
    self.sheetList = nil;
    
    [self.lastIndexPath release];

    [[DownLoadSimpleWebDataServerController getInstance] clearDelegate];

    
	[super dealloc];
    
	
}



@end
