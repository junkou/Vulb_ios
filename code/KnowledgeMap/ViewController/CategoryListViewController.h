//
//  CategoryListViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/01.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "TestDataController.h"
#import "AppApiDelegate.h"


@interface CategoryListViewController : UITableViewController<AppApiDelegate>{

    id delegate;
   
    NSMutableArray		*dataList;
	NSIndexPath	*lastIndexPath;
    int currentCategoryId;

}

@property (nonatomic, assign) id delegate;

@property (nonatomic, retain) NSMutableArray		*dataList;
@property (nonatomic, retain) NSIndexPath	*lastIndexPath;
@property (nonatomic) int currentCategoryId;

@end
