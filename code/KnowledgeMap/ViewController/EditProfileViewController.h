//
//  EditProfileViewController.h
//  KnowledgeMap
//
//  Created by KouJun on 5/19/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import "AppHelper.h"

@interface EditProfileViewController : BaseViewController<UITextFieldDelegate>
{
    UIScrollView *scrollView;
    UIButton *logoBtn;
    UITextField *nameField;
    UILabel *categoryLabel;
    UITextField *categoryField;
    UILabel *genderLabel;
    UITextField *genderField;
    UILabel *birthLabel;
    UITextField *birthField;
    UILabel *careerLabel;
    UITextField *careerField;
    UILabel *addrLabel;
    UITextField *addrField;
    UILabel *xxxLabel;
    UITextField *xxxField;
    UILabel *accountLabel;
    UIButton *mailChangeBtn;
    UIButton *pwdChangeBtn;
}
@end
