//
//  CreateCardViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/21.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "CreateCardViewController.h"
#import "AppDelegate.h"

#define TEXT_VIEW_FONT [UIFont systemFontOfSize:18]
#define ContentTextKey @"title"
#define ContentImageKey @"image"

@interface CreateCardViewController ()
@property (nonatomic, strong) UIButton      *goodButton;
@property (nonatomic, strong) NSMutableDictionary		*filenameDic;

@end

@implementation CreateCardViewController

@synthesize delegate;
@synthesize dataList;
@synthesize cardInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                     name: UIKeyboardWillShowNotification object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                     name: UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [AppHelper getBaseTextColor];
    
    [AppHelper setNavigationBarLeftCancelButton:self.navigationItem controller:self];
    
    UIButton *btn = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)] autorelease];
    [btn setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickDoneButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
    
    if (self.dataList == nil)
    {
        self.dataList = [[[NSMutableArray alloc] init] autorelease];
    }
    
    if (self.cardInfo == nil)
    {
        if ([self.dataList count] == 0)
        {
            [self.dataList addObject:[NSMutableDictionary dictionaryWithObject:@"" forKey:ContentTextKey]];
        }
        
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[AppDelegate sharedInstance] setTabbarHidden:YES animated:NO];
    
    if (toolbarView == nil)
    {
        toolbarView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, TOOL_BAR_HIGHT)] autorelease];
        toolbarView.backgroundColor = [UIColor whiteColor];
        toolbarView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [self.view addSubview:toolbarView];
        
        int x = self.view.frame.size.width/2;
        static NSString* normalIcons[] = {@"icon_tool_camera_gray_normal.png",@"icon_tool_picture_gray_normal.png",@"icon_tool_illust_gray_normal.png"};
        static NSString* touchIcons[] = {@"icon_tool_camera_gray_touch.png",@"icon_tool_picture_gray_touch.png",@"icon_tool_illust_gray_touch.png"};
        for(int i = 0; i < 3; i++)
        {
            UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake(x, 0, 40, toolbarView.frame.size.height)] autorelease];
            [button addTarget:self action:@selector(clickInputButton:) forControlEvents:UIControlEventTouchUpInside];
            UIImage *backgoundImageNormal = [UIImage imageNamed:[AppHelper getResourceIconPath:normalIcons[i]]];
            [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
            UIImage *backgoubdImageHilight = [UIImage imageNamed:[AppHelper getResourceIconPath:touchIcons[i]]];
            [button setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
            button.tag = CONTENTS_MODE_CAMERA+i;
            [toolbarView addSubview:button];
            x += 50;
        }
        
        CGFloat positionY = self.view.bounds.size.height - toolbarView.frame.size.height;
        
        self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, toolbarView.frame.size.height, self.view.bounds.size.width, positionY)] autorelease];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self.view addSubview:self.tableView];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    if ([self.initInput intValue] != CONTENTS_MODE_TEXT)
    {
        [self newContent:[self.initInput intValue] animated:NO];
        self.initInput = nil;
    }
    else
    {
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)newContent:(int)mode animated:(BOOL)animate
{
    contentsMode = mode;
    switch (mode)
    {
        case CONTENTS_MODE_CAMERA:
        {
            photoCameraViewController = [[PhotoCameraViewController alloc] init] ;
            photoCameraViewController.delegate = self;
            [photoCameraViewController startUIImagePickerController];
        }
            break;
        case CONTENTS_MODE_PHOTOLIBRARY:
        {
            photoGroupViewController = [[PhotoGroupViewController alloc] init];
            photoGroupViewController.delegate = self;
            
            BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:photoGroupViewController] autorelease];
            
            [self presentViewController:controller animated:animate completion:nil];
            
            [photoGroupViewController release];
        }
            break;
        case CONTENTS_MODE_ILLUST:
        {
            illustSelectViewController = [[IllustSelectViewController alloc] init];
            illustSelectViewController.delegate = self;
            
            BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:illustSelectViewController] autorelease];
            
            [self presentViewController:controller animated:animate completion:nil];
            
            [illustSelectViewController release];
        }
            break;
        default:
            break;
    }
}

-(CGFloat)getTextHeight:(NSString*)text
{
    if ([text length] == 0)
    {
        text = @"A";
    }
    CGSize size = [text sizeWithFont:TEXT_VIEW_FONT constrainedToSize:CGSizeMake(self.tableView.frame.size.width, FLT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    return size.height + 30;
}

#pragma mark - UITextViewDelegate

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //NSLog(@"[self.dataList count]:%d",[self.dataList count]);
    return [self.dataList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//NSLog(@"====indexPath:%d",indexPath.row);
    CGSize size;
    NSDictionary *info = [self.dataList objectAtIndex:indexPath.row];
    UIImage *image = [info objectForKey:ContentImageKey];
    if (image || [info objectForKey:resourceId_key])
    {
        size.height = self.tableView.frame.size.width;
    }
    else
    {
        size.height = [self getTextHeight:[info objectForKey:ContentTextKey]];
    }
	return MAX(44, size.height);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d", indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor =[UIColor clearColor];
    
        UITextView *textFiled = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
        textFiled.scrollEnabled = NO;
        textFiled.showsHorizontalScrollIndicator = NO;
        textFiled.showsVerticalScrollIndicator = NO;
        textFiled.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        textFiled.tag = 100;
        textFiled.backgroundColor = [UIColor clearColor];
        textFiled.font = TEXT_VIEW_FONT;
        textFiled.delegate = self;
        [cell.contentView addSubview:textFiled];
        
        UIImageView *imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 0)] autorelease];
        imageView.tag = 101;
        [cell.contentView addSubview:imageView];
    }
    
    // Configure the cell...
    cell.contentView.tag = indexPath.row;
    NSDictionary *info = [self.dataList objectAtIndex:indexPath.row];
    UITextView *label = (UITextView*)[cell.contentView viewWithTag:100];
    UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag:101];
    imageView.hidden = YES;
    label.hidden = YES;
    
    if([info objectForKey:ContentImageKey])
    {
        imageView.hidden = NO;
        imageView.image = [info objectForKey:ContentImageKey];
        imageView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.width);
    }
    else if([info objectForKey:resourceId_key])
    {
        imageView.hidden = NO;
        imageView.image = [info objectForKey:ContentImageKey];
        imageView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.width);
        
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setObject:[info objectForKey:resourceId_key] forKey:resourceId_key];
        [paramDic setObject:[param JSONRepresentation] forKey:PARAMS];
        [param release];
        
        [[HttpClientUtils sharedClient] getServerDataSetToView:METHOD_RESOURCE_DOWNLOAD parameter:paramDic view:imageView delegate:nil option:[info objectForKey:resourceId_key]];
        
        [paramDic release];
    }
    else
    {
        label.hidden = NO;
        label.text = [info objectForKey:ContentTextKey];
        label.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self getTextHeight:label.text]);
    }
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    NSMutableDictionary *infoData = [self.dataList objectAtIndex:[indexPath row]];
        
    NSString *resourceType = [AppHelper parseWebDataToString:infoData dataKey:resourceType_key];
    if ([resourceType isEqualToString:DB_RESOURCE_TYPE_WEB])
    {
        NSString* html = [AppHelper parseWebDataToString:infoData dataKey:begining_key];
        
        WebServerViewController *webViewController = [[[WebServerViewController alloc] initWithNibName:@"WebServerView" bundle:nil ] autorelease];
        webViewController.delegate = self;
        webViewController.view.frame = [self.view bounds];
        [webViewController toOpenWebWithURL:html];
        
        BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:webViewController] autorelease];
        [AppHelper setImageToNavigationBar:controller.navigationBar imageName:[AppHelper getResourceIconPath:@"header.png"]];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        NSNumber *resourceId = [infoData objectForKey:resourceId_key];
        if ( resourceId != nil)
        {
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
            [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
            NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
            [param setObject:resourceId forKey:resourceId_key];
            [paramDic setObject:[param JSONRepresentation] forKey:PARAMS];
            [param release];
            
            [[HttpClientUtils sharedClient] postDataWithMethod:@"POST" methodName:METHOD_RESOURCE_DOWNLOAD parameters:paramDic option:nil isShowLoading:YES delegate:self];
            
            [paramDic release];
            
        }
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - AppApiDelegate delegate
- (void)endEditCardSucessed:(NSMutableDictionary *)Info{

//    NSMutableDictionary *cardData = [self.dataList objectAtIndex:[self.selectedIndexPath row]];
//    [cardData setObject:[Info objectForKey:begining_key] forKey:begining_key];
//
//    NSArray* indexPaths = [NSArray arrayWithObject:self.selectedIndexPath];
//    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - AppApiDelegate サーバー間とのデータやり取り成功時に呼ぶ
-(void)dataOperationSuccess:(id)responseObject option:(NSDictionary *)option{
    NSString *resourceType = [responseObject objectForKey:resourceType_key];
    if ([resourceType isEqualToString:DB_RESOURCE_TYPE_PHOTO] || [resourceType isEqualToString:DB_RESOURCE_TYPE_IllUSTRATION]) {
        CardPartsDetailInfoViewController *detailViewController = [[CardPartsDetailInfoViewController alloc] init];
        detailViewController.delegate = self;
        detailViewController.resourceInfo = responseObject;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
        [detailViewController release];
        
    }
}

#pragma mark Memory management
- (void)dealloc {
    self.delegate = nil;
    self.dataList = nil;
    self.cardInfo = nil;

    self.tableView = nil;
    self.goodButton = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object: nil];

    [[DownLoadSimpleWebDataServerController getInstance] clearDelegate];
    
	[super dealloc];
}

#pragma mark - WorkDataControllerDelegate Delegate
- (void)photoSelectConfirm:(NSMutableArray *)list{
    //バッググラウンドでImageデータを作ってサーバーに送る
    for (NSDictionary *item in list)
    {
        UIImage *image = [BssAssetsController getPhotoImage:[item objectForKey:ASSETS_DATA_KEY]];
        if (image)
        {
            NSString *fileName = [BssAssetsController getPhotoImageFileName:[item objectForKey:ASSETS_DATA_KEY]];
            if ([fileName length] == 0)
            {
                fileName = @"no name.jpg";
            }
            if([fileName hasSuffix:@".jpg"] == false && [fileName hasSuffix:@".png"] == false && [fileName hasSuffix:@".gif"] == false)
            {
                fileName = [fileName stringByAppendingString:@".jpg"];
            }
            
            [self.dataList insertObject:[NSMutableDictionary dictionaryWithObject:@"" forKey:ContentTextKey] atIndex:[self.dataList count]-1];
            NSMutableDictionary *item = [NSMutableDictionary dictionaryWithObject:image forKey:ContentImageKey];
            [item setObject:DB_RESOURCE_TYPE_PHOTO forKey:resourceType_key];
            [item setObject:fileName forKey:fileName_key];
            [self.dataList insertObject:item atIndex:[self.dataList count]-1];
        }
    }
    
    if (contentsMode == CONTENTS_MODE_CAMERA) {
        [photoCameraViewController release];
        photoCameraViewController = nil;
    }
    else if(contentsMode == CONTENTS_MODE_PHOTOLIBRARY)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    [self.tableView reloadData];
}

- (void)photoSelectCancel {
    
    if (photoCameraViewController != nil)
    {
        [photoCameraViewController release];
        photoCameraViewController = nil;
    }
}

- (void)illustSelected:(id)data
{
    UIImage *image = [UIImage imageNamed:[AppHelper getResourceIllustPath:[data objectForKey:@"image"]]];
    if (image)
    {
        [self.dataList insertObject:[NSMutableDictionary dictionaryWithObject:@"" forKey:ContentTextKey] atIndex:[self.dataList count]-1];
        NSMutableDictionary *item = [NSMutableDictionary dictionaryWithObject:image forKey:ContentImageKey];
        [item setObject:DB_RESOURCE_TYPE_IllUSTRATION forKey:resourceType_key];
        [item setObject:[data objectForKey:fileName_key] forKey:fileName_key];
        [item setObject:[data objectForKey:illustrationId_key] forKey:illustrationId_key];
        [self.dataList insertObject:item atIndex:[self.dataList count]-1];
    }
    
    [self.tableView reloadData];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    NSLog(@"textViewDidBeginEditing");
}

- (void)textViewDidChange:(UITextView *)textView
{
    [[self.dataList objectAtIndex:textView.superview.tag] setObject:textView.text forKey:ContentTextKey];
    if (fabs([self getTextHeight:textView.text] - textView.bounds.size.height) > 0.1)
    {
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    }
}

#pragma mark - KeyboardNotification
- (void)keyboardWillShow:(NSNotification*)notification
{
    CGRect keyboardFrame = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    float keyboardHeight = keyboardFrame.size.height;
    self.tableView.frame = CGRectMake(0, toolbarView.frame.size.height, self.tableView.frame.size.width, self.view.bounds.size.height-toolbarView.frame.size.height - keyboardHeight);
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    self.tableView.frame = CGRectMake(0, toolbarView.frame.size.height, self.tableView.frame.size.width, self.view.bounds.size.height-toolbarView.frame.size.height);
}
#pragma mark - clickEditTextButton
-(void)clickInputButton:(UIButton*)sender
{
    [self newContent:sender.tag animated:YES];
}

- (void)clickCancelButton:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)clickDoneButton:(id)sender
{
    if ([self.dataList count] == 1)
    {
        NSDictionary *item = [self.dataList objectAtIndex:0];
        if ([[item objectForKey:ContentTextKey] length] == 0 && [item objectForKey:ContentImageKey] == nil && [item objectForKey:resourceId_key] == nil)
        {
            return;
        }
    }
    
    NSMutableArray *output = [NSMutableArray array];
    for (NSDictionary* item in self.dataList)
    {
        if ([[item objectForKey:ContentTextKey] length] || [item objectForKey:ContentImageKey] || [item objectForKey:resourceId_key])
        {
            [output addObject:item];
        }
    }
    
    UploadToWebServerSYNController *uploader = [UploadToWebServerSYNController getUploadServerInstance];
    uploader.delegate = self;
    if (self.cardInfo)
    {
        [uploader updateCard:[self.cardInfo objectForKey:cardId_key] data:output];
    }
    else
    {
        [uploader publishCard:output];
    }
}

+(NSString*)getText:(NSString*)text
{
    if ([text hasPrefix:@"<"])
    {
        return nil;
    }
    return text;
}

+(NSMutableArray*)detailList2EditList:(NSArray*)list
{
    NSMutableArray *ret = [NSMutableArray array];
    bool lastIsText;
    for (NSDictionary *item in list)
    {
        if ([item objectForKey:resourceId_key])
        {
            [ret addObject:[NSMutableDictionary dictionaryWithObject:@"" forKey:ContentTextKey]];
            [ret addObject:[NSMutableDictionary dictionaryWithObject:[item objectForKey:resourceId_key] forKey:resourceId_key]];
            lastIsText = false;
        }
        else
        {
            NSString *text = [self getText:[item objectForKey:begining_key]];
            if ([text length])
            {
                [ret addObject:[NSMutableDictionary dictionaryWithObject:text forKey:ContentTextKey]];
                lastIsText = true;
            }
        }
    }
    if (!lastIsText)
    {
        [ret addObject:[NSMutableDictionary dictionaryWithObject:@"" forKey:ContentTextKey]];
    }
    return ret;
}
@end
