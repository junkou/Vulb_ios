//
//  AgreementViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/09/22.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppHelper.h"
#import "LoginViewController.h"
#import "CreateAccountViewController.h"

@interface AgreementViewController : BaseViewController

@property (nonatomic, strong) UIWebView *agreementView;
@property (nonatomic, strong) UIButton *checkBox;
@property (nonatomic, strong) UIImageView *checkMark;
@property (nonatomic) BOOL    agreementFlag;
@property (nonatomic, strong) UIButton *nextButton;

@property (nonatomic) int    loginType;

@end
