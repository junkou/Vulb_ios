//
//  InviteUserListViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/20.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "InviteUserListViewController.h"
#import "AppDelegate.h"
#import "UIButton+Layout.h"
#import "UserListCell.h"

@interface InviteUserListViewController ()

@end

@implementation InviteUserListViewController
@synthesize delegate;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.historyUserDataList = [[[NSMutableArray alloc] init] autorelease];
    self.fbFreindsDataList = [[[NSMutableArray alloc] init] autorelease];
    listDataType = LIST_DATA_TYPE_INVITED;
    
    self.view.backgroundColor = [AppHelper getBaseTextColor];
    
    rightButton = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)] autorelease];
    if(self.shareMode)
    {
        [rightButton setTitle:NSLocalizedString(@"Share", @"Share") forState:UIControlStateNormal];
    }
    else
    {
        [rightButton setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateNormal];
    }
    [rightButton addTarget:self action:@selector(clickRightButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:rightButton] autorelease];
    

    // セグメンテッドコントロール
    NSArray *arr = [NSArray arrayWithObjects:MultiLangString(@"History Users"), MultiLangString(@"Facebook Users"),  nil];
    memberListTypeSeg =[[[UISegmentedControl alloc] initWithItems:arr] autorelease];
    memberListTypeSeg.frame = CGRectMake(x_delta, y_delta, SCREEN_WIDTH-x_delta*2, 32);
    memberListTypeSeg.segmentedControlStyle = UISegmentedControlStyleBezeled; //スタイルの設定
    memberListTypeSeg.selectedSegmentIndex = 0; //
    //self.memberListTypeSeg.tintColor = HEXCOLOR(0x593D00);
    [self.view addSubview:memberListTypeSeg];
    
    //値が変更された時にメソッドを呼び出す
    [memberListTypeSeg addTarget:self action:@selector(changeMemberListType:) forControlEvents:UIControlEventValueChanged];
    
    //MemberListView
    CGFloat t_y =memberListTypeSeg.frame.origin.y + memberListTypeSeg.frame.size.height +y_delta/2;
    memberListTableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, t_y, SCREEN_WIDTH, self.view.bounds.size.height-t_y-BUTTON_HEIGHT*5) style:UITableViewStylePlain] autorelease];
    memberListTableView.delegate = self;
    memberListTableView.dataSource = self;
    [self.view addSubview:memberListTableView];
    
    //Input MailAdress
    sendMailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sendMailButton.frame = CGRectMake(x_delta, memberListTableView.frame.origin.y + memberListTableView.frame.size.height +y_delta/2, SCREEN_WIDTH-x_delta*2, BUTTON_HEIGHT);
    [sendMailButton addTarget:self action:@selector(clickInputMailAddressButton:) forControlEvents:UIControlEventTouchUpInside];
    [sendMailButton setImage:[AppHelper getWhiteButtonNormalImage:sendMailButton.bounds] forState:UIControlStateNormal];
    [sendMailButton setImage:[AppHelper getWhiteButtonSelectedImage:sendMailButton.bounds] forState:UIControlStateHighlighted];
    [self.view addSubview:sendMailButton];
    
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(x_delta, 0, sendMailButton.bounds.size.width-x_delta*2, sendMailButton.bounds.size.height)] autorelease];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont  boldSystemFontOfSize:BUTTON_TEXT_INIT_SIZE];
    label.text =  NSLocalizedString(@"Invite Users by e-mail",@"Invite Users by e-mail");
    [sendMailButton addSubview:label];
    
    if ([[self getTableListData] count] == 0) {
        [[BusinessDataCtrlManager sharedInstance] requestInvitedList:self];
    }
    
    UISwipeGestureRecognizer *swipeRightGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToRight:)] autorelease];
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	swipeRightGesture.numberOfTouchesRequired = 1;
    swipeRightGesture.delegate = self;
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [super dealloc];
    [_historyUserDataList release];
    [_fbFreindsDataList release];
    [_invitedUserList release];
}

#pragma mark - AppApiDelegate delegate
- (void)invitedListSucessed:(NSArray *)memberList
{
    self.historyUserDataList = [[[NSMutableArray alloc] initWithArray:memberList] autorelease];
    [memberListTableView reloadData];
}

- (void)fbFreindsListSucessed:(NSArray *)memberList
{
    self.fbFreindsDataList = [[[NSMutableArray alloc] initWithArray:memberList] autorelease];
    [memberListTableView reloadData];
}
#pragma mark UISegmentedControl changeMemberListType Methods
-(void)changeMemberListType:(UISegmentedControl*)seg
{
    if (seg.selectedSegmentIndex == 0)
    {
        //from History
        listDataType = LIST_DATA_TYPE_INVITED;
        
        if ([[self getTableListData] count] == 0)
        {
            [[BusinessDataCtrlManager sharedInstance] requestInvitedList:self];
        }
        else
        {
            [memberListTableView reloadData];
        }
        
    }
    else if (seg.selectedSegmentIndex == 1)
    {
        //from Facebook
        listDataType = LIST_DATA_TYPE_FB_FREINDS;
        
        if ([[self getTableListData] count] == 0)
        {
            [[BusinessDataCtrlManager sharedInstance] requestFbFreindsList:self];
        }
        else
        {
            [memberListTableView reloadData];
        }
        
    }
    
}


-(NSMutableArray *)getTableListData{
    if (listDataType == LIST_DATA_TYPE_INVITED)
    {
        return self.historyUserDataList;
    }
    else if(listDataType == LIST_DATA_TYPE_FB_FREINDS)
    {
        return self.fbFreindsDataList;
    }
    else
    {
        return [[[NSMutableArray alloc] init] autorelease];
    }
}

-(BOOL)isInvitedUserList:(NSMutableDictionary *)inData
{
    BOOL invitedUserFlag = FALSE;
    if (self.invitedUserList != nil && [self.invitedUserList count] > 0 && [inData objectForKey:userId_key] != nil)
    {
        for (NSMutableDictionary *invitedUserInfo in self.invitedUserList)
        {
            if ([[invitedUserInfo objectForKey:userId_key] intValue] == [[inData objectForKey:userId_key] intValue])
            {
                NSString *notJoinedStasus = [invitedUserInfo objectForKey:memberStatus_Key];
                if (!(notJoinedStasus != nil && ![notJoinedStasus isKindOfClass:[ NSNull class]] && [notJoinedStasus isEqualToString:@"1"]))
                {
                    //未参加も再招待できるようにする
                    invitedUserFlag = TRUE;
                    break;
                }
            }
        }
    }
    return invitedUserFlag;
}

#pragma mark - clickInputMailAddressButton Call Method
-(void)clickInputMailAddressButton:(id)sender
{
    InputTextfiledViewController *inputTextfiledViewController = [[[InputTextfiledViewController alloc] init] autorelease];
    inputTextfiledViewController.delegate = self;
    inputTextfiledViewController.txtFieldPlaceholder = NSLocalizedString(@"MailAddress",@"MailAddress");
    
    [self.navigationController pushViewController:inputTextfiledViewController animated:YES];
}

-(void)textValueConfirm:(NSString *)text
{
    listDataType = LIST_DATA_TYPE_INVITED;
    
    NSMutableDictionary *data = [[[NSMutableDictionary alloc] init] autorelease];
    [data setObject:[AppHelper getRanDomCode] forKey:RANDOM_CODE_KEY];
    [data setObject:MEMBER_SELECTED_FLAG forKey:MEMBER_SELECTED_FLAG];
    [data setObject:text forKey:userName_Key];
    [self.historyUserDataList insertObject:data atIndex:0];
    
    memberListTypeSeg.selectedSegmentIndex = 0 ;
    
    [memberListTableView reloadData];
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    return [[self getTableListData] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return MEMBER_TABLE_CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableDictionary *memberInfo = [[self getTableListData] objectAtIndex:[indexPath row]];
    
    NSNumber *userId = [memberInfo objectForKey:userId_key];
    NSString *CellIdentifier = @"";
    if (userId != nil) {
        CellIdentifier = [NSString stringWithFormat:@"Cell-%d-%d", [indexPath row], [userId intValue] ];
    }else{
        NSString *randomCode = [memberInfo objectForKey:RANDOM_CODE_KEY];
        CellIdentifier = [NSString stringWithFormat:@"Cell-%d-%@", [indexPath row], randomCode ];
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        
    }
    
    if ([self isInvitedUserList:memberInfo]) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSString *seletedFlag = [memberInfo objectForKey:MEMBER_SELECTED_FLAG];
    if (seletedFlag != nil) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    [self setCellDataContents:memberInfo view:cell.contentView];
    
    
    return cell;
}

- (void)setCellDataContents:(NSMutableDictionary *)inData view:(UIView *)inBaseView {
    UIView *baseView = [inBaseView viewWithTag:MEMBER_CELL_TAG];
    if (baseView != nil) {
        [baseView removeFromSuperview];
    }
    
    BOOL invitedUserFlag = [self isInvitedUserList:inData];
    
    baseView = [[[UILabel alloc] initWithFrame:inBaseView.bounds] autorelease];
    baseView.tag = MEMBER_CELL_TAG;
    if (invitedUserFlag) {
        baseView.backgroundColor = [UIColor grayColor];
    }else{
        baseView.backgroundColor = [UIColor clearColor];
    }
    [inBaseView addSubview:baseView];
    
    CGFloat x = 10;
    
    //User Icon
    UIImageView *UserIconView;
    CGFloat iconSize = USER_ICON_SIZE;
    UserIconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, (baseView.frame.size.height-iconSize)/2, iconSize, iconSize)] autorelease];
    UserIconView.layer.cornerRadius = 5.0f;
    UserIconView.clipsToBounds = YES;
    UserIconView.image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"defaultProfImg.png"]];
    [baseView addSubview:UserIconView];
    NSNumber *userId = [inData objectForKey:userId_key];
    if (userId != nil) {
        [AppHelper setUserIconWith:[userId intValue] imageView:UserIconView];
    }
    
    x = UserIconView.frame.origin.x + UserIconView.frame.size.width + x;
    
    //Set UserName
	NSString *userName = [inData objectForKey:userName_Key];
	if (userName == nil || [userName isKindOfClass:[ NSNull  class]]) {
		userName=@"";
	}
	UILabel *userNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(x, UserIconView.frame.origin.y, SCREEN_WIDTH-x-40, 26)] autorelease];
	userNameLable.backgroundColor = [UIColor clearColor];
	userNameLable.font = [UIFont  boldSystemFontOfSize:20];
	userNameLable.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
	userNameLable.textAlignment = NSTextAlignmentLeft;
	userNameLable.numberOfLines = 1;
	userNameLable.text = userName;
	[baseView addSubview:userNameLable];
	
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *memberInfo = [[self getTableListData] objectAtIndex:[indexPath row]];
    
    if ([self isInvitedUserList:memberInfo]) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    
    
    UITableViewCell *newCell  = [tableView cellForRowAtIndexPath:indexPath];
    
    NSString *seletedFlag = [memberInfo objectForKey:MEMBER_SELECTED_FLAG];
    if (seletedFlag == nil) {
        [memberInfo setObject:MEMBER_SELECTED_FLAG forKey:MEMBER_SELECTED_FLAG];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        [memberInfo removeObjectForKey:MEMBER_SELECTED_FLAG];
        newCell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - PanGestureRecognizer
-(void)viewDidDraggingToRight:(UISwipeGestureRecognizer *)swipe{
    //    NSLog(@"========handleRightSwipeGesture========");
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)clickRightButton:(id)sender
{
    
}
@end
