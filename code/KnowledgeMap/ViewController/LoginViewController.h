//
//  CreateNewBookTitleViewController.h
//  BukurouSS
//
//  Created by k-kin on 11/09/28.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDefine.h"
#import "AppHelper.h"
#import "WorkViewController.h"
#import "BusinessDataCtrlManager.h"
#import "AppApiDelegate.h"

@interface LoginViewController : BaseViewController < UITextFieldDelegate>{
	
    id delegate;
	UITextField	*txtMailAddress;
    UITextField	*txtPassword;

}


@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) UITextField	*txtMailAddress;
@property (nonatomic, strong) UITextField	*txtPassword;


-(void)createDetailLabel:(UIView *)baseView title:(NSString *)labelText rect:(CGRect)rect;
-(void)setTextFiledProperty:(UITextField *)textFiled ;


@end
