//
//  ViolationInputViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/09/23.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppHelper.h"

@interface ViolationInputViewController : BaseViewController<UITextViewDelegate>{
    NSMutableDictionary		*cardInfo;

}


@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, strong) UITextView *txtView;
@property (nonatomic, strong) NSMutableDictionary		*cardInfo;

@end
