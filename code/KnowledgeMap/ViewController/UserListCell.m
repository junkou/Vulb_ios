//
//  UserListCell.m
//  KnowledgeMap
//
//  Created by KouJun on 4/3/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import "UserListCell.h"
#import "AppHelper.h"
#import "AppDefine.h"

@implementation UserListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.backgroundColor = [UIColor clearColor];
        
        defaultLogo = [[UIImage imageNamed:[AppHelper getResourceIconPath:@"defaultProfImg.png"]] retain];
        
        userLogo = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, USER_ICON_SIZE, USER_ICON_SIZE)];
        userLogo.layer.cornerRadius = 5.0f;
        userLogo.clipsToBounds = YES;
        userLogo.image = defaultLogo;
        [self.contentView addSubview:userLogo];
        [userLogo release];
        
        userName = [[UILabel alloc] initWithFrame:CGRectMake(60, 13, 150, 20)];
        userName.backgroundColor = [UIColor clearColor];
        userName.textColor = [UIColor blackColor];
        userName.font = [UIFont boldSystemFontOfSize:20];
        userName.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:userName];
        [userName release];
        
        //is Jointed ?
        notJoined = [[UILabel alloc] initWithFrame:CGRectMake(220, 10, 80, 20)];
        notJoined.backgroundColor = [UIColor clearColor];
        notJoined.font = [UIFont  systemFontOfSize:10];
        notJoined.textColor = [UIColor blueColor];
        notJoined.textAlignment = NSTextAlignmentLeft;
        notJoined.text = MultiLangString(@"Not joined");
        [self.contentView addSubview:notJoined];
        [notJoined release];

    }
    return self;
}
- (void)dealloc
{
    [defaultLogo release];
    [_userInfo release];
    [super dealloc];
}

- (void)setUserInfo:(NSDictionary *)userInfo selected:(bool)selected
{    
    if ([_userInfo isEqualToDictionary:userInfo] == false)
    {
        [_userInfo release];
        _userInfo = [userInfo retain];
        
    }
    
    NSString *notJoinedStasus = [_userInfo objectForKey:memberStatus_Key];
    if (notJoinedStasus != nil && ![notJoinedStasus isKindOfClass:[ NSNull class]] && [notJoinedStasus isEqualToString:@"1"])
    {
        notJoined.hidden = NO;
    }
    else
    {
        notJoined.hidden = YES;
    }
    
    userLogo.image = defaultLogo;
    
    [AppHelper setUserIconWith:[[_userInfo objectForKey:userId_key] longLongValue] imageView:userLogo];

    userName.text = [userInfo objectForKey:userName_Key];
}

- (void)cardButtonClick:(id)sender
{
    
}

- (void)nameClick:(id)sender
{
    
}
@end
