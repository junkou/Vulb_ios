//
//  CreateCardViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/21.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppHelper.h"
#import "CardPartsDetailInfoViewController.h"
#import "WebServerViewController.h"
#import "HttpClientUtils.h"
#import "MEActionSheet.h"
#import "PhotoCameraViewController.h"
#import "PhotoGroupViewController.h"
#import "IllustSelectViewController.h"

@interface CreateCardViewController : BaseViewController <UITableViewDataSource,UITableViewDelegate, UITextViewDelegate>{
    id delegate;
    
    NSMutableArray		*dataList;
    NSMutableDictionary		*cardInfo;
    
    UIView *toolbarView;
    
    int contentsMode;
    PhotoCameraViewController *photoCameraViewController;
    PhotoGroupViewController *photoGroupViewController;
    IllustSelectViewController *illustSelectViewController;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) NSNumber *initInput;
@property (nonatomic, strong) NSNumber *categoryId;
@property (nonatomic, strong) NSMutableArray		*dataList;
@property (nonatomic, strong) NSMutableDictionary		*cardInfo;

@property (nonatomic, strong) UITableView            *tableView;

+(NSMutableArray*)detailList2EditList:(NSArray*)list;
@end
