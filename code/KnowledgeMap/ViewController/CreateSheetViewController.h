//
//  CreateSheetViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/09.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppHelper.h"

#import "InputTextfiledViewController.h"

#define CHECK_MARK_VIEW_TAG     10
#define SHARE_BUTTON_TAG        11
#define NOT_SHARE_BUTTON_TAG        12
#define MEMBER_CELL_TAG        13

#define x_delta 10
#define y_delta 20
#define BUTTON_HEIGHT   30

#define MEMBER_TABLE_CELL_HEIGHT    44

#define LIST_DATA_TYPE_INVITED      1
#define LIST_DATA_TYPE_FB_FREINDS   2

#define RANDOM_CODE_KEY             @"_r_c_k_"
#define MEMBER_SELECTED_FLAG        @"_m_s_f_"

@interface CreateSheetViewController : BaseViewController <UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource>{
    id delegate;

    NSString *viewTitle;
    int operationType;
    BOOL     shareFlag;
    
    NSMutableArray		*invitedUserList;
    
    NSMutableDictionary *sheetSettingInfo;
    

}
@property (nonatomic, assign) id delegate;
@property (nonatomic) int operationType;
@property (nonatomic, strong) NSString *viewTitle;
@property (nonatomic, strong) NSMutableArray		*invitedUserList;
@property (nonatomic, strong) NSMutableDictionary *sheetSettingInfo;


@property (nonatomic, strong) UITextField           *txtSheetName;
@property (nonatomic, strong) UIButton              *sharedButton;
@property (nonatomic, strong) UIButton              *notsharedButton;
@property (nonatomic, strong) UIView                *shareDetalBaseView;

@property (nonatomic, strong) UISegmentedControl    *memberListTypeSeg ;
@property (nonatomic, strong) UITableView           *memberListTableView ;
@property (nonatomic, strong) UIButton              *mailAddressButton;
@property (nonatomic, strong) UITextField           *txtCertificationCode;
@property (nonatomic, strong) UIButton              *certificationCodeButton;
@property (nonatomic, strong) UILabel               *certificationCodeLabel;
@property (nonatomic, strong) UIButton *maskView;


@property (nonatomic) int     listDataType;
@property (nonatomic, strong) NSMutableArray *historyUserDataList;
@property (nonatomic, strong) NSMutableArray *fbFreindsDataList;
@property (nonatomic) BOOL     shareFlag;
@property (nonatomic, strong) NSNumber *theNewSheetId;


@end
