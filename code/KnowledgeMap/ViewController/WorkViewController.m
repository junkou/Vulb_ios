//
//  WorkViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/02.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "WorkViewController.h"
#import "UICardButton.h"
#import "SheetListExtViewController.h"
#import "AppDelegate.h"
#import "CreateCardViewController.h"

NSString *const reroadNotification = @"reroadNotification";

@interface WorkViewController ()

@end

@implementation WorkViewController

@synthesize selectCategoryButton;

@synthesize categoryColorId;
//@synthesize contentsMode;
//@synthesize photoCameraViewController;
@synthesize clickThumbnailCardId;
@synthesize clickCardResourceType;
@synthesize clickCardBeginining;
@synthesize fromOpenURLFlag;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *swipeLeftGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToLeft:)] autorelease];
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	swipeLeftGesture.numberOfTouchesRequired = 1;
    swipeLeftGesture.delegate = self;
	[self.view addGestureRecognizer:swipeLeftGesture];

    //Sheet Select Button
    UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)] autorelease];
    UIImage *backgoundImageNormal = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_list_nomral.png"]];
    [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    UIImage *backgoubdImageHilight = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_list_touch.png"]];
    [button setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];

    [button addTarget:self action:@selector(clickSetSheetButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
    
    button = [[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 44, 0, 44, TOOL_BAR_HIGHT)] autorelease];
    [button setTitle:NSLocalizedString(@"Edit", @"Edit") forState:UIControlStateNormal];
    [button addTarget:self action:@selector(clickEditButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
    
    //Edit Sheetボタンを生成
//    UIButton *editSheetButton = [[[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-buttonSize-10, 0, buttonSize, buttonSize)] autorelease];
//    [editSheetButton addTarget:self action:@selector(clickUserListButton:) forControlEvents:UIControlEventTouchUpInside];
//    [editSheetButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_friendlist_nomral.png"]] forState:UIControlStateNormal];
//    [editSheetButton setImage:[UIImage imageNamed:[Utils getResourceIconPath:@"icon_friendlist_touch.png"]] forState:UIControlStateHighlighted];
//    [self.title_2_View addSubview:editSheetButton];
//
//    
//    NSMutableDictionary *sheetInfo =  [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
//    [self setSheetDataContents:sheetInfo view:self.title_2_View  delegate:self];
    self.view.backgroundColor = [AppHelper getBaseTextColor];
    
    
    CGFloat buttonWidth = 44;
    CGFloat buttonHeight = 44;
    CGFloat buttonX = self.view.bounds.size.width - 10 - (4 * buttonWidth);
    CGFloat buttonY = 0;
    //button Base
//    NSNumber *categoryColoer = [Utils parseWebDataToNumber:categoryInfo dataKey:colorIdx_key];
    floatToolbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, buttonHeight)];
    floatToolbar.backgroundColor = [AppHelper getColor:0];
    
    //inputTextButton
//    UIButton* inputTextButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight)];
//    [inputTextButton addTarget:self action:@selector(clickInputTextButton:) forControlEvents:UIControlEventTouchUpInside];
//    //self.inputTextButton.backgroundColor = [UIColor redColor];
//    backgoundImageNormal = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_input_text_normal.png"]];
//    [inputTextButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
//    backgoubdImageHilight = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_input_text_touch.png"]];
//    [inputTextButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
//    [floatToolbar addSubview:inputTextButton];
 
    //inputCameraButton
    UIButton* inputCameraButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX+buttonWidth, buttonY, buttonWidth, buttonHeight)];
    [inputCameraButton addTarget:self action:@selector(clickInputCameraButton:) forControlEvents:UIControlEventTouchUpInside];
    backgoundImageNormal = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_camera_blue_normal.png"]];
    [inputCameraButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    backgoubdImageHilight = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_camera_blue_touch.png"]];
    [inputCameraButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    [floatToolbar addSubview:inputCameraButton];

    //inputPhotoLibraryButton
    UIButton* inputPhotoLibraryButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX+buttonWidth*2, buttonY, buttonWidth, buttonHeight)];
    [inputPhotoLibraryButton addTarget:self action:@selector(clickInputPhotoLibraryButton:) forControlEvents:UIControlEventTouchUpInside];
    backgoundImageNormal = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_picture_blue_normal.png"]];
    [inputPhotoLibraryButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    backgoubdImageHilight = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_picture_blue_touch.png"]];
    [inputPhotoLibraryButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    [floatToolbar addSubview:inputPhotoLibraryButton];

    //inputIllustButton
    UIButton* inputIllustButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX+buttonWidth*3, buttonY, buttonWidth, buttonHeight)];
    [inputIllustButton addTarget:self action:@selector(clickInputIllustButton:) forControlEvents:UIControlEventTouchUpInside];
    backgoundImageNormal = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_illust_blue_normal.png"]];
    [inputIllustButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    backgoubdImageHilight = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_illust_blue_touch.png"]];
    [inputIllustButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    [floatToolbar addSubview:inputIllustButton];
    
//    UIButton* settingButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX+buttonWidth*4, buttonY, buttonWidth, buttonHeight)];
//    [settingButton addTarget:self action:@selector(clickInputIllustButton:) forControlEvents:UIControlEventTouchUpInside];
//    backgoundImageNormal = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_illust_blue_normal.png"]];
//    [settingButton setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
//    backgoubdImageHilight = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_illust_blue_touch.png"]];
//    [settingButton setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
//    [floatToolbar addSubview:settingButton];
    
    tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(reloadCardList) name:reroadNotification object:nil];
    
    //Add Categoryボタンを生成
    addCategoryButton = [[[UILayButton alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)] autorelease];
    addCategoryButton.backgroundColor = [UIColor whiteColor];
    addCategoryButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    [addCategoryButton setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Category creation", @"Create Category")] forState:UIControlStateNormal];
    [addCategoryButton setTitleColor:COLOR_ADD_BUTTON_TEXT forState:UIControlStateNormal];
    [addCategoryButton addTarget:self action:@selector(clickAddCategoryButton:) forControlEvents:UIControlEventTouchUpInside];
    [addCategoryButton setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_edit_addcategory_normal.png"]] forState:UIControlStateNormal];
    [addCategoryButton setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_edit_addcategory_touch.png"]] forState:UIControlStateNormal];
    //    [addSheetButton setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_add_sheet_touch.png"]] forState:UIControlStateHighlighted];
    addCategoryButton.layout = ImageLeftTextRight;
    [self.view addSubview:addCategoryButton];

//    self.htmlCreateWebView = [[[UIWebView alloc] initWithFrame:self.view.bounds] autorelease];
//    self.htmlCreateWebView.delegate = self;
//    NSString *path = [[NSBundle mainBundle] pathForResource: @"index" ofType: @"html"];
//    [self.htmlCreateWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
    
    categoryFoldState = [[NSMutableDictionary alloc] init];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSDictionary *sheetInfo = [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    
    NSString *sheetName = [sheetInfo objectForKey:sheetName_key];
    if (sheetName == nil || [sheetName isKindOfClass:[ NSNull  class]]) {
        sheetName=@"";
    }
    self.navigationItem.title = sheetName;
    
    [BusinessDataCtrlManager sharedInstance].delegate = self;
    
    if (tableView.isEditing)
    {
        [[AppDelegate sharedInstance] setTabbarHidden:YES animated:NO];
    }
    else
    {
        [[AppDelegate sharedInstance] setTabbarHidden:NO animated:NO];
    }
    
    if ([BusinessDataCtrlManager getUserInfo].lastCategoryId)
    {
        [categoryFoldState setValue:@(1) forKey:[[BusinessDataCtrlManager getUserInfo].lastCategoryId stringValue]];
        
        [[BusinessDataCtrlManager sharedInstance] requestLastCardList:self startIdx:0];
    }
    else
    {
        [tableView reloadData];
    }
    
    fromAppear = YES;
}

- (NSDictionary*)categoryInfoAtSection:(NSUInteger)section
{
    return [[BusinessDataCtrlManager getCategoryList] objectAtIndex:section];
}

- (NSArray*)cardListAtSection:(NSUInteger)section
{
    NSDictionary *categoryInfo = [self categoryInfoAtSection:section];
    return [BusinessDataCtrlManager getCardList:[categoryInfo objectForKey:categoryId_Key]];
}

- (NSMutableDictionary*)cardInfoAtIndexPath:(NSIndexPath*)indexPath
{
    NSArray *cardList = [self cardListAtSection:indexPath.section];
    return [cardList objectAtIndex:indexPath.row];
}
//- (void)webViewDidFinishLoad:(UIWebView *)webView{
//    /**
//       WebViewロード後にカード一覧データを取得する
//     **/
//    
//    [[BusinessDataCtrlManager getInstance] getLastCardList:self startIdx:0];
//    
//    [categoryFoldState setValue:@(1) forKey:[[BusinessDataCtrlManager getUserInfo].lastCategoryId stringValue]];
//}

//#pragma mark - getHtmlHeight
//-(CGFloat)getHtmlHeight:(NSString *)inHtmlText{
//    
//    NSString *htmlText = [inHtmlText stringByReplacingOccurrencesOfString:@"'" withString:@"\""];
//
//    NSString *html = [NSString stringWithFormat:@"function execCreateHtml(){$('#work1').html('%@');};execCreateHtml();", htmlText];
//    //NSLog(@"html:%@",html);
//    
//    [self.htmlCreateWebView stringByEvaluatingJavaScriptFromString:html];
//    [self.htmlCreateWebView setNeedsDisplay];
//    
//    NSString *htmlHeight =  [self.htmlCreateWebView stringByEvaluatingJavaScriptFromString: @"function getHtmlheight(){return $('#work1').outerHeight(); }; getHtmlheight();"];
//    
//    //NSLog(@"htmlHeight:%@",htmlHeight);
//    
//    html = [NSString stringWithFormat:@"function execCreateHtml(){$('#work1').html('');};execCreateHtml();"];
//    //NSLog(@"html:%@",html);
//    
//    [self.htmlCreateWebView stringByEvaluatingJavaScriptFromString:html];
//
//    
//    return [htmlHeight floatValue]+10;
//    
//}

- (bool)isCategoryUnFold:(NSUInteger)section
{
    NSDictionary *categoryInfo = [self categoryInfoAtSection:section];
    return [[categoryFoldState objectForKey:[[categoryInfo objectForKey:categoryId_Key] stringValue]] boolValue];
}

- (bool)isCategoryHaveNew:(NSUInteger)section
{
    NSDictionary *categoryInfo = [self categoryInfoAtSection:section];
    NSArray *arr = [BusinessDataCtrlManager getCategoryStatisticsList];
    for (NSMutableDictionary *cate in arr)
    {
        if ([[cate objectForKey:categoryId_Key] isEqual:[categoryInfo objectForKey:categoryId_Key]])
        {
            return [[cate objectForKey:updateCardCount_key] intValue] && [[cate objectForKey:newCardCount_key] intValue];
        }
    }
    return NO;
}
#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self isCategoryUnFold:section])
    {
        return [[self cardListAtSection:section] count];
    }
    else
    {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[BusinessDataCtrlManager getCategoryList] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [[self cardListAtSection:indexPath.section] count]-1)
    {
        return CARD_HEIGHT + 20;
    }
    return CARD_HEIGHT + 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CATEGORY_LIST_HEIGHT;
}

- (UIView*)tableView:(UITableView *)table viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, table.frame.size.width, CATEGORY_LIST_HEIGHT)] autorelease];
    v.backgroundColor = [AppHelper getCategoryColor:[[BusinessDataCtrlManager getCategoryList] objectAtIndex:section]];
    
    bool unfolded = [self isCategoryUnFold:section];
    bool haveNew = [self isCategoryHaveNew:section];
    int x = 0;
    UIButton *arrowView = CreateView(UIButton, x, 0, 240, 44);
    arrowView.tag = section;
    [arrowView setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 200)];
    if (unfolded)
    {
        [arrowView setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_category_open.png"]] forState:UIControlStateNormal];
    }
    else
    {
        [arrowView setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_category_close.png"]] forState:UIControlStateNormal];
    }
    [arrowView addTarget:self action:@selector(toggleSection:) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:arrowView];
    [arrowView release];
    x += 44;

    NSDictionary *categoryInfo = [self categoryInfoAtSection:section];

    int w = 320 - x - 90;
    if (unfolded == false && haveNew == false)
    {
        w += 90;
    }
    
    UILabel *category = CreateViewWithRect(UILabel, CGRectMake(x, 11, w, 20));
    category.backgroundColor = [UIColor clearColor];
    category.textColor = [UIColor whiteColor];
    category.text = [categoryInfo objectForKey:@"categoryName"];
    [v addSubview:category];
    [category release];
    x += w;
    
    if (unfolded)
    {
        UIView *opView = CreateView(UIView, x, 0, 90, 44);
        opView.backgroundColor = [UIColor clearColor];
        opView.tag = section;
        UIButton *newCard = CreateView(UIButton, 0, 0, 44, 44);
        [newCard setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_addcard_normal.png"]] forState:UIControlStateNormal];
        [newCard setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_addcard_touch.png"]] forState:UIControlStateHighlighted];
//        [newCard setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_input_text_disable.png"]] forState:UIControlStateDisabled];
        [newCard addTarget:self action:@selector(clickInputTextButton:) forControlEvents:UIControlEventTouchUpInside];
        [opView addSubview:newCard];
        [newCard release];
        
        UIButton *moreOp = CreateView(UIButton, 44, 0, 44, 44);
        [moreOp setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_ather_normal.png"]] forState:UIControlStateNormal];
        [moreOp setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_ather_touch.png"]] forState:UIControlStateHighlighted];
//        [moreOp setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_input_text_disable.png"]] forState:UIControlStateDisabled];
        [moreOp addTarget:self action:@selector(moreClick:) forControlEvents:UIControlEventTouchUpInside];
        [opView addSubview:moreOp];
        [moreOp release];
        
        [v addSubview:opView];
        [opView release];
    }
    else if(haveNew)
    {
        
    }
    
    UIView *sep = [[[UIView alloc] initWithFrame:CGRectMake(0, v.frame.size.height-1, v.frame.size.width, 1)] autorelease];
    sep.backgroundColor = [UIColor whiteColor];
    [v addSubview:sep];
    return v;
}

- (UITableViewCell*)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UICardInfoCell *cell = (UICardInfoCell*)[table dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil)
    {
        cell = [[[UICardInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"] autorelease];
    }
    
    NSDictionary *categoryInfo = [self categoryInfoAtSection:indexPath.section];
    NSDictionary *cardInfo = [self cardInfoAtIndexPath:indexPath];
    [cell setCardInfo:cardInfo categoryInfo:categoryInfo delegate:self];
    return cell;
}

- (void)tableView:(UITableView *)table didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cardInfo = [self cardInfoAtIndexPath:indexPath];
    NSNumber *cardId = [cardInfo objectForKey:cardId_key];
//    self.resourceType = [cardInfo objectForKey:resourceType_key];
//    self.beginining = [cardInfo objectForKey:begining_key];
    
    self.clickedCardThumbnailView = [tableView cellForRowAtIndexPath:indexPath].contentView;
    
    [[BusinessDataCtrlManager sharedInstance] requestCardInfoWithCardId:cardId delegate:self];
    
    [table deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (floatToolbar.superview)
    {
        [floatToolbar removeFromSuperview];
    }
}
#pragma mark - PanGestureRecognizer
-(void)viewDidDraggingToLeft:(UISwipeGestureRecognizer *)swipe{
    //    NSLog(@"========handleRightSwipeGesture========");
//    if (self.workBaseView.frame.origin.x > 0) {
//         [self clickSetSheetButton:nil];
//    }else{
        [self clickUserListButton:nil];
//    }
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - setButtonProPerty
- (void)setButtonProPerty:(UIButton *)button lineColor:(int)clolerId imgName:(NSString *)imgName label:(NSString *)labelText {
    button.backgroundColor = [UIColor redColor];
    
    button.layer.cornerRadius = 5.0f;
    button.clipsToBounds = YES;
    
    NSString *backImage = @"";
    UIImage *backgoundImageNormal = [UIImage imageNamed:[AppHelper getResourceIconPath:[NSString stringWithFormat:@"%@Normal.png", backImage]]];
    [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];    
    UIImage *backgoubdImageHilight = [UIImage imageNamed:[AppHelper getResourceIconPath:[NSString stringWithFormat:@"%@Touched.png", backImage]]];
    [button setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
    
    
    CGFloat cplorBarWidth = 5;
    UIView *colorBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cplorBarWidth, button.bounds.size.height)];
    colorBar.tag = WORK_VIEW_MAINBUTTON_COLOR_BAR_TAG;
    colorBar.backgroundColor = [AppHelper getColor:clolerId];
    [button addSubview:colorBar];
	[colorBar release];
    
    UIImage *photoImage = [UIImage imageNamed:imgName];
    UIImageView *photoImageView = [[UIImageView alloc] initWithImage:photoImage];
    [button addSubview:photoImageView];
	[photoImageView release];
        
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(cplorBarWidth+7, 0, button.bounds.size.width-cplorBarWidth, button.bounds.size.height)];
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [AppHelper getBaseTextColor];
	label.textAlignment = NSTextAlignmentLeft;
	label.font = [UIFont  boldSystemFontOfSize:20];
	label.text =  NSLocalizedString(labelText, labelText);
	[button addSubview:label];
	[label release];
	
}

- (void)setListButtonProPerty:(UIButton *)button {
    //button.backgroundColor = [UIColor redColor];
    NSString *imgName = @"";
    UIImage *backgoundImageNormal = [UIImage imageNamed:[AppHelper getResourceIconPath:[NSString stringWithFormat:@"%@Normal.png", imgName]]];
    [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
    
    UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(button.bounds.size.width-40, (button.bounds.size.height-30)/2, 30, 30)];
    [arrowImageView setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"level_arrow_normal.png"]]];
    [button addSubview:arrowImageView];
    [arrowImageView release];
}

#pragma mark - AppApiDelegate delegate
- (void)sheetMemberListSucessed:(NSArray *)memberList{
    UserListViewController *userListViewController = [[[UserListViewController alloc] init] autorelease];
    userListViewController.delegate = self;
    userListViewController.invitedUserList = [[[NSMutableArray alloc] initWithArray:memberList] autorelease];
    
    [self.navigationController pushViewController:userListViewController animated:YES];
    
}

//create new Category
- (void)createCategory:(NSMutableDictionary *)param{
    [[BusinessDataCtrlManager sharedInstance] createCategory:param delegate:self];

}

- (void)reloadCardList
{
    [tableView reloadData];
}

- (void)CategoryListSucessed
{
    NSDictionary *sheetInfo = [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    
    NSString *sheetName = [sheetInfo objectForKey:sheetName_key];
    if (sheetName == nil || [sheetName isKindOfClass:[ NSNull  class]]) {
        sheetName=@"";
    }
    self.navigationItem.title = sheetName;

    if ([BusinessDataCtrlManager getUserInfo].lastCategoryId)
    {
        [categoryFoldState setValue:@(1) forKey:[[BusinessDataCtrlManager getUserInfo].lastCategoryId stringValue]];
        
        [[BusinessDataCtrlManager sharedInstance] requestLastCardList:self startIdx:0];
    }
    else
    {
        [tableView reloadData];
    }
    
}

- (void)cardListSucessed
{
    [tableView reloadData];
//    UIImage *shadowImage = [UIImage imageNamed:[Utils getResourceIconPath:@"back_image_gradation.png"]];
//    CGFloat startHeight = shadowImage.size.height ;
//    
//    UIView *baseView = [self.workBaseView viewWithTag:CARD_THUMBNAIL_BASE_VIEW_TAG];
//    [baseView removeFromSuperview];
//    
//    baseView = [[[UIView alloc] initWithFrame:self.scrollView.bounds] autorelease];
//    //baseView = [[[UIView alloc] initWithFrame:self.workBaseView.bounds] autorelease];
//    baseView.tag = CARD_THUMBNAIL_BASE_VIEW_TAG;
//    CGRect rect = baseView.frame;
//    rect.origin.y = startHeight;
//    baseView.frame = rect;
//    //baseView.backgroundColor = [UIColor redColor];
//    [self.scrollView addSubview:baseView ];
//    
//    NSMutableDictionary *categoryInfo =  [BusinessDataCtrlManager getCategoryInfo:[BusinessDataCtrlManager getUserInfo].lastCategoryId];
//    NSMutableArray *cardList =[BusinessDataCtrlManager getCardList];
//
//    if ([cardList count] < CARD_ROW_COUNT) {
//        [categoryInfo setObject:[NSNumber numberWithInt:[cardList count]] forKey:totalCard_key];
//        
//        //Cateogry 情報を更新（カード件数）
//        UIView *categoryView = [self.workBaseView viewWithTag:SELECT_CATEGORY_BUTTON_TAG];
//        UILabel *nameLabel = (UILabel *)[categoryView viewWithTag:CATEGORY_NAME_LABEL_TAG];
//        NSString *categoryName = [categoryInfo objectForKey:categoryName_key];
//        NSString *categoryNameText = [NSString stringWithFormat:@"%@(%d)",categoryName,[cardList count]];
//        nameLabel.text = categoryNameText;
//
//    }
//
//
//    CGFloat cardWidth = CARD_WIDTH;
//    CGFloat cardHeight = CARD_HEIGHT;
//    
//    NSNumber *categoryColoer = [Utils parseWebDataToNumber:categoryInfo dataKey:colorIdx_key];
//
//    for (int i=0; i<[cardList count] && i<=SHOW_CARD_LIST_INIT_COUNT; i++) {
//        NSMutableDictionary *cardInfo = [cardList objectAtIndex:i];
//        UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-cardWidth)/2, (cardHeight+THUMBNAIL_LIST_DELTA)*i, cardWidth, cardHeight)] autorelease];
//        NSNumber *cardId = [cardInfo objectForKey:cardId_key];
//        button.tag = [cardId intValue];
//        [button addTarget:self action:@selector(clickThumbnailButton:) forControlEvents:UIControlEventTouchUpInside];
//        button.backgroundColor = [UIColor whiteColor];
//        [baseView addSubview:button];
//        
//        [Utils setCardListDataContents:cardInfo view:button colorBar:[categoryColoer intValue] delegate:self];
//      
//        rect = baseView.frame;
//        rect.size.height = (cardHeight+THUMBNAIL_LIST_DELTA)*(i+1);
//        baseView.frame = rect;
//
//        startHeight = startHeight + cardHeight+THUMBNAIL_LIST_DELTA;
//                
//      }
//    
//    self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width, startHeight+65);
//
//    
//    CGFloat moreButtonHeight=30;
//    //CGFloat moreButtonWidth=260;
//    if ([cardList count] > SHOW_CARD_LIST_INIT_COUNT+1) {
//         UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-CARD_WIDTH)/2, (cardHeight+THUMBNAIL_LIST_DELTA)*(SHOW_CARD_LIST_INIT_COUNT+1), CARD_WIDTH, moreButtonHeight)] autorelease];
//        [button addTarget:self action:@selector(clickMoreButton:) forControlEvents:UIControlEventTouchUpInside];
//        [Utils addCommonButtonProPerty:button imgName:@"" label:@"More"];
//        //button.backgroundColor = [UIColor whiteColor];
//        [baseView addSubview:button];
//         
//        rect = baseView.frame;
//        rect.size.height = (cardHeight+THUMBNAIL_LIST_DELTA)*(SHOW_CARD_LIST_INIT_COUNT+1)+moreButtonHeight+THUMBNAIL_LIST_DELTA;
//        baseView.frame = rect;
//
//         self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width, self.scrollView.contentSize.height+moreButtonHeight+THUMBNAIL_LIST_DELTA);
//        
//    }
//    
//    [self.scrollView setContentOffset:CGPointMake(0.0f, 0.0f) animated:NO];
//    
//    UIView *backView = [self.workBaseView viewWithTag:WORK_VIEW_BASE_BACK_VIEW_TAG];
//    rect = backView.frame;
//    rect.size.height = self.scrollView.contentSize.height;
//    backView.frame = rect;
//    
//    
//    if (self.fromOpenURLFlag) {
//        self.fromOpenURLFlag = FALSE;
//        
//        //起動時の１回のみ実行する
//        NSMutableDictionary *option = [[BusinessDataCtrlManager getInstance] getGlobalOptionData] ;
//        
//        NSString *cardId = [option objectForKey:@"card_id"];
//        if (cardId != nil) {
//            self.clickThumbnailCardId = [NSNumber numberWithInt:[cardId intValue]];
//            self.clickCardResourceType = DB_RESOURCE_TYPE_PHOTO;
//            [[BusinessDataCtrlManager getInstance] getCardInfoWithCardId:self.clickThumbnailCardId delegate:self];
//        }
//    }
    
    if (fromAppear)
    {
        //show last category
        NSArray *categoryList = [BusinessDataCtrlManager getCategoryList];
        int i = 0,j = -1;
        for (NSDictionary *cate in categoryList)
        {
            if ([[cate objectForKey:categoryId_Key] isEqual:[BusinessDataCtrlManager getUserInfo].lastCategoryId])
            {
                j = i;
                break;
            }
            i++;
        }
        
        if (j != -1)
        {
            CGRect rc = [tableView rectForHeaderInSection:j];
            [tableView scrollRectToVisible:rc animated:NO];
        }
        
        fromAppear = NO;
    }
    
}

- (void)cardInfoSucessed:(NSMutableDictionary *)cardInfo{
    
    UIView *readStatusView = [self.clickedCardThumbnailView viewWithTag:READ_STATUS_TAG];
    [readStatusView removeFromSuperview];
    
    NSString *contents = [cardInfo objectForKey:CONTENTS];
    //NSLog(contents);

    NSMutableArray *cardInfoDetailList = [[[NSMutableArray alloc] init] autorelease];
    [AppHelper changeCardInfoDetailList:contents output:cardInfoDetailList];
    
    CardDetailViewController *cardDetailViewController = [[[CardDetailViewController alloc] init] autorelease];
    cardDetailViewController.delegate = self;
    [cardDetailViewController.dataList addObjectsFromArray:cardInfoDetailList];
    cardDetailViewController.cardInfo = cardInfo;
    cardDetailViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:cardDetailViewController animated:YES];
}

-(void)returnFromCardInfoDetailView:(NSNumber *)cardId{
    NSArray *subViews = [floatToolbar subviews];
    for (UIView *subView in subViews ) {
        if ([subView isKindOfClass:[UIButton class]]) {
             if (subView.tag == [cardId intValue]) {
                UIView *readStatusView = [subView viewWithTag:READ_STATUS_TAG];
                [readStatusView removeFromSuperview];
            }
        }
    }
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
     return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

#pragma mark - Memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    [categoryFoldState release];
    [floatToolbar release];
	[self.selectCategoryButton release];
    
    [super dealloc];
}

- (void)createCardInSection:(int)section params:(id)data
{
    CreateCardViewController *inputTextViewController = [[[CreateCardViewController alloc] init] autorelease];
    NSDictionary *categoryInfo = [self categoryInfoAtSection:section];
    inputTextViewController.categoryId = [categoryInfo objectForKey:categoryId_Key];
    inputTextViewController.initInput = data;
    BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:inputTextViewController] autorelease];
    
	[self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - clickAddCategoryButton
-(void)clickAddCategoryButton:(id)sender
{
    CreateCategoryViewController *createCategoryViewController = [[[CreateCategoryViewController alloc] init] autorelease];
    createCategoryViewController.delegate = self;
    
    BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:createCategoryViewController] autorelease];
    
	[self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - clickAddSheetButton
-(void)clickUserListButton:(id)sender{
    
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId  forKey:sheetId_Key];
    
    [[BusinessDataCtrlManager sharedInstance] requestSheetMemberList:param delegate:self];
    
}

#pragma mark - main Button
-(void)toggleSection:(UIButton*)sender
{
    NSNumber *categoryID = [[self categoryInfoAtSection:sender.tag] objectForKey:categoryId_Key] ;
    bool fold = [[categoryFoldState objectForKey:[categoryID stringValue]] boolValue];
    [categoryFoldState setObject:@(!fold) forKey:[categoryID stringValue]];
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationNone];
    
    if (!fold && [self cardListAtSection:sender.tag] == 0)
    {
        [[BusinessDataCtrlManager sharedInstance] requestCardListWithCategoryId:categoryID delegate:self startIdx:0];
    }
}

-(void)clickInputTextButton:(UIView*)sender
{
    [self createCardInSection:sender.superview.tag params:nil];
}

-(void)moreClick:(UIView*)sender
{
    if (floatToolbar.superview)
    {
        [floatToolbar removeFromSuperview];
    }
    else
    {
        CGRect frame = [sender.superview convertRect:sender.frame toView:tableView];
        floatToolbar.frame = CGRectMake(0, frame.origin.y+frame.size.height-1, floatToolbar.frame.size.width, floatToolbar.frame.size.height);
        
        NSDictionary *categoryInfo = [self categoryInfoAtSection:sender.superview.tag];
        int colorIdex = [[AppHelper parseWebDataToNumber:categoryInfo dataKey:colorIdx_key] intValue];
        floatToolbar.backgroundColor = [AppHelper getColor:colorIdex];
        floatToolbar.alpha = 0.9;
        [tableView addSubview:floatToolbar];
        lastSection = sender.superview.tag;
    }
}

-(void)clickInputIllustButton:(id)sender{
//    IllustSelectViewController *illustSelectViewController = [[IllustSelectViewController alloc] init];
//    illustSelectViewController.delegate = self;
//    
//    BaseNavigationController *controller = [[BaseNavigationController alloc] initWithRootViewController:illustSelectViewController];
//    
//	[self presentViewController:controller animated:YES completion:nil];
//    
//    [illustSelectViewController release];
//    [controller release];
    [self createCardInSection:lastSection params:@(CONTENTS_MODE_ILLUST)];
}

-(void)clickInputCameraButton:(id)sender{
    
//    self.contentsMode = CONTENTS_MODE_CAMERA;
//    
//    self.photoCameraViewController = [[PhotoCameraViewController alloc] init] ;
//    self.photoCameraViewController.delegate = self;
//    [self.photoCameraViewController startUIImagePickerController];
    [self createCardInSection:lastSection params:@(CONTENTS_MODE_CAMERA)];
}

-(void)clickInputPhotoLibraryButton:(id)sender{
    
//    self.contentsMode = CONTENTS_MODE_PHOTOLIBRARY;
//    
//    PhotoGroupViewController *photoGroupViewController = [[PhotoGroupViewController alloc] init];
//    photoGroupViewController.delegate = self;
//    
//    BaseNavigationController *controller = [[BaseNavigationController alloc] initWithRootViewController:photoGroupViewController];
//    
//	[self presentViewController:controller animated:YES completion:nil];
//    
//    [photoGroupViewController release];
//    [controller release];
    [self createCardInSection:lastSection params:@(CONTENTS_MODE_PHOTOLIBRARY)];
}

- (void)clickScrollToTopButton:(UIButton *)button{
    [tableView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)cardButtonClick:(UIButton *)button{
    //NSLog(@"-------clickThumbnailButton---------");
    
    self.clickedCardThumbnailView = button;
    self.clickThumbnailCardId = @(button.tag);
    [[BusinessDataCtrlManager sharedInstance] requestCardInfoWithCardId:self.clickThumbnailCardId delegate:self];
    
}

//- (void)clickMoreButton:(UIButton *)button{
//    //NSLog(@"-------clickMoreButton---------");
//    CardListViewController *cardListViewController = [[CardListViewController alloc] init];
//    cardListViewController.delegate = self;
//    
//#warning todo
//    cardListViewController.dataList = [NSMutableArray arrayWithArray:[BusinessDataCtrlManager getCardList:@(0)]];
//    cardListViewController.lastPosition = [cardListViewController.dataList count];
//    cardListViewController.title = [NSString stringWithFormat:@"%@(%d)", MultiLangString(@"Card List") ,[cardListViewController.dataList count]];
//    [self.navigationController pushViewController:cardListViewController animated:YES];
//    [cardListViewController release];
//}
//
- (void)clickSetSheetButton:(id)sender
{
    SheetListExtViewController *slvc = [[[SheetListExtViewController alloc] init] autorelease];
    BaseNavigationController *navigation = [[[BaseNavigationController alloc] initWithRootViewController:slvc] autorelease];
    [[[AppDelegate sharedInstance] mainViewController] presentViewController:navigation animated:NO completion:nil];
}

- (void)clickEditButton:(id)sender
{
    [tableView setEditing:!tableView.isEditing];
    
    if (tableView.isEditing)
    {
        [[AppDelegate sharedInstance] setTabbarHidden:YES animated:YES];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            addCategoryButton.frame = CGRectMake(0, self.view.bounds.size.height - addCategoryButton.frame.size.height, addCategoryButton.frame.size.width, addCategoryButton.frame.size.height);
            
            tableView.frame = CGRectMake(0, 0, tableView.frame.size.width, self.view.frame.size.height-addCategoryButton.frame.size.height);
            
        }];
    }
    else
    {
        [[AppDelegate sharedInstance] setTabbarHidden:NO animated:YES];
        [UIView animateWithDuration:0.3 animations:^{
            
            addCategoryButton.frame = CGRectMake(0, self.view.bounds.size.height, addCategoryButton.frame.size.width, addCategoryButton.frame.size.height);
            
            tableView.frame = CGRectMake(0, 0, tableView.frame.size.width, self.view.frame.size.height);
        }];
    }
}
@end
