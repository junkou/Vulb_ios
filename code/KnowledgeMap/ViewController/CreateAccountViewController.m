//
//  CreateAccountViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/13.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "CreateAccountViewController.h"

@interface CreateAccountViewController ()

@end

@implementation CreateAccountViewController
@synthesize delegate;
@synthesize txtMailAddress;
@synthesize txtMailAddressConfirm;
@synthesize txtPassword;
@synthesize txtPasswordConfirm;
@synthesize txtName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = [AppHelper getBaseTextColor];
    
    self.scrollView = [[[UIScrollView alloc] initWithFrame:self.view.bounds] autorelease];
    [self.scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, 700)];
    [self.view addSubview:self.scrollView];
    
    //NavigationBar + LeftButton
    [AppHelper setNavigationBarLeftCancelButton:self.navigationItem controller:self];
    
    //Add NavigationBar Title
    [AppHelper setNavigationBarTitleLabel:self.navigationItem.titleView text:@"Create account"];
    
    //add NavicationBar RightButton
    //[Utils setNavigationBarRightSaveButton:self.navigationItem controller:self];

    
    //Main WorkView
    [self createMainWorkView];
    
}

-(void)createMainWorkView{
    
    CGFloat buttonHeight = 30;
    CGFloat buttonWidth = 260;
    CGFloat y=10;
    CGFloat x=(SCREEN_WIDTH-buttonWidth)/2;
    CGRect rect;
    //MailAddress Label
    rect = [self createDetailLabel:self.scrollView title:NSLocalizedString(@"MailAddress",@"MailAddress") rect:CGRectMake(x, y, buttonWidth, buttonHeight)];
    //MailAddress TextFiled
    self.txtMailAddress = [[UITextField alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y+rect.size.height-y/2, buttonWidth, buttonHeight)];
    [self setTextFiledProperty:self.txtMailAddress  ];
    self.txtMailAddress.keyboardType = UIKeyboardTypeEmailAddress;
    self.txtMailAddress.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.txtMailAddress becomeFirstResponder];
   
    //MailAddress Confirm Label
    rect = [self createDetailLabel:self.scrollView title:NSLocalizedString(@"E-mail address(confirm)",@"E-mail address(confirm)") rect:CGRectMake(x, self.txtMailAddress.frame.origin.y+self.txtMailAddress.frame.size.height+y, buttonWidth, buttonHeight)];
    //MailAddress Confirm TextFiled
    self.txtMailAddressConfirm = [[UITextField alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y+rect.size.height-y/2, buttonWidth, buttonHeight)];
    [self setTextFiledProperty:self.txtMailAddressConfirm  ];
    self.txtMailAddressConfirm.keyboardType = UIKeyboardTypeEmailAddress;
    self.txtMailAddressConfirm.autocapitalizationType = UITextAutocapitalizationTypeNone;

    //Password Label
    rect = [self createDetailLabel:self.scrollView title:NSLocalizedString(@"Password",@"Password") rect:CGRectMake(x, self.txtMailAddressConfirm.frame.origin.y+self.txtMailAddressConfirm.frame.size.height+y, buttonWidth, buttonHeight)];
     //Password TextFiled
    self.txtPassword = [[UITextField alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y+rect.size.height-y/2, buttonWidth, buttonHeight)];
    [self setTextFiledProperty:self.txtPassword ];
    self.txtPassword.keyboardType = UIKeyboardTypeASCIICapable;
    self.txtPassword.secureTextEntry = YES;

    //Password Confirm Label
    rect = [self createDetailLabel:self.scrollView title:NSLocalizedString(@"Password(confirm)",@"Password(confirm)") rect:CGRectMake(x, self.txtPassword.frame.origin.y+self.txtPassword.frame.size.height+y, buttonWidth, buttonHeight)];
    //Password TextFiled
    self.txtPasswordConfirm = [[UITextField alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y+rect.size.height-y/2, buttonWidth, buttonHeight)];
    [self setTextFiledProperty:self.txtPasswordConfirm ];
    self.txtPasswordConfirm.keyboardType = UIKeyboardTypeASCIICapable;
    self.txtPasswordConfirm.secureTextEntry = YES;

    //Name Label
    rect = [self createDetailLabel:self.scrollView title:NSLocalizedString(@"Name",@"Name") rect:CGRectMake(x, self.txtPasswordConfirm.frame.origin.y+self.txtPasswordConfirm.frame.size.height+y, buttonWidth, buttonHeight)];
    //Password TextFiled
    self.txtName = [[UITextField alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y+rect.size.height-y/2, buttonWidth, buttonHeight)];
    [self setTextFiledProperty:self.txtName ];

    
    //CreateButton
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(x, self.txtName.frame.origin.y+self.txtName.frame.size.height+y*3, buttonWidth, buttonHeight);
    [button addTarget:self action:@selector(clickCreateButton:) forControlEvents:UIControlEventTouchUpInside];
    //button.backgroundColor = [UIColor blueColor];
    [button setImage:[AppHelper getBlueButtonNormalImage:button.bounds] forState:UIControlStateNormal];
    [button setImage:[AppHelper getBlueButtonSelectedImage:button.bounds] forState:UIControlStateHighlighted];
    [self.scrollView addSubview:button];
    
    UILabel *label = [[[UILabel alloc] initWithFrame:button.bounds] autorelease];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont  boldSystemFontOfSize:BUTTON_TEXT_INIT_SIZE];
    label.text =  NSLocalizedString(@"Done",@"Done");
    [button addSubview:label];

}

-(CGRect)createDetailLabel:(UIView *)baseView title:(NSString *)labelText rect:(CGRect)rect{
    //NSLog(@"---------");
    int fontSize = 14;
    CGSize size = [labelText sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] constrainedToSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT) lineBreakMode:NSLineBreakByCharWrapping];
    
    UILabel *label = [[[UILabel alloc] initWithFrame:rect] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.lineBreakMode = NSLineBreakByCharWrapping;
	label.font = [UIFont  boldSystemFontOfSize:fontSize];
	label.textColor = [UIColor grayColor];
	label.textAlignment = NSTextAlignmentLeft;
	label.numberOfLines = size.height/fontSize;
	label.text = labelText;
	[baseView addSubview:label];
    
    return label.frame;
    
}

-(void)setTextFiledProperty:(UITextField *)textFiled{
  	textFiled.delegate = self;
    textFiled.backgroundColor = [UIColor whiteColor];
    //textFiled.borderStyle = UITextBorderStyleBezel;
	textFiled.clearButtonMode = UITextFieldViewModeWhileEditing;
    textFiled.autocapitalizationType = UITextAutocapitalizationTypeSentences;
	[self.scrollView addSubview:textFiled ];
    
}

#pragma mark - NavicationBar CancelButton Call Method
-(void)clickCancelButton:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - NavicationBar SaveButton Call Method
-(void)clickCreateButton:(id)sender {
    if (
        (self.txtMailAddress.text == nil || [self.txtMailAddress.text isEqualToString:@""]) ||
        (self.txtMailAddressConfirm.text == nil || [self.txtMailAddressConfirm.text isEqualToString:@""]) ||
        (self.txtPassword.text == nil || [self.txtPassword.text isEqualToString:@""]) ||
        (self.txtPasswordConfirm.text == nil || [self.txtPasswordConfirm.text isEqualToString:@""]) ||
        (self.txtName.text == nil || [self.txtName.text isEqualToString:@""])
        ) {
        
        //未入力項目あり
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:MultiLangString(@"Information")
                              message:[Utils MultiLangStringInfomation:@"I0034"]
                              delegate:nil
                              cancelButtonTitle:MultiLangString(@"Yes")
                              otherButtonTitles: nil];
        [alert show];
        [alert release];

        return;
    }
    
    if (![self.txtMailAddress.text isEqualToString:self.txtMailAddressConfirm.text]) {
        
        //mail address 不一致
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:MultiLangString(@"Information")
                              message:[Utils MultiLangStringInfomation:@"I0035"]
                              delegate:nil
                              cancelButtonTitle:MultiLangString(@"Yes")
                              otherButtonTitles: nil];
        [alert show];
        [alert release];
        
        return;
    }

    if (![self.txtPassword.text isEqualToString:self.txtPasswordConfirm.text]) {
        
        //Password address 不一致
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:MultiLangString(@"Information")
                              message:[Utils MultiLangStringInfomation:@"I0036"]
                              delegate:nil
                              cancelButtonTitle:MultiLangString(@"Yes")
                              otherButtonTitles: nil];
        [alert show];
        [alert release];
        
        return;
    }
    
    [self.txtMailAddress resignFirstResponder];
    [self.txtMailAddressConfirm resignFirstResponder];
    [self.txtPassword resignFirstResponder];
    [self.txtPasswordConfirm resignFirstResponder];
    [self.txtName resignFirstResponder];
    
    
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:self.txtMailAddress.text forKey:mailAddress_key];
    [param setObject:[[self.txtPassword.text MD5String] lowercaseString] forKey:password_key];
    [param setObject:self.txtName.text forKey:userName_Key];
   
    [[BusinessDataCtrlManager sharedInstance] createUser:param delegate:self];

  
}


#pragma mark - WorkDataControllerDelegate Delegate
- (void)createUserSucessed{
    
    [self dismissViewControllerAnimated:NO completion:nil];

    [self.delegate createUserSucessed];
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:MultiLangString(@"Information")
                          message:[Utils MultiLangStringInfomation:@"I0037"]
                          delegate:nil
                          cancelButtonTitle:MultiLangString(@"Yes")
                          otherButtonTitles: nil];
    [alert show];
    [alert release];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
