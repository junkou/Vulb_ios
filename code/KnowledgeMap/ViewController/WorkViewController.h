//
//  WorkViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/02.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppHelper.h"
//#import "InputTextViewController.h"
//#import "PhotoCameraViewController.h"
//#import "PhotoGroupViewController.h"
#import "CardListViewController.h"
#import "CardDetailViewController.h"
//#import "IllustSelectViewController.h"
#import "CreateCategoryViewController.h"
#import "UIButton+Layout.h"
#import "UserListViewController.h"

#define TITLE_VIEW_X_DELTA  8

@interface WorkViewController : BaseViewController <UIGestureRecognizerDelegate, /*UIWebViewDelegate,*/UITableViewDataSource,UITableViewDelegate>
{
    UIButton	*selectCategoryButton;
    UILayButton    *addCategoryButton;
    UIView *floatToolbar;
    UITableView *tableView;
    
    int lastSection;
    int categoryColorId;
    
//    int contentsMode;  //1:Camera 2:PhotoLaibray
    
//    PhotoCameraViewController *photoCameraViewController;
    
    NSNumber *clickThumbnailCardId;
    NSString *clickCardResourceType;
    NSString *clickCardBeginining;
   
    BOOL    fromOpenURLFlag;
    
    BOOL    fromAppear;

    UIWebView *htmlCreateWebView;
    
    NSMutableDictionary *categoryFoldState;
}

@property (nonatomic, strong) UIButton	*selectSheetButton;
@property (nonatomic, strong) UIButton	*selectCategoryButton;

@property (nonatomic) int     categoryColorId;
@property (nonatomic) int     contentsMode;
@property (nonatomic) int     currentSheetId;
@property (nonatomic, strong) NSMutableDictionary		*categoryLists;

//@property (nonatomic, assign) PhotoCameraViewController *photoCameraViewController;
@property (nonatomic, strong) NSNumber *clickThumbnailCardId;
@property (nonatomic, strong) NSString *clickCardResourceType;
@property (nonatomic, strong) NSString *clickCardBeginining;
@property (nonatomic, strong) UIView    *clickedCardThumbnailView;
@property (nonatomic) BOOL    fromOpenURLFlag;

//@property (nonatomic, strong) UIWebView *htmlCreateWebView;

- (void)setButtonProPerty:(UIButton *)button lineColor:(int)clolerId imgName:(NSString *)imgName label:(NSString *)labelText ;

- (void)setListButtonProPerty:(UIButton *)button;


@end
