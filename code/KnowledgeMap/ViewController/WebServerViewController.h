//
//  WebServerViewController.h
//  BookMaker
//
//  Created by k-kin on 10/07/08.
//  Copyright 2010 ICCESOFT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EnvDefine.h"
#import "AppHelper.h"

@interface WebServerViewController : BaseViewController <UIWebViewDelegate> {
	id delegate;
	UIWebView		*serverWebView;
	UILabel			*url_lable;
	UIButton		*returnButton;
	UIButton		*doneButton;
	UILabel			*loading_lable ;
	UIActivityIndicatorView *activityIndicator ;
	NSString *HyperLinkURLString;
	
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, retain) IBOutlet UIWebView	*serverWebView;
@property (nonatomic, retain) IBOutlet UILabel		*url_lable;
@property (nonatomic, retain) IBOutlet UIButton		*returnButton;
@property (nonatomic, retain) IBOutlet UIButton		*doneButton;
@property (nonatomic, retain) IBOutlet UILabel		*loading_lable;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) NSString *HyperLinkURLString;

-(IBAction)doReturn:(id)sender;
-(IBAction)doDone:(id)sender;

-(void)toOpenGoogleWeb;
-(void)toOpenWebWithURL:(NSString *)inUrl;
-(void)loadWebContents:(NSString *)webContents;
-(void)toOpenLoacalFile:(NSString *)fileName;


@end
