//
//  CardDetailViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/21.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppHelper.h"
#import "CardPartsDetailInfoViewController.h"
#import "CommentListViewController.h"
#import "WebServerViewController.h"
#import "HttpClientUtils.h"
#import "ViolationInputViewController.h"
#import "EditTextViewController.h"
#import "MEActionSheet.h"
#import "UIViewExt.h"

#define IS_LEFT     1
#define IS_RIGHT    2

@interface WriteCommentView : UIView
@property(nonatomic,retain,readonly) UITextView *textView;
@property(nonatomic,retain,readonly) UIButton *sendButton;
@end

@interface CardDetailViewController : BaseViewController <UITableViewDataSource,UITableViewDelegate, UIGestureRecognizerDelegate, UIWebViewDelegate,UITextViewDelegate>{
    id delegate;
    
    NSMutableArray		*dataList;
    NSMutableDictionary		*cardInfo;
 
    NSMutableArray      *userList;
    NSMutableArray      *commentList;
    
//    NSMutableDictionary		*cardDetailInfoList;
    int currentCardIndex;
    
    
    UIView *toolbarView;
    WriteCommentView *writeCommentView;
    
    NSMutableArray *rightBarItems;
    
    int contentMode;
    bool isEditing;
    
    __unsafe_unretained UIButton *lastSelectButton;
}

@property (nonatomic, assign) id delegate;

@property (nonatomic, strong) NSMutableArray		*dataList;
@property (nonatomic, strong) NSMutableDictionary		*cardInfo;
@property (nonatomic, strong) UIButton *maskView;
@property (nonatomic, strong) UILabel *commentCountLable;

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;


//@property (nonatomic, strong) NSMutableArray            *cardThumbnailList;
//@property (nonatomic, strong) NSMutableDictionary		*cardDetailInfoList;
//@property (nonatomic) int currentCardIndex;

//@property (nonatomic) int leftOrRightFlag;

@property (nonatomic, strong) UITableView            *tableView;
@end
