//
//  AllCardListViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/14.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "AllCardListViewController.h"

@interface AllCardListViewController ()

@end

@implementation AllCardListViewController
@synthesize delegate;
@synthesize categoryList;
@synthesize allCategoryAndCardDic;
@synthesize currentCategoryId;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
        self.categoryList = [[[NSMutableArray alloc] init] autorelease];
        self.allCategoryAndCardDic = [[[NSMutableDictionary alloc] init] autorelease];

        self.currentCategoryId = 0;
        self.openedSectionIds =[[[NSMutableArray alloc] init] autorelease];
        self.sectionOpenedFlag = TRUE;
        self.firstFlag = TRUE;
        
 
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [AppHelper getBaseTextColor];
    
    //Add NavigationBar Title
    [AppHelper setNavigationBarTitleLabel:self.navigationItem.titleView text:@"All cards"];
        
    [self searchCardListDatas];

}

#pragma mark - NavicationBar CancelButton Call Method
-(void)clickCancelButton:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(int)getCurrentCategoryRowNum{
    int rowNumber = 0;
    for (int i=0; i<[self.categoryList count]; i++) {
        NSMutableDictionary *info =[self.categoryList objectAtIndex:i];
        NSNumber *categoryId = [info objectForKey: categoryId_Key];
        if ([categoryId intValue] == self.currentCategoryId ) {
            rowNumber = i;
            break;
        }
    }
    
    return rowNumber;
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.categoryList count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSMutableDictionary *info = [self.categoryList objectAtIndex:section];
    return [info objectForKey:categoryName_key];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	//NSLog(@"====indexPath:%d",indexPath.row);
	return CATEGORY_LIST_HEIGHT+10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
	//NSLog(@"====indexPath:%d",indexPath.row);
    NSMutableDictionary *categoryInfo = [self.categoryList objectAtIndex:section];
    NSNumber *totalCardCount = [categoryInfo objectForKey:totalCard_key];
    
    NSNumber *categoryId = [categoryInfo objectForKey:categoryId_Key];
    NSMutableArray *cardList = [self.allCategoryAndCardDic objectForKey:categoryId];

    int cnt = 0;
    if (cardList != nil) {
        cnt = [cardList count] ;
    }
    if ((cardList != nil) && ([totalCardCount intValue] > 0) && (cnt < [totalCardCount intValue])) {
        return MORE_BUTTON_HEIGHT;
     }else{
         return 0;
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSNumber *secNum = [NSNumber numberWithInt:section];
    if ([self.openedSectionIds containsObject:secNum]) {
        NSMutableDictionary *categoryInfo = [self.categoryList objectAtIndex:section];
        int cnt = [[self.allCategoryAndCardDic objectForKey:[categoryInfo objectForKey:categoryId_Key] ] count];
        return cnt;
    }else{
        return 0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
	return CARD_HEIGHT+10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UIView *baseView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, CATEGORY_LIST_HEIGHT+10)] autorelease];
    
    NSMutableDictionary *categoryInfo = [self.categoryList objectAtIndex:section];
    UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, CATEGORY_LIST_HEIGHT)] autorelease];
    button.tag =section;
    [button addTarget:self action:@selector(clickCategorySheetButton:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [AppHelper getBaseTextColor];
    [baseView addSubview:button];
    
    UIView *lineView = [[[UIView alloc] initWithFrame:CGRectMake(0, CATEGORY_LIST_HEIGHT-1, SCREEN_WIDTH, 1)] autorelease];
    lineView.backgroundColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:0.4];
    [button addSubview:lineView];
    
    NSString *openIcon = @"icon_close_normal.png";
    for (int i=0; i<[self.openedSectionIds count]; i++) {
        if ([[self.openedSectionIds objectAtIndex:i] intValue] == section) {
            openIcon = @"icon_open_normal.png";
        }
    }
    
    [self setCategoryCellContents:categoryInfo view:button iconImage:openIcon delegate:self];
    
    return baseView;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, MORE_BUTTON_HEIGHT)] autorelease];
    //view.backgroundColor = [UIColor whiteColor];
    
    UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake(10, 2, CARD_WIDTH, MORE_BUTTON_HEIGHT)] autorelease];
    [button addTarget:self action:@selector(searchCardListDatas) forControlEvents:UIControlEventTouchUpInside];
    CGRect rect = CGRectMake(0, 0, CARD_WIDTH, 32);
    [AppHelper setCommonButtonProperti:button rect:rect title:@"More"];

    [view addSubview:button];
    
    return view;
}


-(void)clickCategorySheetButton:(UIButton *)sender{
    
    self.firstFlag = FALSE;
    
    int sectionNum = [self getCurrentCategoryRowNum];
    if (sectionNum == sender.tag ) {
        
 		NSNumber *secNum = [NSNumber numberWithInt:sender.tag];
        if ([self.openedSectionIds containsObject:secNum]) {
            [self.openedSectionIds removeObject:secNum];
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sectionNum] withRowAnimation:UITableViewRowAnimationNone];
            
            return;
            
        }
    }
    
     NSMutableDictionary *categoryData = [self.categoryList objectAtIndex:sender.tag];
    self.currentCategoryId = [[categoryData objectForKey:categoryId_Key] intValue];
    
    [self searchCardListDatas];
    
}

- (UITableViewCell *)tableView:(UITableView *)loacltableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //static NSString *CellIdentifier = @"Cell";
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d_%d",indexPath.section, [indexPath row]] ;
    
    UITableViewCell *cell = [loacltableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
     if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
         
        // Configure the cell...
        
    }
    
    [self setCellContent:cell IndexPath:indexPath];
    
    
    return cell;
}

-(void)setCellContent:(UITableViewCell *)cell IndexPath:(NSIndexPath *)indexPath{
    // セクションを取得する
    NSMutableDictionary *categoryData = [self.categoryList objectAtIndex:indexPath.section];
    
    NSArray *items = [self.allCategoryAndCardDic objectForKey:[categoryData objectForKey:categoryId_Key]];
    NSMutableDictionary *cardInfo = [items objectAtIndex:indexPath.row];
    
    NSNumber *categoryColoer = [AppHelper parseWebDataToNumber:categoryData dataKey:colorIdx_key];
    
    UIView *baseView = [[[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-CARD_WIDTH)/2, 0, CARD_WIDTH, CARD_HEIGHT)] autorelease];
    baseView.backgroundColor = [UIColor whiteColor];
    [cell.contentView addSubview:baseView];
    cell.backgroundColor = [UIColor clearColor];

    // Configure the cell...
    [AppHelper setCardInfo:cardInfo superview:baseView size:CARD_SIZE colorBar:[categoryColoer intValue] delegate:self];
    
    
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *categoryData = [self.categoryList objectAtIndex:indexPath.section];
    self.currentCategoryId =[[categoryData objectForKey:categoryId_Key] intValue];
    
    NSArray *items = [self.allCategoryAndCardDic objectForKey:[categoryData objectForKey:categoryId_Key]];
    NSMutableDictionary *cardInfo = [items objectAtIndex:indexPath.row];
    
    self.cardId = [cardInfo objectForKey:cardId_key];
    self.resourceType = [cardInfo objectForKey:resourceType_key];
    self.beginining = [cardInfo objectForKey:begining_key];
//    if ([self.resourceType isEqualToString:DB_RESOURCE_PERSON_IMG]
//        || [self.resourceType isEqualToString:DB_RESOURCE_TYPE_PDF]) {
//        if (self.beginining != nil && ![self.beginining isKindOfClass:[NSNull class]] && ![self.beginining isEqualToString:@""]) {
//            self.beginining = [self.beginining substringFromIndex:[self.beginining rangeOfString:@"?"].location+1];
//        }
//    }
    
    self.clickedCardThumbnailView = [tableView cellForRowAtIndexPath:indexPath].contentView;
    
    [[BusinessDataCtrlManager sharedInstance] requestCardInfoWithCardId:self.cardId delegate:self];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
}


- (void)setCategoryCellContents:(NSMutableDictionary *)inData view:(UIView *)inBaseView iconImage:(NSString *)openIcon delegate:(id)inDelegate{
    
    UIView *baseView = [inBaseView viewWithTag:CATEGORY_LIST_CONTENTS_VIEW_TAG];
    if (baseView != nil) {
        [baseView removeFromSuperview];
    }
    
    baseView = [[[UILabel alloc] initWithFrame:inBaseView.bounds] autorelease];
    baseView.tag = CATEGORY_LIST_CONTENTS_VIEW_TAG;
    baseView.backgroundColor = [UIColor clearColor];
    [inBaseView addSubview:baseView];
    
    CGFloat x = 0;

    //open Icon
    UIImage *iconImage =[UIImage imageNamed:[AppHelper getResourceIconPath:openIcon]];
    UIImageView *iconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, (baseView.frame.size.height-iconImage.size.height)/2, iconImage.size.width, iconImage.size.height)] autorelease];
    iconView.image = iconImage;
    [baseView addSubview:iconView];
    x +=iconImage.size.width;

    
    //Set categoryName
    
	NSString *categoryName = [inData objectForKey:categoryName_key];
    NSNumber *cardCnt = [AppHelper parseWebDataToNumber:inData dataKey:totalCard_key];

	if (categoryName == nil || [categoryName isKindOfClass:[ NSNull  class]]) {
		categoryName=@"";
	}
    categoryName = [NSString stringWithFormat:@"%@(%d)",categoryName,[cardCnt intValue]];
    
    UILabel *categoryNameLable = (UILabel *)[baseView viewWithTag:CATEGORY_NAME_LABEL_TAG];
    if (categoryNameLable != nil) {
        [categoryNameLable removeFromSuperview];
    }
	categoryNameLable = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, baseView.bounds.size.width-x, baseView.bounds.size.height)];
    categoryNameLable.tag = CATEGORY_NAME_LABEL_TAG;
	categoryNameLable.backgroundColor = [UIColor clearColor];
	categoryNameLable.font = [UIFont  boldSystemFontOfSize:20];
	categoryNameLable.textColor = [AppHelper getCategoryColor:inData];
	categoryNameLable.textAlignment = NSTextAlignmentLeft;
	categoryNameLable.numberOfLines = 1;
	categoryNameLable.text = categoryName;
	[baseView addSubview:categoryNameLable];
	[categoryNameLable release];
	
    UIView *lineView = [[[UIView alloc] initWithFrame:CGRectMake(x, baseView.bounds.size.height-0.5, baseView.bounds.size.width-x, 0.5)] autorelease];
    lineView.backgroundColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:0.4];
    [baseView addSubview:lineView];
    
}


- (void)cardInfoSucessed:(NSMutableDictionary *)cardInfo{
    
    UIView *readStatusView = [self.clickedCardThumbnailView viewWithTag:READ_STATUS_TAG];
    [readStatusView removeFromSuperview];
    
    
    NSString *contents = [cardInfo objectForKey:CONTENTS];
    // NSLog(contents);
    
    NSMutableArray *cardInfoDetailList = [[NSMutableArray alloc] init];
    [AppHelper changeCardInfoDetailList:contents output:cardInfoDetailList];
    
    CardDetailViewController *cardDetailViewController = [[CardDetailViewController alloc] init];
    cardDetailViewController.delegate = self;
    [cardDetailViewController.dataList addObjectsFromArray:cardInfoDetailList];
    cardDetailViewController.cardInfo = cardInfo;
    [self.navigationController pushViewController:cardDetailViewController animated:YES];
    [cardDetailViewController release];
    
    [cardInfoDetailList release];
    
    
}

-(void)searchCardListDatas{
    //Card一覧を取得する
    
     NSArray *items = [self.allCategoryAndCardDic objectForKey:[NSNumber numberWithInt:self.currentCategoryId]];

    self.lastPosition = [items count];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    NSMutableArray *categories = [[NSMutableArray alloc] init];
    NSMutableDictionary *category = [[NSMutableDictionary alloc] init];
    [category setObject:[NSNumber numberWithInt:self.currentCategoryId] forKey:categoryId_Key];
    [category setObject:[NSNumber numberWithInt:self.lastPosition] forKey:START];
    [category setObject:[NSNumber numberWithInt:CARD_ROW_COUNT] forKey:COUNT];
    [categories addObject:category];
    [category release];
    [param setObject:categories forKey:CATEGORYIES];
    [categories release];
    [paramDic setObject:param forKey:PARAMS];
    [param release];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BssBookAppURLAdress,METHOD_CARD_LIST];
    [[WebServerDownLoadController getInstance] sendWorkDataToWebServer:paramDic showConnectionFlag:YES serverUrl:url delegate:self];
    [paramDic release];
}



// Card一覧データを受信したとき
- (void)recieveOprationData:(WebTask*)inData
{
    //NSLog(@"%@",inData);
	NSMutableDictionary *recieveData = [inData.response JSONValue];
    
    NSMutableDictionary *result = [recieveData objectForKey:RESULT_KEY];
    
    NSString *resultStatus = [result objectForKey:RESULT_STATUS_KEY];
    
    if (resultStatus != nil && [resultStatus isEqualToString:RESULT_STATUS_VALUE_0]) {
        
        // セクション名をキーにしてそのセクションの項目をすべて取得
        NSNumber *categoryId = [NSNumber numberWithInt:self.currentCategoryId];
        
        NSMutableDictionary *data = [recieveData objectForKey:RESULT_DATA_VALUE_KEY];
        NSMutableDictionary *categoies = [data objectForKey:CATEGORYIES];
        NSMutableArray *cardList = [categoies objectForKey:[categoryId stringValue]];
        NSMutableArray *oldCardList = [self.allCategoryAndCardDic objectForKey:categoryId];
        if (oldCardList == nil) {
            oldCardList = [[[NSMutableArray alloc] init] autorelease];
            [self.allCategoryAndCardDic setObject:oldCardList forKey:categoryId];
        }
        [oldCardList addObjectsFromArray:cardList];

         int sectionNum = [self getCurrentCategoryRowNum];
        
        [self.openedSectionIds addObject:[NSNumber numberWithInt:sectionNum]];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sectionNum] withRowAnimation:UITableViewRowAnimationNone];
        
        //NSIndexPath* indexPath  = [NSIndexPath indexPathForRow:0 inSection:sectionNum];
        //[self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }else{
        NSString *errMsg = [result objectForKey:RESULT_ERROR_MESSGATE_KEY];
        
        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle:MultiLangString(@"Information")
							  message:errMsg
							  delegate:nil
							  cancelButtonTitle:MultiLangString(@"Yes")
							  otherButtonTitles: nil];
		[alert show];
		[alert release];
        
        [self.delegate loginFaild];
        
    }
    
}



@end
