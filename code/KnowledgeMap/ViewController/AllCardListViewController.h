//
//  AllCardListViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/14.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppHelper.h"
#import "AppApiDelegate.h"
#import "CardDetailViewController.h"

#define MORE_BUTTON_HEIGHT  32

@interface AllCardListViewController : BaseTableViewController {
    id delegate;
    NSMutableArray		*categoryList;
    NSMutableDictionary *allCategoryAndCardDic;
    int currentCategoryId;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) NSMutableArray	*categoryList;
@property (nonatomic, strong) NSMutableDictionary *allCategoryAndCardDic;

@property (nonatomic) int currentCategoryId;
@property (nonatomic, strong) NSIndexPath	*lastIndexPath;
@property (nonatomic, strong) NSMutableArray *openedSectionIds;
@property (nonatomic) BOOL sectionOpenedFlag;
@property (nonatomic) BOOL firstFlag;
@property int               lastPosition ;


@property (nonatomic, strong) NSNumber *cardId;
@property (nonatomic, strong) NSString *resourceType;
@property (nonatomic, strong) NSString *beginining;
@property (nonatomic, strong) UIView    *clickedCardThumbnailView;


@end
