//
//  CreateCategoryViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/07.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "CreateCategoryViewController.h"

@interface CreateCategoryViewController ()

@end

@implementation CreateCategoryViewController

@synthesize delegate;
@synthesize categoryColorId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.categoryColorId = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = [AppHelper getBaseTextColor];
    
    //NavigationBar + LeftButton
    [AppHelper setNavigationBarLeftCancelButton:self.navigationItem controller:self];

    
    //Add NavigationBar Title
    NSDictionary *sheetInfo =  [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    [AppHelper setNavigationBarTitleLabel:self.navigationItem.titleView text:[sheetInfo objectForKey:sheetName_key]];
    
    //add NavicationBar RightButton
    [AppHelper setNavigationBarRightSaveButton:self.navigationItem controller:self];

    //add Main WorkView
    [self addSelectColorView];

}


#pragma mark - NavicationBar CancelButton Call Method
-(void)clickCancelButton:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - NavicationBar SaveButton Call Method
-(void)clickSaveButton:(id)sender {
    
    if (self.txtCagegoryName.text != nil && ![self.txtCagegoryName.text isEqualToString:@""]) {
        NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
        [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId  forKey:sheetId_Key];
        [param setObject:self.txtCagegoryName.text forKey:categoryName_key];
        [param setObject:@"categorized_0"  forKey:dispCol_key];
        [param setObject:[NSNumber numberWithInt:self.categoryColorId] forKey:colorIdx_key];
        
        [self.delegate createCategory:param];

        [self dismissViewControllerAnimated:YES completion:nil];

    }

}

#pragma mark selectColor
-(void)addSelectColorView{
 
    CGFloat x = 10;
    CGFloat y = 18;
    

 	UIView *panelBase = [[[UIView alloc] initWithFrame:self.view.bounds] autorelease];
	panelBase.backgroundColor = [UIColor clearColor];
 	[self.view addSubview:panelBase ];


    //txtCagegoryName TextFiled
    self.txtCagegoryName = [[[UITextField alloc] initWithFrame:CGRectMake(x, y, SCREEN_WIDTH-x*2, 30)] autorelease];
    //self.txtCagegoryName.borderStyle = UITextBorderStyleBezel;
    self.txtCagegoryName.font = [UIFont systemFontOfSize:14];
    self.txtCagegoryName.backgroundColor = [UIColor whiteColor];
	self.txtCagegoryName.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.txtCagegoryName.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.txtCagegoryName.placeholder = MultiLangString(@"Category Name");
    [self.txtCagegoryName becomeFirstResponder];
    [panelBase addSubview:self.txtCagegoryName ];


    
	CGFloat y_text = self.txtCagegoryName.frame.origin.y+self.txtCagegoryName.frame.size.height+20;
	CGFloat width = 30;
	CGFloat height = 30;
	CGFloat x_rate = 54;
	CGFloat y_rate = 50;
    
	for (int i=0; i < 2; i++) {
		for (int j=0; j < 6; j++) {
			UIButton *colorButton = [[[UIButton alloc] initWithFrame:CGRectMake(x+j*x_rate, y_text+i*y_rate, width, height)] autorelease];
			colorButton.backgroundColor = [AppHelper getColor:(i*6+j)];
			colorButton.tag = COLOR_TYPE_BUTTON_TAG + i*6 + j;
			[colorButton addTarget:self action:@selector(clickTextColor:) forControlEvents:UIControlEventTouchUpInside];
			[panelBase addSubview:colorButton ];
		}
	}
	
	//Text
    UIImage *image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"bss_colorSampleTouched.png"]];
	UIImageView *textColorActiveSelected = [[[UIImageView alloc] initWithFrame:CGRectMake(0,0, image.size.width, image.size.height)] autorelease];
	textColorActiveSelected.tag = COLOR_ACTIVE_TEXT_BASE_TAG;
	textColorActiveSelected.image = image;
	[panelBase addSubview:textColorActiveSelected];

	[self setActiveColorPosition:[self.view viewWithTag:COLOR_TYPE_BUTTON_TAG].frame tag:COLOR_ACTIVE_TEXT_BASE_TAG];
    
    
}

-(void)setActiveColorPosition:(CGRect)inRect tag:(int)tag{
	//Select状態にする
	CGRect frame ;
	frame.origin.x = inRect.origin.x - EDIT_ACTIVE_SELECT_BASE_OFFSET;
	frame.origin.y = inRect.origin.y - EDIT_ACTIVE_SELECT_BASE_OFFSET;
	frame.size.width = inRect.size.width + EDIT_ACTIVE_SELECT_BASE_OFFSET*2;
	frame.size.height = inRect.size.height + EDIT_ACTIVE_SELECT_BASE_OFFSET*2;
	
	UIView *view = [self.view viewWithTag:tag];
	view.frame = frame;
}


-(void)clickTextColor:(id)inSender{
	UIView *colorButton = (UIView *)inSender;
	[self setActiveColorPosition:colorButton.frame tag:COLOR_ACTIVE_TEXT_BASE_TAG];
	
	self.categoryColorId = colorButton.tag - COLOR_TYPE_BUTTON_TAG;
    self.txtCagegoryName.textColor = [AppHelper getColor:self.categoryColorId];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
