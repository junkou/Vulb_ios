//
//  InputTextfiledViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/10.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "InputTextfiledViewController.h"

@interface InputTextfiledViewController ()

@end

@implementation InputTextfiledViewController
@synthesize delegate;
@synthesize txtField;
@synthesize txtFieldPlaceholder;
@synthesize txtFieldValue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    //add NavicationBar RightButton
    [AppHelper setNavigationBarRightSaveButton:self.navigationItem controller:self];

    self.view.backgroundColor = [AppHelper getBaseTextColor];

    // TextFiled
    self.txtField = [[UITextField alloc] initWithFrame:CGRectMake(10, 20, SCREEN_WIDTH-20, 30)];
    //self.txtField.borderStyle = UITextBorderStyleBezel;
    self.txtField.backgroundColor = [UIColor whiteColor];
	self.txtField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.txtField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.txtField.keyboardType = UIKeyboardTypeEmailAddress;
    self.txtField.placeholder = self.txtFieldPlaceholder;
    self.txtField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.txtField becomeFirstResponder];
    self.txtField.text = self.txtFieldValue;
    [self.view addSubview:self.txtField];

}

#pragma mark - NavicationBar SaveButton Call Method
-(void)clickSaveButton:(id)sender {
    
    if (self.txtField.text != nil && ![self.txtField.text isEqualToString:@""]) {
        
        [self.delegate textValueConfirm:self.txtField.text];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
