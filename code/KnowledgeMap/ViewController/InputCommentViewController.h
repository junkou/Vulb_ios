//
//  InputCommentViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/07/31.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppHelper.h"

@interface InputCommentViewController : BaseViewController<UITextViewDelegate>{
    id delegate;
    NSNumber *cardId;
    NSDictionary *commnetInfo;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) NSNumber *cardId;
@property (nonatomic, strong) NSDictionary *commnetInfo;
@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, strong) UITextView *txtView;

@end
