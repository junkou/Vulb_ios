//
//  UserListViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/20.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "UserListViewController.h"
#import "AppDelegate.h"
#import "UIButton+Layout.h"
#import "InviteUserListViewController.h"
#import "UserListCell.h"

@interface UserListViewController ()

@end

@implementation UserListViewController

@synthesize delegate;
@synthesize invitedUserList;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [AppHelper getBaseTextColor];
    self.navigationItem.title = [[BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId] objectForKey:sheetName_key];
    
   
    editButton = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)] autorelease];
    [editButton setTitle:NSLocalizedString(@"Edit", @"Edit") forState:UIControlStateNormal];
    [editButton addTarget:self action:@selector(clickEditButton:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:editButton] autorelease];
    
    tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorInset = UIEdgeInsetsZero;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];

    addUserButton = [[[UILayButton alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)] autorelease];
    addUserButton.backgroundColor = [UIColor whiteColor];
    addUserButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    [addUserButton setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"Add User", @"Add User")] forState:UIControlStateNormal];
    [addUserButton setTitleColor:COLOR_ADD_BUTTON_TEXT forState:UIControlStateNormal];
    [addUserButton addTarget:self action:@selector(clickInviteUserButton:) forControlEvents:UIControlEventTouchUpInside];
    [addUserButton setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_edit_addfriends_normal.png"]] forState:UIControlStateNormal];
    [addUserButton setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_edit_addfriends_touch.png"]] forState:UIControlStateNormal];
    addUserButton.layout = ImageLeftTextRight;
    [self.view addSubview:addUserButton];

    UISwipeGestureRecognizer *swipeRightGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToRight:)] autorelease];
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	swipeRightGesture.numberOfTouchesRequired = 1;
    swipeRightGesture.delegate = self;
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (tableView.isEditing)
    {
        [[AppDelegate sharedInstance] setTabbarHidden:YES animated:NO];
    }
    else
    {
        [[AppDelegate sharedInstance] setTabbarHidden:NO animated:NO];
    }
}

#pragma mark - clickInviteUserButton
-(void)clickInviteUserButton:(id)sender
{
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId  forKey:sheetId_Key];
    
    [[BusinessDataCtrlManager sharedInstance] requestReferenceSheetSetting:param delegate:self];
}

- (void)referenceSheetSettingSucessed:(NSMutableDictionary *)Info{
    
//    CreateSheetViewController *createSheetViewController = [[[CreateSheetViewController alloc] init] autorelease];
//    createSheetViewController.delegate = self;
//    createSheetViewController.operationType = SHEET_OPERATION_TYPE_EDIT;
//    
//    NSDictionary *sheetInfo =  [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
//    createSheetViewController.viewTitle = [sheetInfo objectForKey:sheetName_key];
//    
//    NSString *shareFlag = [Info objectForKey:shareFlg_key];
//    if (shareFlag != nil && [shareFlag isEqualToString:@"1"]) {
//        createSheetViewController.shareFlag = TRUE;
//    }
//    
//    //招待リスト
//    createSheetViewController.invitedUserList = [[[NSMutableArray alloc] initWithArray:self.invitedUserList] autorelease];
//    
//    //シート設定情報
//    createSheetViewController.sheetSettingInfo = [[[NSMutableDictionary alloc] initWithDictionary:Info] autorelease];
//
//    [self.navigationController pushViewController:createSheetViewController animated:YES];
    InviteUserListViewController *controller = [[InviteUserListViewController alloc] init];
    NSDictionary *sheetInfo =  [BusinessDataCtrlManager getSheetInfo:[BusinessDataCtrlManager getUserInfo].lastSheetId];
    controller.navigationItem.title = [sheetInfo objectForKey:sheetName_key];
    controller.delegate = self;
    NSString *shareFlag = [Info objectForKey:shareFlg_key];
    if (shareFlag != nil && [shareFlag isEqualToString:@"1"])
    {
        controller.shareMode = TRUE;
    }
    
    //招待リスト
    controller.invitedUserList = [[[NSMutableArray alloc] initWithArray:self.invitedUserList] autorelease];
    [self.navigationController pushViewController:controller animated:YES];
    [[AppDelegate sharedInstance] setTabbarHidden:YES animated:YES];
}

#pragma mark - AppApiDelegate delegate
- (void)createNewSheetOrUpdateSheetSucessed:(NSMutableDictionary *)Info{
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId  forKey:sheetId_Key];
    
    [[BusinessDataCtrlManager sharedInstance] requestSheetMemberList:param delegate:self];

}

- (void)sheetMemberListSucessed:(NSArray *)memberList{
    self.invitedUserList = [[[NSMutableArray alloc] initWithArray:memberList] autorelease];
    [tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.invitedUserList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"====indexPath:%d",indexPath.row);
	return USER_ICON_SIZE+16;
}

- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *info = [self.invitedUserList objectAtIndex:[indexPath row]];
    // NSNumber *resourceId = [Utils parseWebDataToNumber:cardData dataKey:resourceId_key];
    
    NSString *CellIdentifier = @"Cell";
    UserListCell *cell = [table dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
     */
    
    if (cell == nil) {
        cell = [[[UserListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [cell setUserInfo:info selected:NO];
    return cell;
    
}

//- (void)setCellDataContents:(NSMutableDictionary *)inData view:(UIView *)baseView {
//    
//    CGFloat x = 16;
//    
//    //User Icon
//    UIImageView *UserIconView;
//    CGFloat iconSize = USER_ICON_SIZE;
//    UserIconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, (baseView.frame.size.height-iconSize)/2, iconSize, iconSize)] autorelease];
//    UserIconView.layer.cornerRadius = 5.0f;
//    UserIconView.clipsToBounds = YES;
//    UserIconView.image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"defaultProfImg.png"]];
//    NSNumber *userId = [inData objectForKey:userId_key];
//    [baseView addSubview:UserIconView];
//    [AppHelper setUserIconWith:[userId intValue] imageView:UserIconView];
//    
//    x += iconSize +10 ;
//    
//    NSString *notJoinedStasus = [inData objectForKey:memberStatus_Key];
//    if (notJoinedStasus != nil && ![notJoinedStasus isKindOfClass:[ NSNull class]] && [notJoinedStasus isEqualToString:@"1"]) {
//        
//        //Set UserName
//        NSString *userName = [inData objectForKey:userName_Key];
//        if (userName == nil || [userName isKindOfClass:[ NSNull  class]]) {
//            userName=@"";
//        }
//        UILabel *userNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(x, UserIconView.frame.origin.y, SCREEN_WIDTH-x-40, iconSize/2)] autorelease];
//        userNameLable.backgroundColor = [UIColor clearColor];
//        userNameLable.font = [UIFont  boldSystemFontOfSize:20];
//        userNameLable.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
//        userNameLable.textAlignment = NSTextAlignmentLeft;
//        userNameLable.numberOfLines = 1;
//        userNameLable.text = userName;
//        [baseView addSubview:userNameLable];
//
//        //is Jointed ?
//        UILabel *jointedLable = (UILabel *)[baseView viewWithTag:MEMBER_LIST_DATA_TAG];
//        jointedLable = [[[UILabel alloc] initWithFrame:CGRectMake(x, UserIconView.frame.origin.y+iconSize/2, SCREEN_WIDTH-x-40, iconSize/2)] autorelease];
//        jointedLable.backgroundColor = [UIColor clearColor];
//        jointedLable.font = [UIFont  systemFontOfSize:10];
//        jointedLable.textColor = [UIColor blueColor];
//        jointedLable.textAlignment = NSTextAlignmentLeft;
//        jointedLable.numberOfLines = 1;
//        jointedLable.text = MultiLangString(@"Not joined");
//        [baseView addSubview:jointedLable];
//
//    }else{
//        //Set UserName
//        NSString *userName = [inData objectForKey:userName_Key];
//        if (userName == nil || [userName isKindOfClass:[ NSNull  class]]) {
//            userName=@"";
//        }
//        UILabel *userNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(x, UserIconView.frame.origin.y, SCREEN_WIDTH-x-40, iconSize)] autorelease];
//        userNameLable.backgroundColor = [UIColor clearColor];
//        userNameLable.font = [UIFont  boldSystemFontOfSize:20];
//        userNameLable.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
//        userNameLable.textAlignment = NSTextAlignmentLeft;
//        userNameLable.numberOfLines = 1;
//        userNameLable.text = userName;
//        [baseView addSubview:userNameLable];
//    }
//	
//    
//}
//
#pragma mark - Table view delegate
- (void)tableView:(UITableView *)table didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [table deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - PanGestureRecognizer
-(void)viewDidDraggingToRight:(UISwipeGestureRecognizer *)swipe{
    //    NSLog(@"========handleRightSwipeGesture========");
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)clickEditButton:(id)sender
{
    [tableView setEditing:!tableView.editing];
    if (tableView.isEditing)
    {
        [[AppDelegate sharedInstance] setTabbarHidden:YES animated:YES];
        [UIView animateWithDuration:0.3 animations:^{
            
            addUserButton.frame = CGRectMake(0, self.view.bounds.size.height - addUserButton.frame.size.height, addUserButton.frame.size.width, addUserButton.frame.size.height);
            
            tableView.frame = CGRectMake(0, 0, tableView.frame.size.width, self.view.frame.size.height-addUserButton.frame.size.height);
        }];
    }
    else
    {
        [[AppDelegate sharedInstance] setTabbarHidden:NO animated:YES];
        [UIView animateWithDuration:0.3 animations:^{
            
            addUserButton.frame = CGRectMake(0, self.view.bounds.size.height, addUserButton.frame.size.width, addUserButton.frame.size.height);
            
            tableView.frame = CGRectMake(0, 0, tableView.frame.size.width, self.view.frame.size.height);
        }];
    }
}
@end
