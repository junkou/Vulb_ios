    //
//  WebServerViewController.m
//  BookMaker
//
//  Created by k-kin on 10/07/08.
//  Copyright 2010 ICCESOFT. All rights reserved.
//

#import "WebServerViewController.h"

@implementation WebServerViewController

@synthesize delegate;
@synthesize serverWebView;
@synthesize url_lable;
@synthesize returnButton;
@synthesize doneButton;
@synthesize loading_lable;
@synthesize activityIndicator;
@synthesize HyperLinkURLString;

int urlListCount = 1;
BOOL backFlag = FALSE;
BOOL returnFlag = FALSE;

- (void)viewDidLoad {
    [super viewDidLoad];

	self.HyperLinkURLString = @"";
	self.url_lable.text		= @"";
	self.url_lable.backgroundColor = [UIColor clearColor];
	self.url_lable.textColor = [UIColor whiteColor];
	
	self.serverWebView.scalesPageToFit = YES;
	
    //NavigationBar + LeftButton
    [AppHelper setNavigationBarLeftCancelButton:self.navigationItem controller:self];
 }

-(void)clickCancelButton:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)doReturn:(id)sender{	
	if (self.serverWebView.canGoBack) {
		[self.serverWebView goBack];
	}else {
		self.doneButton.hidden = NO;
		[self.serverWebView stopLoading];
	}
}

-(IBAction)doDone:(id)sender{
	[self.serverWebView stopLoading];
}

-(void)toOpenGoogleWeb{
	NSURL *theURL = [NSURL URLWithString:@"http://www.google.com"];
	[self.serverWebView loadRequest:[NSURLRequest requestWithURL:theURL]];	
}

-(void)toOpenWebWithURL:(NSString *)inUrl{
	//self.doneButton.hidden = YES;
	self.url_lable.text = inUrl;
	NSURL *theURL = [NSURL URLWithString:inUrl];
	[self.serverWebView loadRequest:[NSURLRequest requestWithURL:theURL]];	
}

-(void)toOpenLoacalFile:(NSString *)fileName{
	//self.doneButton.hidden = YES;
    
    NSString *appFile = [AppHelper getLocalCatchfilePath:fileName];

    if (appFile == nil || [appFile isKindOfClass:[NSNull class]] ) {
        return;
    }

	NSURL *theURL = [NSURL fileURLWithPath:appFile];
    

	[self.serverWebView loadRequest:[NSURLRequest requestWithURL:theURL]];
}


-(void)loadWebContents:(NSString *)webContents{
    [self.serverWebView loadHTMLString:webContents baseURL:nil];  
}


- (void)webViewDidFinishLoad:(UIWebView *)webView{
	self.loading_lable.hidden = YES;
	[self.activityIndicator stopAnimating];
	self.activityIndicator.hidden = YES;

}	

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
	self.HyperLinkURLString = [[[request URL] standardizedURL] absoluteString];	
	self.url_lable.text = self.HyperLinkURLString;
	if ([self.activityIndicator isAnimating]) {
	}else {
		self.activityIndicator.hidden = NO;
		[self.activityIndicator startAnimating];
	}	
	//NSLog(self.HyperLinkURLString);
	return YES;
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[returnButton release];
	[doneButton release];
	[loading_lable release];
	[activityIndicator release];
	serverWebView.delegate = nil;
	self.delegate = nil;
	[serverWebView release];
	[self.HyperLinkURLString release];
	
	[super dealloc];

}

@end
