//
//  SheetListExtViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/09/27.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppHelper.h"
#import "AppApiDelegate.h"
#import "DownLoadSimpleWebDataServerController.h"
#import "AllCardListViewController.h"
#import "UIButton+Layout.h"

@interface SheetListExtViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource, AppApiDelegate>{
    id delegate;
	
    int currentGroupIndex;
    
    UIButton *editButton;
    UIButton *profileButton;
    UILayButton *addSheetButton;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic) int currentSheetId;
@property (nonatomic, strong) NSMutableDictionary		*categoryLists;
@property (nonatomic, strong) NSIndexPath	*lastIndexPath;
@property (nonatomic, strong) NSMutableArray *sheetList;
@property (nonatomic, strong) NSMutableArray *openedSectionIds;
@property int clickedSectionRow;
//@property (nonatomic, strong) NSIndexPath *beforeIndexPath;
//@property (nonatomic, strong) NSIndexPath *clickedindexPath;
@property (nonatomic) BOOL sectionOpenedFlag;
@property (nonatomic) BOOL createCategoryFlag;
@property (nonatomic, strong) NSString *methodName;
@property (nonatomic, strong) NSMutableDictionary		*categoryStatisticsLists;


-(void) initWorkData;

@end
