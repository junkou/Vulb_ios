//
//  UICardButton.m
//  KnowledgeMap
//
//  Created by KouJun on 4/3/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import "UICardButton.h"
#import "AppHelper.h"
#import "AppDefine.h"

@implementation UICardInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.backgroundColor = [UIColor clearColor];
        
        defaultLogo = [[UIImage imageNamed:[AppHelper getResourceIconPath:@"defaultProfImg.png"]] retain];
        cardButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 10, CARD_WIDTH, CARD_HEIGHT)];
        [cardButton addTarget:self action:@selector(cardButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:cardButton];
        [cardButton release];
        
        commentBg = [[UIImageView alloc] initWithFrame:CGRectMake(5+CARD_WIDTH+5, 10, 110, CARD_HEIGHT)];
        commentBg.image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"comment_bg.png"]];
        commentBg.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:commentBg];
        [commentBg release];
        
        UIImageView *rightBg = [[UIImageView alloc] initWithFrame:CGRectMake(5+CARD_WIDTH+5, 5, 110, CARD_HEIGHT-10)];
        rightBg.image = [[UIImage imageNamed:[AppHelper getResourceIconPath:@"base_talk_friend_normal.png"]] resizableImageWithCapInsets:UIEdgeInsetsMake(30, 10, 8, 20) resizingMode:UIImageResizingModeStretch];
        [self.contentView addSubview:rightBg];
        
        userLogo = [[UIImageView alloc] initWithFrame:CGRectMake(commentBg.frame.origin.x + 9, 15, 30, 30)];
        userLogo.image = defaultLogo;
        [self.contentView addSubview:userLogo];
        [userLogo release];
        
        userName = [[UILabel alloc] initWithFrame:CGRectMake(userLogo.frame.origin.x + userLogo.frame.size.width + 5, 10, 90, 20)];
        userName.textColor = COLOR_CARD_USER_NAME;
        userName.font = [UIFont systemFontOfSize:10];
        userName.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:userName];
        [userName release];

        time = [[UILabel alloc] initWithFrame:CGRectMake(userName.frame.origin.x, 30, 100, 15)];
        time.backgroundColor = [UIColor clearColor];
        time.font = [UIFont systemFontOfSize:10];
        time.textColor = [UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1];
        [self.contentView addSubview:time];
        [time release];
        
        author = [[UILabel alloc] initWithFrame:CGRectMake(time.frame.origin.x, 40, 105, 20)];
        author.backgroundColor = [UIColor clearColor];
        author.font = [UIFont systemFontOfSize:10];
        author.text = MultiLangString(@"Author");
        author.textColor = [UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1];
        [self.contentView addSubview:author];
        [author release];
        
        comment = [[UILabel alloc] initWithFrame:CGRectMake(userLogo.frame.origin.x, 70, 105, CARD_HEIGHT-40)];
        comment.backgroundColor = [UIColor clearColor];
        comment.font = [UIFont systemFontOfSize:12];
        comment.textColor = [UIColor colorWithRed:40/255.0 green:40/255.0 blue:40/255.0 alpha:1];
        [self.contentView addSubview:comment];
        [comment release];
    }
    return self;
}

- (void)dealloc
{
    [_cardId release];
    [_cardInfo release];
    [_categoryInfo release];
    [defaultLogo release];
    [super dealloc];
}

- (void)setCardInfo:(NSDictionary*)cardInfo categoryInfo:(NSDictionary*)categoryInfo delegate:(id)delegate
{
    if (_cardInfo != cardInfo)
    {
        [_cardInfo release];
        _cardInfo = [cardInfo retain];
        
        [_cardId release];
        _cardId = [[cardInfo objectForKey:cardId_key] retain];
    }
    
    if (_categoryInfo != categoryInfo)
    {
        _categoryInfo = categoryInfo;
        _categoryInfo = [categoryInfo retain];
        
    }
    
    for (UIView *v in cardButton.subviews)
    {
        [v removeFromSuperview];
    }
    cardButton.tag = [[_cardInfo objectForKey:@"cardId"] longValue];
    userLogo.image = defaultLogo;
    time.text  = [Utils getShowTime:[_cardInfo objectForKey:@"valuationUpdateAt"]];
    
    [AppHelper setCardInfo:cardInfo superview:cardButton size:CARD_SIZE colorBar:[[AppHelper parseWebDataToNumber:categoryInfo dataKey:colorIdx_key] intValue] delegate:self];
    
//    if ([[_cardInfo objectForKey:@"commentCount"] intValue] > 0)
//    {
//        [AppHelper setUserIconWith:[[_cardInfo objectForKey:@"createUserId"] longLongValue] imageView:userLogo];
//
//        commentBg.hidden = NO;
//        commentButton.hidden = NO;
//        author.hidden = YES;
//        
//        comment.text = [_cardInfo objectForKey:@"comment"];
//        userName.text = @"UserName";
//    }
//    else
    {
        [AppHelper setUserIconWith:[[_cardInfo objectForKey:@"createUserId"] longLongValue] imageView:userLogo];

        commentBg.hidden = YES;
        commentButton.hidden = YES;
        author.hidden = NO;
        
        userName.text = [BusinessDataCtrlManager getUserInfo].userName;
    }
}

- (void)cardButtonClick:(id)sender
{
    
}

- (void)nameClick:(id)sender
{
    
}
@end
