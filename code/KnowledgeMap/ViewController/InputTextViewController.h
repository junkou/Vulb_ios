//
//  InputTextViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/02.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppHelper.h"
#import "AppApiDelegate.h"

@interface InputTextViewController : BaseViewController <AppApiDelegate,UITextViewDelegate>{
    id delegate;
    int     categoryColorId;

}

@property (nonatomic, assign) id delegate;
@property (nonatomic) int     categoryColorId;

@property (nonatomic, strong) UIButton *saveButton;

-(UIView *)createTextBaseView;
-(void)saveAnimation;

@end
