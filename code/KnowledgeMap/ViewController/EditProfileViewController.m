//
//  EditProfileViewController.m
//  KnowledgeMap
//
//  Created by KouJun on 5/19/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import "EditProfileViewController.h"

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [AppHelper getBaseTextColor];
    
    scrollView = [[[UIScrollView alloc] initWithFrame:self.view.bounds] autorelease];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    scrollView.pagingEnabled = YES;
    
    int x = 10,y = 10;
    logoBtn = CreateView(UIButton, x, y, 60, 60);
    logoBtn.backgroundColor = [UIColor whiteColor];
    [logoBtn addTarget:self action:@selector(logoClick:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:logoBtn];
    [logoBtn release];
    x += 60 + 10;
    
    nameField = CreateView(UITextField, x, y+20, self.view.bounds.size.width- x - 10, 30);
    nameField.backgroundColor = [UIColor whiteColor];
    nameField.placeholder = MultiLangString(@"Name");
    nameField.delegate = self;
    [scrollView addSubview:nameField];
    [nameField release];
    x = 10;
    y += 60;
    
    categoryLabel = CreateView(UILabel, x, y, 300, 20);
    categoryLabel.backgroundColor = [UIColor clearColor];
    categoryLabel.text = MultiLangString(@"ProfileCategory");
    categoryLabel.textColor = [UIColor blackColor];
    [scrollView addSubview:categoryLabel];
    [categoryLabel release];
    y += categoryLabel.bounds.size.height + 10;
    
    categoryField = CreateView(UITextField, x, y, self.view.bounds.size.width- x - 10, 30);
    categoryField.backgroundColor = [UIColor whiteColor];
    categoryField.delegate = self;
    [scrollView addSubview:categoryField];
    [categoryField release];
    y += categoryField.bounds.size.height + 10;
    
    genderLabel = CreateView(UILabel, x, y, 300, 20);
    genderLabel.backgroundColor = [UIColor clearColor];
    genderLabel.text = MultiLangString(@"Gender");
    genderLabel.textColor = [UIColor blackColor];
    [scrollView addSubview:genderLabel];
    [genderLabel release];
    y += genderLabel.bounds.size.height + 10;
    
    genderField = CreateView(UITextField, x, y, self.view.bounds.size.width- x - 10, 30);
    genderField.backgroundColor = [UIColor whiteColor];
    genderField.delegate = self;
    [scrollView addSubview:genderField];
    [genderField release];
    y += genderField.bounds.size.height + 10;

    birthLabel = CreateView(UILabel, x, y, 300, 20);
    birthLabel.backgroundColor = [UIColor clearColor];
    birthLabel.text = MultiLangString(@"Birthday");
    birthLabel.textColor = [UIColor blackColor];
    [scrollView addSubview:birthLabel];
    [birthLabel release];
    y += birthLabel.bounds.size.height + 10;
    
    birthField = CreateView(UITextField, x, y, self.view.bounds.size.width- x - 10, 30);
    birthField.backgroundColor = [UIColor whiteColor];
    birthField.delegate = self;
    [scrollView addSubview:birthField];
    [birthField release];
    y += birthField.bounds.size.height + 10;

    careerLabel = CreateView(UILabel, x, y, 300, 20);
    careerLabel.backgroundColor = [UIColor clearColor];
    careerLabel.text = MultiLangString(@"Career");
    [scrollView addSubview:careerLabel];
    [careerLabel release];
    y += careerLabel.bounds.size.height + 10;
    
    careerField = CreateView(UITextField, x, y, self.view.bounds.size.width- x - 10, 30);
    careerField.backgroundColor = [UIColor whiteColor];
    careerField.delegate = self;
    [scrollView addSubview:careerField];
    [careerField release];
    y += careerField.bounds.size.height + 10;
    
    addrLabel = CreateView(UILabel, x, y, 300, 20);
    addrLabel.backgroundColor = [UIColor clearColor];
    addrLabel.text = MultiLangString(@"Addr");
    [scrollView addSubview:addrLabel];
    [addrLabel release];
    y += addrLabel.bounds.size.height + 10;
    
    addrField = CreateView(UITextField, x, y, self.view.bounds.size.width- x - 10, 30);
    addrField.backgroundColor = [UIColor whiteColor];
    addrField.delegate = self;
    [scrollView addSubview:addrField];
    [addrField release];
    y += addrField.bounds.size.height + 10;

    xxxLabel = CreateView(UILabel, x, y, 300, 20);
    xxxLabel.backgroundColor = [UIColor clearColor];
    xxxLabel.text = MultiLangString(@"xxx");
    [scrollView addSubview:xxxLabel];
    [xxxLabel release];
    y += xxxLabel.bounds.size.height + 10;
    
    xxxField = CreateView(UITextField, x, y, self.view.bounds.size.width- x - 10, 30);
    xxxField.backgroundColor = [UIColor whiteColor];
    xxxField.delegate = self;
    [scrollView addSubview:xxxField];
    [xxxField release];
    y += xxxField.bounds.size.height + 10;
    
    accountLabel = CreateView(UILabel, x, y, 300, 20);
    accountLabel.backgroundColor = [UIColor clearColor];
    accountLabel.text = MultiLangString(@"account name");
    [scrollView addSubview:accountLabel];
    [accountLabel release];
    y += accountLabel.bounds.size.height + 10;
    
    mailChangeBtn = CreateView(UIButton, x, y, 300, 40);
    mailChangeBtn.backgroundColor = [UIColor whiteColor];
    [mailChangeBtn addTarget:self action:@selector(mailChangeClick:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:mailChangeBtn];
    [mailChangeBtn release];
    y += mailChangeBtn.bounds.size.height + 10;
    
    pwdChangeBtn = CreateView(UIButton, x, y, 300, 40);
    pwdChangeBtn.backgroundColor = [UIColor whiteColor];
    [pwdChangeBtn addTarget:self action:@selector(pwdChangeClick:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:pwdChangeBtn];
    [pwdChangeBtn release];
    y += pwdChangeBtn.bounds.size.height + 10;
    
    [scrollView setContentSize:CGSizeMake(scrollView.bounds.size.width, y)];
    
    [self.view addSubview:scrollView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
