//
//  UICardButton.h
//  KnowledgeMap
//
//  Created by KouJun on 4/3/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UICardInfoCell : UITableViewCell
{
    UIButton *cardButton;
    UIImageView *userLogo;
    UILabel *userName;
    UIButton *commentButton;
    UILabel *time;
    UILabel *comment;
    UIImageView *commentBg;
    UILabel *author;
    
    UIImage *defaultLogo;
}

@property(nonatomic, strong, readonly) NSNumber *cardId;
@property(nonatomic, strong, readonly) NSDictionary *cardInfo;
@property(nonatomic, strong, readonly) NSDictionary *categoryInfo;

- (void)setCardInfo:(NSDictionary*)cardInfo categoryInfo:(NSDictionary*)categoryInfo delegate:(id)delegate;
@end