//
//  UserListViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/20.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppHelper.h"
#import "AppApiDelegate.h"
#import "CreateSheetViewController.h"
#import "UIButton+Layout.h"

@interface UserListViewController : BaseViewController <UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate>{
    id delegate;
    
    UIButton    *editButton;
    UILayButton    *addUserButton;
    UITableView *tableView;
    
    NSMutableArray		*invitedUserList;
    
}

@property (nonatomic, assign) id delegate;

@property (nonatomic, strong) NSMutableArray		*invitedUserList;

@end
