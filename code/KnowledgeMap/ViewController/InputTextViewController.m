//
//  InputTextViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/02.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "InputTextViewController.h"
#import "UploadToWebServerSYNController.h"

@interface InputTextViewController ()

@end

@implementation InputTextViewController

@synthesize delegate;
@synthesize categoryColorId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //NavigationBar + LeftButton
    [AppHelper setNavigationBarLeftCancelButton:self.navigationItem controller:self];

    //add NavicationBar RightButton
    self.saveButton = [AppHelper setNavigationBarRightSaveButton:self.navigationItem controller:self];
    self.saveButton.enabled = NO;

    self.view.backgroundColor = [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0];
    
    //BackgroundImage
    UIImageView *baseImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    baseImageView.tag = WORK_VIEW_BASE_BACK_VIEW_TAG;
    baseImageView.image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"bss_back_image.png"]];
    [self.view addSubview:baseImageView];
    [baseImageView release];


    [self createTextBaseView];
    
}

-(UIView *)createTextBaseView{
    UIView *textBaseView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, SCREEN_HEIGHT-TOOL_BAR_HIGHT- KEY_BOARD_HEIGHT)];
    //textBaseView.backgroundColor = [UIColor redColor];
    textBaseView.tag = TEXT_INPUT_VIEW_TAG;
    textBaseView.layer.cornerRadius = 5.0f;
    textBaseView.clipsToBounds = YES;
    [self.view addSubview:textBaseView];
	[textBaseView release];
    
	// ColorBar
    CGFloat cplorBarWidth = COLOR_BAR_WIDTH*1.5;
    UIView *colorBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cplorBarWidth, textBaseView.bounds.size.height)];
    colorBar.backgroundColor = [AppHelper getColor:self.categoryColorId];
    [textBaseView addSubview:colorBar];
	[colorBar release];
    
    //TextFiled
    UITextView *textView = [[[UITextView alloc] initWithFrame:CGRectMake(cplorBarWidth, 0, textBaseView.bounds.size.width-cplorBarWidth, textBaseView.bounds.size.height)] autorelease];
    textView.tag = TEXT_INPUT_VAULE_VIEW_TAG;
    textView.backgroundColor = [UIColor whiteColor];
    textView.font = [UIFont systemFontOfSize:16];
    textView.delegate = self;
    [textBaseView addSubview:textView];
    [textView becomeFirstResponder];
    
    //Shadow
    UIImage *image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"card_drop_shadow.png"]];
    UIImageView *shadowImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(-10, textBaseView.bounds.size.height, textView.bounds.size.width+20, image.size.height)] autorelease];
    shadowImageView.image =image;
    [textView addSubview:shadowImageView];

    return textBaseView;

}

#pragma mark - NavicationBar CancelButton Call Method
-(void)clickCancelButton:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - NavicationBar SaveButton Call Method
-(void)clickSaveButton:(id)sender {
    
    [self saveAnimation];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)saveAnimation{
    UIView *textBaseView = [self.view viewWithTag:TEXT_INPUT_VIEW_TAG];
        //Cardデータも作成し、DBに保存する。
    UITextView *textView = (UITextView *)[textBaseView viewWithTag:TEXT_INPUT_VAULE_VIEW_TAG];
    NSString *stringValue = textView.text;
    
    NSDataDetector *dataDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *resultArray = [dataDetector matchesInString:stringValue
                                                 options:0
                                                   range:NSMakeRange(0,[stringValue length])];

    if ([ resultArray count] != 0) {
        for (NSTextCheckingResult *result in resultArray)
        {
            if ([result resultType] == NSTextCheckingTypeLink)
            {
                NSURL *url = [result URL];
                NSString *urlDescription = [url description];
                NSString *changedUrlDescription = [NSString stringWithFormat:WEBLINK_META, urlDescription, urlDescription];
                stringValue = [stringValue stringByReplacingOccurrencesOfString:urlDescription withString:changedUrlDescription];

                //NSLog(@"%@",stringValue);
            }
        }
    }

    NSString *contents = [CONTENTS_META_TEXT stringByReplacingOccurrencesOfString:CONTENTS_META_DUMMY withString:stringValue];
    
    contents = [contents stringByReplacingMatchesOfPattern:@"\n" withString:@"<br>"];

    
    
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[BusinessDataCtrlManager getUserInfo].lastSheetId forKey:sheetId_Key];
    [param setObject:[BusinessDataCtrlManager getUserInfo].lastCategoryId forKey:categoryId_Key];
    
    [AppHelper careateCardDataToSave:param contents:contents sync_status:SYNC_STATUS_ON option:@""];
    
    CGRect rect = textBaseView.frame;
    if ([Utils isIOS7orLater]) {
        rect.origin.y = rect.origin.y+self.view.frame.origin.y;
    }else{
        rect.origin.y = rect.origin.y+TOOL_BAR_HIGHT+20;
    }
    textBaseView.frame = rect;
    textBaseView.tag = TEXT_INPUT_VIEW_OLD_TAG;
    
    UIView *newBaseView = [self createTextBaseView];
    newBaseView.alpha = 0.0;
    
    UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
    [mainWindow addSubview:textBaseView];
    rect.origin.y = -rect.size.height;
    
    [UIView beginAnimations:@"saveAnimation" context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(endSaveAnimation)];
    textBaseView.frame = rect;
    newBaseView.alpha = 1.0;
    [UIView commitAnimations];
}

-(void)endSaveAnimation{
    UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *txtBaseView = [mainWindow viewWithTag:TEXT_INPUT_VIEW_OLD_TAG];
    [txtBaseView removeFromSuperview];
    
    UITextView *textView = (UITextView *)[self.view viewWithTag:TEXT_INPUT_VAULE_VIEW_TAG];
    [textView becomeFirstResponder];
    
    //データをサーバーへのアップ処理を起動する
    [[[UploadToWebServerSYNController getUploadServerInstance] initData:self] startUpload];
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
     return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

#pragma mark  UITextViewのデリゲートメソッド
- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text != nil && ![textView.text isEqualToString:@""]) {
        self.saveButton.enabled = YES;
    }else{
        self.saveButton.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Memory management
- (void)dealloc {
    self.delegate = nil;
    
	[super dealloc];
    
	
}


@end
