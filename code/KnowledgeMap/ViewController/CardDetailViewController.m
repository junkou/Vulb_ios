//
//  CardDetailViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/21.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "CardDetailViewController.h"
#import "AppDelegate.h"
#import "CreateCardViewController.h"

#define TEXTVIEW_HEIGHT_EDITING 80
#define TEXTVIEW_HEIGHT_NORMAL  40

@implementation WriteCommentView

-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        _sendButton = [[UIButton alloc] initWithFrame:CGRectMake(self.bounds.size.width-55, self.bounds.size.height-TEXTVIEW_HEIGHT_NORMAL+5, 45, TEXTVIEW_HEIGHT_NORMAL-10)];
        _sendButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [_sendButton setTitle:MultiLangString(@"Send") forState:UIControlStateNormal];
        [_sendButton setTitleColor:COLOR_ADD_BUTTON_TEXT forState:UIControlStateNormal];
        _sendButton.layer.borderColor = COLOR_ADD_BUTTON_TEXT.CGColor;
        _sendButton.layer.borderWidth = 1;
        _sendButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [self addSubview:_sendButton];

        _textView = [[UITextView alloc] initWithFrame:CGRectMake(10, _sendButton.frame.origin.y, self.bounds.size.width - _sendButton.frame.size.width - 10 - 20, _sendButton.frame.size.height)];
        _textView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        [self addSubview:_textView];
        
    }
    return self;
}

-(void)dealloc
{
    [_sendButton release];
    [_textView release];
    [super dealloc];
}
@end

@interface CardDetailViewController ()
@property (nonatomic, strong) UIButton      *goodButton;
@property (nonatomic, strong) NSMutableDictionary		*filenameDic;

@end

@implementation CardDetailViewController

@synthesize delegate;
@synthesize dataList;
@synthesize cardInfo;
//@synthesize cardDetailInfoList;
//@synthesize currentCardIndex;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.dataList = [[[NSMutableArray alloc] init] autorelease];
        self.filenameDic = [[[NSMutableDictionary alloc] init] autorelease];
        
//        self.currentCardIndex = 0;
//        self.cardDetailInfoList = [[[NSMutableDictionary alloc] init] autorelease];
        
//        self.cardThumbnailList = [NSMutableArray arrayWithArray:[BusinessDataCtrlManager getCardList:[self.cardInfo objectForKey:cardId_key]]];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                     name: UIKeyboardWillShowNotification object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                     name: UIKeyboardWillHideNotification object:nil];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view = [[[UIViewExt alloc] initWithFrame:self.view.frame] autorelease];
    
    self.view.backgroundColor = [UIColor whiteColor];

//    NSMutableDictionary *newCardInfo = [[[NSMutableDictionary alloc] initWithDictionary:self.cardInfo] autorelease];
//    [self.cardDetailInfoList setObject:self.cardInfo forKey:[newCardInfo objectForKey:cardId_key]];

    
//    for (int i=0; i<[self.cardThumbnailList count]; i++) {
//        NSMutableDictionary *carThumbnailInfo = [self.cardThumbnailList objectAtIndex:i];
//        if ([[carThumbnailInfo objectForKey:cardId_key] intValue] == [[self.cardInfo objectForKey:cardId_key] intValue]) {
//            self.currentCardIndex = i;
//            break;
//        }
//    }
    rightBarItems = [[NSMutableArray array] retain];
    for (int i = 0; i < 3; i++)
    {
        if (i == 0)
        {
            UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 98, 44)] autorelease];
            UIButton *btn = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)] autorelease];
            
            [btn addTarget:self action:@selector(clickEditCardButton:) forControlEvents:UIControlEventTouchUpInside];
            [btn setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_addcard_normal-65.png"]] forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_tool_addcard_touch.png"]] forState:UIControlStateHighlighted];
            [v addSubview:btn];
            
            btn = [[[UIButton alloc] initWithFrame:CGRectMake(54, 0, 44, 44)] autorelease];
            
            [btn addTarget:self action:@selector(clickDelButton:) forControlEvents:UIControlEventTouchUpInside];
            [btn setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_edit_delete_normal.png"]] forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_edit_delete_touch.png"]] forState:UIControlStateHighlighted];
            [v addSubview:btn];
            
            [rightBarItems addObject:[[[UIBarButtonItem alloc] initWithCustomView:v] autorelease]];
        }
        else if(i == 1)
        {
            self.goodButton = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)] autorelease];
            
            [self.goodButton addTarget:self action:@selector(clickGoodButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.goodButton setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_good_normal.png"]] forState:UIControlStateNormal];
            [self.goodButton setBackgroundImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_good_touch.png"]] forState:UIControlStateHighlighted];
            [self.goodButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.goodButton setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
            [rightBarItems addObject:[[[UIBarButtonItem alloc] initWithCustomView:self.goodButton] autorelease]];
        }
        else if(i == 2)
        {
            UIButton *btn = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)] autorelease];
            [btn setTitle:NSLocalizedString(@"Edit", @"Edit") forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(clickEditButton:) forControlEvents:UIControlEventTouchUpInside];
            [rightBarItems addObject:[[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease]];
        }
    }
//    self.tableView.tableHeaderView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)]autorelease];
    
//    UISwipeGestureRecognizer *swipeRightGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToRight:)] autorelease];
//	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
//	swipeRightGesture.numberOfTouchesRequired = 1;
//    swipeRightGesture.delegate = self;
//	[self.view addGestureRecognizer:swipeRightGesture];
//
//    UISwipeGestureRecognizer *swipeLefGesture = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidDraggingToLeft:)] autorelease];
//	swipeLefGesture.direction = UISwipeGestureRecognizerDirectionLeft;
//	swipeLefGesture.numberOfTouchesRequired = 1;
//    swipeLefGesture.delegate = self;
//	[self.view addGestureRecognizer:swipeLefGesture];
//   
    /*
    // タップジェスチャーの作成
    UITapGestureRecognizer *tapGesture = [[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(handleTapGesture:)] autorelease];
    tapGesture.numberOfTapsRequired = 1;    // シングル
    tapGesture.numberOfTouchesRequired = 1; // ジェスチャーを認識する指の数
    [self.view addGestureRecognizer:tapGesture];
     */
}

- (void)updateTitleView
{
    self.navigationItem.rightBarButtonItem = [rightBarItems objectAtIndex:contentMode];
    if (contentMode == 1)
    {
        bool enable = YES;
        NSString *valuationStatus = [self.cardInfo objectForKey:valuationStatus_key];
        if ([valuationStatus isEqualToString:CARD_VALUATION_GOOD]) {
            enable = NO;
        }

        self.navigationItem.rightBarButtonItem.enabled = enable;
    }
}

-(void)updateTableView
{
    if (contentMode == 2)
    {
        self.view.backgroundColor = [UIColor colorWithRed:222/255.0 green:218/255.0 blue:206/255.0 alpha:1];
        if (writeCommentView.superview == nil)
        {
            [self.view addSubview:writeCommentView];
        }
        
        int height = TEXTVIEW_HEIGHT_NORMAL;
        if (isEditing)
        {
            height = TEXTVIEW_HEIGHT_EDITING;
        }
        
        writeCommentView.frame = CGRectMake(0, self.view.bounds.size.height-height-toolbarView.frame.size.height, writeCommentView.frame.size.width, height);
        self.tableView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, self.view.bounds.size.height-toolbarView.frame.size.height-writeCommentView.frame.size.height);
    }
    else
    {
        self.view.backgroundColor = [AppHelper getBaseTextColor];
        
        [writeCommentView removeFromSuperview];
        self.tableView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, self.view.bounds.size.height-toolbarView.frame.size.height);
    }
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[AppDelegate sharedInstance] setTabbarHidden:YES animated:NO];
    
    if (toolbarView == nil)
    {
        toolbarView = [[[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-44, self.view.frame.size.width, TOOL_BAR_HIGHT)] autorelease];
        toolbarView.backgroundColor = [UIColor blackColor];
        toolbarView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [self.view addSubview:toolbarView];
        
        //違反申告ボタン
        UIButton *button = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, toolbarView.frame.size.width/3, toolbarView.frame.size.height)] autorelease];
        [button addTarget:self action:@selector(clickDetailButton:) forControlEvents:UIControlEventTouchUpInside];
        //    UIImage *backgoundImageNormal = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_notice.png"]];
        //    [button setBackgroundImage:backgoundImageNormal forState:UIControlStateNormal];
        //    UIImage *backgoubdImageHilight = [UIImage imageNamed:[AppHelper getResourceIconPath:@"icon_notice.png"]];
        //    [button setBackgroundImage:backgoubdImageHilight forState:UIControlStateHighlighted];
        button.tag = 100;
        [button setTitle:NSLocalizedString(@"Card", @"Card") forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [button setTitleColor:COLOR_ADD_BUTTON_TEXT forState:UIControlStateSelected];
        [toolbarView addSubview:button];
        
        UIView *sep = [[UIView alloc] initWithFrame:CGRectMake(button.frame.size.width, 10, 2, toolbarView.frame.size.height-20)];
        sep.backgroundColor = [UIColor grayColor];
        [toolbarView addSubview:sep];
        
        //Goodボタンを生成
        NSNumber *cnt = [AppHelper parseWebDataToNumber:self.cardInfo dataKey:@"good"];
        ;
        NSString *cntStr = [NSString stringWithFormat:@"(%d)",[cnt intValue]];
        UIButton *goodButton = [[[UIButton alloc] initWithFrame:CGRectMake(toolbarView.frame.size.width/3, 0, toolbarView.frame.size.width/3, toolbarView.frame.size.height)] autorelease];
        goodButton.tag = 101;
        [goodButton addTarget:self action:@selector(clickUserButton:) forControlEvents:UIControlEventTouchUpInside];

        [goodButton setTitle:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"Good", @"Good"),cntStr] forState:UIControlStateNormal];
        goodButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [goodButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [goodButton setTitleColor:COLOR_ADD_BUTTON_TEXT forState:UIControlStateSelected];
        [toolbarView addSubview:goodButton];
        sep = [[UIView alloc] initWithFrame:CGRectMake(goodButton.frame.origin.x + goodButton.frame.size.width-2, 10, 2, toolbarView.frame.size.height-20)];
        sep.backgroundColor = [UIColor grayColor];
        [toolbarView addSubview:sep];
        
        //commentボタンを生成
        UIButton *commentButton = [[[UIButton alloc] initWithFrame:CGRectMake(toolbarView.frame.size.width*2/3, 0, toolbarView.frame.size.width - toolbarView.frame.size.width*2/3, toolbarView.frame.size.height)] autorelease];
        commentButton.tag = 102;
        [commentButton addTarget:self action:@selector(clickCommentButton:) forControlEvents:UIControlEventTouchUpInside];
        
        cnt = [AppHelper parseWebDataToNumber:self.cardInfo dataKey:@"commentCount"];
        ;
        cntStr = [NSString stringWithFormat:@"(%d)",[cnt intValue]];
        [commentButton setTitle:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"Comment", @"Comment"),cntStr] forState:UIControlStateNormal];
        commentButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [commentButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [commentButton setTitleColor:COLOR_ADD_BUTTON_TEXT forState:UIControlStateSelected];
        [toolbarView addSubview:commentButton];
        
        CGFloat positionY = self.view.bounds.size.height - toolbarView.frame.size.height;
        
        self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, positionY)] autorelease];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self.view addSubview:self.tableView];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        writeCommentView = [[WriteCommentView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        writeCommentView.textView.delegate = self;
        [writeCommentView.sendButton addTarget:self action:@selector(sendComment:) forControlEvents:UIControlEventTouchUpInside];
        
        button.selected = YES;
        lastSelectButton = button;
    }
    
    [self updateTitleView];
    [self updateTableView];
}

-(void)clickEditCardButton:(id)sender
{
    CreateCardViewController *inputTextViewController = [[[CreateCardViewController alloc] init] autorelease];
    inputTextViewController.categoryId = [self.cardInfo objectForKey:categoryId_Key];
    inputTextViewController.cardInfo = self.cardInfo;
    inputTextViewController.dataList = [CreateCardViewController detailList2EditList:self.dataList];
    BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:inputTextViewController] autorelease];
    //[[AppDelegate sharedInstance] mainViewController]
	[self presentViewController:controller animated:YES completion:nil];
}

-(void)clickDelButton:(id)sender
{
    MEActionSheet *actionSheet = [[[MEActionSheet alloc] initWithTitle:MultiLangString(@"really Delete?")] autorelease];
    [actionSheet setCancelButtonWithTitle:MultiLangString(@"Delete") onTapped:^{
        [[BusinessDataCtrlManager sharedInstance] deleteCards:[NSArray arrayWithObject:[self.cardInfo objectForKey:cardId_key]] delegate:self];
    }];
    [actionSheet setCancelButtonWithTitle:MultiLangString(@"Cancel") onTapped:^{
    }];
    [actionSheet showInView:self.view];
}

-(void)clickGoodButton:(id)sender
{
    [[BusinessDataCtrlManager sharedInstance] goodWithCardId:[self.cardInfo objectForKey:cardId_key] delegate:self];
}

-(void)clickDetailButton:(UIButton*)sender
{
    lastSelectButton.selected = NO;
    lastSelectButton = sender;
    sender.selected = YES;
    contentMode = 0;
    [self updateTitleView];
    [self updateTableView];
}

-(void)clickUserButton:(UIButton*)sender
{
    lastSelectButton.selected = NO;
    lastSelectButton = sender;
    sender.selected = YES;
    contentMode = 1;
    [self updateTitleView];
    [self updateTableView];
    if([userList count] == 0)
    {
        [[BusinessDataCtrlManager sharedInstance] getGoodList:[self.cardInfo objectForKey:cardId_key] delegate:self];
    }
    else
    {
        [self.tableView reloadData];
    }
}

-(void)clickCommentButton:(UIButton*)sender
{
    lastSelectButton.selected = NO;
    lastSelectButton = sender;
    sender.selected = YES;
    contentMode = 2;
    [self updateTitleView];
    [self updateTableView];
    
    if ([commentList count] == 0)
    {
        [[BusinessDataCtrlManager sharedInstance] getCommentList:[self.cardInfo objectForKey:cardId_key] delegate:self];
    }
    else
    {
        [self.tableView reloadData];
    }
}

- (void)sendComment:(id)sender
{
    if([writeCommentView.textView.text length])
    {
        writeCommentView.textView.text = nil;
        
        NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
        [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
        [param setObject:writeCommentView.textView.text forKey:content_key];
        
//        if (self.commnetInfo != nil) {
//            [param setObject:[self.commnetInfo objectForKey:commentId_key] forKey:@"toCommentId"];
//        }
//        
        [[BusinessDataCtrlManager sharedInstance] writeCommentData:param delegate:self];
    }
}
//-(void)clickViolationButton:(id)sender{
//    ViolationInputViewController *violationInputViewController = [[[ViolationInputViewController alloc] init] autorelease];
//    violationInputViewController.cardInfo = self.cardInfo;
//    BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:violationInputViewController] autorelease];
//    
//	[self presentViewController:controller animated:YES completion:nil];
//
//}

- (void)newCommnetSucessed
{
    [[BusinessDataCtrlManager sharedInstance] getCommentList:[self.cardInfo objectForKey:cardId_key] delegate:self];
}

- (void)goodCardSucessed
{
    self.goodButton.enabled = FALSE;
}

- (void)commentListSucessed:(NSArray *)cl
{
    [commentList release];
    commentList = [[NSMutableArray arrayWithArray:cl] retain];
    [self.tableView reloadData];
}

- (void)goodUserListSucessed:(NSArray*)memberList
{
    [userList release];
    userList = [[NSMutableArray arrayWithArray:memberList] retain];
    [self.tableView reloadData];
}

-(void)resetCommnetCount:(int)cnt
{
    NSString *cntStr = @"";
    if (cnt > 0)
    {
        cntStr = [NSString stringWithFormat:@"(%d)",cnt];
        self.commentCountLable.hidden = NO;
        self.commentCountLable.text = cntStr;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSArray*)currentDataList
{
    switch (contentMode)
    {
        case 0:
            return self.dataList;
        case 1:
            return userList;
        case 2:
            return commentList;
        default:
            break;
    }
    return nil;
}

-(CGFloat)getCommentHeight:(NSDictionary*)commentInfo
{
    NSString *commentDetail = [AppHelper parseWebDataToString:commentInfo dataKey:content_key];
    CGSize size = [commentDetail sizeWithFont:[UIFont boldSystemFontOfSize:CommentDetailSize] constrainedToSize:CGSizeMake(self.tableView.frame.size.width-55, 100) lineBreakMode:NSLineBreakByWordWrapping];
    return size.height + 54 + 10;
}

#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    isEditing = YES;
    [self updateTableView];
    return YES;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //NSLog(@"[self.dataList count]:%d",[self.dataList count]);
    return [[self currentDataList] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//NSLog(@"====indexPath:%d",indexPath.row);
    CGSize size;
    if (contentMode == 0)
    {
        NSMutableDictionary *cardData = [[self currentDataList] objectAtIndex:[indexPath row]];
        size = [AppHelper getCardDetailTextSize:cardData];
    }
    else if(contentMode == 1)
    {
        size.height = 44;
    }
    else if(contentMode == 2)
    {
        int c = [[self currentDataList] count];
        NSDictionary *commentInfo = [[self currentDataList] objectAtIndex:c - 1 - indexPath.row];
        size.height = [self getCommentHeight:commentInfo];
    }
	return size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%d",  contentMode];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor =[UIColor clearColor];
    
        if (contentMode == 0)
        {
            
        }
        else if(contentMode == 1)
        {
            CGFloat x = 10;
            //User Icon
            UIImageView *UserIconView;
            CGFloat iconSize = USER_ICON_SIZE;
            UserIconView = [[[UIImageView alloc] initWithFrame:CGRectMake(x, (44.0-iconSize)/2, iconSize, iconSize)] autorelease];
            UserIconView.layer.cornerRadius = 5.0f;
            UserIconView.clipsToBounds = YES;
            UserIconView.image = [UIImage imageNamed:[AppHelper getResourceIconPath:@"defaultProfImg.png"]];
            UserIconView.tag = 100;
            [cell.contentView addSubview:UserIconView];
            x += iconSize +10 ;
            
            //Set UserName
            UILabel *userNameLable = [[[UILabel alloc] initWithFrame:CGRectMake(x, UserIconView.frame.origin.y, SCREEN_WIDTH-x-40, iconSize/2)] autorelease];
            userNameLable.backgroundColor = [UIColor clearColor];
            userNameLable.font = [UIFont  boldSystemFontOfSize:20];
//            userNameLable.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
            userNameLable.textAlignment = NSTextAlignmentLeft;
            userNameLable.numberOfLines = 1;
            userNameLable.tag = 101;
            [cell.contentView addSubview:userNameLable];

            //is Jointed ?
            UILabel *jointedLable = [[[UILabel alloc] initWithFrame:CGRectMake(x, UserIconView.frame.origin.y+iconSize/2, SCREEN_WIDTH-x-40, iconSize/2)] autorelease];
            jointedLable.backgroundColor = [UIColor clearColor];
            jointedLable.font = [UIFont  systemFontOfSize:10];
            jointedLable.textColor = [UIColor blueColor];
            jointedLable.textAlignment = NSTextAlignmentLeft;
            jointedLable.numberOfLines = 1;
            jointedLable.tag = 102;
            [cell.contentView addSubview:jointedLable];
        }
        else if(contentMode == 2)
        {
            UIImageView *coverImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
            [coverImg setImage: [UIImage imageNamed:[AppHelper getResourceIconPath:@"defaultProfImg.png"]]];
            coverImg.tag = 100;
            [cell.contentView addSubview:coverImg];
            [coverImg release];
            
            UIImageView *contentBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width-30-20-5, 0)];
            contentBg.tag = 105;
            [cell.contentView addSubview:contentBg];
            [contentBg release];
            
            CGFloat y = 14;
        
            UILabel *authNameLable = [[UILabel alloc] initWithFrame:CGRectMake(0, y, tableView.bounds.size.width-70, 20)];
            authNameLable.backgroundColor = [UIColor clearColor];
            authNameLable.font = [UIFont  boldSystemFontOfSize:CommentAuthNameSize];
            authNameLable.textColor = [UIColor blackColor];
            authNameLable.textAlignment = NSTextAlignmentLeft;
            authNameLable.numberOfLines = 1;
            authNameLable.tag = 101;
            [cell.contentView addSubview:authNameLable];
            [authNameLable release];
            
            y += 20;
            
            UILabel *registTimeLable = [[UILabel alloc] initWithFrame:CGRectMake(70, y, tableView.bounds.size.width-70, 16)];
            registTimeLable.backgroundColor = [UIColor clearColor];
            registTimeLable.font = [UIFont systemFontOfSize:CommentDetailSize];
            registTimeLable.textColor = [UIColor grayColor];
            registTimeLable.textAlignment = NSTextAlignmentLeft;
            registTimeLable.numberOfLines = 1;
            registTimeLable.tag = 102;
            [cell.contentView addSubview:registTimeLable];
            [registTimeLable release];
            y += 16;
            
            y += 4;
            UILabel *commentDetailLable = [[UILabel alloc] initWithFrame:CGRectMake(0, y, tableView.bounds.size.width-70, 0)];
            commentDetailLable.backgroundColor = [UIColor clearColor];
            commentDetailLable.font = [UIFont  systemFontOfSize:CommentDetailSize];
            commentDetailLable.textColor = [UIColor blackColor];
            commentDetailLable.textAlignment = NSTextAlignmentLeft;
            commentDetailLable.numberOfLines = 0;
            commentDetailLable.tag = 103;
            [cell.contentView addSubview:commentDetailLable];
            [commentDetailLable release];
        }
    }
    
    NSArray *currentList = [self currentDataList];
     // Configure the cell...
    
    if (contentMode == 0)
    {
        NSMutableDictionary *cardContent = [self.dataList objectAtIndex:[indexPath row]];
        [AppHelper setCardDetailDataContents:cardContent view:cell.contentView  delegate:self];
    }
    else if(contentMode == 1)
    {
        NSDictionary *inData = [currentList objectAtIndex:indexPath.row];
        UIImageView *UserIconView = (UIImageView*)[cell.contentView viewWithTag:100];
        NSNumber *userId = [inData objectForKey:userId_key];
        [AppHelper setUserIconWith:[userId intValue] imageView:UserIconView];

        NSString *userName = [inData objectForKey:userName_Key];
        if (userName == nil || [userName isKindOfClass:[ NSNull  class]])
        {
            userName= @"";
        }
        
        UILabel *userNameLable = (UILabel*)[cell.contentView viewWithTag:101];
        userNameLable.text = userName;
        
//        UILabel *jointedLable = (UILabel*)[cell.contentView viewWithTag:102];
//        NSString *notJoinedStasus = [inData objectForKey:memberStatus_Key];
//        
//        if (notJoinedStasus != nil && ![notJoinedStasus isKindOfClass:[ NSNull class]] && [notJoinedStasus isEqualToString:@"1"])
//        {
//            jointedLable.hidden = NO;
//            jointedLable.text = MultiLangString(@"Not joined");
//        }
//        else
//        {
//            jointedLable.hidden = YES;
//        }
    }
    else if(contentMode == 2)
    {
        int c = [[self currentDataList] count];
        NSDictionary *info = [currentList objectAtIndex:c - 1 - indexPath.row];
        NSNumber *userId = [info objectForKey:userId_key];
        UIImageView *UserIconView = (UIImageView*)[cell.contentView viewWithTag:100];
        if (userId != nil) {
            [AppHelper setUserIconWith:[userId intValue] imageView:UserIconView];
        }
        NSString *authName = [AppHelper parseWebDataToString:info dataKey:userName_Key];
        UILabel *userNameLable = (UILabel*)[cell.contentView viewWithTag:101];
        userNameLable.text = authName;
        
        NSString *regsitTime = [AppHelper parseWebDataToString:info dataKey:updateDate_key];
        regsitTime = [Utils getShowTime:regsitTime];
        UILabel *registTimeLable = (UILabel*)[cell.contentView viewWithTag:102];
        registTimeLable.text = regsitTime;
        NSString *commentDetail = [AppHelper parseWebDataToString:info dataKey:content_key];
        
        UILabel *commentDetailLable = (UILabel*)[cell.contentView viewWithTag:103];
        commentDetailLable.text = commentDetail;
        [commentDetailLable sizeToFit];
        
        UIImageView *contentBg = (UIImageView*)[cell.contentView viewWithTag:105];
        bool isMe = [[AppHelper parseWebDataToString:info dataKey:userId_key] isEqual:[BusinessDataCtrlManager getUserInfo].userId];
        if (isMe)
        {
            UserIconView.frame = CGRectMake(self.tableView.frame.size.width - 10 - 30, UserIconView.frame.origin.y, UserIconView.frame.size.width, UserIconView.frame.size.height);
            userNameLable.frame = CGRectMake(20, userNameLable.frame.origin.y, userNameLable.frame.size.width, userNameLable.frame.size.height);
            contentBg.image = [[UIImage imageNamed:[AppHelper getResourceIconPath:@"base_talk_selef_normal.png"]] resizableImageWithCapInsets:UIEdgeInsetsMake(30, 10, 8, 20) resizingMode:UIImageResizingModeStretch];
            userNameLable.textColor = [UIColor whiteColor];
            commentDetailLable.textColor = [UIColor whiteColor];
            
        }
        else
        {
            UserIconView.frame = CGRectMake(10, UserIconView.frame.origin.y, UserIconView.frame.size.width, UserIconView.frame.size.height);
            userNameLable.frame = CGRectMake(UserIconView.frame.size.width + UserIconView.frame.origin.x + 5, userNameLable.frame.origin.y, userNameLable.frame.size.width, userNameLable.frame.size.height);
            contentBg.image = [[UIImage imageNamed:[AppHelper getResourceIconPath:@"base_talk_friend_normal.png"]] resizableImageWithCapInsets:UIEdgeInsetsMake(30, 10, 8, 20) resizingMode:UIImageResizingModeStretch];
            userNameLable.textColor = COLOR_CARD_USER_NAME;
            commentDetailLable.textColor = [UIColor blackColor];
        }
        
        NSDictionary *commentInfo = [[self currentDataList] objectAtIndex:indexPath.row];
        CGFloat height = [self getCommentHeight:commentInfo];

        contentBg.frame = CGRectMake(userNameLable.frame.origin.x-10, userNameLable.frame.origin.y-10, contentBg.frame.size.width, height);
        registTimeLable.frame = CGRectMake(userNameLable.frame.origin.x, registTimeLable.frame.origin.y, registTimeLable.frame.size.width, registTimeLable.frame.size.height);
        commentDetailLable.frame = CGRectMake(userNameLable.frame.origin.x, commentDetailLable.frame.origin.y, commentDetailLable.frame.size.width, commentDetailLable.frame.size.height);
    }
    return cell;
}



#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (contentMode == 0)
    {
        // Navigation logic may go here. Create and push another view controller.
        NSMutableDictionary *infoData = [self.dataList objectAtIndex:[indexPath row]];
        
        NSString *resourceType = [AppHelper parseWebDataToString:infoData dataKey:resourceType_key];
        if ([resourceType isEqualToString:DB_RESOURCE_TYPE_WEB])
        {
            NSString* html = [AppHelper parseWebDataToString:infoData dataKey:begining_key];
            
            WebServerViewController *webViewController = [[[WebServerViewController alloc] initWithNibName:@"WebServerView" bundle:nil ] autorelease];
            webViewController.delegate = self;
            webViewController.view.frame = [self.view bounds];
            [webViewController toOpenWebWithURL:html];
            
            BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:webViewController] autorelease];
            [AppHelper setImageToNavigationBar:controller.navigationBar imageName:[AppHelper getResourceIconPath:@"header.png"]];
            
            [self presentViewController:controller animated:YES completion:nil];
        }
        else if([resourceType isEqualToString:DB_RESOURCE_TYPE_BEGINING])
        {
            
            self.selectedIndexPath = indexPath;
            
            //        CHRichTextEditorViewController *vc = [[[CHRichTextEditorViewController alloc] initWithNibName:@"CHRichTextEditorViewController" bundle:nil] autorelease];
            //        vc.delegate = self;
            //        vc.view.userInteractionEnabled = YES;
            //        [self.navigationController pushViewController:vc animated:YES];
            
            
        }
        else
        {
            NSNumber *resourceId = [infoData objectForKey:resourceId_key];
            if ( resourceId != nil)
            {
                NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
                [paramDic setObject:[BusinessDataCtrlManager getUserInfo].sessionId forKey:JSESSIONID];
                NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
                [param setObject:resourceId forKey:resourceId_key];
                [paramDic setObject:[param JSONRepresentation] forKey:PARAMS];
                [param release];
                
                [[HttpClientUtils sharedClient] postDataWithMethod:@"POST" methodName:METHOD_RESOURCE_DOWNLOAD parameters:paramDic option:nil isShowLoading:YES delegate:self];
                
                [paramDic release];
                
            }
            
        }
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - AppApiDelegate delegate
//- (void)endEditCardSucessed:(NSMutableDictionary *)Info{
//
//    NSMutableDictionary *cardData = [self.dataList objectAtIndex:[self.selectedIndexPath row]];
//    [cardData setObject:[Info objectForKey:begining_key] forKey:begining_key];
//
//    NSArray* indexPaths = [NSArray arrayWithObject:self.selectedIndexPath];
//    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
//    
//    
//}

#pragma mark - AppApiDelegate サーバー間とのデータやり取り成功時に呼ぶ
-(void)dataOperationSuccess:(id)responseObject option:(NSDictionary *)option{
    NSString *resourceType = [responseObject objectForKey:resourceType_key];
    if ([resourceType isEqualToString:DB_RESOURCE_TYPE_PHOTO] || [resourceType isEqualToString:DB_RESOURCE_TYPE_IllUSTRATION]) {
        CardPartsDetailInfoViewController *detailViewController = [[CardPartsDetailInfoViewController alloc] init];
        detailViewController.delegate = self;
        detailViewController.resourceInfo = responseObject;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
        [detailViewController release];
        
    }
}


#pragma mark - clickEditTextButton
-(void)clickEditTextButton:(UIButton *)sender{

}

#pragma mark - getHtmlHeight
-(CGFloat)getHtmlHeight:(NSString *)htmlText{
    
    return [Utils getHtmlHeight:htmlText inWidth:self.tableView.frame.size.width];
    
}

#pragma mark - clickOpenResourceButton
-(void)clickOpenResourceButton:(UIButton *)sender{

    UILabel *label = (UILabel *)[sender viewWithTag:RESOURCE_OPEN_RESOURCE_ID_LABEL_TAG];
    NSNumber *resourceId = [NSNumber numberWithInt:[label.text intValue]];
    
    label = (UILabel *)[sender viewWithTag:RESOURCE_OPEN_RESOURCE_TYPE_LABEL_TAG];
    NSString *resourceType = label.text;
 
    if([resourceType isEqualToString:DB_RESOURCE_PERSON_IMG]){
        //Youtube
        label = (UILabel *)[sender viewWithTag:RESOURCE_OPEN_RESOURCE_YOUTUBE_MOVE_LABEL_TAG];
        NSString *youtubeMoveId = label.text;
        
        NSString *videoUrl = [NSString stringWithFormat:YouTubeMoveImageURL,youtubeMoveId];        
        NSString* html = [AppHelper getVideoPlayHtml:videoUrl];
        
        WebServerViewController *webViewController = [[WebServerViewController alloc] initWithNibName:@"WebServerView" bundle:nil ];
        webViewController.delegate = self;
        webViewController.view.frame = [self.view bounds];
        [webViewController loadWebContents:html];
        
        BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:webViewController] autorelease];
        [AppHelper setImageToNavigationBar:controller.navigationBar imageName:[AppHelper getResourceIconPath:@"header.png"]];
        
        [self presentViewController:controller animated:YES completion:nil];
        [webViewController release];
        [controller release];

    }else{
        
        label = (UILabel *)[sender viewWithTag:RESOURCE_OPEN_RESOURCE_FILE_NAME_LABEL_TAG];
        NSString *fileName = label.text;
        NSRange range1 = [ fileName rangeOfString:@"." options:NSBackwardsSearch];
		if (range1.location != NSNotFound) {
            NSString *fileNameExt = [fileName substringFromIndex :range1.location + range1.length ];
            NSString *localCashFileName = [NSString stringWithFormat:@"%d.%@",[resourceId integerValue],fileNameExt];
            [self.filenameDic setObject:localCashFileName forKey:resourceId];

            NSString *appFile = [AppHelper getLocalCatchfilePath:localCashFileName];
            if (appFile != nil) {
                [self readApplicationFile:localCashFileName];
                
                return;
            }
        }else {
            [self.filenameDic setObject:fileName forKey:resourceId];
  
        }
        
        
        [self dataLoadingAnimation];
        
        [[BusinessDataCtrlManager sharedInstance] requestResourceDownloadKey:resourceId delegate:self];
    }
    
}

- (void)resourceInfoSucessed:(NSMutableDictionary *)resourceInfo{
    
    
    // confirmId   confirmKey resourceId
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[resourceInfo objectForKey:confirmId_key] forKey:confirmId_key];
    [param setObject:[resourceInfo objectForKey:confirmKey_key] forKey:confirmKey_key];
    [param setObject:[resourceInfo objectForKey:resourceId_key] forKey:resourceId_key];
    
    NSString *url = [NSString stringWithFormat:@"%@resourceId=%@&confirmId=%@&confirmKey=%@&mode=2",METHOD_GET_DOWNLOAD_RESOURCE_FILE,[resourceInfo objectForKey:resourceId_key],[resourceInfo objectForKey:confirmId_key],[resourceInfo objectForKey:confirmKey_key]];
    
    [[BusinessDataCtrlManager sharedInstance] requestResourceFileDownload:url delegate:self filename:[self.filenameDic objectForKey:[resourceInfo objectForKey:resourceId_key]]];
  
}

- (void)readApplicationFile:(NSString *)fileName{
    
    [self.maskView removeFromSuperview];
    
    NSString *appFile = [AppHelper getLocalCatchfilePath:fileName];
    
    if (appFile == nil || [appFile isKindOfClass:[NSNull class]] ) {
        return;
    }
    
    WebServerViewController *webViewController = [[WebServerViewController alloc] initWithNibName:@"WebServerView" bundle:nil ];
    webViewController.delegate = self;
    webViewController.view.frame = [self.view bounds];
    [webViewController toOpenLoacalFile:fileName];
    
    BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:webViewController] autorelease];
    [AppHelper setImageToNavigationBar:controller.navigationBar imageName:[AppHelper getResourceIconPath:@"header.png"]];
    
	[self presentViewController:controller animated:YES completion:nil];
    [webViewController release];
    [controller release];

}



#pragma mark LoadingAnimation
- (void)dataLoadingAnimation{
	UIWindow *mainWindow;
	mainWindow = [[UIApplication sharedApplication] keyWindow];
	self.maskView = (UIButton *)[mainWindow viewWithTag:DATA_LOADIN_ANI_MASK_VIEW_TAG];
	if (self.maskView != nil) {
		//すでにアニメーション中のため、リターンする
		return;
	}
	
	self.maskView = [[UIButton alloc] initWithFrame:FULL_MAX_RECT_SIZE];
    [self.maskView addTarget:self action:@selector(clickCancelResourceDowloadButton:) forControlEvents:UIControlEventTouchUpInside];

    //    maskView.backgroundColor = [UIColor blueColor];
	[mainWindow addSubview:self.maskView];
	
	UIImageView *animationBaseView = [[UIImageView alloc] initWithFrame:CGRectMake(135, 215, 50, 50)];
	[animationBaseView setImage:[UIImage imageNamed:[AppHelper getResourceIconPath:@"loadingBase.png"]]];
	[self.maskView addSubview:animationBaseView];
	[animationBaseView release];
	
	UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, 230, 20, 20)];
	indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
	[indicator startAnimating];
	[self.maskView addSubview:indicator];
	[indicator release];
    
    [self.maskView release];
    
	
}

-(void)clickCancelResourceDowloadButton:(id)sender{
    MEActionSheet *actionSheet = [[MEActionSheet alloc] initWithTitle:NSLocalizedString(@"I0039",@"I0039")];
    [actionSheet addButtonWithTitle:MultiLangString(@"Yes") onTapped:^{
        [[BusinessDataCtrlManager sharedInstance] cancelResourceFileDownload];
    }];
    [actionSheet setCancelButtonWithTitle:MultiLangString(@"Cancel")];
    [actionSheet showInView:self.view];
}

#pragma mark Delegate
-(void)connectNetWorkError{

    [self.maskView removeFromSuperview];
        
}

//#pragma mark - PanGestureRecognizer
//
//-(void)viewDidDraggingToRight:(UISwipeGestureRecognizer *)swipe{
//    //    NSLog(@"========handleRightSwipeGesture========");
//    if (self.currentCardIndex == 0) {
//        [self.navigationController popViewControllerAnimated:YES];
//    }else{
//        self.leftOrRightFlag = IS_LEFT;
//        [self nextCardDetailInfo:self.currentCardIndex-1];
//    }
//    
//}
//
//-(void)viewDidDraggingToLeft:(UISwipeGestureRecognizer *)swipe{
//    //    NSLog(@"========handleRightSwipeGesture========");
//    if (self.currentCardIndex == [self.cardThumbnailList count]-1) {
//        [UIView animateWithDuration:0.3f
//                         animations:^{
//                             CGRect rect =self.tableView.frame;
//                             rect.origin.x = rect.origin.x - 20;
//                             self.tableView.frame = rect;
//                             
//                         }
//                         completion:^(BOOL finished) {
//                             [UIView animateWithDuration:0.3f
//                                              animations:^{
//                                                  CGRect rect =self.tableView.frame;
//                                                  rect.origin.x = rect.origin.x + 20;
//                                                  self.tableView.frame = rect;
//                                                  
//                                              }
//                                              completion:^(BOOL finished) {
//                                              }];
//                         }];
//        
//    }else{
//        self.leftOrRightFlag = IS_RIGHT;
//        [self nextCardDetailInfo:self.currentCardIndex+1];
//
//    }
//}
//
//-(void)nextCardDetailInfo:(int)idx{
//    NSMutableDictionary *carThumbnailInfo = [self.cardThumbnailList objectAtIndex:idx];
//    NSMutableDictionary *cardDetailInfo = [self.cardDetailInfoList objectForKey:[carThumbnailInfo objectForKey:cardId_key]];
//    if (cardDetailInfo == nil) {
//        [[BusinessDataCtrlManager sharedInstance] requestCardInfoWithCardId:[carThumbnailInfo objectForKey:cardId_key] delegate:self];
//
//    }else {
//        if (self.leftOrRightFlag == IS_LEFT) {
//            self.currentCardIndex--;
//        }else{
//            self.currentCardIndex++;
//        }
//        
//        self.cardInfo = [[[NSMutableDictionary alloc] initWithDictionary:cardDetailInfo] autorelease];;
//        
//        [self showNextCardView];
//
//    }
//
//}

//-(void)showNextCardView{
//    
//    NSString *contents = [self.cardInfo objectForKey:CONTENTS];
//    //NSLog(contents);
//    
//    self.dataList = [[[NSMutableArray alloc] init] autorelease];
//    [AppHelper changeCardInfoDetailList:contents output:self.dataList];
//    
//    CGRect new_rect;
//    if (self.leftOrRightFlag == IS_LEFT) {
//        new_rect = CGRectMake(-SCREEN_WIDTH, 0, self.view.bounds.size.width, self.view.bounds.size.height);
//    }else{
//        new_rect = CGRectMake(SCREEN_WIDTH, 0, self.view.bounds.size.width, self.view.bounds.size.height);
//    }
//    
//    self.tableView = [[[UITableView alloc] initWithFrame:new_rect] autorelease];
//    self.tableView.delegate = self;
//    self.tableView.dataSource = self;
//    self.tableView.backgroundColor = [AppHelper getBaseTextColor];
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [self.view addSubview:self.tableView];
//    self.tableView.tableHeaderView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)]autorelease];
//    
//    [UIView animateWithDuration:0.3f
//                     animations:^{
//                         CGRect rect =self.oldTableView.frame;
//                         if (self.leftOrRightFlag == IS_LEFT) {
//                             rect.origin.x = SCREEN_WIDTH;
//                         }else{
//                             rect.origin.x = -SCREEN_WIDTH;
//                        }
//                         self.oldTableView.frame = rect;
//                         
//                         rect =self.tableView.frame;
//                         rect.origin.x = 0;
//                         self.tableView.frame = rect;
//                         
//                     }
//                     completion:^(BOOL finished) {
//                         [self.oldTableView removeFromSuperview];
//                         
//                         [self.tableView reloadData];
//                     }];
//
//}

//- (void)cardInfoSucessed:(NSMutableDictionary *)newCardInfo{
//    self.cardInfo = [[[NSMutableDictionary alloc] initWithDictionary:newCardInfo] autorelease];
//    [self.cardDetailInfoList setObject:self.cardInfo forKey:[newCardInfo objectForKey:cardId_key]];
//    
//    if (self.leftOrRightFlag == IS_LEFT) {
//        self.currentCardIndex--;
//    }else{
//        self.currentCardIndex++;
//    }
//    
//    [self showNextCardView];
//    
//    [self.delegate returnFromCardInfoDetailView:[self.cardInfo objectForKey:cardId_key]];
//    
//}
//

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
     NSString *url = [[[request URL] standardizedURL] absoluteString];
     //NSLog(@"url: %@", url);
     NSRange range1 = [[url lowercaseString] rangeOfString:@"http://"];
     NSRange range2 = [[url lowercaseString] rangeOfString:@"https://"];
     if (range1.location != NSNotFound || range2.location != NSNotFound) {
         WebServerViewController *webViewController = [[[WebServerViewController alloc] initWithNibName:@"WebServerView" bundle:nil ] autorelease];
         webViewController.delegate = nil;
         webViewController.view.frame = [self.view bounds];
         [webViewController toOpenWebWithURL:url];
         
         BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:webViewController] autorelease];
         [AppHelper setImageToNavigationBar:controller.navigationBar imageName:[AppHelper getResourceIconPath:@"header.png"]];
         
         [self presentViewController:controller animated:YES completion:nil];

         return FALSE;
     }
	
	return TRUE;
}


/*
 *  キーボード表示
 */
- (void)keyboardWillShow:(NSNotification *)notificatioin{
//    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
//    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//    [cell.superview bringSubviewToFront:cell];
    
//    /*
    //NSLog(@"---keyboardWillShow---");
    // キーボードに合わせたアニメーション処理
    CGRect keybord = [[notificatioin.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIViewExt *extView = (UIViewExt *)self.view;
    if (extView.tapPoint.y > (extView.bounds.size.height-keybord.size.height)) {
        [UIView animateWithDuration:0.3f
                         animations:^{
                             
                             // キーボードのサイズを取得
                             //NSLog(@"contentOffset: %@", NSStringFromCGPoint(keybord.origin));
                             //NSLog(@"contentSize: %@", NSStringFromCGSize(keybord.size));
                             
                             CGRect viewRect  = self.tableView.frame;
                             //Viewの高さ - キーボードの高さ - buttonBaseの高さを引いて、その位置にする
                             viewRect.size.height = self.view.bounds.size.height - writeCommentView.frame.size.height - keybord.size.height;
                             
                             self.tableView.frame = viewRect;
                             writeCommentView.frame = CGRectMake(0, self.tableView.frame.size.height, writeCommentView.frame.size.width, writeCommentView.frame.size.height);
                         }];

    }
//     */
}

- (void)keyboardWillHide:(NSNotification *)notificatioin{
    //NSLog(@"---keyboardWillShow---");
    // キーボードに合わせたアニメーション処理
    [UIView animateWithDuration:0.3f
                     animations:^{
                         
                         CGRect viewRect  = self.tableView.frame;
                         //Viewの高さ - キーボードの高さ - buttonBaseの高さを引いて、その位置にする
                         writeCommentView.frame = CGRectMake(0, self.view.bounds.size.height-toolbarView.frame.size.height, writeCommentView.frame.size.width, TEXTVIEW_HEIGHT_NORMAL);
                         
                         viewRect.size.height = self.view.bounds.size.height - toolbarView.frame.size.height - writeCommentView.frame.size.height;
                         
                         self.tableView.frame = viewRect;
                         
                     }];

}


-(void)clickSaveButton:(id)sender {
    
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
    
    [[BusinessDataCtrlManager sharedInstance] startEditCard:param delegate:self];
    
    
}

#pragma mark - AppApiDelegate delegate
//- (void)startEditCardSucessed:(NSMutableDictionary *)Info{
//    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
//    [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
//    
//    NSString *contents = [self.cardInfo objectForKey:CONTENTS];
//    contents = [contents stringByReplacingOccurrencesOfString:nil withString:[self.textView.text  stringByReplacingMatchesOfPattern:@"\n" withString:@"<br>"]];
//    [param setObject:contents forKey:CONTENTS];
//    
//    [param setObject:[self.cardInfo objectForKey:RESOURCEIDS] forKey:RESOURCEIDS];
//    
//    [[BusinessDataCtrlManager getInstance] updateCard:param delegate:self];
//    
//}
//
//- (void)updateCardSucessed:(NSMutableDictionary *)Info{
//    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
//    [param setObject:[self.cardInfo objectForKey:cardId_key] forKey:cardId_key];
//    
//    [[BusinessDataCtrlManager sharedInstance] endEditCard:param delegate:self];
//    
//}

//- (void)endEditCardSucessed:(NSMutableDictionary *)Info{
//    
//    NSMutableDictionary *data = [[[NSMutableDictionary alloc] init] autorelease];
//    [data setObject:DB_RESOURCE_TYPE_BEGINING forKey:resourceType_key];
//    
//    [data setObject:self.textView.text forKey:begining_key];
//    
//    [self.delegate endEditCardSucessed:data];
//    
//    [self.navigationController popViewControllerAnimated:YES];
//    
//}
-(void)deleteSheetSuccessed:(NSMutableDictionary*)Info
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Memory management
- (void)dealloc {
    self.delegate = nil;
//    self.cardThumbnailList = nil;
//    self.cardDetailInfoList = nil;
    self.dataList = nil;
    self.cardInfo = nil;
    self.selectedIndexPath = nil;
    self.maskView = nil;
    self.commentCountLable = nil;
    self.tableView = nil;
    self.goodButton = nil;
    
    [commentList release];
    [userList release];
    [rightBarItems release];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object: nil];

    [[DownLoadSimpleWebDataServerController getInstance] clearDelegate];
    
	[super dealloc];
    
	
}


@end
