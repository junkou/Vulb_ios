//
//  CreateCategoryViewController.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 2013/11/07.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppHelper.h"
#import "AppApiDelegate.h"

#define EDIT_ACTIVE_SELECT_BASE_OFFSET	3

@interface CreateCategoryViewController : BaseViewController < UITextFieldDelegate, AppApiDelegate>{
    id delegate;
    int     categoryColorId;

}

@property (nonatomic, assign) id delegate;
@property (nonatomic) int     categoryColorId;

@property (nonatomic, strong) UIButton *saveButton;

@property (nonatomic, strong) UITextField    *txtCagegoryName;

@end
