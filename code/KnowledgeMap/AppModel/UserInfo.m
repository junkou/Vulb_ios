//
//  UserInfo.m
//  BookMaker
//
//  Created by k-kin on 10/07/05.
//  Copyright 2010 ICCESOFT. All rights reserved.
//

#import "UserInfo.h"


@implementation UserInfo

@synthesize userId;
@synthesize mailAddress; 
@synthesize pass;
@synthesize tokenId;
@synthesize sessionId;
@synthesize userName;
@synthesize cardSize;
@synthesize local;
@synthesize personImg;
@synthesize lastSheetId;
@synthesize lastCategoryId;

- (void)toChangeObjectValues:(NSDictionary *)indata{	
	NSArray *param_list = [indata allKeys];
	for (int i=0; i < [param_list count] ; i++) {
		NSString *key = [param_list objectAtIndex:i];
		id value = [indata objectForKey:key];
		if ( [value isKindOfClass:[ NSNull class]]) {
			continue;
		}
        
        if ([key isEqualToString:userId_key]) {
			self.userId = value;
		} else if ([key isEqualToString:mailAddress_key]) {
			self.mailAddress = value;
		}else if ([key isEqualToString:password_key]) {
			self.pass = value;
		}else if ([key isEqualToString:tokenId_Key]) {
			self.tokenId = value;
		}else if ([key isEqualToString:sessionId_Key]) {
			self.sessionId = value;
		}else if ([key isEqualToString:userName_Key]) {
			self.userName = value;
		}else if ([key isEqualToString:cardSize_Key]) {
			self.cardSize = value;
		}else if ([key isEqualToString:local_Key]) {
			self.local = value;
		}else if ([key isEqualToString:personImg_Key]) {
			self.personImg = value;
		}else if ([key isEqualToString:sheetId_Key]) {
			self.lastSheetId = value;
		}else if ([key isEqualToString:categoryId_Key]) {
			self.lastCategoryId = value;
		}
	}	
}

- (void)toCreateDictionaryValues:(NSDictionary *)indata{
	[indata setValue:self.userId forKey:userId_key];
	[indata setValue:self.mailAddress forKey:mailAddress_key];
	[indata setValue:self.pass forKey:password_key];
	[indata setValue:self.tokenId forKey:tokenId_Key];
	[indata setValue:self.sessionId forKey:sessionId_Key];
	[indata setValue:self.userName forKey:userName_Key];	
	[indata setValue:self.cardSize forKey:cardSize_Key];
	[indata setValue:self.local forKey:local_Key];
	[indata setValue:self.personImg forKey:personImg_Key];
	[indata setValue:self.lastSheetId forKey:sheetId_Key];
	[indata setValue:self.lastCategoryId forKey:categoryId_Key];
	
}

-(void)clearData{
    self.userId = [NSNumber numberWithInt:0];
    self.mailAddress = @"";
    self.pass = @"";
    self.tokenId = @"";
    self.sessionId = @"";
    self.userName = @"";
    self.cardSize = [NSNumber numberWithInt:0];
    self.local = @"";
    self.personImg = @"";
    self.lastSheetId = [NSNumber numberWithInt:0];
    self.lastCategoryId = [NSNumber numberWithInt:0];
   
}

- (void)dealloc {	
	[self.mailAddress release];
	[self.pass release];
	[self.tokenId release];
	[self.sessionId release];
	[self.userName release];
	[self.local release];
	[self.personImg release];
	[self.lastSheetId release];
	[self.lastCategoryId release];
	
	[super dealloc];
	
}


@end
