//
//  LoginManager.h
//  BukurouSS
//
//  Created by k-kin on 11/11/13.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfo.h"
#import "WebServerDownLoadController.h"
#import "AppApiDelegate.h"
#import "DataRegistry.h"

@interface BusinessDataCtrlManager : NSObject {
    id delegate;

    NSString *methodName ;
    NSString *resourceFileName ;
    NSNumber *currentSheetId;
   

}

@property (nonatomic, assign) id delegate;

@property  (nonatomic,strong) NSString *methodName;
@property  (nonatomic,strong) NSString *resourceFileName;
@property  (nonatomic,strong) NSNumber *currentSheetId;


+(BusinessDataCtrlManager *)sharedInstance;


+(void)setColorDatas;
-(NSDictionary *)getGlobalOptionData;

+(UserInfo *)getUserInfo;
+(NSArray *)getColorMasterList;
+(NSArray *)getSheetList;

+(NSDictionary *)getSheetInfo:(NSNumber *)sheetId;
+(NSArray *)getCategoryList;
+(NSDictionary *)getSheetIdAndCategoryList;
+(NSDictionary *)getCategoryInfo:(NSNumber *)categoryId;
+(NSDictionary *)getCategoryInfo:(NSNumber *)categoryId sheetId:(NSNumber *)sheetId;
+(NSArray *)getCategoryStatisticsList;
+(NSArray *)getCardList:(id)categoryId;
- (NSString*)getSheetCreator:(NSDictionary*)sheetInfo;

-(void)updateOption:(NSDictionary*)option;

-(void)goSheet:(NSNumber*)sheetID;

//operation with request
-(void)createUser:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)comfirmUser:(NSString *)confirmKey userId:(NSString *)userId delegate:(id)inDelegate;
-(void)loginWithFacebook:(id)inDelegate;
-(void)loginWithTokenId:(NSString *)tokenId delegate:(id)inDelegate;
-(void)loginWithJSessionId:(NSString *)sessionId delegate:(id)inDelegate;
-(BOOL)loadUserInfoDataWithDelegate:(id)inDelegate;
-(void)loginWithUserIdAndDelegete:(NSString *)inMailAddress pass:(NSString *)inPass delegate:(id)inDelegate;
-(void)loginWithUserId:(NSString *)inMailAddress pass:(NSString *)inPass;
-(void)logOut;

//data source for cardlist
-(void)requestLastCardList:(id)inDelegate startIdx:(int )idx;
-(void)requestCardListWithCategoryId:(NSNumber *)inCategoryId delegate:(id)inDelegate startIdx:(int )startIdx;
-(void)requestCardInfoWithCardId:(NSNumber *)inCardId delegate:(id)inDelegate ;
-(void)goodWithCardId:(NSNumber *)inCardId delegate:(id)inDelegate ;
-(void)getGoodList:(NSNumber *)inCardId delegate:(id)inDelegate;
-(void)getCommentList:(NSNumber *)inCardId delegate:(id)inDelegate ;
-(void)writeCommentData:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)deleteComment:(NSNumber*)commmentId delegate:(id)inDelegate;

-(void)requestResourceDownloadKey:(NSNumber *)inResourceId delegate:(id)inDelegate ;
-(void)requestResourceFileDownload:(NSString *)inUrl delegate:(id)inDelegate filename:(NSString *)fileName;
-(void)cancelResourceFileDownload;
-(void)sendViolationData:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)requestSheetListUnconfirm:(id)inDelegate ;
-(void)registGroup:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)refreshSheetList:(id)inDelegate ;
-(void)createCategory:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)requestInvitedList:(id)inDelegate ;
-(void)requestFbFreindsList:(id)inDelegate ;
-(void)createNewSheet:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)inviteWithUserid:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)inviteWithMailaddress:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)requestSheetMemberList:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)requestReferenceSheetSetting:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)updateReferenceSheetSetting:(NSMutableDictionary *)param delegate:(id)inDelegate ;
//-(void)getGategoryStatistics:(NSMutableDictionary *)param delegate:(id)inDelegate ;
//-(void)getGategoryStatistics:(NSMutableDictionary *)param;


-(void)deleteCards:(NSArray*)cardIds delegate:(id)inDelegate;
-(void)deleteSheets:(NSArray*)sheetIds delegate:(id)inDelegate;
-(void)deleteCategorys:(NSArray*)categoryIds delegate:(id)inDelegate;

-(void)createCard:(id)inDelegate;


-(void)startEditCard:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)updateCard:(NSMutableDictionary *)param delegate:(id)inDelegate ;
-(void)endEditCard:(NSMutableDictionary *)param delegate:(id)inDelegate ;

-(void)getProfileWithDelegate:(id)inDelegate;
-(void)updateProfile:(NSMutableDictionary*)param delegate:(id)inDelegate;
-(void)changeProifeImage:(NSDictionary*)param delegate:(id)inDelegate;
@end
