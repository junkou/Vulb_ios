//
//  WorkDataControllerDelegate.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/05/02.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AppApiDelegate <NSObject>

@optional

//サーバー間とのデータやり取り成功時に呼ぶ
-(void)dataOperationSuccess:(id)responseObject option:(NSDictionary *)option;

//サーバー間とのデータやり取り失敗時に呼ぶ
-(void)dataOperationFailed:(id)error option:(NSDictionary *)option;

- (void)readApplicationFile:(NSString *)fileName;

- (void)recieveApplicationData:(NSMutableData *)data fileType:(NSString *)fileType;

- (void)resourceInfoSucessed:(NSMutableDictionary *)resourceInfo;

// comment一覧取得成功時に呼ばれる
- (void)commentListSucessed:(NSArray *)commentList;

//card good member list
- (void)goodUserListSucessed:(NSArray*)memberList;

// commnet投稿が成功時に呼ばれる
- (void)newCommnetSucessed;

- (void)commentDeleteSuccessed;

//未参加シート一覧取得
- (void)sheetListUnconfirmSucessed:(NSArray *)sheetList;

//シート参加成功
- (void)groupRegistSucessed;

-(void)resetCommnetCount:(int)cnt;

// 違反申告投稿が成功時に呼ばれる
- (void)sendViolationSucessed;

// goodCard成功時に呼ばれる
- (void)goodCardSucessed;

// card詳細情報取得成功時に呼ばれる
- (void)cardInfoSucessed:(NSMutableDictionary *)cardInfo;

// 新規Cateory作成
- (void)createCategory:(NSMutableDictionary *)param;

// 新規Cateory作成に成功時に呼ばれる
- (void)createCategorySucessed;

// Cateory取得に成功時に呼ばれる
- (void)CategoryListSucessed;

// cardList成功時に呼ばれる
- (void)cardListSucessed;

// Login成功時に呼ばれる
- (void)loginSucessed;

- (void)facebookUrlSucessed:(NSMutableDictionary *)facebookInfo;

// CreateUser成功時に呼ばれる
- (void)createUserSucessed;

// LogOutに呼ばれる
- (void)logOut;

// Login失敗時に呼ばれる
- (void)loginFaild;


// 写真が選択された時に呼ばれる
- (void)photoSelectConfirm:(NSMutableArray *)dataList;

// 写真の選択をキャンセルした時に呼ばれる
- (void)photoSelectCancel;

// 新しいSheehIdとCategoryIdを設定する
- (void)newSheetIDAndCategoryID:(int)sheetId categoryId:(int)categoryId;

// 新しいCategoryIdを設定する
- (void)newCategoryID:(int)categoryId;

-(void)textValueConfirm:(NSString *)text;

- (void)invitedListSucessed:(NSArray *)memberList;

- (void)fbFreindsListSucessed:(NSArray *)memberList;

- (void)sheetMemberListSucessed:(NSArray *)memberList;

- (void)referenceSheetSettingSucessed:(NSMutableDictionary *)Info;

// 新規Sheet作成に成功時に呼ばれる
- (void)createNewSheetOrUpdateSheetSucessed:(NSMutableDictionary *)Info;

-(void)inviteWithUseridSucessed;

-(void)inviteWithMailAddressSucessed;

- (void)startEditCardSucessed:(NSMutableDictionary *)Info;

- (void)updateCardSucessed:(NSMutableDictionary *)Info;

- (void)endEditCardSucessed:(NSMutableDictionary *)Info;

- (void)createCardSuccessed:(NSMutableDictionary*)Info;

- (void)deleteCardSuccessed:(NSMutableDictionary*)Info;

- (void)deleteSheetSuccessed:(NSMutableDictionary*)Info;

- (void)deleteCategorySuccessed:(NSMutableDictionary*)Info;

-(void)returnFromCardInfoDetailView:(NSNumber *)cardId;

-(void)getProfileSuccessed:(NSMutableDictionary*)info;
@end
