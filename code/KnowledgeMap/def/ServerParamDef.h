//
//  ServerParamDef.h
//  BukurouSS
//
//  Created by k-kin on 11/11/11.
//  Copyright 2011 iccesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include <unistd.h>
#include <netdb.h>
#import "OAuthCore.h"
#import "OAuth+Additions.h"
#import "JSON.h"
#import "EnvDefine.h"
#import "AppDefine.h"
#import "DatabaseTableNmaes.h"
#import "AppHelper.h"
#import "Reachability.h"

@protocol ServerParamDef

#define SYNC_WEBSEVER_TIME_INTERVAL     0.2
#define SYNC_WEBSEVER_TIME_STOP_WORK	20.0
#define SYNC_WEBSEVER_RETRY_TIME		60.0

#define SYNC_TBL_COUNTS 3			//同期対象テーブル数
#define SYNC_IMGDATA_MULTI_COUNTS 3	//画像データの同時アップロード件数
#define SYNC_THREAD_MULTI_COUNTS  3 //同時に平行で実行されるスレッドのMAX件数 


#define POST      @"POST"
#define BOUNDARY  @"___boundary___"
#define CONTENT_TYPE @"Content-Type"
#define SEND_DATA_TYPE @"multipart/form-data; boundary=___boundary___"

#define RESULT_NATIVE_ERROR		@"ERROR"
#define CMD_KILL_THREAD			@"_kill_thread_"
#define RESULT_SUCCESS			@"{\"_result_\":\"0\",\"_value_\":[]}"
#define RESULT_ERROR_SYSTEM		@"{\"_result_\":\"1\"}"
#define RESULT_ERROR_NETWORK	@"{\"_result_\":\"3\"}"
#define RESULT_ERROR_BUSINESS	@"{\"_result_\":\"5\"}"
#define RESULT_NEW_STATUS_CNT	@"{\"_value_\":[{\"nonreadcommentcnt\":0,\"favoritecnt\":0}],\"_result_\":\"0\"}"
#define RESULT_KILL_DATA_LOAD	@"{\"_result_\":\"6\"}"


#define LocalServerURLAdress						@"http://localhost:"

#define YouTubeThumnailImageURL                     @"http://i.ytimg.com/vi/%@/0.jpg"
#define YouTubeMoveImageURL                         @"http://www.youtube.com/v/%@"





#define BssBookAppWebHP_URLAdress               @"http://www.vulb.jp/"
#define BssBookAppURLAdress                     @"https://vulb.jp:8443/knowledge"
#define BssBookWsURLAdress                      @"wss://vulb.jp:8443/knowledge"
#define redirectURL_Sucess                      @"vulbappfb001://com.komugi.vulb"
#define redirectURL_Failure                     @"vulbappfb001://com.komugi.vulb"


/*
#define BssBookAppWebHP_URLAdress               @"http://test.komugi-office.com/knowledge/"
#define BssBookAppURLAdress                     @"http://test.komugi-office.com:8080/knowledge"
#define BssBookWsURLAdress                      @"ws://test.komugi-office.com:8080/knowledge"
#define redirectURL_Sucess                      @"vulbappfb001://com.komugi.vulb"
#define redirectURL_Failure                     @"vulbappfb001://com.komugi.vulb"
*/




/*
 * メソッド名
 */
#define METHOD_HEART_BEAT                  @"/web/heart_beat"            // heart_beat
#define METHOD_CREATE_USER                  @"/web/create_user"          // create_user
#define METHOD_NAME_CONFIRM_USER            @"/web/confirm_user"	     // confirm_user
#define METHOD_NAME_LOGIN_FAST              @"/web/login_fast"	         // login_fast
#define METHOD_NAME_LOGIN_FACEBOOK          @"/web/auth_url_fb"	         // oath_url_fb
#define METHOD_NAME_LOGIN                   @"/web/login_normal"	     // Login
#define METHOD_NAME_LOOUT                   @"/web/logout"			     // Logout
#define METHOD_SHEET_LIST                   @"/web/sheet_list"			 // sheet_list

#define METHOD_RESOURCE_UPLOAD              @"/web/upload"			     // upload
#define METHOD_RESOURCE_DOWNLOAD            @"/web/download"			 // download
#define METHOD_SHEET_MEMBER_LIST            @"/web/member_list"			 // member_list
#define METHOD_CATEGORY_LIST                @"/web/category_list"	     // category_list
#define METHOD_PERSON_IMG                   @"/web/person_img"			 // person_img
#define METHOD_SESSTION                     @"/ws/servcie"			     // ws/servcie
#define METHOD_MASTER_COLOR                 @"/web/master_color"         // web/master_color
#define METHOD_CARD_LIST                    @"/web/card_list"			 // card_list
#define METHOD_DOWNLOAD_THUMBNAIL           @"/web/download_thumbnail"   // download_thumbnail
#define METHOD_REFERENCE_CARD               @"/web/reference_card"	     // reference_card
#define METHOD_CARD_VALUATION               @"/web/card_valuation"	     // card_valuation
#define METHOD_CARD_COMMENT_LIST            @"/web/comment_list"	     // comment_list
#define METHOD_CARD_NEW_COMMENT             @"/web/new_comment"          // new_comment
#define METHOD_CARD_DELETE_COMMENT          @"/web/delete_comment"
#define METHOD_CARD_GOOD_USER_LIST          @"/web/card_good_member_list"
#define METHOD_GET_DOWNLOAD_KEY             @"/web/get_download_key"     // get_download_key
#define METHOD_GET_DOWNLOAD_RESOURCE_FILE   @"/web/download_resource_bin?"     // download_resource_bin
#define METHOD_VIOLATION                    @"/web/violation"            // violation
#define METHOD_VIOLATION                    @"/web/violation"            // violation
#define METHOD_SHEET_LIST_UNCONFIRM         @"/web/sheet_list_unconfirm" // sheet_list_unconfirm
#define METHOD_GROUP_REGIST                 @"/web/group_regsit"         // group_regist
#define METHOD_REFERENCE_CONFIG             @"/web/reference_config"	 // reference_config
#define METHOD_CREATE_CATEGORY              @"/web/create_category"      // create_category
#define METHOD_DELETE_CATEGORY              @"/web/delete_category"
#define METHOD_INVITED_LIST                 @"/web/invited_list"         // invited_list
#define METHOD_FB_GET_FRIENDS               @"/web/fb_get_friends"       // fb_get_friends
#define METHOD_CREATE_SHEET                 @"/web/create_sheet"         // create_sheet
#define METHOD_DELETE_SHEET                 @"/web/delete_sheet"

#define METHOD_INVITE_WITH_USERID           @"/web/invite_with_userid"   // invite_with_userid
#define METHOD_INVITE_WITH_MAIL             @"/web/invite_with_mail"     // invite_with_mail
#define METHOD_REFERENCE_SHEET_SETTING      @"/web/reference_sheet_setting"  // reference_sheet_setting
#define METHOD_UPDATE_SHEET_SETTING         @"/web/update_sheet_setting"     // update_sheet_setting
#define METHOD_CATEGORY_STATISTICS          @"/web/category_statistics"      // category_statistics

#define METHOD_CREATE_CARD                  @"/web/create_card"			 // create_card
#define METHOD_DELETE_CARD                  @"/web/delete_card"			 // create_card
#define METHOD_START_EDIT_CARD              @"/web/start_edit_card"         // start_edit_card
#define METHOD_UPDATE_CARD                  @"/web/update_card"             // update_card
#define METHOD_END_EDIT_CARD                @"/web/end_edit_card"           // end_edit_card
#define METHOD_GET_PROFIE                   @"/web/select_profile"
#define METHOD_UPDATE_PROFIE                @"/web/update_profile"
#define METHOD_GET_PROFIE_LOGO              @"/web/upload_person_img"
#define METHOD_UPDATE_PROFIE_LOGO           @"/web/preson_img"

#define METHOD_SHARE_CATEGORY               @"/web/share_category"
/*
 * 共通
 */
#define RESULT_KEY                  @"result"
#define RESULT_STATUS_KEY           @"statusCode"
#define RESULT_ERROR_MESSGATE_KEY   @"message"
#define RESULT_DATA_VALUE_KEY       @"data"

#define METHOD_NAME_KEY				@"_methodName_"
#define IMG_FILE_KEY				@"_imgFile_"
#define PARAMETER_KEY				@"_param_"
#define PARAMETER_EXT_KEY			@"_param_ext_"



/*
 * エラーコード
 */
#define RESULT_STATUS_VALUE_0 @"0" //正常 
#define RESULT_STATUS_VALUE_1 @"1" //システムエラー
#define RESULT_STATUS_VALUE_2 @"2" //業務的エラー//bussiness error
#define RESULT_STATUS_VALUE_3 @"3" //LOCK ERROR
#define RESULT_STATUS_VALUE_4 @"4" //本人未確認
#define RESULT_STATUS_VALUE_5 @"5" //縮退運転
#define RESULT_STATUS_VALUE_6 @"6" //セッションエラー//session error
#define STRUTS_RESULT_SUCCESS  @"success"



@end
