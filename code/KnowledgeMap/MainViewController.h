//
//  MainViewController.h
//  KnowledgeMap
//
//  Created by KouJun on 3/30/14.
//  Copyright (c) 2014 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHTabBarController.h"

@class UserListViewController;
@class WorkViewController;
@class BaseNavigationController;

@interface MainViewController : MHTabBarController<MHTabBarControllerDelegate>
{
    UserListViewController *userListController;
    WorkViewController *workController;
    
    BaseNavigationController *navigation;
    BaseNavigationController *navigation1;
    BaseNavigationController *navigation2;
}

@end
