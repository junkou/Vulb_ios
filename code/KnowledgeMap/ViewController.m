//
//  ViewController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/04/30.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "ViewController.h"
#import "MainViewController.h"
#import "AppDelegate.h"

#define DEBUG_LOGIN 0

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    UIImage *logoimage = [UIImage imageNamed:@"splash_iphone4_1.png"];
    if ([Utils deviceIs1136H])
    {
        logoimage = [UIImage imageNamed:@"splash_iphone5_1.png"];
    }
    
    CGRect rect = self.view.frame;
    if ([Utils isIOS7orLater]) {
        rect.origin.y = 20;
        rect.size.height -= 20;
    }
    UIImageView *logoImageView = [[UIImageView alloc] initWithFrame:rect];
    [logoImageView setImage:logoimage];
    [self.view addSubview:logoImageView];
    [logoImageView release];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadingAnimation];
    
#if !DEBUG_LOGIN
    BOOL isLoaded = [[BusinessDataCtrlManager sharedInstance] loadUserInfoDataWithDelegate:self];
#else
    BOOL isLoaded = false;
#endif
     [self endLoadingAnimation];
    if (!isLoaded)
    {
        //初めてのログインの場合
       [self addButtonParts];
    }
}

-(void)loadingAnimation{
	UIImageView *loading_view = [[UIImageView alloc] initWithFrame:self.view.bounds];
    loading_view.tag = WORK_VIEW_LOADING_BASE_TAG;
	[loading_view setFrame:self.view.bounds];
	[self.view addSubview:loading_view];
	[loading_view release];
    
	UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(100.0f, 220.0f, 20.0f, 20.0f)];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [loading_view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    [activityIndicator release];
	
	UILabel *loading_lable = [[UILabel alloc] init];
	[loading_lable setFrame:CGRectMake(125, 220, 69, 21)];
	loading_lable.text = @"Loading...";
	loading_lable.font = [UIFont  systemFontOfSize:14.0] ;
	loading_lable.textAlignment = NSTextAlignmentCenter;
	loading_lable.textColor = [UIColor grayColor];
	loading_lable.backgroundColor = [UIColor clearColor];
	[loading_view addSubview:loading_lable];
	[loading_lable release];
}

-(void)endLoadingAnimation{
    UIView *view = [self.view viewWithTag:WORK_VIEW_LOADING_BASE_TAG];
	[view removeFromSuperview];
}

-(void)addButtonParts{
    //Login Button
    CGFloat screenHight = SCREEN_HEIGHT;
    CGFloat buttonHeight = 44;
    CGFloat buttonWidth = 260;
    CGFloat x=(SCREEN_WIDTH-buttonWidth)/2;
    CGFloat y = 45;
    

    //Login
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(x, screenHight-y-buttonHeight-15-buttonHeight-15-buttonHeight- buttonHeight/2, buttonWidth, buttonHeight)];
    button.layer.cornerRadius = 6;
    button.clipsToBounds = YES;
    button.backgroundColor = [UIColor colorWithRed:80/255.0 green:160/255.0 blue:240/255.0 alpha:1];
    [button addTarget:self action:@selector(clickLoginButton:) forControlEvents:UIControlEventTouchUpInside];
//    [button setBackgroundImage:[self getBlueButtonNormalImage:button.bounds] forState:UIControlStateNormal];
//	[button setBackgroundImage:[self getWhiteButtonSelectedImage:button.bounds] forState:UIControlStateHighlighted];
    [self setButtonProperty:button rect:button.bounds title:@"Login" color:[UIColor whiteColor]];
    [self.view addSubview:button];
    [button release];
    
    //create
    button = [[UIButton alloc] initWithFrame:CGRectMake(x, screenHight-y-buttonHeight-15-buttonHeight-buttonHeight/2 , buttonWidth, buttonHeight)];
    button.layer.cornerRadius = 6;
    button.clipsToBounds = YES;
    button.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1];
    [button addTarget:self action:@selector(clickCreateAccountButton:) forControlEvents:UIControlEventTouchUpInside];
//    [button setBackgroundImage:[AppHelper getWhiteButtonNormalImage:button.bounds] forState:UIControlStateNormal];
//	[button setBackgroundImage:[AppHelper getWhiteButtonSelectedImage:button.bounds] forState:UIControlStateHighlighted];
    //UIColor *color1 = HEXCOLOR(0x3c3c3c);
    [self setButtonProperty:button rect:button.bounds title:@"create" color:[UIColor grayColor]];
    [self.view addSubview:button];
    [button release];
    
    //Facebook
    button = [[UIButton alloc] initWithFrame:CGRectMake(x, screenHight-y-buttonHeight, buttonWidth, buttonHeight)];
    button.layer.cornerRadius = 6;
    button.clipsToBounds = YES;
    button.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1];
    [button addTarget:self action:@selector(clickFacebookButton:) forControlEvents:UIControlEventTouchUpInside];
//    [button setBackgroundImage:[self getBlueButtonNormalImage:button.bounds] forState:UIControlStateNormal];
//	[button setBackgroundImage:[self getWhiteButtonSelectedImage:button.bounds] forState:UIControlStateHighlighted];
    [self setButtonProperty:button rect:button.bounds title:@"Login with Facebook" color:[UIColor whiteColor]];
    [self.view addSubview:button];
    [button release];
    
    NSString *str = MultiLangString(@"You can login with your Facebook account.");
    UIFont *font = [UIFont  systemFontOfSize:14];
    CGSize size = [str sizeWithFont:font constrainedToSize:CGSizeMake(button.frame.size.width, SCREEN_HEIGHT) lineBreakMode:NSLineBreakByWordWrapping];
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y+button.frame.size.height+5, button.frame.size.width, size.height)] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.lineBreakMode = NSLineBreakByWordWrapping;
	label.font = font;
	label.textColor = [UIColor grayColor];
	label.textAlignment = NSTextAlignmentLeft;
	label.numberOfLines = size.height/14;
	label.text = str;
	[self.view addSubview:label];


}

-(void) setButtonProperty:(UIButton *)button rect:(CGRect)rect title:(NSString *)title color:(UIColor *)inColor{
    button.titleLabel.font = [UIFont boldSystemFontOfSize:BUTTON_TEXT_INIT_SIZE];
    [button setTitle:MultiLangString(title) forState:UIControlStateNormal];
    [button setTitle:MultiLangString(title) forState:UIControlStateHighlighted];
    [button setTitleColor: inColor forState:UIControlStateNormal];
    [button setTitleColor: inColor forState:UIControlStateHighlighted];
}

-(UIImage *)getBlueButtonNormalImage:(CGRect)rect{
	ImagePartitionView *extImageView = [[[ImagePartitionView alloc] initWithFrame:rect] autorelease];
	
	NSArray *parts = [NSArray arrayWithObjects:
					  [AppHelper getResourceIconPath:@"bt_blue_normal_left.png"],
					  [AppHelper getResourceIconPath:@"bt_blue_normal_center.png"],
					  [AppHelper getResourceIconPath: @"bt_blue_normal_right.png"],
					  nil];
	
	extImageView.images = parts;
	//[extImageView setNeedsDisplay];
	return  [extImageView getScreenShotImage];
}

-(UIImage *)getWhiteButtonSelectedImage:(CGRect)rect{
	ImagePartitionView *extImageView = [[[ImagePartitionView alloc] initWithFrame:rect] autorelease];
	NSArray *parts = [NSArray arrayWithObjects:
					  [AppHelper getResourceIconPath:@"bt_blue_touch_left.png"],
					  [AppHelper getResourceIconPath:@"bt_blue_touch_center.png"],
					  [AppHelper getResourceIconPath: @"bt_blue_touch_right.png"],
					  nil];
	extImageView.images = parts;
    
	return  [extImageView getScreenShotImage];
}

-(void)clickFacebookButton:(UIButton *)button{
    
    [self createAgrrementViewCtrl:LOGIN_TYPE_FACEBOOK];
}

-(void)clickLoginButton:(UIButton *)button
{
    [[AppDelegate sharedInstance] showLogin];
}

-(void)createAgrrementViewCtrl:(int)loginType{
    AgreementViewController *agreementViewController = [[[AgreementViewController alloc] init] autorelease];
    agreementViewController.loginType = loginType;
    
    BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:agreementViewController] autorelease];
	[self presentViewController:controller animated:YES completion:nil];

}

-(void)clickCreateAccountButton:(UIButton *)button{
    [self createAgrrementViewCtrl:CREATE_NEW_ACCOUNT];
}



#pragma mark - WorkDataControllerDelegate Delegate
- (void)CategoryListSucessed{
    
    [self endLoadingAnimation];
    
    /*  WorkView  */
    //メールから開いた場合、シCard IDを再設定する
    NSDictionary *option = [[BusinessDataCtrlManager sharedInstance] getGlobalOptionData] ;
    
    //招待メールのリンクから開いとき、
    NSString *invitation_sheet_id = [option objectForKey:@"invitation_sheet_id"];
   
    if (invitation_sheet_id != nil && [[BusinessDataCtrlManager getUserInfo].lastSheetId intValue] != [invitation_sheet_id intValue]) {

        [[BusinessDataCtrlManager sharedInstance] requestSheetListUnconfirm:self];
        
        return;
    }
    
    [self dismissViewControllerAnimated:NO completion:nil];
    [[AppDelegate sharedInstance] showMainView];
}

-(void)showWorkView
{
    [self dismissViewControllerAnimated:NO completion:nil];

    //メールから開いた場合、シCard IDを再設定する
    NSDictionary *option = [[BusinessDataCtrlManager sharedInstance] getGlobalOptionData] ;

    WorkViewController* workViewController = nil;
    workViewController = [[[WorkViewController alloc] init] autorelease];
//    workViewController.delegate = self;
    //workViewController.title = [NSString stringWithFormat:@"%@(%d)", MultiLangString(@"Sheet Select") ,[workViewController.sheetList count]];
//    workViewController.currentSheetId = [[BusinessDataCtrlManager getUserInfo].lastSheetId intValue];
    
    if ([option count] > 0) {
        workViewController.fromOpenURLFlag = TRUE;
    }
    
    
    BaseNavigationController *controller = [[[BaseNavigationController alloc] initWithRootViewController:workViewController] autorelease];
    
    NSString *cardId = [option objectForKey:@"card_id"];
    if (cardId == nil) {
        [self presentViewController:controller animated:YES completion:nil];
    }else{
        [self presentViewController:controller animated:NO completion:nil];
        
    }
}

- (void)sheetListUnconfirmSucessed:(NSArray *)sheetList{
    //メールから開いた場合、シCard IDを再設定する
    NSDictionary *option = [[BusinessDataCtrlManager sharedInstance] getGlobalOptionData] ;
    
    //招待メールのリンクから開いとき、
    NSString *invitation_sheet_id = [option objectForKey:@"invitation_sheet_id"];
    for (int i=0; i < [sheetList count]; i++) {
        NSMutableDictionary *sheetData = [sheetList objectAtIndex:i];
        NSNumber *sheetId = [sheetData objectForKey:sheetId_Key];
        if ([invitation_sheet_id intValue] == [sheetId intValue]) {
            
            NSString *userName =[sheetData objectForKey:userName_Key];
            NSString *massage = [MultiLangString(@"I0031") stringByReplacingOccurrencesOfString:MESSAGE_TEXT_DUMMY withString:userName];
            
            NSString *confirmFlag =[sheetData objectForKey:@"confirmFlg"];
            if ([confirmFlag isEqualToString:@"0"]) {
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:[sheetData objectForKey:sheetName_key]
                                      message:massage
                                      delegate:self
                                      cancelButtonTitle:MultiLangString(@"Cancel")
                                      otherButtonTitles: MultiLangString(@"Yes"), nil];
                
                [alert show];
                [alert release];

            }else{
                
                [self showInputPasscodeScreen:[sheetData objectForKey:sheetName_key] message:massage];

                

            }

            break;
        }
    }
}


// アラートのボタンが押された時に呼ばれるデリゲート例文
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0:
			//No
            [self showWorkView];
            
			break;
		case 1:
			//Yes
            [self confirmInvitation:alertView];
            
			break;
	}
		
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField{
	UIAlertView *alertView = (UIAlertView *)[textField superview];
	[alertView dismissWithClickedButtonIndex:0 animated:YES];
	[self alertView:alertView clickedButtonAtIndex:0];
	
	return TRUE;
	
}

-(void)confirmInvitation:(UIAlertView *)alertView{
    NSDictionary *option = [[BusinessDataCtrlManager sharedInstance] getGlobalOptionData] ;
    
    //招待メールのリンクから開いとき、
    NSString *invitation_sheet_id = [option objectForKey:@"invitation_sheet_id"];
    
    NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
    [param setObject:invitation_sheet_id forKey:sheetId_Key];
    
    [[BusinessDataCtrlManager sharedInstance] registGroup:param delegate:self];
    
}


- (void)groupRegistSucessed{
    
    [self hiddenInputPasscodeBasePanel:nil];
    
    NSDictionary *option = [[BusinessDataCtrlManager sharedInstance] getGlobalOptionData] ;
    NSString *invitation_sheet_id = [option objectForKey:@"invitation_sheet_id"];
    [BusinessDataCtrlManager getUserInfo].lastSheetId = [NSNumber numberWithInt:[invitation_sheet_id intValue]];
    
    [[BusinessDataCtrlManager sharedInstance] refreshSheetList:self];

}


- (void)showInputPasscodeScreen:(NSString *)sheetName message:(NSString *)message {
    UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
    
    UIButton *maskView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    //    [maskView addTarget:self action:@selector(hiddenInputPasscodeBasePanel:) forControlEvents:UIControlEventTouchUpInside];
	maskView.backgroundColor = [UIColor clearColor];
	maskView.tag = TOUCH_MASK_VIEW_TAG;
	[mainWindow addSubview:maskView ];
	[maskView release];
	
 	UILabel *basePanel = [[UILabel alloc] initWithFrame:maskView.bounds];
    basePanel.backgroundColor = [UIColor blackColor];
    basePanel.alpha = 0.8;
	[maskView addSubview:basePanel];
	[basePanel release];
    
    //sheetName
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, TOOL_BAR_HIGHT+50, maskView.bounds.size.width-20, 20)];
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont  boldSystemFontOfSize:LABEL_FONT_SIZE];
	label.textColor = [UIColor whiteColor];
	label.textAlignment = NSTextAlignmentCenter;
	label.numberOfLines = 1;
	label.text = sheetName;
	[maskView addSubview:label];
	[label release];

    //message
    CGSize size = [message sizeWithFont:[UIFont systemFontOfSize:LABEL_FONT_SIZE] constrainedToSize:CGSizeMake(maskView.bounds.size.width-20, SCREEN_HEIGHT) lineBreakMode:NSLineBreakByWordWrapping];
    label = [[UILabel alloc] initWithFrame:CGRectMake(10, TOOL_BAR_HIGHT+70, maskView.bounds.size.width-20, size.height)];
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont  boldSystemFontOfSize:LABEL_FONT_SIZE];
	label.textColor = [UIColor whiteColor];
	label.textAlignment = NSTextAlignmentLeft;
	label.numberOfLines = size.height/LABEL_FONT_SIZE;
	label.text = message;
	[maskView addSubview:label];
	[label release];

    UITextField *textFile = [[UITextField alloc] initWithFrame:CGRectMake(10,label.frame.origin.y+label.frame.size.height+5, self.view.bounds.size.width-20,30)];
    textFile.tag = SHEET_PASSCODE_TAG;
    textFile.borderStyle = UITextBorderStyleBezel;
    textFile.backgroundColor = [UIColor whiteColor];
    textFile.keyboardType = UIKeyboardTypeASCIICapable;
    textFile.secureTextEntry = YES;
    textFile.placeholder = @"Password";
    textFile.placeholder =MultiLangString(@"Certification code");
	[maskView addSubview:textFile ];
	[textFile release];
    [textFile becomeFirstResponder];
    
	//add CancelButton
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(EDIT_COMMBUTTON_BUTTON_LEFT_MARGIN_X, textFile.frame.origin.y+textFile.frame.size.height+25, EDIT_COMMBUTTON_BUTTON_WIDTH, EDIT_COMMBUTTON_BUTTON_HEIGHT)];
    [AppHelper addCommonButtonProPerty:button imgName:@"" label:@"Cancel"];
    [button addTarget:self action:@selector(clickCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    [maskView addSubview:button];
    [button release];
    
	
	//add DoneButton
    button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - EDIT_COMMBUTTON_BUTTON_LEFT_MARGIN_X-EDIT_COMMBUTTON_BUTTON_WIDTH, textFile.frame.origin.y+textFile.frame.size.height+25, EDIT_COMMBUTTON_BUTTON_WIDTH, EDIT_COMMBUTTON_BUTTON_HEIGHT)];
    [AppHelper addCommonButtonProPerty:button imgName:@"" label:@"Done"];
    [button addTarget:self action:@selector(clickDoneButton:) forControlEvents:UIControlEventTouchUpInside];
    [maskView addSubview:button];
    [button release];
}

-(void)hiddenInputPasscodeBasePanel:(UIButton*)sender{
    UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *maskView = [mainWindow viewWithTag:TOUCH_MASK_VIEW_TAG];
    if (maskView != nil) {
        
        UITextField *textFile = (UITextField *)[maskView viewWithTag:SHEET_PASSCODE_TAG];
        if (textFile != nil) {
            [textFile resignFirstResponder];
        }
        [maskView removeFromSuperview];
    }
    

}


-(void)clickCancelButton:(UIButton*)sender{
    [self hiddenInputPasscodeBasePanel:nil];
    
    [self showWorkView];

}

-(void)clickDoneButton:(UIButton*)sender{
    UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *maskView = [mainWindow viewWithTag:TOUCH_MASK_VIEW_TAG];
    UITextField *textFile = (UITextField *)[maskView viewWithTag:SHEET_PASSCODE_TAG];
    
    if (textFile.text != nil && ![textFile.text isEqualToString:@""] ) {
        
        NSDictionary *option = [[BusinessDataCtrlManager sharedInstance] getGlobalOptionData] ;
        
        //招待メールのリンクから開いとき、
        NSString *invitation_sheet_id = [option objectForKey:@"invitation_sheet_id"];
        
        NSMutableDictionary *param = [[[NSMutableDictionary alloc] init] autorelease];
        [param setObject:invitation_sheet_id forKey:sheetId_Key];
        [param setObject:textFile.text forKey:@"confirmCd"];
        
        [[BusinessDataCtrlManager sharedInstance] registGroup:param delegate:self];
        
    }
    
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
