//
//  AppBusinessLogicController.m
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/09/13.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import "AppBusinessLogicController.h"

@implementation AppBusinessLogicController

static AppBusinessLogicController	*_businessLogicController;

#pragma mark - class method overrade
+(AppBusinessLogicController *)getInstance{
    if (!_businessLogicController) {
        _businessLogicController = [[self alloc] init];
    }
    return _businessLogicController;
	
}

#pragma mark - instance method
- (AppBusinessLogicController *)init
{
    self = [super init];
    if (self) {

        self.globalUserInfo = [[UserInfo alloc] init] ;
        [self.globalUserInfo clearData];        
        self.globalSheetList = [[NSMutableArray alloc] init];
        self.globalCategoryList = [[NSMutableArray alloc] init];
        self.globalCardList = [[NSMutableArray alloc] init];
        self.globalOption = [[NSMutableDictionary alloc] init];
        
        self.globalColorMasterList = [[NSMutableArray alloc] init];
       [AppHelper createColorMetaDatas:self.globalColorMasterList];

    }
    return self;
}

@end
