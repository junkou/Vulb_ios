//
//  AppDelegate.h
//  KnowledgeMap
//
//  Created by 金 康龍 on 13/04/30.
//  Copyright (c) 2013年 金 康龍. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServerDownLoadController.h"
#import "UploadToWebServerSYNController.h"
#import "TestFlight.h"
#import "AppHelper.h"
#import "MainViewController.h"
#import "LoginViewController.h"
#import "ViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    MainViewController* mainViewController;
}
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

-(void)activeService;

+ (id)sharedInstance;
- (MainViewController*)mainViewController;
- (void)showLogin;
- (void)showMainView;
- (void)setTabbarHidden:(BOOL)hidden animated:(BOOL)animated;
@end
